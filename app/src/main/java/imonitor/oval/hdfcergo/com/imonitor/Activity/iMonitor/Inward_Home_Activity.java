package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;
import static imonitor.oval.hdfcergo.com.imonitor.R.id.chart;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getInwardGraph.METHOD_NAME_INW_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getInwardGraph.SOAP_ACTION_INW_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getPeriodWiseInward.METHOD_NAME_INW_PRD_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getPeriodWiseInward.SOAP_ACTION_INW_PRD_Type;

public class Inward_Home_Activity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    public static final String MY_PREFS_NAME = "HdfcErgo";

    BarDataSet barDataSet1,barDataSet2;
    // Barchart Parameter
    String C_Name="";
    String C_Value="";
    String P_Name="";
    String P_Value="";

    TextView Inw_Type;
    TextView Inw_Quar,Inw_Year;
    TextView Inw_From,Inw_To,Inw_Show;
    ImageView btnNext, btnPrevious;

    ProgressDialog progressDialog;
    ConsUrl con_url;
    BarChart barChart;
    Dialog dialog;
    ArrayList<String>  labels = new ArrayList<String>();
    ArrayList<BarEntry> group1 = new ArrayList<>();
    ArrayList<BarEntry> group2 = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_inward__home_);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        con_url = new ConsUrl();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplication(),MainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        // all Compnent Value initialize
        ViewInitilize();

        check_internet_connection();


    }

    private void ViewInitilize() {
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);
        //BarChart of Gwp
        barChart = (BarChart)findViewById( chart);

        //TextView of Gwp
        Inw_Type=(TextView)findViewById( R.id.Inw_type);

        Inw_Quar=(TextView)findViewById( R.id.inward_qurterly );
        Inw_Year=(TextView)findViewById( R.id.inward_yearly );

        //
        Inw_From=(TextView)findViewById( R.id.inward_from);
        Inw_To=(TextView)findViewById( R.id.inward_to );
        Inw_Show=(TextView)findViewById( R.id.inward_result );
    }

    private void check_internet_connection() {
        ConnectivityManager cManager=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(),"Please Check Internet Connection",Toast.LENGTH_SHORT).show();
        } else {
            //Open BarChart
            OpenBarchart();
            // Calling GWP webserivce
            new Gwp_Service().execute(SOAP_ACTION_INW_Type,METHOD_NAME_INW_Type,"GetInwardGraph","");
        }
    }

    private void OpenBarchart() {

        //BarDataSet dataset = new BarDataSet(entries, "# of Calls");

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            private int xIndex;

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {


                xIndex = e.getXIndex();
                Log.i( "BAR", xIndex +"");


                String value = barChart.getBarData().getXVals().get(e.getXIndex())+" = "+String.valueOf(e.getVal());
                Toast toast= Toast.makeText( getApplication(),
                        value, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();


            }

            @Override
            public void onNothingSelected() {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        menu.getItem(5).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(),MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.inward_qurterly:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Inw_Quar.setTextColor(Color.RED);
                Inw_Year.setTextColor(Color.BLACK);

                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_INW_Type,METHOD_NAME_INW_Type,"GetQuartlyInward","");
                barChart.setVisibility( View.VISIBLE );
                Inw_Type.setText( "Quarterly Inward" );
                break;

            case R.id.inward_yearly:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Inw_Quar.setTextColor(Color.BLACK);
                Inw_Year.setTextColor(Color.RED);

                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_INW_Type,METHOD_NAME_INW_Type,"GetYearlyInward","");
                barChart.setVisibility( View.VISIBLE );
                Inw_Type.setText( "Yearly Inward" );
                break;






            case R.id.inward_result:
                if(Inw_From.getText().toString().equals("From Month"))
                {
                    Toast.makeText(Inward_Home_Activity.this,"Select 'From Month First' ",Toast.LENGTH_SHORT).show();
                }else {
                    if(Inw_To.getText().toString().equals("To Month"))
                    {
                        Toast.makeText(Inward_Home_Activity.this,"Select 'To Month First' ",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        barChart.clear();
                        labels.clear();
                        group1.clear();
                        group2.clear();
                        //Gwp Service Call
                        new Gwp_Service().execute(SOAP_ACTION_INW_PRD_Type,METHOD_NAME_INW_PRD_Type,"4","");
                        barChart.setVisibility( View.VISIBLE );
                        Inw_Type.setText( "Inward of Specific Time" );

                    }
                }

                break;

            case R.id.inward_from:
                // Calender
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int  mMonth = c.get(Calendar.MONTH);
                int  mDay = c.get(Calendar.DAY_OF_MONTH);
                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(Inward_Home_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month= monthOfYear+1;
                                String Mon = null;
                                int lengthMonth = String.valueOf(month).length();
                                if (lengthMonth==1)
                                {
                                    Mon="0"+month;
                                }
                                else {
                                    Mon=month+"";
                                }
                                String Day = null;
                                int lengthDay = String.valueOf(dayOfMonth).length();

                                if (lengthDay==1)
                                {
                                    Day="0"+dayOfMonth;
                                }
                                else {
                                    Day=dayOfMonth+"";
                                }

                                Inw_From.setText(year+"-"+Mon);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();

                break;
            case R.id.inward_to:
                // Calender

                if(Inw_From.getText().toString().equals("From Month"))
                {
                    Toast.makeText(Inward_Home_Activity.this,"Select 'From Month' First ",Toast.LENGTH_SHORT).show();
                }else

                {
                    Calendar c2 = Calendar.getInstance();
                    int mYear2 = c2.get(Calendar.YEAR);
                    int  mMonth2 = c2.get(Calendar.MONTH);
                    int  mDay2 = c2.get(Calendar.DAY_OF_MONTH);
                    // Launch Date Picker Dialog
                    DatePickerDialog dpd2 = new DatePickerDialog(Inward_Home_Activity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int month= monthOfYear+1;
                                    String Mon = null;
                                    int lengthMonth = String.valueOf(month).length();
                                    if (lengthMonth==1)
                                    {
                                        Mon="0"+month;
                                    }
                                    else {
                                        Mon=month+"";
                                    }
                                    String Day = null;
                                    int lengthDay = String.valueOf(dayOfMonth).length();

                                    if (lengthDay==1)
                                    {
                                        Day="0"+dayOfMonth;
                                    }
                                    else {
                                        Day=dayOfMonth+"";
                                    }

                                    Inw_To.setText(year+"-"+Mon);
                                }
                            }, mYear2, mMonth2, mDay2);
                    dpd2.show();
                }



                break;
            case R.id.btn_next:
                Constants.callIntent(Inward_Home_Activity.this, Loss_Ratio_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                break;
            case R.id.btn_previous:
                Constants.callIntent(Inward_Home_Activity.this, Survey_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;


        }
    }

    private class Gwp_Service extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Inward_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("graphType",params[2] );
            request.addProperty("sseId",sharedPreferences.getString("NTID", "0") );
            request.addProperty("FromDate",Inw_From.getText().toString() );
            request.addProperty("ToDate",Inw_To.getText().toString() );

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error="";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error=e.toString();
            } catch (XmlPullParserException e)
            {
                e.printStackTrace();
                soap_error=e.toString();
            }



            // Check Network Exception

            if(soap_error.equals(""))
            {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error=soapFault.toString();
                    soapFault.printStackTrace();
                }
                try  {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    int index_x=0;
                    int index_y=0;
                    for (int i=0;i<soapObject2.getPropertyCount();i++)
                    {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();

                        if (pi.getName().equals( "Inward" ))
                        {

                            Object property1 = propertyTest.getProperty("Name");
                            Object property2 = propertyTest.getProperty("ValueInDec");
                            C_Name=property1.toString();
                            C_Value=property2.toString();
                            labels.add(C_Name);
                            double value = Double.parseDouble(C_Value);


                            group1.add(new BarEntry((float)value, index_x));
                            //

                            Log.i( "TAG",index_x+"" +": "+value);
                            index_x=index_x+1;


                        }else
                        {

                            if(pi.getName().equals( "Discrepancy" ))
                            {

                                Object property1 = propertyTest.getProperty("Name");
                                Object property2 = propertyTest.getProperty("ValueInDec");
                                P_Name=property1.toString();
                                P_Value=property2.toString();

                                // example String
                                double value = Double.parseDouble(P_Value);
                                group2.add(new BarEntry((float)value , index_y));
                                Log.i( "TAG",index_y+"" +": "+value);
                                index_y=index_y+1;



                            }else
                            {
                                if(pi.getName().equals( "CurrentYear" ))
                                {

                                    Object property1 = propertyTest.getProperty("Name");
                                    Object property2 = propertyTest.getProperty("ValueInDec");
                                    Object property3 = propertyTest.getProperty("Value");
                                    P_Name=property1.toString();
                                    P_Value=property2.toString();
                                    labels.add(P_Name);
                                    // example String
                                    double value = Double.parseDouble(P_Value);
                                    double value1 = Double.parseDouble(property3.toString());
                                    group1.add(new BarEntry((float)value , index_y));
                                    Log.i( "TAG",index_y+"" +": "+value);
                                    group2.add(new BarEntry((float)value1, index_y));
                                    index_y=index_y+1;

                                }else
                                {
                                    group2.add(new BarEntry(0 , index_x));

                                }
                            }

                        }


                    }

                }catch (Exception  e)
                {
                    e.printStackTrace();
                }
            }else {

                soap_error="Error";
            }




            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            if(progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (s.equals(""))
            {
                barDataSet1 = new BarDataSet(group1, "Inward");
                barDataSet2 = new BarDataSet(group2, "Discrepancy");
                barDataSet1.setColors(new int[]{Color.rgb( 102,180,216 )});
                barDataSet2.setColors(new int[]{Color.rgb( 248,208,3 )});
                if(group1.isEmpty())
                {
                    barChart.setVisibility( View.INVISIBLE );
                    Toast.makeText(Inward_Home_Activity.this,"No Business Yet",Toast.LENGTH_SHORT ).show();
                }

                ArrayList<BarDataSet> dataset = new ArrayList<>();
                dataset.add(barDataSet1);
                dataset.add(barDataSet2);


                try {
                    BarData data = new BarData(labels, dataset);
                    barChart.setData(data);
                    barChart.setDescription("");
                    barChart.animateY(1000);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }else
            {
                Toast.makeText(Inward_Home_Activity.this,"Error in Response",Toast.LENGTH_SHORT ).show();
            }




        }
    }


}
