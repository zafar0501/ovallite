package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.ETrackerHomeNewActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Login;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.MainActivity;
import imonitor.oval.hdfcergo.com.imonitor.R;

public class OvalHomeScreenActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout ovalhomerel;
    Button etrackerbtn, imonitorbtn;
    Toolbar toolbar;
    TextView mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ovalhomescreen);
        setAppToolbar();
        findViewById();

    }
    // edited by Pooja Patil at 15/06/17
    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Oval Lite Home");
        setSupportActionBar(toolbar);
    }

    private void findViewById() {
        ovalhomerel = (RelativeLayout) findViewById(R.id.ovalhomerel);
        etrackerbtn = (Button) findViewById(R.id.etrackerbtn);
        imonitorbtn = (Button) findViewById(R.id.imonitorbtn);

        etrackerbtn.setOnClickListener(this);
        imonitorbtn.setOnClickListener(this);

    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    @Override
    public void onClick(View v) {
        if (v == etrackerbtn) {
         // Constants.callIntent(OvalHomeScreenActivity.this, EtrackerHomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
           Constants.callIntent(OvalHomeScreenActivity.this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        } else if (v == imonitorbtn) {
            Constants.callIntent(this, MainActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);

        }
    }

    @Override
    public void onBackPressed() {
        closeAppAction();

    }

    // edited by Pooja Patil at 13/06/17
    private void closeAppAction() {

        AlertDialog.Builder builder = new AlertDialog.Builder(OvalHomeScreenActivity.this);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to close the app ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert1 = builder.create();
        alert1.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.logout) {
            // closeAppAction();
            gotoLoginScreen();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void gotoLoginScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(OvalHomeScreenActivity.this);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to log out ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Constants.callIntent(OvalHomeScreenActivity.this, Login.class, R.anim.slide_in_left, R.anim.slide_out_left);
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert1 = builder.create();
        alert1.show();


    }
}
