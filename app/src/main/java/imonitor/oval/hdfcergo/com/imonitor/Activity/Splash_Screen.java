package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import imonitor.oval.hdfcergo.com.imonitor.R;

public class Splash_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash__screen );

        WebView view = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = view.getSettings();
        webSettings.setDefaultFontSize(12);
        view.setBackgroundColor( Color.parseColor("#FF0000"));
        String text;
        text = "<html><body><p align=\"justify\">";
        text+= "\n" +"<font color='#FFFFFF'>\n" +
                " iMonitor allows the sales manager to monitor on ongoing basis and in real time, the agent performance with regard to client relation. It helps to observe and check the progress of liquidity flow over a period of time.</font>";
        text+= "</p></body></html>";

        view.loadData(text, "text/html", "utf-8");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
              startActivity( new Intent( Splash_Screen.this,Login.class ) );
            }
        }, 3000);
    }
}
