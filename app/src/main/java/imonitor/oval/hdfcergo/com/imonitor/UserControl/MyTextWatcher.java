package imonitor.oval.hdfcergo.com.imonitor.UserControl;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.WindowManager;
import android.widget.ArrayAdapter;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.MyFilterableAdapter;

/**
 * Created by Zafar.Hussain on 27/06/2017.
 */

public class MyTextWatcher implements TextWatcher {
    private MyFilterableAdapter lAdapter;
    HomeScreenActivity activity;

    public MyTextWatcher(MyFilterableAdapter lAdapter) {
        this.lAdapter = lAdapter;
        activity =new HomeScreenActivity();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void afterTextChanged(Editable s) {
        lAdapter.getFilter().filter(s.toString().toLowerCase());


    }
}