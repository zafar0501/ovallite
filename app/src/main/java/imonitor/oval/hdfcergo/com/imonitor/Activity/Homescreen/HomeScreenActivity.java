package imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.AddDailySourcingActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.AddEtrackerExistinguserActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.GetDailySourcingListActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.LeadActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.FeedbackActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Login;
import imonitor.oval.hdfcergo.com.imonitor.Activity.NotificationListActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.PremiumCalculatorActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.ReleaseNotesScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.RenewalDashboardActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Claims_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Gwp_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Interaction_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Inward_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Loss_Ratio_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.MainActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Poss_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Renewal_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor.Survey_Home_Activity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.HomeScreenRecyclerAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.MenuListAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.MyFilterableAdapter;
import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Entity.DataList;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.MyTextWatcher;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getProfileImage.METHOD_GETINMAGE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getProfileImage.SOAP_ACTION_GETINMAGE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertFosData.METHOD_INSERT_FOS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertFosData.SOAP_ACTION_INSERT_FOS;

/**
 * Created by Pooja Patil on 19/06/2017.
 */

/*Methods used in HomeScreenActivity
* getRegistrationToken -- To get token of registered device on firebase
* getLoginData -- To get login details like ssid, loginntid & Loginname preferances from login activity
* getSaveLogin -- To get save login details & email preferences from login activity
* getVersionDetails -- To get application version details from package manager
* getIMEINOPref -- To get IMEI No details from preference from login activity
* getSharedprefLatLong -- To get lattitude & longitude from prefences from login activity
* getImageService -- To call api  getImage to get profile Image of user
* insertFOSData --  To call api to insert user details to server like ssid, lattitude, longitude,imeino,app version, token Id
* getLocation --  To get current location of device/ enabling gps
* setLatLongPreferences -- to set lat long value in preference for further use
* clickEventOnSearchEdt -- event when click on list items based on search editext is triggered
* getPos -- to get position of list items from search edittext
* setAppToolbar -- adding toolbar to activity
* SetActionBarTitle -- to set toolbar title
* findViews -- attach all views to activity using findViewById
* setUpRecyclerView -- adding recylerview to activity
* setListeners -- handling all listeners of components used
* addListItems -- arraylist to add items of homescreen like engagement tracker, daily sourcing & imonitor to adapter
* closeAppAction -- alert dialog occur on backpressed event
* showMenuPopUpView -- dialog to show popupmenu on click of overflow icon in toolbar
* addMenuListItems -- adding popup menu items to arraylist
* openProfilePopUp -- dialog to show profile pop up
* gotoLoginScreen -- occurs when click of logout button
*/

public class HomeScreenActivity extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "HdfcErgo";
    private static final String TAG = "HomeScreen";
    SoapPrimitive result;
    Bitmap decodedByte;
    String TAG_Soap_Response;
    Toolbar toolbar;
    TextView mTitle, lbl;
    RelativeLayout searchLayout;
    RecyclerView mRecycler;
    HomeScreenRecyclerAdapter mAdapter;
    MenuListAdapter mMenuAdapter;
    RecyclerView menuRecycler;
    LinearLayoutManager mLayoutManager;
    // EditText edtSearch;
    AutoCompleteTextView edtSearch;
    Button btnSearch;
    ArrayList<DataList> mListItems, mMenuListItems;
    DividerItemDecoration dividerItemDecoration;
    SharedPreferences sharedPreferences;
    String agentEmail = "", rmdEmail = "", mobNo = "", LName = "", FName = "", userId = "";
    String saveLoginDetails;
    HomeScreenActivity homeScreenActivity;
    List<String> arrList;
    String arr1[];
    View profileLayout;
    CoordinatorLayout coordinatorLayout;
    Dialog mDialog, menuDialog;
    View popuplayout, menupopuplayout;
    String ssid, Loginname, Loginntid, email;
    Typeface halveticaFont;
    ConsUrl consUrl;
    ETrackerHomeNewActivity etNewActivity;
    String lattitude, longitude, imeiNo;
    GPSTracker gps;
    String appVersion;
    String regToken, tokenId;
    private Menu menu;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home_screen);

        halveticaFont = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueM.ttf");
        homeScreenActivity = new HomeScreenActivity();
        consUrl = new ConsUrl();
        etNewActivity = new ETrackerHomeNewActivity();

        //  getAmsIntent();



        getLoginData();
        getSaveLogin();
        getVersionDetails();
        getIMEINOPref();
        setAppToolbar();
        getSharedprefLatLong();
        findViews();
        getRegistrationToken();
        setUpRecyclerView();
        setListeners();
        getImageService();
        insertFOSData();


    }

    private void getRegistrationToken() {
        //sender-id==project number of app registered on firebase console == 453954366792

        // Get token
        tokenId = FirebaseInstanceId.getInstance().getToken();
        // Log and toast
        regToken = getString(R.string.msg_token_fmt, tokenId);
        Log.d(TAG, regToken);
       lbl.setText(regToken);
        // lbl.setVisibility(View.GONE);
        //Toast.makeText(HomeScreenActivity.this, regToken, Toast.LENGTH_SHORT).show();
    }

    private void getVersionDetails() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        appVersion = "OvalLite-" + pInfo.versionName;
        Log.v("", "AppVersion: " + appVersion);
    }

    private void getIMEINOPref() {
        SharedPreferences preferences = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        imeiNo = preferences.getString("imeiNo", "");

        Log.v("", " imeiNo: =>" + imeiNo);
        //   Toast.makeText(getApplicationContext()," imeiNo "+imeiNo,Toast.LENGTH_SHORT).show();
    }

    private void insertFOSData() {
        if (Constants.isNetworkInfo(HomeScreenActivity.this)) {
            try {

                Log.v("insertFOSData ", "Loginssid:=>" + ssid);
                Log.v("insertFOSData ", " Latitude: =>" + lattitude);
                Log.v("insertFOSData ", " Longitude: =>" + longitude);
                Log.v("insertFOSData ", " imeiNo: =>" + imeiNo);
                Log.v("insertFOSData ", " AppVersion: =>" + appVersion);
                Log.v("insertFOSData ", " tokenId: =>" + tokenId);

                // Toast.makeText(getApplicationContext(), "insertFOSData " + " lat " + lattitude + "\n long " + longitude + "\n ssid " + ssid + "\n imeiNo " + imeiNo, Toast.LENGTH_SHORT).show();
               /* Toast.makeText(getApplicationContext(),"params: "+ " ssid "+ssid+"\nlat "+lattitude + ""+"\nlongi "+longitude + ""
                        +"\nimeiNo "+imeiNo + "\nappVersion "+appVersion
                        +"\ntokenId "+tokenId,Toast.LENGTH_SHORT).show();*/
                new InsertFosData().execute(ssid, lattitude + "", longitude + "", imeiNo, appVersion, tokenId);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getImageService() {
        new GetImage().execute(email);
    }

    private void getSaveLogin() {
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        saveLoginDetails = sharedPreferences.getString("SaveLogin", "");
        email = sharedPreferences.getString("Email", "");
        System.out.println("email:=>" + email);
    }

    private void getSharedprefLatLong() {
        String TAG = "Inside getSharedprefLatLong ";
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences preferences = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        lattitude = preferences.getString("Latitude", "");
        longitude = preferences.getString("Longitude", "");

        imeiNo = preferences.getString("imeiNo", "");
        //  Toast.makeText(getApplicationContext(), "In Pref: lat: " + lattitude + " longi: " + longitude, Toast.LENGTH_SHORT).show();
        if ((lattitude.equals("") || lattitude.equals("0.0")) && (longitude.equals("") || longitude.equals("0.0"))) {
            gps = new GPSTracker(HomeScreenActivity.this);
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                gps.getLocation();

                double latitude1 = gps.getLatitude();
                double longitude1 = gps.getLongitude();
                setLatLongPreferences(latitude1, longitude1, "22");
                lattitude = String.valueOf(latitude1);
                longitude = String.valueOf(longitude1);
                //  Toast.makeText(getApplicationContext(), "22 lat " + lattitude + " long " + longitude, Toast.LENGTH_SHORT).show();
            } else {
                gps.showSettingsAlert();
            }
        } else {
            getLocation();
        }

    }

    private void getLocation() {
        //  System.out.println("InsideGetLocation");
        gps = new GPSTracker(HomeScreenActivity.this);

        // check if GPS enabled
        // if (gps.canGetLocation()) {


        // check if GPS enabled
        if (gps.canGetLocation()) {

            double newLatitude = gps.getLatitude();
            double newLongitude = gps.getLongitude();
            setLatLongPreferences(newLatitude, newLongitude, "33");
            lattitude = String.valueOf(newLatitude);
            longitude = String.valueOf(newLongitude);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    private void getLoginData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(HomeScreenActivity.this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");


        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);

    }

    public void setLatLongPreferences(double latitude, double longitude, String tt) {
        String TAG = "setLatLongPreferences ";

        Log.i(TAG, "Inside setLatLongPreferences ");
        System.out.println(TAG + tt + " latitude : " + latitude + " longitude: " + longitude);

        // SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Login.this);
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Latitude", latitude + "");
        editor.putString("Longitude", longitude + "");
        //  editor.putString("imeiNo", imeiNo);
        editor.commit();


    }

    private void setListeners() {
        // added on 27/06/17

        try {
            String arr = "";
            arrList = new ArrayList<>();

          /*  // DataList dataList= new DataList();
            for (int i = 0; i < mListItems.size(); i++) {
                arr = arr + "," + mListItems.get(i).getTitle();
                System.out.println("10 arr" + arr);
                //arr = arr.substring(3);
               // System.out.println("11 arr" + arr);
                // arrList = Arrays.asList(arr.split(","));
            }


            System.out.println("11 arr" + arr);*/
            //arr = arr.substring(3);
            //arr.trim();
            //System.out.println("12 arr" + arr);
            //  arrList = Arrays.asList(arr.split(","));
            //  System.out.println("11 arrList" + arrList);
            arrList.add("iMonitor"); //0
            arrList.add("Engagement Tracker"); //1
            arrList.add("Daily Sourcing"); //2
            // arrList.add("Existing Agent");
            //arrList.add("New Prospect");
            arrList.add("Add Existing Agent"); //3
            arrList.add("Add New Prospect"); //4
            arrList.add("Add Daily Sourcing"); //5

            //iMonitor Sub Function titles
            arrList.add("GWP"); //6
            arrList.add("Renewal"); //7
            arrList.add("Survey"); //8
            arrList.add("Inward"); // 9
            arrList.add("Interaction"); //10
            arrList.add("Loss Ratio"); //11
            arrList.add("Claims"); //12
            arrList.add("POS"); //13

            //Pop up menu
            arrList.add("Profile"); //14
            arrList.add("Notifications"); //15
            arrList.add("Feedback"); //16
            arrList.add("Premium Calculator"); //17
            arrList.add("Release Notes"); //18

            arrList.add("Renewal Dashboard"); //19
            System.out.println("22 arrList" + arrList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //  ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, arrList);
        //EditText etSearch = (EditText) view.findViewById(R.id.et_search);
        MyFilterableAdapter adapter = new MyFilterableAdapter(this, arrList, "HomeScreen");

        edtSearch.addTextChangedListener(new MyTextWatcher(adapter));

        edtSearch.setAdapter(adapter);

        edtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                InputMethodManager imm = (InputMethodManager) getSystemService(view.getContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                String foundItem = adapterView.getItemAtPosition(i).toString();
                System.out.println("You have selected --->" + foundItem);

                int pos = getPos(foundItem);
                System.out.println("Pos--->" + pos);

                clickEventOnSearchEdt(foundItem);


            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager) getSystemService(view.getContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                String listValue = "";
                String searchResult = edtSearch.getText().toString();

                clickEventOnSearchEdt(searchResult);

            }
        });


        mRecycler.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecycler, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                int pos = (int) mAdapter.getItemId(position);
                System.out.println("position on recCLick: " + pos);
                switch (pos) {
                    case 1:
                        System.out.println("position 11: " + position);
                        Constants.callIntent(HomeScreenActivity.this, GetDailySourcingListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 0:
                        System.out.println("position 00: " + position);
                        Constants.callIntent(HomeScreenActivity.this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 2:
                        System.out.println("position 22: " + position);
                        Constants.callIntent(HomeScreenActivity.this, MainActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 3:
                        System.out.println("position 33: " + position);
                        Constants.callIntent(HomeScreenActivity.this, RenewalDashboardActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;

                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void clickEventOnSearchEdt(String foundItem) {
        if (foundItem.equals("")) {

        } else {
            if (arrList.get(0).contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, MainActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(1).contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(2).contains(foundItem) || arrList.get(2).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, GetDailySourcingListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(3).contains(foundItem) || arrList.get(3).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, AddEtrackerExistinguserActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(4).contains(foundItem) || arrList.get(4).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, LeadActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(5).contains(foundItem) || arrList.get(5).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, AddDailySourcingActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
            // iMonitor
            else if (arrList.get(6).contains(foundItem) || arrList.get(6).toLowerCase().contains(foundItem)) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                Constants.callIntent(HomeScreenActivity.this, Gwp_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(7).contains(foundItem) || arrList.get(7).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, Renewal_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(8).contains(foundItem) || arrList.get(8).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, Survey_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(9).contains(foundItem) || arrList.get(9).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, Inward_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(10).contains(foundItem) || arrList.get(10).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, Interaction_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(11).contains(foundItem) || arrList.get(11).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, Loss_Ratio_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(12).contains(foundItem) || arrList.get(12).toLowerCase().contains(foundItem)) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                Constants.callIntent(HomeScreenActivity.this, Claims_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(13).contains(foundItem) || arrList.get(13).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, Poss_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
            //popmenu items
            else if (arrList.get(14).contains(foundItem) || arrList.get(14).toLowerCase().contains(foundItem)) {
                //profile
                openProfilePopUp("search");
            } else if (arrList.get(15).contains(foundItem) || arrList.get(15).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, NotificationListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(16).contains(foundItem) || arrList.get(16).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, FeedbackActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(17).contains(foundItem) || arrList.get(17).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, PremiumCalculatorActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else if (arrList.get(18).contains(foundItem) || arrList.get(18).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, ReleaseNotesScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }else if (arrList.get(19).contains(foundItem) || arrList.get(19).toLowerCase().contains(foundItem)) {
                Constants.callIntent(HomeScreenActivity.this, RenewalDashboardActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }

        }

    }

    private int getPos(String name) {
        return arrList.indexOf(name);
    }

    private void findViews() {
        lbl = (TextView) findViewById(R.id.lbl);
       // lbl.setVisibility(View.GONE);
        edtSearch = (AutoCompleteTextView) findViewById(R.id.et_search);
        btnSearch = (Button) findViewById(R.id.btn_search);

        profileLayout = findViewById(R.id.profileLayout);
        // edtSearch.setText(regToken);
        // edtSearch.setText("abcd");


    }

    private void setUpRecyclerView() {

        mRecycler = (RecyclerView) findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);
        mRecycler.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mLayoutManager);

        addListItems();
        mAdapter = new HomeScreenRecyclerAdapter(this, mListItems);
        mRecycler.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


    }

    private void addListItems() {
        mListItems = new ArrayList<>();
        mListItems.add(new DataList(getResources().getDrawable(R.drawable.eng_icon), "Engagement Tracker", getString(R.string.eng_desc))); //0
        mListItems.add(new DataList(getResources().getDrawable(R.drawable.daily_sourcing_a), "Daily Sourcing", getString(R.string.daily_src))); //1
        mListItems.add(new DataList(getResources().getDrawable(R.drawable.imonitor), "iMonitor", getString(R.string.imon_desc))); //2
        mListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_renewal_dashboard), "Renewal Dashboard", getString(R.string.renewal_desc))); //2
    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        SetActionBarTitle("Oval Lite");
        setSupportActionBar(toolbar);

    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (saveLoginDetails.equals("0")) {
            closeAppAction();
        } else {
            closeAppAction();
        }

    }

    private void closeAppAction() {

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this, R.style.AlertDialogCustom);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to close the app ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finishAffinity();
                        // finish();

                        //for lower api version than 16
                        ActivityCompat.finishAffinity(HomeScreenActivity.this);


                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert1 = builder.create();
        alert1.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_bar) {

            showMenuPopUpView();
        }
       /* if (id == R.id.logout) {
            //  menu.findItem(R.id.logout).getIcon().setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary), PorterDuff.Mode.SRC_IN);;

            if (saveLoginDetails.equals("1")) {
                gotoLoginScreen();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("SaveLogin", "0");
                editor.commit();
            } else {
                gotoLoginScreen();
            }

            return true;
        }
        if (id == R.id.profile) {
            // menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.profile));
            //  item.setIcon(getResources().getDrawable(R.drawable.profile));
            // item.getIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            openProfilePopUp();
            return true;
        }
        if (id == R.id.calculator) {
            //  menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.calculator));
            // item.setIcon(getResources().getDrawable(R.drawable.calculator));
            // item.getIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            Constants.callIntent(HomeScreenActivity.this, PremiumCalculatorActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            return true;
        }
        if (id == R.id.feedback) {

            Constants.callIntent(HomeScreenActivity.this, FeedbackActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }


    private void showMenuPopUpView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        menupopuplayout = inflater.inflate(R.layout.menu_pop_up_layout, null, false);
        menupopuplayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        menuDialog = new Dialog(HomeScreenActivity.this);
        menuDialog.setCancelable(true);


        menuRecycler = (RecyclerView) menupopuplayout.findViewById(R.id.menuItemsRecycler);
        menuRecycler.setHasFixedSize(true);
        menuRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(HomeScreenActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        menuRecycler.setLayoutManager(mLayoutManager);

        addMenuListItems();
        mMenuAdapter = new MenuListAdapter(HomeScreenActivity.this, mMenuListItems);
        menuRecycler.setAdapter(mMenuAdapter);
        mMenuAdapter.notifyDataSetChanged();


        menuRecycler.addOnItemTouchListener(new RecyclerTouchListener(HomeScreenActivity.this, menuRecycler, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                int pos = (int) mMenuAdapter.getItemId(position);
                System.out.println("position on recCLick: " + pos);

                switch (pos) {
                    case 0:
                        openProfilePopUp("popup");
                        break;
                    case 3:
                        Constants.callIntent(HomeScreenActivity.this, PremiumCalculatorActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 2:
                        Constants.callIntent(HomeScreenActivity.this, FeedbackActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 1:
                        Constants.callIntent(HomeScreenActivity.this, NotificationListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 4:
                        Intent i = new Intent(HomeScreenActivity.this, ReleaseNotesScreenActivity.class);
                        i.putExtra("from", "HomeScreenActivity");
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        // Constants.callIntent(HomeScreenActivity.this, IntroScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case 5:
                        menuDialog.dismiss();
                        if (saveLoginDetails.equals("1")) {
                            gotoLoginScreen();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("SaveLogin", "0");
                            editor.commit();
                        } else {
                            gotoLoginScreen();
                        }
                        break;
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }

        }));

        menuDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        menuDialog.setContentView(menupopuplayout);
        menuDialog.getWindow().setDimAmount(0.0f);

        switch (getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                // ...
                menuDialog.getWindow().getAttributes().width = 500;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                menuDialog.getWindow().getAttributes().width = 700;

                break;
            case DisplayMetrics.DENSITY_HIGH:
                // ...
                menuDialog.getWindow().getAttributes().width = 800;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                menuDialog.getWindow().getAttributes().width = 500;

                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                menuDialog.getWindow().getAttributes().width = 800;
                break;
            default:
                break;
        }
        //menuDialog.getWindow().getAttributes().x=1000;
        // menuDialog.getWindow().getAttributes().y = 70;
        //   menuDialog.getWindow().getAttributes().width = 500;
        menuDialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
        menuDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        menuDialog.show();
    }


    private void addMenuListItems() {
        mMenuListItems = new ArrayList<>();

        mMenuListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_profile), "Profile", ""));
        mMenuListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_notification_menu), "Notifications", ""));
        mMenuListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_feedback), "Feedback", ""));
        mMenuListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_calculator), "Premium Calculator", ""));
        mMenuListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_release), "Release Notes", ""));
        mMenuListItems.add(new DataList(getResources().getDrawable(R.drawable.ic_logout), "Log Out", ""));

    }

    private void openProfilePopUp(final String popup) {
        if (popup.equals("popup")) {
            if (this.menuDialog.isShowing()) {
                this.menuDialog.dismiss();
            }
        }
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        popuplayout = inflater.inflate(R.layout.profile_snack_layout, null, false);
        popuplayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mDialog = new Dialog(HomeScreenActivity.this);
        mDialog.setCancelable(false);
        TextView txt_userName, txt_ntdid, txt_email, lbl_user_profile, txt_ssid, txt_app_version;
        Button btn_exit;
        ImageView profile_icon;

        txt_userName = (TextView) popuplayout.findViewById(R.id.txt_userName);
        txt_ntdid = (TextView) popuplayout.findViewById(R.id.txt_ntdid);
        lbl_user_profile = (TextView) popuplayout.findViewById(R.id.lbl_user_profile);
        txt_email = (TextView) popuplayout.findViewById(R.id.txt_email);
        txt_app_version = (TextView) popuplayout.findViewById(R.id.txt_app_version);
        txt_ssid = (TextView) popuplayout.findViewById(R.id.txt_ssid);
        btn_exit = (Button) popuplayout.findViewById(R.id.btn_exit);
        profile_icon = (ImageView) popuplayout.findViewById(R.id.profile_icon);

        lbl_user_profile.setTypeface(halveticaFont);
        txt_userName.setText(Loginname);
        txt_ntdid.setText(Loginntid);
        txt_ssid.setText(ssid);
        txt_email.setText(email.toLowerCase());
        getVersionDetails();
        txt_app_version.setText(appVersion);

        try {
            if (decodedByte == null || decodedByte.equals("")) {
                profile_icon.setImageResource(R.mipmap.user_icon);
            } else {
                profile_icon.setImageBitmap(decodedByte);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(popuplayout);
        mDialog.getWindow().setGravity(Gravity.TOP);
        // mDialog.getWindow().getAttributes().x=100;
        mDialog.getWindow().getAttributes().y = 80;
        mDialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;

        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
       /* WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(mDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(lp);*/
        mDialog.setCancelable(false);
        mDialog.show();


        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                if (popup.equals("search")) {
                    edtSearch.setText("");
                }
            }
        });

    }

    private void gotoLoginScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this, R.style.AlertDialogCustom);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to log out ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Constants.callIntent(HomeScreenActivity.this, Login.class, R.anim.slide_in_left, R.anim.slide_out_left);
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert1 = builder.create();
        alert1.show();


    }

    public class InsertFosData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

            SoapObject request = new SoapObject(NAMESPACE, METHOD_INSERT_FOS);

            request.addProperty("FOSID", params[0]);
            request.addProperty("Latitude", params[1]);
            request.addProperty("Longitude", params[2]);
            request.addProperty("imeiNo", params[3]);
            request.addProperty("MobileAppVersion", params[4]);
            request.addProperty("TokenId", params[5]);

            Log.v("InsertFosData ", "request params: " + request.toString());


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);

            envelope.dotNet = true;

            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String strREsult = "", resp_error = "";
            try {
                httpTransport.call(SOAP_ACTION_INSERT_FOS, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                strREsult = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                strREsult = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                strREsult = e.toString();
            }

            if (strREsult.equals("")) {
                SoapPrimitive result_new = null;
                try {
                    result_new = (SoapPrimitive) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                    resp_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    resp_error = "Error";
                }
                if (resp_error.equals("Error")) {
                    strREsult = "Error";
                } else {
                    strREsult = result_new.toString();
                }

            } else {
                strREsult = "";
            }
            return strREsult;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class GetImage extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETINMAGE);
            request.addProperty("eMailId", params[0]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETINMAGE, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {
                try {
                    result = (SoapPrimitive) envelope.getResponse();
                    Log.v("", "Response getImage:=>" + result.toString());
                    soap_error = result.toString();
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }

            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.v("onPostExecute:", " result: " + s);

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {

                byte[] decodedString = Base64.decode(s, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                System.out.println("bitmap get image:=>" + decodedByte);

            }
        }
    }


    /*@Override
    public void finish() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            super.finishAndRemoveTask();
        } else {
            super.finish();
        }
    }*/
}
