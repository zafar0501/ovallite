package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.Entity.FeedbackListData;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 06/09/2017.
 */

public class FeedbackListAdapter extends RecyclerView.Adapter<FeedbackListAdapter.ViewHolder> {
    Context mContext;
    ArrayList<FeedbackListData> mList;
    ArrayList<String> letterArraylist;
    ColorGenerator generator = ColorGenerator.MATERIAL;
    String letter, dateString;
    Date createdDate;
    View popuplayout1;
    Dialog connectiondialogue;

    public FeedbackListAdapter(Context mContext, ArrayList<FeedbackListData> mList, ArrayList letterlistname) {
        this.mContext = mContext;
        this.mList = mList;
        this.letterArraylist = letterlistname;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_existagentuser, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tv_venue.setVisibility(View.GONE);
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); // first example
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy"); // first example
        try {
            createdDate = sdf3.parse(mList.get(position).CREATED_DATE);
            System.out.println("createdDate 11 : " + createdDate);


            dateString = sdf2.format(createdDate);

            //  tv_meetingdate.setText(dateString);
            Log.v("", "dateString: " + dateString);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        letter = String.valueOf(letterArraylist.get(position).charAt(0));
        TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());
        holder.letter.setImageDrawable(drawable);

        holder.tv_createdBy.setText(letterArraylist.get(position).toUpperCase());
        holder.tv_feedbackType.setText(mList.get(position).TYPE);
        holder.tv_createdDate.setText(dateString);
        holder.view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPopUpView(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setPopUpView(int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popuplayout1 = inflater.inflate(R.layout.popup_feedback_layout, null, false);
        popuplayout1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        connectiondialogue = new Dialog(mContext);


        popuplayout1.setFocusable(false);
        TextView tv_createdBy, tv_ssid, tv_appname, tv_appversion, tv_type, tv_remarks, tv_createdDate, popup_label;
        Button exit;
        TextView lbl_ssid, lbl_appname, lbl_appversion, lbl_type, lbl_remarks, lbl_createdBy, lbl_createdDate;
        LinearLayout ll_vertical, ll_subvertical, ll_meetingtype, ll_meetingdate;


        popup_label = (TextView) popuplayout1.findViewById(R.id.popup_label);
        exit = (Button) popuplayout1.findViewById(R.id.btn_exit);

        tv_remarks = (TextView) popuplayout1.findViewById(R.id.tv_remarks);
        tv_createdBy = (TextView) popuplayout1.findViewById(R.id.tv_createdby);
        tv_createdDate = (TextView) popuplayout1.findViewById(R.id.tv_created_date);

        tv_createdBy.setText(mList.get(position).CREATED_BY);
        tv_remarks.setText(mList.get(position).REMARKS);
        tv_createdDate.setText(dateString);

       /* SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // first example


        String dateMet = items.get(position).DATEOFMEETING;
        try {
            dateOfMet = sdf.parse(dateMet);
            String dateString = sdf.format(dateOfMet);
            tv_meetingdate.setText(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
*/
        // tv_actionreport.setText(items.get(position).ACTIONTAKENREPORT);


        try {
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectiondialogue.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }


        connectiondialogue.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        connectiondialogue.setContentView(popuplayout1);

      /*  WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(connectiondialogue.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;*/
        //connectiondialogue.getWindow().setAttributes(lp);
        // connectiondialogue.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // connectiondialogue.getWindow().setGravity(Gravity.CENTER);
        connectiondialogue.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        connectiondialogue.setCancelable(true);
        connectiondialogue.show();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_createdBy, tv_feedbackType, view, tv_createdDate, tv_venue;
        ImageView letter;
        Typeface calibriFont;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_createdBy = (TextView) itemView.findViewById(R.id.from);
            //tv_meetingtype = (TextView) itemview.findViewById(R.id.tv_meetingtype);
            tv_feedbackType = (TextView) itemView.findViewById(R.id.tv_vertical);
            tv_createdDate = (TextView) itemView.findViewById(R.id.tv_subvertical);
            tv_venue = (TextView) itemView.findViewById(R.id.tv_venue);
            view = (TextView) itemView.findViewById(R.id.view);
            letter = (ImageView) itemView.findViewById(R.id.gmailitem_letter);

            calibriFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/calibri.ttf");

            tv_createdBy.setTypeface(calibriFont);
            tv_feedbackType.setTypeface(calibriFont);
            tv_createdDate.setTypeface(calibriFont);
            tv_venue.setTypeface(calibriFont);
            view.setTypeface(calibriFont);
        }
    }
}
