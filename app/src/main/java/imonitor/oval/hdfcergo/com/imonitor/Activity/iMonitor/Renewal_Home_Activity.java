package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.R.id.chart;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getFilteredRenewal.METHOD_NAME_REN_FLT_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getFilteredRenewal.SOAP_ACTION_REN_FLT_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.METHOD_NAME_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.SOAP_ACTION_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getProductRenewal.METHOD_NAME_REN_PRD_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getProductRenewal.SOAP_ACTION_REN_PRD_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getRenewalGraph.METHOD_NAME_REN_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getRenewalGraph.SOAP_ACTION_REN_Type;

public class Renewal_Home_Activity extends AppCompatActivity implements View.OnClickListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";
    BarDataSet barDataSet1, barDataSet2;
    //Filter NAME AND VALUE
    String FLT_Name = "";
    String FLT_Value = "";
    // Barchart Parameter
    String C_Name = "";
    String C_Value = "";
    String P_Name = "";
    String P_Value = "";
    TextView Ren_Type, Ren_Quar, Ren_Year, Ren_Product;
    TextView Ren_From, Ren_To, Ren_Show;
    ImageView btnNext, btnPrevious;
    ProgressDialog progressDialog;
    ConsUrl con_url;
    BarChart barChart;
    Dialog dialog;
    ArrayList<String> labels = new ArrayList<String>();
    ArrayList<BarEntry> group1 = new ArrayList<>();
    ArrayList<BarEntry> group2 = new ArrayList<>();
    ArrayAdapter<String> adapter;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        setContentView(R.layout.activity_renewal__home_);

        con_url = new ConsUrl();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplication(), MainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        // all Compnent Value initialize
        ViewInitilize();

        check_internet_connection();


    }

    private void ViewInitilize() {

        //BarChart of Gwp
        barChart = (BarChart) findViewById(chart);

        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);

        //TextView of Gwp

        Ren_Type = (TextView) findViewById(R.id.ren_type);
        Ren_Quar = (TextView) findViewById(R.id.ren_qurterly);
        Ren_Year = (TextView) findViewById(R.id.ren_yearly);
        Ren_Product = (TextView) findViewById(R.id.ren_product);

        //
        Ren_From = (TextView) findViewById(R.id.ren_from);
        Ren_To = (TextView) findViewById(R.id.ren_to);
        Ren_Show = (TextView) findViewById(R.id.ren_result);
        Ren_Type.setText("Renewal vs Renewal Due Month Wise(Current Freeze Month)");
    }

    private void check_internet_connection() {
        ConnectivityManager cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
        } else {
            //Open BarChart
            OpenBarchart();
            // Calling GWP webserivce
            new Gwp_Service().execute(SOAP_ACTION_REN_Type, METHOD_NAME_REN_Type, "1", "");
        }
    }

    private void OpenBarchart() {

        //BarDataSet dataset = new BarDataSet(entries, "# of Calls");

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                String value = barChart.getBarData().getXVals().get(e.getXIndex()) + " = " + String.valueOf(e.getVal());
                Toast toast = Toast.makeText(getApplication(),
                        value, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();


            }

            @Override
            public void onNothingSelected() {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(5).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case R.id.action_toolbar_1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                FLT_Name = "";
                FLT_Value = "";
                new Ren_Filter().execute(SOAP_ACTION_GWP_FLT, METHOD_NAME_GWP_FLT, "Agents", "NEW_DEALER_CODE");


                return true;
            case R.id.action_toolbar_5:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                FLT_Name = "";
                FLT_Value = "";

                new Ren_Filter().execute(SOAP_ACTION_GWP_FLT, METHOD_NAME_GWP_FLT, "Product", "product_name");
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(), MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ren_qurterly:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Ren_Quar.setTextColor(Color.RED);
                Ren_Year.setTextColor(Color.BLACK);
                Ren_Product.setTextColor(Color.BLACK);
                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_REN_Type, METHOD_NAME_REN_Type, "2", "");
                barChart.setVisibility(View.VISIBLE);
                Ren_Type.setText("Renewal vs Renewal Due Month Wise(Quarterly)");
                break;

            case R.id.ren_yearly:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Ren_Quar.setTextColor(Color.BLACK);
                Ren_Year.setTextColor(Color.RED);
                Ren_Product.setTextColor(Color.BLACK);


                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_REN_Type, METHOD_NAME_REN_Type, "3", "");
                barChart.setVisibility(View.VISIBLE);
                Ren_Type.setText("Renewal vs Renewal Due Month Wise(Yearly)");
                break;


            case R.id.ren_product:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Ren_Quar.setTextColor(Color.BLACK);
                Ren_Year.setTextColor(Color.BLACK);
                Ren_Product.setTextColor(Color.RED);

                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_REN_PRD_Type, METHOD_NAME_REN_PRD_Type, "1", "");

                barChart.setVisibility(View.VISIBLE);
                Ren_Type.setText("Renewal vs Renewal Due Product Type Wise(Yearly)");
                break;


            case R.id.ren_result:
                if (Ren_From.getText().toString().equals("From Month")) {
                    Toast.makeText(Renewal_Home_Activity.this, "Select 'From Month First' ", Toast.LENGTH_SHORT).show();
                } else {
                    if (Ren_To.getText().toString().equals("To Month")) {
                        Toast.makeText(Renewal_Home_Activity.this, "Select 'To Month First' ", Toast.LENGTH_SHORT).show();
                    } else {
                        barChart.clear();
                        labels.clear();
                        group1.clear();
                        group2.clear();
                        //Gwp Service Call
                        new Gwp_Service().execute(SOAP_ACTION_REN_Type, METHOD_NAME_REN_Type, "4", "");
                        barChart.setVisibility(View.VISIBLE);
                        Ren_Type.setText("Renewal vs Renewal Due Month Wise(Selected Period)");

                    }
                }

                break;

            case R.id.ren_from:
                // Calender
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(Renewal_Home_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String Mon = null;
                                int lengthMonth = String.valueOf(month).length();
                                if (lengthMonth == 1) {
                                    Mon = "0" + month;
                                } else {
                                    Mon = month + "";
                                }
                                String Day = null;
                                int lengthDay = String.valueOf(dayOfMonth).length();

                                if (lengthDay == 1) {
                                    Day = "0" + dayOfMonth;
                                } else {
                                    Day = dayOfMonth + "";
                                }

                                Ren_From.setText(year + "-" + Mon);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();

                break;
            case R.id.ren_to:
                // Calender

                if (Ren_From.getText().toString().equals("From Month")) {
                    Toast.makeText(Renewal_Home_Activity.this, "Select 'From Month' First ", Toast.LENGTH_SHORT).show();
                } else

                {
                    Calendar c2 = Calendar.getInstance();
                    int mYear2 = c2.get(Calendar.YEAR);
                    int mMonth2 = c2.get(Calendar.MONTH);
                    int mDay2 = c2.get(Calendar.DAY_OF_MONTH);
                    // Launch Date Picker Dialog
                    DatePickerDialog dpd2 = new DatePickerDialog(Renewal_Home_Activity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int month = monthOfYear + 1;
                                    String Mon = null;
                                    int lengthMonth = String.valueOf(month).length();
                                    if (lengthMonth == 1) {
                                        Mon = "0" + month;
                                    } else {
                                        Mon = month + "";
                                    }
                                    String Day = null;
                                    int lengthDay = String.valueOf(dayOfMonth).length();

                                    if (lengthDay == 1) {
                                        Day = "0" + dayOfMonth;
                                    } else {
                                        Day = dayOfMonth + "";
                                    }

                                    Ren_To.setText(year + "-" + Mon);
                                }
                            }, mYear2, mMonth2, mDay2);
                    dpd2.show();
                }
                break;
            case R.id.btn_next:
                Constants.callIntent(Renewal_Home_Activity.this, Survey_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                break;
            case R.id.btn_previous:
                Constants.callIntent(Renewal_Home_Activity.this, Gwp_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;


        }
    }

    @Override
    protected void onDestroy() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private class Gwp_Service extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Renewal_Home_Activity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("monthType", params[2]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            request.addProperty("startDate", Ren_From.getText().toString());
            request.addProperty("endDate", Ren_To.getText().toString());

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    int index_x = 0;
                    int index_y = 0;
                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo(i, pi);
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals("CurrentYear")) {

                            Object property1 = propertyTest.getProperty("Name");
                            Object property2 = propertyTest.getProperty("ValueInDec");
                            C_Name = property1.toString();
                            C_Value = property2.toString();
                            double value = Double.parseDouble(C_Value);
                            group1.add(new BarEntry((float) value, index_x));
                            Log.i("TAG", index_x + "" + ": " + value);
                            index_x = index_x + 1;


                        } else {

                            if (pi.getName().equals("PreviousYear")) {

                                Object property1 = propertyTest.getProperty("Name");
                                Object property2 = propertyTest.getProperty("ValueInDec");
                                P_Name = property1.toString();
                                P_Value = property2.toString();
                                // example String
                                labels.add(P_Name);
                                double value = Double.parseDouble(P_Value);
                                group2.add(new BarEntry((float) value, index_y));
                                Log.i("TAG", index_y + "" + ": " + value);
                                index_y = index_y + 1;


                            } else {

                                if (pi.getName().equals("GWPYearlyData")) {
                                    Object property1 = propertyTest.getProperty("Name");
                                    Object property2 = propertyTest.getProperty("ValueInDec");
                                    P_Name = property1.toString();
                                    P_Value = property2.toString();

                                    labels.add(P_Name);
                                    // example String
                                    double value = Double.parseDouble(P_Value);
                                    group1.add(new BarEntry((float) value, index_y));

                                    Log.i("TAG", index_y + "" + ": " + value);
                                    index_y = index_y + 1;
                                } else {
                                    if (pi.getName().equals("GWPQuartlyData")) {
                                        Object property1 = propertyTest.getProperty("Name");
                                        Object property2 = propertyTest.getProperty("ValueInDec");
                                        P_Name = property1.toString();
                                        P_Value = property2.toString();

                                        labels.add(P_Name);
                                        // example String
                                        double value = Double.parseDouble(P_Value);
                                        group1.add(new BarEntry((float) value, index_y));

                                        Log.i("TAG", index_y + "" + ": " + value);
                                        index_y = index_y + 1;
                                    } else

                                    {
                                        if (pi.getName().equals("GWPPeriodWiseData")) {
                                            Object property1 = propertyTest.getProperty("Name");
                                            Object property2 = propertyTest.getProperty("ValueInDec");
                                            P_Name = property1.toString();
                                            P_Value = property2.toString();

                                            labels.add(P_Name);
                                            // example String
                                            double value = Double.parseDouble(P_Value);
                                            group1.add(new BarEntry((float) value, index_y));

                                            Log.i("TAG", index_y + "" + ": " + value);
                                            index_y = index_y + 1;
                                        } else {

                                        }
                                    }
                                }

                            }

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                soap_error = "Error";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (s.equals("")) {
                barDataSet1 = new BarDataSet(group1, "CurrentYear");
                barDataSet2 = new BarDataSet(group2, "PreviousYear");
                barDataSet1.setColors(new int[]{Color.rgb(102, 180, 216)});
                barDataSet2.setColors(new int[]{Color.rgb(248, 208, 3)});
                if (group1.isEmpty()) {
                    barChart.setVisibility(View.INVISIBLE);
                    Toast.makeText(Renewal_Home_Activity.this, "No Business Yet", Toast.LENGTH_SHORT).show();
                }

                ArrayList<BarDataSet> dataset = new ArrayList<>();
                dataset.add(barDataSet1);
                dataset.add(barDataSet2);


                try {
                    BarData data = new BarData(labels, dataset);
                    barChart.setData(data);
                    barChart.setDescription("");
                    barChart.animateY(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(Renewal_Home_Activity.this, "Error in Response", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private class Ren_Filter extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Renewal_Home_Activity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                soap_error = e.toString();
                e.printStackTrace();
            }


            if (soap_error.equals("")) {
                try {
                    SoapObject result = null;
                    try {
                        result = (SoapObject) envelope.getResponse();
                    } catch (SoapFault soapFault) {

                        soapFault.printStackTrace();
                    }
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);


                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo(i, pi);
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals(params[2])) {

                            Object property1 = propertyTest.getProperty("Name");
                            FLT_Name = FLT_Name + property1.toString() + ",";

                            if (params[2].equals("Agents")) {
                                Object property2 = propertyTest.getProperty("Value");
                                FLT_Value = FLT_Value + property2.toString() + ",";
                            } else {

                                if (params[2].equals("Product")) {
                                    FLT_Value = FLT_Name + property1.toString() + ",";
                                }

                            }


                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                soap_error = params[2] + ":" + params[3];
            } else {
                soap_error = "Error";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            final String Res[] = s.split(":");
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (s.equals("Error")) {
                Toast.makeText(Renewal_Home_Activity.this, "Error in Response", Toast.LENGTH_SHORT).show();
            } else {
                final String names[] = FLT_Name.split(",");
                final String values[] = FLT_Value.split(",");

                // Create custom dialog object
                dialog = new Dialog(Renewal_Home_Activity.this);
                // Include dialog.xml file
                dialog.setContentView(R.layout.custom);
                // Set dialog title
                dialog.setTitle(Res[0]);
                final ListView lv = (ListView) dialog.findViewById(R.id.listView);
                adapter = new ArrayAdapter<String>(Renewal_Home_Activity.this, android.R.layout.simple_list_item_1, names);
                lv.setAdapter(adapter);
                EditText inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
                inputSearch.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        Renewal_Home_Activity.this.adapter.getFilter().filter(cs);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = 500;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.RIGHT;

                dialog.getWindow().setAttributes(lp);
                dialog.show();

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        barChart.clear();
                        labels.clear();
                        group1.clear();
                        group2.clear();
                        new Ren_Flt_Service().execute(values[position], Res[1], "3");
                        Ren_Type.setText("Renewal vs Renewal Due Month Wise(Selcted Period) -" + names[position]);
                        dialog.dismiss();
                        barChart.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
    }

    private class Ren_Flt_Service extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_REN_FLT_Type);
            request.addProperty("filterValue", params[0]);
            request.addProperty("graphType", params[1]);
            request.addProperty("monthType", params[2]);
            request.addProperty("startDate", "2016-01");
            request.addProperty("endDate", "2016-12");
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_REN_FLT_Type, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                soap_error = e.toString();
                e.printStackTrace();
            }

            SoapObject result = null;
            try {
                result = (SoapObject) envelope.getResponse();
            } catch (SoapFault soapFault) {
                soap_error = soapFault.toString();
                soapFault.printStackTrace();
                soap_error = soapFault.toString();
            }


            if (soap_error.equals("")) {
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    int index_x = 0;
                    int index_y = 0;
                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo(i, pi);
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals("CurrentYear")) {

                            Object property1 = propertyTest.getProperty("Name");
                            Object property2 = propertyTest.getProperty("ValueInDec");
                            C_Name = property1.toString();
                            C_Value = property2.toString();

                            double value = Double.parseDouble(C_Value);
                            group1.add(new BarEntry((float) value, index_x));

                            Log.i("TAG", index_x + "" + ": " + value);
                            index_x = index_x + 1;


                        } else {
                            if (pi.getName().equals("PreviousYear")) {

                                Object property1 = propertyTest.getProperty("Name");
                                Object property2 = propertyTest.getProperty("ValueInDec");
                                P_Name = property1.toString();
                                P_Value = property2.toString();
                                labels.add(P_Name);
                                // example String
                                double value = Double.parseDouble(P_Value);
                                group2.add(new BarEntry((float) value, index_y));

                                Log.i("TAG", index_y + "" + ": " + value);
                                index_y = index_y + 1;


                            }

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (s.equals("")) {
                barDataSet1 = new BarDataSet(group1, "CurrentYear");
                barDataSet2 = new BarDataSet(group2, "PreviousYear       Value in Thousand ( K )");
                barDataSet1.setColors(new int[]{Color.rgb(102, 180, 216)});
                barDataSet2.setColors(new int[]{Color.rgb(248, 208, 3)});
                if (group1.isEmpty()) {
                    barChart.setVisibility(View.INVISIBLE);
                    Toast.makeText(Renewal_Home_Activity.this, "No Business Yet", Toast.LENGTH_SHORT).show();
                }

                ArrayList<BarDataSet> dataset = new ArrayList<>();
                dataset.add(barDataSet1);
                dataset.add(barDataSet2);
                if (group2.isEmpty()) {
                    dataset.remove(barDataSet2);
                }
                try {
                    BarData data = new BarData(labels, dataset);
                    barChart.setDescription("");
                    barChart.setData(data);
                    barChart.animateY(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(Renewal_Home_Activity.this, "Error in Response", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
