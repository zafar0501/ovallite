package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 04/08/2017.
 */

public class Splash_Screen_New extends AppCompatActivity {
    ImageView imageView;
    TextView txtTitle;
   // Animation zoomInAnim,bottomUpAnim;
    Animation animation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_new);
        findViews();
    }

    private void findViews() {
        imageView = (ImageView) findViewById(R.id.imageView);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setVisibility(View.GONE);

        startAnimations();




        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                txtTitle.setVisibility(View.VISIBLE);
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_up);
                animation.reset();

                txtTitle.clearAnimation();
                txtTitle.startAnimation(animation);
            }
        }, 3000);
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash_Screen_New.this, Login.class));

            }
        }, 6000);

        /*animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
                imageView.startAnimation(animation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startActivity( new Intent( Splash_Screen_New.this,Login.class ) );
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });*/
    }

    private void startAnimations() {
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        animation.reset();

        imageView.clearAnimation();
        imageView.startAnimation(animation);



    }
}
