package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Entity.DailySourceDataAddList;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 20/06/2017.
 */

public class DailySourceListAddAdapter extends RecyclerView.Adapter<DailySourceListAddAdapter.ViewHolder> {
    Context mContext;
    ArrayList<DailySourceDataAddList> mItems;
    ColorGenerator generator = ColorGenerator.MATERIAL;
    String letter;

    public DailySourceListAddAdapter(Context mContext, ArrayList<DailySourceDataAddList> mItems) {
        this.mContext = mContext;
        this.mItems = mItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_list_item, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        /*holder.txtProdName.setText(mItems.get(position).product_name);
        holder.txtPolSrcd.setText(mItems.get(position).policy_sourced);
        holder.txtPremium.setText(mItems.get(position).premium);*/

        holder.txtProdName.setText("Product Name: " + mItems.get(position).getProduct_name());
        holder.txtPolSrcd.setText("Policy Sourced: " + mItems.get(position).getPolicy_sourced());
        holder.txtPremium.setText("Premium: " + mItems.get(position).getPremium());
        // holder.txt_date.setText("Created By: "+mItems.get(position).getCreatedByName());

        letter = String.valueOf(mItems.get(position).product_name.charAt(0));
        TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());

        holder.letter.setImageDrawable(drawable);


        holder.btnRemove.getTag();
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItems.remove(holder.getPosition());
                notifyDataSetChanged();
                notifyItemRemoved(position);
            }
        });

        holder.itemView.setTag(holder);


    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        //return super.getItemId(position);
        return mItems.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPremium, txtPolSrcd, txtProdName;
        public TextView btnRemove, txt_date;
        ImageView letter;
        Typeface calibriFont;


        public ViewHolder(View itemView) {
            super(itemView);

            calibriFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/calibri.ttf");
            txtProdName = (TextView) itemView.findViewById(R.id.txt_prod_name);
            txtPolSrcd = (TextView) itemView.findViewById(R.id.txt_pol_srcd);
            txtPremium = (TextView) itemView.findViewById(R.id.txt_premium);
            btnRemove = (TextView) itemView.findViewById(R.id.txt_remove);
            letter = (ImageView) itemView.findViewById(R.id.gmailitem_letter);
            //txt_date =(TextView) itemView.findViewById(R.id.txt_date);

            txtProdName.setTypeface(calibriFont);
            txtPolSrcd.setTypeface(calibriFont);
            txtPremium.setTypeface(calibriFont);
            btnRemove.setTypeface(calibriFont);

        }


    }
}
