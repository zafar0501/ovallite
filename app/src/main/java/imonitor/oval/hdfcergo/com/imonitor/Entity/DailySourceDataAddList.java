package imonitor.oval.hdfcergo.com.imonitor.Entity;

/**
 * Created by Zafar.Hussain on 20/06/2017.
 */

public class DailySourceDataAddList {
    public String product_name;
    public String policy_sourced;
    public String premium;

    public String rowId;
    public String createdBySSID;
    public String createdDate;
    public String createdByName;
    public String latitude;
    public String longitude;


   /* public DailySourceDataAddList(String product_name, String policy_sourced, String premium) {
        this.product_name = product_name;
        this.policy_sourced = policy_sourced;
        this.premium = premium;
    }
*/
    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPolicy_sourced() {
        return policy_sourced;
    }

    public void setPolicy_sourced(String policy_sourced) {
        this.policy_sourced = policy_sourced;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getCreatedBySSID() {
        return createdBySSID;
    }

    public void setCreatedBySSID(String createdBySSID) {
        this.createdBySSID = createdBySSID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
