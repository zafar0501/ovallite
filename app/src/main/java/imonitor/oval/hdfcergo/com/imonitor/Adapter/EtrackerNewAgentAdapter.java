package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackernewagentListData;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Meenakshi Aher on 29/05/2017.
 */


public class EtrackerNewAgentAdapter extends RecyclerView.Adapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    Context context;
    ColorGenerator generator = ColorGenerator.MATERIAL;
    String letter;
    View popuplayout1;
    Dialog connectiondialogue;
    private ArrayList<EtrackernewagentListData> items;
    private ArrayList<String> letterArraylist;
    private int itemLayout;


    public EtrackerNewAgentAdapter(Context context, ArrayList<EtrackernewagentListData> items, RecyclerView recyclerview, int itemLayout, ArrayList letterlistname) {
        this.items = items;
        this.itemLayout = itemLayout;
        this.context = context;
        this.letterArraylist = letterlistname;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from( parent.getContext() ).inflate( itemLayout, parent, false );
            vh = new ActivityViewHolder( v );

        } else {
            View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.progressbar_item, parent, false );
            vh = new ProgressViewHolder( v );

        }

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ActivityViewHolder) {
            try {
                EtrackernewagentListData item = items.get( position );

                System.out.println( "Allnames:=>" + items.get( position ).CONTACTPERSON );

                if (items.get( position ).CONTACTPERSON != null || items.get( position ).CONTACTPERSON != "") {
                    String names = items.get( position ).CONTACTPERSON;

                    String names_list[] = names.split( "," );
                    String name1 = names_list[0];
                    ((ActivityViewHolder) holder).name.setText( name1 );
                } else {
                    ((ActivityViewHolder) holder).name.setText( "No Name Found" );
                }

                ((ActivityViewHolder) holder).tv_meetingtype.setText( items.get( position ).AGENTNAME );
                ((ActivityViewHolder) holder).tv_venue.setText( items.get( position ).VENUE );

                letter = String.valueOf( items.get( position ).CONTACTPERSON.charAt( 0 ) );

                TextDrawable drawable = TextDrawable.builder()
                        .buildRound( letter, generator.getRandomColor() );

                ((ActivityViewHolder) holder).letter.setImageDrawable( drawable );

             /*   ((ActivityViewHolder) holder).view.setOnClickListener( new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        setPopUpView(position);

                    }
                } );*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate( true );

        }

    }

    public void setPopUpView(int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        popuplayout1 = inflater.inflate( R.layout.popup_newagentdetails, null );

        connectiondialogue = new Dialog( context );
        popuplayout1.setFocusable( false );
        TextView tv_contactpersn, tv_role, tv_venue, tv_contactnumber, tv_agentname, tv_emailid, tv_isactive, tv_createby, tv_mom,
                tv_gpsaddr, tv_metngatendby, tv_channel,tv_metoutcome;

        Button btn_newagntexit;
        btn_newagntexit = (Button) popuplayout1.findViewById( R.id.btn_newagntexit );
        tv_contactpersn = (TextView) popuplayout1.findViewById( R.id.tv_contactpersn );
        tv_channel = (TextView) popuplayout1.findViewById( R.id.tv_channel );

        tv_role = (TextView) popuplayout1.findViewById( R.id.tv_nrole );
        tv_venue = (TextView) popuplayout1.findViewById( R.id.tv_nvenue );
        tv_contactnumber = (TextView) popuplayout1.findViewById( R.id.tv_contactnumber );
        tv_agentname = (TextView) popuplayout1.findViewById( R.id.tv_agentname );
        tv_emailid = (TextView) popuplayout1.findViewById( R.id.tv_emailid );
        tv_metoutcome = (TextView) popuplayout1.findViewById( R.id .tv_meetingOutcome );
        //tv_isactive = (TextView) popuplayout1.findViewById( R.id .tv_nisactive );
                      /*  tv_createby = (TextView) popuplayout1.findViewById( R.id.tv_ncreateby );
                        tv_mom = (TextView) popuplayout1.findViewById( R.id.tv_nmom );
                        tv_gpsaddr = (TextView) popuplayout1.findViewById( R.id.tv_gpsaddr );
                        tv_metngatendby = (TextView) popuplayout1.findViewById( R.id.tv_metngatendby );
                        tv_traineecode = (TextView) popuplayout1.findViewById( R.id.tv_traineecode );
*/
        tv_contactpersn.setText( items.get( position ).CONTACTPERSON );
        tv_role.setText( items.get( position ).ROLE );
        tv_venue.setText( items.get( position ).VENUE );
        tv_contactnumber.setText( items.get( position ).CONTACTNUMBER );
        tv_agentname.setText( items.get( position ).AGENTNAME );
        tv_emailid.setText( items.get( position ).EMAILID );
        tv_metoutcome.setText( items.get( position ).MEETINGOUTCOME );
                        /*tv_createby.setText( items.get( position ).CREATEDBY );
                        tv_mom.setText( items.get( position ).MINUTESOFMEETING );
                        tv_gpsaddr.setText( items.get( position ).GPSADDRESS );
                        tv_metngatendby.setText( items.get( position ).MEETINGATTENDEDBY );
                        tv_traineecode.setText( items.get( position ).TRAINEECODE );*/


        try {
            btn_newagntexit.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectiondialogue.dismiss();
                }
            } );

        } catch (Exception e) {
            e.printStackTrace();

        }
        connectiondialogue.getWindow().requestFeature( Window.FEATURE_NO_TITLE );
        connectiondialogue.setContentView( popuplayout1 );
        connectiondialogue.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN );
        connectiondialogue.setCancelable( true );
        connectiondialogue.show();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get( position ) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ActivityViewHolder extends RecyclerView.ViewHolder {
        public TextView name, tv_meetingtype, tv_venue, view;
        ImageView letter;

        public ActivityViewHolder(View itemview) {
            super( (itemview) );
            name = (TextView) itemview.findViewById( R.id.nfrom );
            tv_meetingtype = (TextView) itemview.findViewById( R.id.tv_nmeetingtype );
            tv_venue = (TextView) itemview.findViewById( R.id.tv_nvenue );
            view = (TextView) itemview.findViewById( R.id.nview );
            letter = (ImageView) itemView.findViewById( R.id.gmailitem_lettern );
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super( v );
            progressBar = (ProgressBar) v.findViewById( R.id.progressBar1 );
        }
    }

}

