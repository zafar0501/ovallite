package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";
    TextView textView;
    Button btnGWP,btnRenewal,btnLossRatio,btnClaims,btnSurvey,btnInward,btnPos,btnInteraction;
    TextView User_Name;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        btnGWP = (Button)findViewById(R.id.btn_gwp);
        btnRenewal = (Button)findViewById(R.id.btn_renewal);
        btnLossRatio = (Button)findViewById(R.id.btn_loss_ratio);
        btnClaims = (Button)findViewById(R.id.btn_claims);
        btnSurvey = (Button)findViewById(R.id.btn_survey);
        btnInward = (Button)findViewById(R.id.btn_inward);
        btnPos = (Button)findViewById(R.id.btn_pos);
        btnInteraction = (Button)findViewById(R.id.btn_interaction);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        User_Name = (TextView) header.findViewById(R.id.txt_nav);
        User_Name.setText("Welcome  " + sharedPreferences.getString("User_Name", "").toUpperCase());

        WebView view = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = view.getSettings();
        webSettings.setDefaultFontSize(14);
        view.setBackgroundColor(Color.parseColor("#FFFEFE"));
        String text;
        text = "<html><body><p align=\"justify\">";
        text += "\n" + "<font color='#000000'>\n" +
                "iMonitor allows the sales manager to monitor on ongoing basis and in real time, the agent performance with regard to client relation. It helps to observe and check the progress of liquidity flow over a period of time.</font>";
        text += "</p></body></html>";

        view.loadData(text, "text/html", "utf-8");


        btnGWP.setOnClickListener(this);
        btnRenewal.setOnClickListener(this);
        btnLossRatio.setOnClickListener(this);
        btnClaims.setOnClickListener(this);
        btnSurvey.setOnClickListener(this);
        btnInward.setOnClickListener(this);
        btnPos.setOnClickListener(this);
        btnInteraction.setOnClickListener(this);

    }


    @Override
    public void onBackPressed() {
        gotoLoginScreen();

    }

    private void gotoLoginScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this,R.style.AlertDialogCustom);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to close the iMonitor ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // finishAffinity();
                        Constants.callIntent(MainActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert1 = builder.create();
        alert1.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        menu.getItem(5).setVisible(false);
        getMenuInflater().inflate(R.menu.imonitor_home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case R.id.action_toolbar_1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_toolbar_2:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_toolbar_3:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_toolbar_4:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_toolbar_5:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_toolbar_6:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;

            case R.id.home:
                closeAppAction();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.navigation_item_one) {
            Intent i = new Intent(MainActivity.this, Gwp_Home_Activity.class);
            startActivity(i);

        } else if (id == R.id.navigation_item_two) {
            Intent i = new Intent(MainActivity.this, Renewal_Home_Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.navigation_item_three) {
            Intent i = new Intent(MainActivity.this, Claims_Home_Activity.class);
            startActivity(i);


        } else if (id == R.id.navigation_item_four) {
            Intent i = new Intent(MainActivity.this, Survey_Home_Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.navigation_item_five) {
            Intent i = new Intent(MainActivity.this, Inward_Home_Activity.class);
            startActivity(i);


        } else if (id == R.id.navigation_item_six) {
            Intent i = new Intent(MainActivity.this, Poss_Home_Activity.class);
            startActivity(i);


        }/*else if (id == R.id.navigation_item_seven)
        {
            Intent i = new Intent(MainActivity.this, Lead_Home_Activity.class);
            startActivity(i);



        }*/ else if (id == R.id.navigation_item_eight) {
            Intent i = new Intent(MainActivity.this, Interaction_Home_Activity.class);
            startActivity(i);


        } else if (id == R.id.navigation_item_nine) {
            Intent i = new Intent(MainActivity.this, Loss_Ratio_Home_Activity.class);
            startActivity(i);

        } else if (id == R.id.navigation_item_logout) {
           /* SharedPreferences.Editor editor = sharedPreferences.edit();
            //  editor.putString("NTID",Str_NTID);
            editor.putString("NTID", "");
            editor.putString("User_Name", "");
            editor.putString("SaveLogin", "0");
            editor.commit();
            Intent i = new Intent(MainActivity.this, Login.class);
            startActivity(i);*/
            closeAppAction();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // edited by Pooja Patil at 13/06/17
    private void closeAppAction() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this,R.style.AlertDialogCustom);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to close the iMonitor ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finishAffinity();
                        Constants.callIntent(MainActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert1 = builder.create();
        alert1.show();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){

            case R.id.btn_gwp:
                Constants.callIntent(MainActivity.this, Gwp_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_renewal:
                Constants.callIntent(MainActivity.this, Renewal_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_claims:
                Constants.callIntent(MainActivity.this, Claims_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_loss_ratio:
                Constants.callIntent(MainActivity.this, Loss_Ratio_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_inward:
                Constants.callIntent(MainActivity.this, Inward_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_survey:
                Constants.callIntent(MainActivity.this, Survey_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_interaction:
                Constants.callIntent(MainActivity.this, Interaction_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.btn_pos:
                Constants.callIntent(MainActivity.this, Poss_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }
}
