package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 27/06/2017.
 */

public class MyFilterableAdapter extends BaseAdapter implements Filterable {
    String value;
    private Context context;
    private List<String> items;
    private List<String> filteredItems;
    private String activity;
    private ItemFilter mFilter = new ItemFilter();

    public MyFilterableAdapter(Context context, List<String> items, String activity) {
        //super(context, R.layout.your_row, items);
        this.context = context;
        this.items = items;
        this.filteredItems = items;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return filteredItems.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.spinner_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.spinner_text);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String location = filteredItems.get(position);
        if (!location.isEmpty() || viewHolder != null) {
            viewHolder.tvTitle.setText(location);
        }

        /*value = viewHolder.tvTitle.getText().toString();

        if (activity.equals("HomeScreen")) {
            viewHolder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    *//*if (value.equals("iMonitor")) {
                        Constants.callIntent(context, MainActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                    } else if (value.equals("Engagement Tracker")) {
                        Constants.callIntent(context, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                    } else if (value.equals("Daily Sourcing")) {
                        Constants.callIntent(context, GetDailySourcingListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                    }*//*

                    if(items.get(0).equals(value)){

                    }
                }
            });
        }*/
        return convertView;
    }

    public Filter getFilter() {
        return mFilter;
    }

    public static class ViewHolder {
        TextView tvTitle;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            int count = items.size();
            final List<String> tempItems = new ArrayList<>(count);

            for (int i = 0; i < count; i++) {
                if (items.get(i).toLowerCase().contains(filterString)) {
                    tempItems.add(items.get(i));
                }
            }

            results.values = tempItems;
            results.count = tempItems.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredItems = (ArrayList<String>) results.values;
            notifyDataSetChanged();
        }
    }
}
