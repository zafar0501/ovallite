package imonitor.oval.hdfcergo.com.imonitor.Entity;

/**
 * Created by Zafar.Hussain on 22/06/2017.
 */

public class DailySourceDetailsData {
    public String rowId;
    public String productName;
    public String policySourced;
    public String premium;
    public String createdBySSID;
    public String createdDate;
    public String createdByName;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPolicySourced() {
        return policySourced;
    }

    public void setPolicySourced(String policySourced) {
        this.policySourced = policySourced;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getCreatedBySSID() {
        return createdBySSID;
    }

    public void setCreatedBySSID(String createdBySSID) {
        this.createdBySSID = createdBySSID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }
}
