package imonitor.oval.hdfcergo.com.imonitor.UserControl;

import android.view.View;

/**
 * Created by Zafar.Hussain on 12/06/2017.
 */
public interface ClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);

}
