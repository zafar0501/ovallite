package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.ETrackerHomeNewActivity;
import imonitor.oval.hdfcergo.com.imonitor.Entity.BranchCodellistdata;
import imonitor.oval.hdfcergo.com.imonitor.Entity.MeetingTypellistdata;
import imonitor.oval.hdfcergo.com.imonitor.Entity.Peoplemeetllistdata;
import imonitor.oval.hdfcergo.com.imonitor.Entity.SubVerticallistdata;
import imonitor.oval.hdfcergo.com.imonitor.Entity.Verticallistdata;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.GPS.LocationAddress;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.CustomOnItemSelectedListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.MultiSpinner;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getSubVertical.METHOD_NAME_SUBVERTICAL;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getSubVertical.SOAP_ACTION_SUBVERTICAL;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.saveExistingUser.METHOD_NAME_SAVE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.saveExistingUser.SOAP_ACTION_SAVE;


public class AddBankAssuranceActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";
    public static final String TAG = "AddBankAssurance";

    int mProgressStatus = 0;
    boolean valid = false;
    private Spinner verticalspinner, subverticalspinner, meetingtypespinner, branchspinner, metByspinner, rolespinner;
    private ProgressDialog progressDialog;
    private MultiSpinner peoplemeetMultiSpinner;
    private EditText ed_venue, ed_minutsofmeeting, ed_actionTaken, edt_peoplemet_op, edt_branchcode_op;
    private TextView mTitle, txt_appoint_date, txt_appoint_time;
    private LinearLayout ll_br_spinner;
    private String ssid, Loginname, Loginntid, branchCodeValue;
    private ArrayList<String> mverticallist, mVerticalStringList, mbranchcodelist;
    private ArrayList<Verticallistdata> verticalItemlist;
    private ArrayList<SubVerticallistdata> subverticalItemlist;
    private ArrayList<MeetingTypellistdata> meetingtypeItemlist;
    private ArrayList<BranchCodellistdata> branchcodeItemlist;
    private ArrayList<Peoplemeetllistdata> peoplemeetItemlist;
    private ConsUrl consUrl;
    private ArrayAdapter<String> metAttByAdapter, branchdataadapter, peopleMetAdapter, subverticaldataadapter, meetingtypedataadapter, verticaldataadapter, roleAdapter;
    private String verticalname, subverticalname, meetingAttByValue;
    private String venueAddr, meetingtypename, peoplemeetname, peopleMetStatus, branchname, roleName;
    private Button btn_save;
    private GPSTracker gps;
    private RelativeLayout exreliu;
    private String[] metByList, roleList;
    private SharedPreferences sharedPreferences;
    private Toolbar toolbar;
    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            // Do something here with the selected items
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(peopleMetAdapter.getItem(i)).append(" ");
                    peopleMetStatus = "spinner";
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_etrackerexistusernew);
        consUrl = new ConsUrl();
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        setAppToolbar();
        getSharedpreferebnceData();
        getLocation();
        findViews();
        setViews();
        // getVerticalDataWebCall();
        setListeners();
    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        SetActionBarTitle("Add Bancassurance");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  finish();
                onBackPressed();
            }
        });
    }

    private void setViews() {
       // ed_venue.setText(venueAddr.trim());
        ed_venue.setText(venueAddr);
        ed_venue.clearFocus();
    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);
    }


    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        // System.out.println("Loginname 11:=>" + Loginname);
        //   System.out.println("Loginssid 11:=>" + ssid);
        // System.out.println("Loginntid 11:=>" + Loginntid);
    }

    private void setListeners() {
        btn_save.setOnClickListener(this);
        txt_appoint_date.setOnClickListener(this);
        txt_appoint_time.setOnClickListener(this);


        mVerticalStringList.add("Select Vertical");
        mVerticalStringList.add("B1");
        mVerticalStringList.add("B2");
        mVerticalStringList.add("B3");
        mVerticalStringList.add("B4");
        mVerticalStringList.add("B5"); //B5 - Deutsche Bank
        mVerticalStringList.add("B6"); //B6 - SVC
        mVerticalStringList.add("B7"); //B7 - L&T Finance
        mVerticalStringList.add("B8"); //B8 - Saraswat Bank
        mVerticalStringList.add("B9");
        mVerticalStringList.add("CSC");
        mVerticalStringList.add("RABG");

        verticaldataadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mVerticalStringList);
        verticaldataadapter.setDropDownViewResource(R.layout.spinner_text);
        verticalspinner.setAdapter(verticaldataadapter);

        roleList = new String[]{"Select Role", "Sales", "Central Team"};
        roleAdapter = new ArrayAdapter<String>(AddBankAssuranceActivity.this, R.layout.spinner_text, roleList);
        rolespinner.setAdapter(roleAdapter);
        rolespinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                roleName = rolespinner.getSelectedItem().toString();
                //   System.out.println("Role Name: " + roleName);


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                super.onNothingSelected(arg0);
            }
        });

        //add meeting attended by data to spinner
        metByList = new String[]{"Select Meeting By", "Self", "Trainee"};
        metAttByAdapter = new ArrayAdapter<String>(AddBankAssuranceActivity.this, R.layout.spinner_text, metByList);
        metByspinner.setAdapter(metAttByAdapter);

        metByspinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                meetingAttByValue = metByspinner.getSelectedItem().toString();
                // System.out.println("meetingAttByValue:=>" + meetingAttByValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        try {
            verticalspinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    verticalname = verticalspinner.getSelectedItem().toString();
                    //  System.out.println("VerticalSelectedValue:=>" + verticalname);
                    if (verticalname.equals("B6") || verticalname.equals("B7") || verticalname.equals("B8") || verticalname.equals("B9")) {
                        Toast.makeText(getApplicationContext(), "Sub vertical, Meeting Type,etc. details not available.", Toast.LENGTH_LONG).show();
                    } else {
                        getSubVerticalDataWebCall(verticalname);

                        /*if (peoplemeetItemlist != null) {
                            peoplemeetItemlist.clear();
                        }*/
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });

            subverticalspinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    subverticalname = subverticalspinner.getSelectedItem().toString();
                    //  System.out.println("SubVerticalSelectedValue:=>" + subverticalname.toString());
                    String mainstring = verticalname + "__" + subverticalname;
                    //  System.out.println("mainstring:=>" + mainstring);
                    getmeetingtypeWebCall(mainstring);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });


            meetingtypespinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {


                    if (/*meetingtypespinner.getSelectedItem().toString().equals("Circle Head")
                            || */meetingtypespinner.getSelectedItem().toString().equals("Branch Manager")
                            /*|| meetingtypespinner.getSelectedItem().toString().equals("RPM")
                            || meetingtypespinner.getSelectedItem().toString().equals("Sr.RPM")*/
                            || meetingtypespinner.getSelectedItem().toString().equals("Cluster Head")) {


                        meetingtypename = meetingtypespinner.getSelectedItem().toString();
                        // System.out.println("meetingtypenameSelectedValue:=>" + meetingtypename);

                        //setEditTxtPeopleMet();
                        peoplemeetMultiSpinner.setVisibility(View.GONE);
                        edt_peoplemet_op.setVisibility(View.VISIBLE);
                        //  peoplemeetname = edt_peoplemet_op.getText().toString();
                        peopleMetStatus = "edittext";

                        //  branchspinner.setVisibility(View.GONE);
                        branchspinner.setVisibility(View.GONE);
                        ll_br_spinner.setVisibility(View.GONE);
                        edt_branchcode_op.setVisibility(View.VISIBLE);
                        edt_branchcode_op.setText("");
                        branchCodeValue = "Others";


                    } else {
                        meetingtypename = meetingtypespinner.getSelectedItem().toString();
                        // System.out.println("meetingtypenameSelectedValue:=>" + meetingtypename);

                       /* edt_peoplemet_op.setVisibility(View.GONE);
                        peoplemeetMultiSpinner.setVisibility(View.VISIBLE);*/

                      /*  edt_branchcode_op.setVisibility(View.GONE);
                        branchspinner.setVisibility(View.VISIBLE);*/


                        //service to get people met
                        getpeoplemeetWebCall(meetingtypename);

                        // service to get branchcode
                        getbranchcodeWebCall(meetingtypename);
                    }


                }


                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });

            branchspinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    if (branchspinner.getSelectedItem().toString().equals("Other")) {
                        edt_branchcode_op.setVisibility(View.VISIBLE);
                        edt_branchcode_op.setText("");
                        branchCodeValue = "Others";

                    } else {
                        edt_branchcode_op.setVisibility(View.GONE);
                        branchCodeValue = "";

                    }


                }


                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setEditTxtPeopleMet() {

    }

    private void openDateDialog() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // Launch Date Picker Dialog

        Date today = new Date();
        c.setTime(today);
        c.add(Calendar.DAY_OF_MONTH, -4); // Subtract 4 days
        long minDate = c.getTime().getTime();

        DatePickerDialog dpd = new DatePickerDialog(AddBankAssuranceActivity.this,R.style.PickerTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int month = monthOfYear + 1;
                        String Mon = null;
                        int lengthMonth = String.valueOf(month).length();

                        if (lengthMonth == 1) {
                            Mon = "0" + month;
                        } else {
                            Mon = month + "";
                        }
                        String Day = null;
                        int lengthDay = String.valueOf(dayOfMonth).length();

                        if (lengthDay == 1) {
                            Day = "0" + dayOfMonth;
                        } else {
                            Day = dayOfMonth + "";
                        }
                        txt_appoint_date.setText(year + "-" + Mon + "-" + Day);
                    }
                }, mYear, mMonth, mDay);

        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.getDatePicker().setMinDate(minDate);
        dpd.show();
    }

    private void openTimeDialog() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddBankAssuranceActivity.this,R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int lengthHour = String.valueOf(selectedHour).length();
                int lengthMinutes = String.valueOf(selectedMinute).length();

                String Hour = null;

                if (lengthHour == 1) {
                    Hour = "0" + selectedHour;
                } else {
                    Hour = selectedHour + "";
                }
                String Minutes = null;


                if (lengthMinutes == 1) {
                    Minutes = "0" + selectedMinute;
                } else {
                    Minutes = selectedMinute + "";
                }
                txt_appoint_time.setText(Hour + ":" + Minutes);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    private void getLocation() {
        //  System.out.println("InsideGetLocation");
        gps = new GPSTracker(AddBankAssuranceActivity.this);

        // check if GPS enabled
        // if (gps.canGetLocation()) {


        // check if GPS enabled
        if (gps.canGetLocation()) {

            double newLatitude = gps.getLatitude();
            double newLongitude = gps.getLongitude();

               /* LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(latitude, longitude,
                        getApplicationContext(), new GeocoderHandler());

                */
            if (newLatitude == 0.0 && newLongitude == 0.0) {
                Toast.makeText(getApplicationContext(), "Cant get Location !! ", Toast.LENGTH_SHORT).show();

            } else {
                venueAddr = LocationAddress.getCompleteAddressString(AddBankAssuranceActivity.this, newLatitude, newLongitude);
               // Toast.makeText(getApplicationContext(), "latitude " + newLatitude + "\n longitude " + newLongitude, Toast.LENGTH_SHORT).show();
                // \n is for new line
               /* Toast.makeText( getApplicationContext(), "Your Location is - \nLat: "
                        + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG ).show();*/

                //  System.out.println("Mlatitude:=>" + latitude + "\n " + "Mlaongotude:=>" + longitude);
            }
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    private void getbranchcodeWebCall(String meetingtypename) {

        if (Constants.isNetworkInfo(AddBankAssuranceActivity.this)) {
            try {
                //  System.out.println("meetingType:" + meetingtypename.toString());

                //  getbranchcodelist( "4", verticalname );

                new Getbranchcodelist().execute("4", meetingtypename);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }

    private void findViews() {
        mverticallist = new ArrayList<>();
        mVerticalStringList = new ArrayList<>();
        branchcodeItemlist = new ArrayList<>();
        subverticalItemlist = new ArrayList<>();
        peoplemeetItemlist = new ArrayList<>();
        meetingtypeItemlist = new ArrayList<>();
        mbranchcodelist = new ArrayList<>();
        verticalItemlist = new ArrayList<>();

        exreliu = (RelativeLayout) findViewById(R.id.exreliu);
        ed_venue = (EditText) findViewById(R.id.ed_venue);
        ed_minutsofmeeting = (EditText) findViewById(R.id.ed_minutsofmeeting);
        ed_actionTaken = (EditText) findViewById(R.id.ed_actiontaken);
        edt_peoplemet_op = (EditText) findViewById(R.id.edt_peoplemet_op);
        edt_branchcode_op = (EditText) findViewById(R.id.edt_branchcode_op);

        verticalspinner = (Spinner) findViewById(R.id.verticalspinner);
        subverticalspinner = (Spinner) findViewById(R.id.subverticalspinner);
        meetingtypespinner = (Spinner) findViewById(R.id.meetingtypespinner);
        //peoplemeetspinner = (Spinner) findViewById(R.id.peoplemeetspinner);
        branchspinner = (Spinner) findViewById(R.id.branchspinner);
        metByspinner = (Spinner) findViewById(R.id.mtattby_spinner);
        rolespinner = (Spinner) findViewById(R.id.role_spinner);

        txt_appoint_date = (TextView) findViewById(R.id.txt_appoint_date);
        txt_appoint_time = (TextView) findViewById(R.id.txt_appoint_time);
        ll_br_spinner = (LinearLayout) findViewById(R.id.ll_br_spinner);

        peoplemeetMultiSpinner = (MultiSpinner) findViewById(R.id.peoplemeetspinner);


        btn_save = (Button) findViewById(R.id.btn_save);


    }

    private void getpeoplemeetWebCall(String mainstring) {

        if (Constants.isNetworkInfo(AddBankAssuranceActivity.this)) {
            try {
                //  System.out.println("mverticalname:" + verticalname.toString());
                // getpeoplemeetlist( "3", mainstring );

                new Getpeoplemeetlist().execute("3", mainstring);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }

    private void getSubVerticalDataWebCall(String verticalname) {
        if (Constants.isNetworkInfo(AddBankAssuranceActivity.this)) {
            try {
                //  System.out.println("verticalname:" + verticalname.toString());
                // getSubVerticallist( "1", verticalname );


                new GetSubVerticallist().execute("1", verticalname);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar.make(exreliu, Constants.offline_msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    private void getmeetingtypeWebCall(String verticalname) {

        if (Constants.isNetworkInfo(AddBankAssuranceActivity.this)) {
            try {
                //   System.out.println("mverticalname:" + verticalname.toString());
                //getmeetingtypelist( "2", verticalname );
                new Getmeetingtypelist().execute("2", verticalname);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // Constants.callIntent(this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        Intent newIntent = new Intent(AddBankAssuranceActivity.this, ETrackerHomeNewActivity.class);
        newIntent.putExtra("addFrom", "banca");
        startActivity(newIntent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:

                if (validateFields()) {
                    saveDataCall("1");
                }

                break;

            case R.id.txt_appoint_date:
                openDateDialog();
                break;

            case R.id.txt_appoint_time:
                openTimeDialog();
                break;
        }
    }

    private boolean validateFields() {

        if(rolespinner.getSelectedItem().toString().equals("Select Role"))
        {
            Toast.makeText(getApplicationContext(), "Select Role", Toast.LENGTH_SHORT).show();
            return valid;
        }

        if(verticalspinner.getSelectedItem().toString().equals("Select Vertical"))
        {
            Toast.makeText(getApplicationContext(), "Select Vertical", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if(subverticalspinner.getSelectedItem().toString().equals("Select Sub Vertical"))
        {
            Toast.makeText(getApplicationContext(), "Select Sub Vertical", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if ((meetingtypespinner.getSelectedItem().toString().equals("Select Meeting Type"))) {
            Toast.makeText(getApplicationContext(), "Select Meeting Type", Toast.LENGTH_SHORT).show();
            return valid;
        }

        if ((ll_br_spinner.getVisibility() == View.VISIBLE) && (branchspinner.getSelectedItem().toString().equals("Select Branch Code")))
        {
            Toast.makeText(getApplicationContext(), "Select Branch Code", Toast.LENGTH_SHORT).show();
            return valid;

        }
        if ((edt_branchcode_op.getVisibility() == View.VISIBLE) && (edt_branchcode_op.getText().toString().trim().equals("")))
        {
            Toast.makeText(getApplicationContext(), "Enter Branch Code", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if ((peoplemeetMultiSpinner.getVisibility() == View.VISIBLE) && (peoplemeetMultiSpinner.getText().toString().equals("")))
        {
            Toast.makeText(getApplicationContext(), "Select People Met", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if ((edt_peoplemet_op.getVisibility() == View.VISIBLE) && (edt_peoplemet_op.getText().toString().trim().equals("")))
        {
            Toast.makeText(getApplicationContext(), "Enter People Met", Toast.LENGTH_SHORT).show();
            return valid;

        }
        if (metByspinner.getSelectedItem().toString().equals("Select Meeting By"))
        {
            Toast.makeText(getApplicationContext(), "Select Meeting By", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (txt_appoint_date.getText().toString().trim().equals(""))
        {
            Toast.makeText(getApplicationContext(), "Select Meeting Date", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if ((txt_appoint_time.getText().toString().trim().equals("")))
        {
            Toast.makeText(getApplicationContext(), "Select Meeting Time", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (ed_venue.getText().toString().trim().equals(""))
        {
            Toast.makeText(getApplicationContext(), "Enter a Venue", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (ed_minutsofmeeting.getText().toString().trim().equals(""))
        {
            Toast.makeText(getApplicationContext(), "Enter Minutes of Meeting ", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (ed_actionTaken.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Action Taken", Toast.LENGTH_SHORT).show();
            return valid;
        }
        else{
            valid=true;
        }
        return valid;
    }

    private void saveDataCall(String when) {
        Log.v("saveDataCall ", "called in " + when);
        if (Constants.isNetworkInfo(AddBankAssuranceActivity.this)) {
            try {

                String venue = ed_venue.getText().toString().trim();
                String minOfMet = ed_minutsofmeeting.getText().toString().trim();
                String actionTaken = ed_actionTaken.getText().toString().trim();
                String metDate = txt_appoint_date.getText().toString().trim();
                String metTime = txt_appoint_time.getText().toString().trim();

                if (branchCodeValue.equals("Others")) {
                    branchname = "Others: " + edt_branchcode_op.getText().toString().trim();
                    edt_branchcode_op.clearFocus();
                } else {
                    branchname = branchspinner.getSelectedItem().toString();
                }

                if (peopleMetStatus.equals("spinner")) {
                    peoplemeetname = peoplemeetMultiSpinner.getText().toString();
                } else if (peopleMetStatus.equals("edittext")) {
                    peoplemeetname = edt_peoplemet_op.getText().toString().trim();
                    edt_peoplemet_op.clearFocus();
                }

//                System.out.println(TAG + " Role:" + roleName);
//                System.out.println(TAG + " Vertical:" + verticalname);
//                System.out.println(TAG + " SubVertical:" + subverticalname);
//                System.out.println(TAG + " MeetingType:" + meetingtypename);
//                System.out.println(TAG + " BranchCode:" + branchname);
//                System.out.println(TAG + " PeopleMet:" + peoplemeetname);
//                System.out.println(TAG + " MeetAttdBy:" + meetingAttByValue);
//                System.out.println(TAG + " MeetDate:" + metDate);
//                System.out.println(TAG + " MeetTime:" + metTime);
//                System.out.println(TAG + " Venue:" + venue);
//                System.out.println(TAG + " MeetStatus:" + minOfMet);
//                System.out.println(TAG + " ActionTaken:" + actionTaken);

                new SaveDetails().execute(ssid, roleName, verticalname, subverticalname, meetingtypename, meetingAttByValue,
                        venue,
                        branchname, peoplemeetname, minOfMet, actionTaken, Loginname, Loginntid, metDate, metTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }

    public class Getbranchcodelist extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddBankAssuranceActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
            if (branchcodeItemlist != null) {
                branchcodeItemlist.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SUBVERTICAL);
            request.addProperty("keyId", params[0]);
            request.addProperty("keyName", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SUBVERTICAL, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response branchcodelist:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        // Name Description OtherValue ShortDesc
                        String strNAME = "";
                        String strVALUE = "";
                        String strDESCRIPTION = "";
                        String strOTHERVALUE = "";
                        String strSHORTDESC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                // if (soapObject2.getProperty(i).toString().contains("Value")) {
                                //     System.out.println("branch list contains others value");
                                //  } else {
                                try {
                                    strNAME = soapObject3.getPropertyAsString("Name");

                                    strSHORTDESC = soapObject3.getPropertyAsString("ShortDesc");
                                    strDESCRIPTION = soapObject3.getPropertyAsString("Description");
                                    strOTHERVALUE = soapObject3.getPropertyAsString("OtherValue");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();


                                editor.putString("Name" + i, strNAME);
                                editor.putString("ShortDesc" + i, strSHORTDESC);
                                editor.putString("Description" + i, strDESCRIPTION);
                                editor.putString("OtherValue" + i, strOTHERVALUE);
                                editor.commit();
                                /*if (strNAME.equals("Other")) {

                                    soap_error = "0";

                                } else {
                                    soap_error = "1";*/

                                BranchCodellistdata items = new BranchCodellistdata();
                                items.Name = strNAME;
                                items.Description = strDESCRIPTION;
                                items.ShortDesc = strSHORTDESC;
                                items.OtherValue = strOTHERVALUE;

                                branchcodeItemlist.add(items);

                                Log.v("branchcodeItemlist:=>", "11 listSize: " + branchcodeItemlist.size());


                                //}
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                // TAG_Soap_Response = "0";
            }

            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            ArrayList<String> mStringList = new ArrayList<String>();
            //   System.out.println("onPostExecute: s ==> " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    //  Toast.makeText(getApplicationContext(), "Null Response", Toast.LENGTH_SHORT).show();
                    branchspinner.setVisibility(View.GONE);
                    ll_br_spinner.setVisibility(View.GONE);
                    edt_branchcode_op.setVisibility(View.VISIBLE);
                    branchCodeValue = "Others";
                } else {
                    edt_branchcode_op.setVisibility(View.GONE);
                    branchspinner.setVisibility(View.VISIBLE);
                    ll_br_spinner.setVisibility(View.VISIBLE);

                    if (mStringList != null) {
                        mStringList.clear();
                    }
                    if (mbranchcodelist != null) {
                        mbranchcodelist.clear();
                    }


                    if (branchcodeItemlist.size() != 0)
                        for (BranchCodellistdata obj : branchcodeItemlist) {
                            mStringList.add(obj.Name);
                            mbranchcodelist.add(obj.Value);
                        }

                    mStringList.add(0, "Select Branch Code");
                    branchdataadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mStringList);
                    branchdataadapter.setDropDownViewResource(R.layout.spinner_text);
                    branchspinner.setAdapter(branchdataadapter);
                    branchdataadapter.notifyDataSetChanged();
                }
            }

        }
    }

    public class GetSubVerticallist extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddBankAssuranceActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            if (subverticalItemlist != null) {
                subverticalItemlist.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SUBVERTICAL);
            request.addProperty("keyId", params[0]);
            request.addProperty("keyName", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SUBVERTICAL, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                      System.out.println("Response subVertical:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        // Name Description OtherValue ShortDesc
                        String strNAME = "";
                        String strDESCRIPTION = "";
                        String strOTHERVALUE = "";
                        String strSHORTDESC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strNAME = soapObject3.getPropertyAsString("Name");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    strDESCRIPTION = soapObject3.getPropertyAsString("Description");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    strOTHERVALUE = soapObject3.getPropertyAsString("OtherValue");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    strSHORTDESC = soapObject3.getPropertyAsString("ShortDesc");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();


                                editor.putString("Name" + i, strNAME);
                                editor.putString("Description" + i, strDESCRIPTION);
                                editor.putString("OtherValue" + i, strOTHERVALUE);
                                editor.putString("ShortDesc" + i, strSHORTDESC);

                                // Log.i("TAG", "i :" + i + " MEETING_CLIENT_NAME :" + strCLIENT_NAME);
                                editor.commit();

                                SubVerticallistdata items = new SubVerticallistdata();
                                items.Name = strNAME;
                                items.Description = strDESCRIPTION;
                                items.ShortDesc = strSHORTDESC;
                                items.OtherValue = strOTHERVALUE;

                                subverticalItemlist.add(items);

                                Log.i("VerticalList:=>", subverticalItemlist.toString());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                //TAG_Soap_Response = "0";
            }

            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;

            }
            //   System.out.println("onPostExec: result " + s);
            ArrayList<String> mStringList = new ArrayList<String>();

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                //  System.out.println("VerticalList:=>" + subverticalItemlist.toString());


                if (mStringList != null) {
                    mStringList.clear();
                }

                mStringList.add(0, "Select Sub Vertical");

                if (subverticalItemlist.size() != 0)
                    for (SubVerticallistdata obj : subverticalItemlist) {
                        mStringList.add(obj.Name);
                    }

                subverticaldataadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mStringList);
                subverticaldataadapter.setDropDownViewResource(R.layout.spinner_text);
                subverticalspinner.setAdapter(subverticaldataadapter);
                subverticaldataadapter.notifyDataSetChanged();

                Log.i("status:=>", "1");

            }
        }

    }

    public class Getmeetingtypelist extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddBankAssuranceActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            if (meetingtypeItemlist != null) {
                meetingtypeItemlist.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SUBVERTICAL);
            request.addProperty("keyId", params[0]);
            request.addProperty("keyName", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SUBVERTICAL, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response meetingtypelist:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        // Name Description OtherValue ShortDesc
                        String strNAME = "";
                        String strDESCRIPTION = "";
                        String strOTHERVALUE = "";
                        String strSHORTDESC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strNAME = soapObject3.getPropertyAsString("Name");
                                    strDESCRIPTION = soapObject3.getPropertyAsString("Description");
                                    strOTHERVALUE = soapObject3.getPropertyAsString("OtherValue");
                                    strSHORTDESC = soapObject3.getPropertyAsString("ShortDesc");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();


                                editor.putString("Name" + i, strNAME);
                                editor.putString("Description" + i, strDESCRIPTION);
                                editor.putString("OtherValue" + i, strOTHERVALUE);
                                editor.putString("ShortDesc" + i, strSHORTDESC);

                                editor.commit();

                                MeetingTypellistdata items = new MeetingTypellistdata();
                                items.Name = strNAME;
                                items.Description = strDESCRIPTION;
                                items.ShortDesc = strSHORTDESC;
                                items.OtherValue = strOTHERVALUE;

                                meetingtypeItemlist.add(items);

                                // Log.i("meetingtypeItemlist:=>", meetingtypeItemlist.toString());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                // TAG_Soap_Response = "0";
            }

            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                // progressDialog = null;

            }
            //   System.out.println(TAG + " onPostExec: result: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                ArrayList<String> mStringList = new ArrayList<String>();

                if (mStringList != null) {
                    mStringList.clear();
                }

                if (meetingtypeItemlist.size() != 0)
                    for (MeetingTypellistdata obj : meetingtypeItemlist) {
                        mStringList.add(obj.Name);
                    }

                mStringList.add(0, "Select Meeting Type");
                meetingtypedataadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mStringList);
                meetingtypedataadapter.setDropDownViewResource(R.layout.spinner_text);
                meetingtypespinner.setAdapter(meetingtypedataadapter);
                meetingtypedataadapter.notifyDataSetChanged();

                Log.i("status:=>", "1");
            }
        }
    }

    public class Getpeoplemeetlist extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

           /* progressDialog = new ProgressDialog(AddBankAssuranceActivity.this);
            progressDialog.setMessage("Loading..");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.setCancelable(false);
            progressDialog.show();
            mProgressStatus = 0;*/
            if (peoplemeetItemlist != null) {
                peoplemeetItemlist.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
           /* while(mProgressStatus<100){
                try{

                    mProgressStatus++;

                    *//** Invokes the callback method onProgressUpdate *//*
                    publishProgress(mProgressStatus);

                    *//** Sleeps this thread for 100ms *//*
                    Thread.sleep(100);

                }catch(Exception e){
                    Log.d("Exception", e.toString());
                }
            }*/
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SUBVERTICAL);
            request.addProperty("keyId", params[0]);
            request.addProperty("keyName", params[1]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SUBVERTICAL, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response peoplemeetlist:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        // Name Description OtherValue ShortDesc
                        String strNAME = "";
                        String strDESCRIPTION = "";
                        String strOTHERVALUE = "";
                        String strSHORTDESC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strNAME = soapObject3.getPropertyAsString("Name");
                                    strDESCRIPTION = soapObject3.getPropertyAsString("Description");
                                    strOTHERVALUE = soapObject3.getPropertyAsString("OtherValue");
                                    strSHORTDESC = soapObject3.getPropertyAsString("ShortDesc");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();


                                editor.putString("Name" + i, strNAME);
                                editor.putString("Description" + i, strDESCRIPTION);
                                editor.putString("OtherValue" + i, strOTHERVALUE);
                                editor.putString("ShortDesc" + i, strSHORTDESC);

                                editor.commit();

                                Peoplemeetllistdata items = new Peoplemeetllistdata();
                                items.Name = strNAME;
                                items.Description = strDESCRIPTION;
                                items.ShortDesc = strSHORTDESC;
                                items.OtherValue = strOTHERVALUE;

                                peoplemeetItemlist.add(items);

                                Log.i("peoplemeetItemlist:=>", "listSize: " + peoplemeetItemlist.size());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                //TAG_Soap_Response = "0";
            }

            return soap_error;
        }

      /*  @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(mProgressStatus);
        }*/

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           /* if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                //  progressDialog = null;

            }*/
            //  System.out.println(TAG + " onPostExecute: result: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    // Toast.makeText(getApplicationContext(), "Null Response", Toast.LENGTH_SHORT).show();
                    peoplemeetMultiSpinner.setVisibility(View.GONE);
                    edt_peoplemet_op.setVisibility(View.VISIBLE);
                    //  peoplemeetname = edt_peoplemet_op.getText().toString();
                    peopleMetStatus = "edittext";
                } else {
                    edt_peoplemet_op.setVisibility(View.GONE);
                    peoplemeetMultiSpinner.setVisibility(View.VISIBLE);
                    //   System.out.println("peoplemeetItemlist:=>" + peoplemeetItemlist);

                    ArrayList<String> mStringList = new ArrayList<String>();

                    if (mStringList != null) {
                        mStringList.clear();
                    }


                    if (peoplemeetItemlist.size() != 0)
                        for (Peoplemeetllistdata obj : peoplemeetItemlist) {
                            mStringList.add(obj.Name);

                        }
                    //  mStringList.add(0, "Select People Met");
                    peopleMetAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mStringList);
                    peopleMetAdapter.setDropDownViewResource(R.layout.spinner_text);
                    //peoplemeetspinner.setAdapter(branchdataadapter);
                    peoplemeetMultiSpinner.setAdapter(peopleMetAdapter, false, onSelectedListener);
                    //  branchdataadapter.notifyDataSetChanged();

                }
            }
        }
    }

   /* public class GetVerticallist extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (verticalItemlist != null) {
                verticalItemlist.clear();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_VERTICAL);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_VERTICAL, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response Vertical:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strVERTICALID = "";
                        String strVERTICALNAME = "";
                        String strISACTIVE = "";

                        for (int i = 2; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strVERTICALID = soapObject3.getPropertyAsString("VERTICALID");
                                    strVERTICALNAME = soapObject3.getPropertyAsString("VERTICALNAME");
                                    strISACTIVE = soapObject3.getPropertyAsString("ISACTIVE");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();


                                editor.putString("VERTICALID" + i, strVERTICALID);
                                editor.putString("VERTICALNAME" + i, strVERTICALNAME);
                                editor.putString("ISACTIVE" + i, strISACTIVE);

                                // Log.i("TAG", "i :" + i + " MEETING_CLIENT_NAME :" + strCLIENT_NAME);
                                editor.commit();

                                Verticallistdata items = new Verticallistdata();
                                items.VERTICALID = strVERTICALID;
                                items.VERTICALNAME = strVERTICALNAME;
                                items.ISACTIVE = strISACTIVE;

                                verticalItemlist.add(items);

                                Log.i("VerticalList:=>", verticalItemlist.toString());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                //  TAG_Soap_Response = "0";
            }

            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                //     if (s.equals("0")) {

                //  } else {
                ArrayList<String> mStringList = new ArrayList<String>();

                if (mverticallist != null)
                    mverticallist.clear();

                if (mStringList != null)
                    mStringList.clear();
                mStringList.add(0, "Select Vertical");
                if (verticalItemlist.size() != 0)
                    for (Verticallistdata obj : verticalItemlist) {

                        mStringList.add(obj.VERTICALNAME);
                        mverticallist.add(obj.VERTICALID);
                    }

                //  ArrayAdapter<String> verticaldataadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mStringList);
                verticaldataadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, mStringList);
                verticaldataadapter.setDropDownViewResource(R.layout.spinner_text);
                verticalspinner.setAdapter(verticaldataadapter);
                //  }
            }

        }
    }*/

    private class SaveDetails extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddBankAssuranceActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait.. Data is submitting");
            progressDialog.setCancelable(false);
            progressDialog.show();

            btn_save.setVisibility(View.INVISIBLE);

        }

        @Override
        protected String doInBackground(String... params) {
            String parameter = "{  \n" +
                    "   \"ID\":" + "0" + ", \n" +
                    "   \"SSEID\":\"" + params[0] + "\", \n" +
                    "   \"Role\":\"" + params[1] + "\", \n" +
                    "   \"Vertical\":\"" + params[2] + "\",\n" +
                    "   \"SubVertical\":\"" + params[3] + "\",\n" +
                    "   \"MeetingType\":\"" + params[4] + "\",\n" +
                    "   \"MeetingAttendedBy\":\"" + params[5] + "\", \n" +
                    "   \"DateOfMeeting\":\"" + params[13] + "\", \n" +
                    "   \"Time\":\"" + params[14] + "\", \n" +
                    "   \"Venue\":\"" + params[6] + "\",\n" +
                    "   \"BranchEmpCode\":\"" + params[7] + "\",\n" +
                    "   \"PeopleMet\":\"" + params[8] + "\",\n" +
                    "   \"SelectedPeople\":null, \n" +
                    "   \"MinutesOfMeeting\":\"" + params[9] + "\",\n" +
                    "   \"ActionTakenReport\":\"" + params[10] + "\",\n" +
                    "   \"IsActive\":true, \n" +
                    "   \"CreatedBy\":\"" + params[11] + "\",\n" +
                    "   \"UserName\":\"" + params[12] + "\", \n" +
                    "   \"TraineeCode\":null, \n" +
                    "}";

              System.out.println("ParametersAsObject:=>" + parameter);

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SAVE);
            request.addProperty("jsonResult", parameter);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SAVE, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {
                SoapPrimitive result = null;
                // getting response
                try {
                    result = (SoapPrimitive) envelope.getResponse();
                    //     System.out.println(TAG + "Response SaveDetails:=>" + result.toString()); //response = "1"

                    soap_error = result.toString();
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                }


            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                //  progressDialog = null;

            }
            if (s.equals("Error") || s.equals("")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("\"1\"")) {
                    Toast.makeText(getApplicationContext(), "Record Inserted Successfully.", Toast.LENGTH_SHORT).show();
                    Intent newIntent = new Intent(AddBankAssuranceActivity.this, ETrackerHomeNewActivity.class);
                    newIntent.putExtra("addFrom", "banca");
                    startActivity(newIntent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    //  Constants.callIntent(AddEtrackerExistinguserActivity.this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                } else {
                    Toast.makeText(getApplicationContext(), "Data Not Insert Successfully !!!", Toast.LENGTH_SHORT).show();

                }
            }

        }
    }
    //changed, vertical list is static 24/07/17
  /*  private void getVerticalDataWebCall() {
        if (Constants.isNetworkInfo(AddBankAssuranceActivity.this)) {
            try {


                // edited by Pooja Patil at 09-06-17
                //  new GetVerticallist().execute();


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }*/
}
