package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.DailySourceDetailsAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.DailySourceDataAddList;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getDailySourcingDetails.METHOD_GET_SOURCE_DETAILS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getDailySourcingDetails.SOAP_ACTION_GET_SOURCE_DETAILS;

/**
 * Created by Zafar.Hussain on 22/06/2017.
 */

public class GetDailySourceDetailsListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";

    ConsUrl consUrl;
    Context context;
    String ssid, Loginname, Loginntid, strCreatedDate, TAG_Soap_Response;
    Date createdDate;
    SwipeRefreshLayout swipeRefreshLayout;
    Toolbar mToolbar;
    TextView mTitle;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    DividerItemDecoration dividerItemDecoration;
    DailySourceDetailsAdapter mAdapter;
    DailySourceDataAddList detailsList;
    ArrayList<DailySourceDataAddList> detailsArrList;
    LinearLayout ll_no_data;
    TextView txtNoData;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_daily_source_details);

        consUrl = new ConsUrl();
        context = this;
        setAppToolbar();
        getSharedpreferebnceData();
        getIntentData();
        initializeComponents();
        // getDailySourceList();
        setUpRecyclerView();
    }

    private void getIntentData() {
        strCreatedDate = getIntent().getExtras().getString("CREATED_DATE");
        System.out.println("CreatedDate 11:=>" + strCreatedDate);
    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SetActionBarTitle("Daily Sourcing Details");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname 11:=>" + Loginname);
        System.out.println("Loginssid 11:=>" + ssid);
        System.out.println("Loginntid 11:=>" + Loginntid);




    }

    private void initializeComponents() {
        detailsArrList = new ArrayList<>();
        ll_no_data=(LinearLayout)findViewById(R.id.ll_no_data);
        txtNoData=(TextView)findViewById(R.id.tv_no_data);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        /*if (NewAgentArrayList != null) {
                            NewAgentArrayList.clear();
                        }
                        Log.i("NewAgentItemlist 33:=>", NewAgentItemlist.toString());*/

                        getDailySourceDetails();

                    }
                }
        );
    }

    private void getDailySourceDetails() {

        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); // first example
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy"); // first example

        if (Constants.isNetworkInfo(GetDailySourceDetailsListActivity.this)) {

            try {
                createdDate = sdf3.parse(strCreatedDate);
                System.out.println("createdDate 11 : " + createdDate);


                String dateString = sdf2.format(createdDate);

                //  tv_meetingdate.setText(dateString);
                Log.v("","dateString: " + dateString);

                new GetDailySourceDetails().execute(ssid, dateString);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void setUpRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.dd_listRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Constants.callIntent(GetDailySourceDetailsListActivity.this, GetDailySourcingListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loaddata() {
        mAdapter = new DailySourceDetailsAdapter(context, detailsArrList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    public class GetDailySourceDetails extends AsyncTask<String, Void, String> {
        public String TAG = "GetDailySourceDetails";

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_SOURCE_DETAILS);
            request.addProperty("SSE_ID", params[0]);
            request.addProperty("CREATED_DATE", params[1]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_SOURCE_DETAILS, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }catch (Exception e){
                e.printStackTrace();
                soap_error = e.toString();
            }

            if (soap_error.equals("")) {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response DailySourceDetails:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    soap_error = "Error";
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strRowID = "", strProdName = "", strPolicy = "", strPremium = "";
                        String strSSID = "", strCreatedDate = "", strCreatedName = "", strLatitude = "", strLongitude = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                try {
                                    if (soapObject3.toString().contains("Message")) {
                                        soap_error = "Message";
                                    } else {
                                        try {
                                            strRowID = soapObject3.getPropertyAsString("ROW_ID");
                                            strProdName = soapObject3.getPropertyAsString("PRODUCTNAME");
                                            strPolicy = soapObject3.getPropertyAsString("POLICYSOURCED");
                                            strPremium = soapObject3.getPropertyAsString("PREMIUM");
                                            strSSID = soapObject3.getPropertyAsString("CREATEDBY_SSEID");
                                            strCreatedDate = soapObject3.getPropertyAsString("CREATEDATE");
                                            strCreatedName = soapObject3.getPropertyAsString("CREATEDBY_NAME");

                                            strLatitude = soapObject3.getPropertyAsString("LATITUDE");
                                            strLongitude = soapObject3.getPropertyAsString("LONGITUDE");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();

                                        editor.putString("ROW_ID" + i, strRowID);
                                        editor.putString("PRODUCTNAME" + i, strProdName);
                                        editor.putString("POLICYSOURCED" + i, strPolicy);
                                        editor.putString("PREMIUM" + i, strPremium);
                                        editor.putString("CREATEDBY_SSEID" + i, strSSID);
                                        editor.putString("CREATEDATE" + i, strCreatedDate);
                                        editor.putString("CREATEDBY_NAME" + i, strCreatedName);
                                        editor.putString("LATITUDE" + i, strLatitude);
                                        editor.putString("LONGITUDE" + i, strLongitude);

                                        editor.commit();

                                        detailsList = new DailySourceDataAddList();

                                        detailsList.setRowId(strRowID);
                                        detailsList.setProduct_name(strProdName);
                                        detailsList.setPolicy_sourced(strPolicy);
                                        detailsList.setPremium(strPremium);
                                        detailsList.setCreatedBySSID(strSSID);
                                        detailsList.setCreatedDate(strCreatedDate);
                                        detailsList.setCreatedByName(strCreatedName);
                                        detailsList.setLatitude(strLatitude);
                                        detailsList.setLongitude(strLongitude);

                                        detailsArrList.add(detailsList);

                                        System.out.println("detailsArrList: " + detailsArrList.size() + "" + detailsArrList.toString());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println(TAG + " onPostExec: result: " + s);
            if (s.equals("Error")) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No data available !!", Toast.LENGTH_SHORT).show();

                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");
                }else if (s.equals("Message")) {
                    swipeRefreshLayout.setRefreshing(false);
                   /* swipeRefreshLayout.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");*/
                    Toast.makeText(getApplicationContext(), "Error while fetching Daily Sourcing data !!", Toast.LENGTH_SHORT).show();
                } else {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);

                    swipeRefreshLayout.setRefreshing(false);
                    ll_no_data.setVisibility(View.GONE);

                    loaddata();

                }

            }
        }
    }

}
