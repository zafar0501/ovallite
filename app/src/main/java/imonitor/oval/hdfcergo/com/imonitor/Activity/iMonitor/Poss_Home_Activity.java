package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;
import static imonitor.oval.hdfcergo.com.imonitor.R.id.chart;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getPostGraph.METHOD_NAME_POS_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getPostGraph.SOAP_ACTION_POSS_Type;

public class Poss_Home_Activity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    public static final String MY_PREFS_NAME = "HdfcErgo";

    BarDataSet barDataSet1,barDataSet2;
    // Barchart Parameter
    String C_Name="";
    String C_Value="";
    String P_Name="";
    String P_Value="";

    TextView Pos_Type,Pos_Quar,Pos_Year;
    TextView Pos_From,Pos_To,Pos_Show;
    ImageView btnNext, btnPrevious;
    ProgressDialog progressDialog;
    ConsUrl con_url;
    BarChart barChart;
    Dialog dialog;
    ArrayList<String>  labels = new ArrayList<String>();
    ArrayList<BarEntry> group1 = new ArrayList<>();
    ArrayList<BarEntry> group2 = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_poss__home_);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        con_url = new ConsUrl();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplication(),MainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        // all Compnent Value initialize
        ViewInitilize();

        check_internet_connection();


    }

    private void ViewInitilize() {
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);
        //BarChart of Gwp
        barChart = (BarChart)findViewById( chart);

        //TextView of Gwp

        Pos_Type=(TextView)findViewById( R.id.pos_type );
        Pos_Quar=(TextView)findViewById( R.id.pos_qurterly );
        Pos_Year=(TextView)findViewById( R.id.pos_yearly );

        //
        Pos_From=(TextView)findViewById( R.id.pos_from);
        Pos_To=(TextView)findViewById( R.id.pos_to );
        Pos_Show=(TextView)findViewById( R.id.pos_result );
        Pos_Type.setText("Pos And Non Pos Count");
    }

    private void check_internet_connection() {
        ConnectivityManager cManager=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(),"Please Check Internet Connection",Toast.LENGTH_SHORT).show();
        } else {
            //Open BarChart
            OpenBarchart();
            // Calling GWP webserivce
            new Gwp_Service().execute(SOAP_ACTION_POSS_Type,METHOD_NAME_POS_Type,"1","");
        }
    }

    private void OpenBarchart() {

        //BarDataSet dataset = new BarDataSet(entries, "# of Calls");

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                String value = barChart.getBarData().getXVals().get(e.getXIndex())+" = "+String.valueOf(e.getVal());
                Toast toast= Toast.makeText( getApplication(),
                        value, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();


            }

            @Override
            public void onNothingSelected() {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        menu.getItem(5).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(),MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.pos_qurterly:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Pos_Quar.setTextColor(Color.RED);
                Pos_Year.setTextColor(Color.BLACK);

                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_POSS_Type,METHOD_NAME_POS_Type,"2","");
                barChart.setVisibility( View.VISIBLE );
                Pos_Type.setText("Pos And Non Pos Count (Quarterly)");
                break;

            case R.id.pos_yearly:
                barChart.clear();
                labels.clear();
                group1.clear();
                group2.clear();
                Pos_Quar.setTextColor(Color.BLACK);
                Pos_Year.setTextColor(Color.RED);


                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_POSS_Type,METHOD_NAME_POS_Type,"3","");
                barChart.setVisibility( View.VISIBLE );
                Pos_Type.setText("Pos And Non Pos Count (Yearly)");
                break;






            case R.id.pos_result:
                if(Pos_From.getText().toString().equals("From Month"))
                {
                    Toast.makeText(Poss_Home_Activity.this,"Select 'From Month First' ",Toast.LENGTH_SHORT).show();
                }else {
                    if(Pos_To.getText().toString().equals("To Month"))
                    {
                        Toast.makeText(Poss_Home_Activity.this,"Select 'To Month First' ",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        barChart.clear();
                        labels.clear();
                        group1.clear();
                        group2.clear();
                        //Gwp Service Call
                        new Gwp_Service().execute(SOAP_ACTION_POSS_Type,METHOD_NAME_POS_Type,"4","");
                        barChart.setVisibility( View.VISIBLE );
                        Pos_Type.setText("Pos And Non Pos Count (Selected Period wise)");

                    }
                }

                break;

            case R.id.pos_from:
                // Calender
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int  mMonth = c.get(Calendar.MONTH);
                int  mDay = c.get(Calendar.DAY_OF_MONTH);
                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(Poss_Home_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month= monthOfYear+1;
                                String Mon = null;
                                int lengthMonth = String.valueOf(month).length();
                                if (lengthMonth==1)
                                {
                                    Mon="0"+month;
                                }
                                else {
                                    Mon=month+"";
                                }
                                String Day = null;
                                int lengthDay = String.valueOf(dayOfMonth).length();

                                if (lengthDay==1)
                                {
                                    Day="0"+dayOfMonth;
                                }
                                else {
                                    Day=dayOfMonth+"";
                                }

                                Pos_From.setText(year+"-"+Mon);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();

                break;
            case R.id.pos_to:
                // Calender

                if(Pos_From.getText().toString().equals("From Month"))
                {
                    Toast.makeText(Poss_Home_Activity.this,"Select 'From Month' First ",Toast.LENGTH_SHORT).show();
                }else

                {
                    Calendar c2 = Calendar.getInstance();
                    int mYear2 = c2.get(Calendar.YEAR);
                    int  mMonth2 = c2.get(Calendar.MONTH);
                    int  mDay2 = c2.get(Calendar.DAY_OF_MONTH);
                    // Launch Date Picker Dialog
                    DatePickerDialog dpd2 = new DatePickerDialog(Poss_Home_Activity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int month= monthOfYear+1;
                                    String Mon = null;
                                    int lengthMonth = String.valueOf(month).length();
                                    if (lengthMonth==1)
                                    {
                                        Mon="0"+month;
                                    }
                                    else {
                                        Mon=month+"";
                                    }
                                    String Day = null;
                                    int lengthDay = String.valueOf(dayOfMonth).length();

                                    if (lengthDay==1)
                                    {
                                        Day="0"+dayOfMonth;
                                    }
                                    else {
                                        Day=dayOfMonth+"";
                                    }

                                    Pos_To.setText(year+"-"+Mon);
                                }
                            }, mYear2, mMonth2, mDay2);
                    dpd2.show();
                }



                break;
            case R.id.btn_next:
                Constants.callIntent(Poss_Home_Activity.this, Interaction_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                break;
            case R.id.btn_previous:
                Constants.callIntent(Poss_Home_Activity.this, Claims_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;


        }
    }

    private class Gwp_Service extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Poss_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("filterType",params[2] );
            request.addProperty("sseId",sharedPreferences.getString("NTID", "0") );
            request.addProperty("fromDate","2016-06" );
            request.addProperty("toDate","2016-10" );
            request.addProperty("fromDate",Pos_From.getText().toString() );
            request.addProperty("toDate",Pos_To.getText().toString() );

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error="";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error=e.toString();
            } catch (XmlPullParserException e)
            {
                e.printStackTrace();
                soap_error=e.toString();
            }


            // Check Network Exception

            if(soap_error.equals(""))
            {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error=soapFault.toString();
                    soapFault.printStackTrace();
                }

                try  {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    int index_x=0;
                    int index_y=0;
                    for (int i=0;i<soapObject2.getPropertyCount();i++)
                    {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals( "Pos" ))
                        {

                            Object property1 = propertyTest.getProperty("Name");
                            Object property2 = propertyTest.getProperty("ValueInDec");
                            C_Name=property1.toString();
                            C_Value=property2.toString();

                            double value = Double.parseDouble(C_Value);

                            labels.add(C_Name);
                            group1.add(new BarEntry((float)value, index_x));

                            Log.i( "TAG",index_x+"" +": "+value);
                            index_x=index_x+1;


                        }else
                        {
                            if(pi.getName().equals( "NonPos" ))
                            {

                                Object property1 = propertyTest.getProperty("Name");
                                Object property2 = propertyTest.getProperty("ValueInDec");
                                P_Name=property1.toString();
                                P_Value=property2.toString();

                                // example String
                                double value = Double.parseDouble(P_Value);
                                group2.add(new BarEntry((float)value , index_y));
                                Log.i( "TAG",index_y+"" +": "+value);
                                index_y=index_y+1;



                            }

                        }

                    }

                }catch (Exception  e)
                {
                    e.printStackTrace();
                }
            }else {

                soap_error="Error";
            }




            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            if(progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (s.equals(""))
            {
                barDataSet1 = new BarDataSet(group1, "CurrentYear");
                barDataSet2 = new BarDataSet(group2, "PreviousYear");
                barDataSet1.setColors(new int[]{Color.rgb( 102,180,216 )});
                barDataSet2.setColors(new int[]{Color.rgb( 248,208,3 )});
                if(group1.isEmpty())
                {
                    barChart.setVisibility( View.INVISIBLE );
                    Toast.makeText(Poss_Home_Activity.this,"No Business Yet",Toast.LENGTH_SHORT ).show();
                }

                ArrayList<BarDataSet> dataset = new ArrayList<>();
                dataset.add(barDataSet1);
                dataset.add(barDataSet2);


                try {
                    BarData data = new BarData(labels, dataset);
                    barChart.setDescription( "" );
                    barChart.setData(data);
                    barChart.animateY(1000);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }else
            {
                Toast.makeText(Poss_Home_Activity.this,"Error in Response",Toast.LENGTH_SHORT ).show();
            }




        }
    }


}
