package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.FeedbackListAdapter;
import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Entity.FeedbackListData;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetMobileFeedback.METHOD_GET_FEEDBACK;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetMobileFeedback.SOAP_ACTION_GET_FEEDBACK;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;

public class FeedbackActivity extends AppCompatActivity {
    public static final String MY_PREFS_NAME = "HdfcErgo";
    private String ssid, Loginname, Loginntid;
    private ConsUrl consUrl;
    private ProgressDialog progressDialog;

    private String strAPP_NAME = "", strVERSION = "", strTYPE = "", strREMARKS = "", strCREATED_BY = "";
    private String strCREATED_DATE = "", strSSE_ID = "";

    private RecyclerView mFeedbackRecycler;
    private LinearLayoutManager mLayoutManager;
    private SharedPreferences sharedPreferences;
    private ArrayList<FeedbackListData> feedbackItemList;
    private ArrayList<FeedbackListData> feedbackArrayList;
    private ArrayList<String> dataList;
    private FeedbackListAdapter mFeedbackAdapter;
    private Toolbar mToolbar;
    private TextView mTitle,txtNoData;
    private LinearLayout ll_no_data;
    private FrameLayout container;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_feedback);
        consUrl = new ConsUrl();
        getSharedpreferenceData();
        setAppToolbar();
        initializeViews();

        getMobileFeedback();
        addFabButton();
    }

    private void initializeViews() {
        feedbackItemList = new ArrayList<>();
        feedbackArrayList = new ArrayList<>();
        dataList = new ArrayList<>();

        txtNoData = (TextView) findViewById(R.id.tv_no_data);
        ll_no_data = (LinearLayout) findViewById(R.id.ll_no_data);
        container=(FrameLayout)findViewById(R.id.container);

        mFeedbackRecycler = (RecyclerView) findViewById(R.id.feedbackList);
        mFeedbackRecycler.setHasFixedSize(true);
        mFeedbackRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(FeedbackActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFeedbackRecycler.setLayoutManager(mLayoutManager);
    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Feedback");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    private void addFabButton() {
       fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Click action
                Constants.callIntent(FeedbackActivity.this, AddFeedbackActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
    }

    public void getMobileFeedback() {
        if (Constants.isNetworkInfo(FeedbackActivity.this)) {
            try {
                new GetMobileFeedback().execute(ssid);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void getSharedpreferenceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(FeedbackActivity.this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void loaddata() {

        if (feedbackArrayList.isEmpty() || feedbackArrayList == null || feedbackArrayList.equals("")) {
            System.out.println("EmptySize:=>" + feedbackArrayList.size());
        } else {//context, ExistAgentArrayList, existagentrecyclerview, R.layout.template_existagentuser, dataList
            mFeedbackAdapter = new FeedbackListAdapter(FeedbackActivity.this, feedbackArrayList,dataList);
            mFeedbackRecycler.setAdapter(mFeedbackAdapter);
            mFeedbackAdapter.notifyDataSetChanged();

            mFeedbackRecycler.addOnItemTouchListener(new RecyclerTouchListener(FeedbackActivity.this, mFeedbackRecycler, new ClickListener() {
                        @Override
                        public void onClick(View view,final int position) {
                            LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                            parentLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    System.out.println("position 11: " + position);
                                    mFeedbackAdapter.setPopUpView(position);
                                }
                            });
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
          /*  mFeedbackRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if(dy > 0){
                        fab.hide();
                    } else{
                        fab.show();
                    }

                    super.onScrolled(recyclerView, dx, dy);

                }
            });*/
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.callIntent(FeedbackActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public class GetMobileFeedback extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FeedbackActivity.this, R.style.AlertDialogCustom);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_FEEDBACK);
            request.addProperty("SSE_ID", params[0]);
            Log.v("GetMobileFeedback "," SSE_ID "+params[0]);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_FEEDBACK, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "getFeedback:=>" + result.toString());
                  //  soap_error = result.toString();
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (Exception e) {
                    soap_error = "Error";
                    e.printStackTrace();
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);


                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            try {
                                if (soapObject3.getProperty(0).equals("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strTYPE = soapObject3.getPropertyAsString("TYPE");
                                        strREMARKS = soapObject3.getPropertyAsString("REMARKS");
                                        strCREATED_DATE = soapObject3.getPropertyAsString("CREATED_DATE");
                                        strSSE_ID = soapObject3.getPropertyAsString("SSE_ID");
                                        try {
                                            strAPP_NAME = soapObject3.getPropertyAsString("APP_NAME");
                                        } catch (RuntimeException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            strVERSION = soapObject3.getPropertyAsString("VERSION");
                                        } catch (RuntimeException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            strCREATED_BY = soapObject3.getPropertyAsString("CREATED_BY");
                                        } catch (RuntimeException e) {
                                            e.printStackTrace();
                                        }

                                        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();

                                        editor.putString("APP_NAME" + i, strAPP_NAME);
                                        editor.putString("VERSION" + i, strVERSION);
                                        editor.putString("TYPE" + i, strTYPE);
                                        editor.putString("REMARKS" + i, strREMARKS);
                                        editor.putString("CREATED_BY" + i, strCREATED_BY);
                                        editor.putString("CREATED_DATE" + i, strCREATED_DATE);
                                        editor.putString("SSE_ID" + i, strSSE_ID);
                                        editor.commit();

                                        FeedbackListData items = new FeedbackListData();
                                        items.APPNAME = strAPP_NAME;
                                        items.VERSION = strVERSION;
                                        items.TYPE = strTYPE;
                                        items.REMARKS = strREMARKS;
                                        items.CREATED_BY = strCREATED_BY;
                                        items.CREATED_DATE = strCREATED_DATE;
                                        items.SSE_ID = strSSE_ID;


                                        dataList.add(items.CREATED_BY);
                                        feedbackItemList.add(items);
                                    }catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;

            }
            System.out.println("onPostExecfeedback: result " + result);
            if (result.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (result.equals("0")) {
                    //  swipeRefreshLayout.setRefreshing(false);
                    //Toast.makeText(context, "Invalid response", Toast.LENGTH_SHORT).show();
                    // swipeRefreshLayout.setVisibility(View.GONE);

                    // txtNoData.setVisibility(View.VISIBLE);
                    // txtNoData.setText("No Data Available");
                    mFeedbackRecycler.setVisibility(View.GONE);
                    container.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");

                   //Toast.makeText(getApplicationContext(), "No Data Available", Toast.LENGTH_SHORT).show();
                } else if (result.equals("Message")) {
                    mFeedbackRecycler.setVisibility(View.GONE);
                    container.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("Error while fetching Feedback in Oval Lite !!");

                  //  Toast.makeText(getApplicationContext(), "Error while fetching Notifications in Oval Lite !!", Toast.LENGTH_SHORT).show();
                }
                else {
                    // swipeRefreshLayout.setVisibility(View.VISIBLE);
                    //// swipeRefreshLayout.setRefreshing(false);
                    // txtNoData.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.GONE);
                    mFeedbackRecycler.setVisibility(View.VISIBLE);
                    container.setVisibility(View.VISIBLE);

                    if (feedbackItemList != null) {
                        feedbackArrayList.clear();
                    }
                    if (feedbackItemList.size() != 0)
                        for (FeedbackListData obj : feedbackItemList)
                            feedbackArrayList.add(obj);

                    System.out.println("feedbackArrayList:=>" + feedbackArrayList.size());
                    System.out.println("dataList:=>" + dataList.size());

                    loaddata();

                }
            }
        }
    }
}
