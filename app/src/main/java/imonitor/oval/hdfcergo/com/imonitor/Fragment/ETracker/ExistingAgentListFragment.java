package imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.AddEtrackerExistinguserActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.EtrackerExistingAgentAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackerexistagentListData;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.VerticalSpaceItemDecorartion;

import static android.content.Context.MODE_PRIVATE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getExistingUser.METHOD_GET_EX_USER;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getExistingUser.SOAP_ACTION_GET_EX_USER;


public class ExistingAgentListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";

    String TAG_Soap_Response;
    RelativeLayout reliu;
    ConsUrl consUrl;
    ArrayList<EtrackerexistagentListData> ExistAgentItemlist;
    ArrayList<EtrackerexistagentListData> ExistAgentArrayList;
    RecyclerView existagentrecyclerview;
    TextView txtNoData;
    LinearLayoutManager mLayoutManager;
    EtrackerExistingAgentAdapter existagentadapter;
    ArrayList<String> dataList;
    TextView mTitle;
    ImageView addagent;
    String ssid, Loginname, Loginntid;
    Context context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_existing_agent_list, container, false);

        context = getActivity();
        consUrl = new ConsUrl();
        sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        getSharedpreferebnceData();
        findViews(view);
        addFabButton(view);
        return view;

    }

    private void addFabButton(View view) {
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Click action
                Constants.callIntent(context, AddEtrackerExistinguserActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_exist);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        WebserviceCall();
                    }
                }
        );
    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(context);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void WebserviceCall() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(getActivity())) {
            try {
               // String existagentparameter = ssid + "_" + Loginname;
                String existagentparameter = ssid + "_" +Loginname+ "_500";
               // String existagentparameter = "1637" + "_" +"ankur boharey"+ "_500";
               // String existagentparameter = "1637" + "_" +"ankur boharey";
                // String existagentparameter = "1968__sandeep shewale";
                System.out.println("existagentparameter:=>" + existagentparameter);

                // edited by Pooja Patil at 08-06-17
                new GetExistingAgentList().execute(existagentparameter);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Constants.snackbar(reliu, Constants.offline_msg);

            Snackbar.make(getView(), Constants.offline_msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }


    private void findViews(View view) {
        ExistAgentItemlist = new ArrayList<>();
        ExistAgentArrayList = new ArrayList<>();
        dataList = new ArrayList<>();

        txtNoData = (TextView) view.findViewById(R.id.tv_no_data);
        existagentrecyclerview = (RecyclerView) view.findViewById(R.id.existagentList);
        existagentrecyclerview.setHasFixedSize(true);
        existagentrecyclerview.setItemAnimator(new DefaultItemAnimator());
        existagentrecyclerview.addItemDecoration(new VerticalSpaceItemDecorartion(10));
        mLayoutManager = new LinearLayoutManager(context);
        existagentrecyclerview.setLayoutManager(mLayoutManager);



    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        // WebserviceCall();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loaddata() {

        if (ExistAgentArrayList.isEmpty() || ExistAgentArrayList == null || ExistAgentArrayList.equals("")) {
            System.out.println("EmptySize:=>" + ExistAgentArrayList.size());
        } else {
            existagentadapter = new EtrackerExistingAgentAdapter(context, ExistAgentArrayList, existagentrecyclerview, R.layout.template_existagentuser, dataList);
            existagentrecyclerview.setAdapter(existagentadapter);
            existagentadapter.notifyDataSetChanged();


            existagentrecyclerview.addOnItemTouchListener(new RecyclerTouchListener(context, existagentrecyclerview, new ClickListener() {
                @Override
                public void onClick(View view, final int position) {
                    LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                    parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("position 11: " + position);
                            existagentadapter.setPopUpView(position);
                        }
                    });

                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
        }
    }

    // edited by Pooja Patil at 08-06-17
    //start pooja
    public class GetExistingAgentList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_EX_USER);
            request.addProperty("sseId", params[0]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_EX_USER, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }catch (Exception e){
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("","Response getExAgentList:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                }catch(ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch(Exception e){
                    e.printStackTrace();
                    soap_error = "Error";
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strPEOPLEMET = "", strMEETINGTYPE = "", strVENUE = "", strROLE = "", strVERTICAL = "";
                        String strSUBVERTICAL = "", strDATEOFMEETING = "", strMOM = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                    try {
                                        strPEOPLEMET = soapObject3.getPropertyAsString("PEOPLEMET");
                                        strROLE = soapObject3.getPropertyAsString("ROLE");
                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");
                                        strSUBVERTICAL = soapObject3.getPropertyAsString("SUBVERTICAL");
                                        strDATEOFMEETING = soapObject3.getPropertyAsString("DATEOFMEETING");
                                        strVENUE = soapObject3.getPropertyAsString("VENUE");
                                        strMOM = soapObject3.getPropertyAsString("MINUTESOFMEETING");
                                        strMEETINGTYPE = soapObject3.getPropertyAsString("MEETINGTYPE");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (strVERTICAL.equals("B1") ||
                                            strVERTICAL.equals("B2") ||
                                            strVERTICAL.equals("B3") ||
                                            strVERTICAL.equals("B4") ||
                                            strVERTICAL.equals("B5") ||
                                            strVERTICAL.equals("B6") ||
                                            strVERTICAL.equals("B7") ||
                                            strVERTICAL.equals("B8") ||
                                            strVERTICAL.equals("B9") ||
                                            strVERTICAL.equals("CSC") ||
                                            strVERTICAL.equals("RABG")) {

                                    } else {
                                        sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();

                                        editor.putString("MEETINGTYPE" + i, strMEETINGTYPE);
                                        editor.putString("VENUE" + i, strVENUE);
                                        editor.putString("PEOPLEMET" + i, strPEOPLEMET);
                                        editor.putString("ROLE" + i, strROLE);
                                        editor.putString("VERTICAL" + i, strVERTICAL);
                                        editor.putString("SUBVERTICAL" + i, strSUBVERTICAL);
                                        editor.putString("DATEOFMEETING" + i, strDATEOFMEETING);
                                        editor.putString("MINUTESOFMEETING" + i, strMOM);
                                        // Log.i("TAG", "i :" + i + " MEETING_CLIENT_NAME :" + strCLIENT_NAME);
                                        editor.commit();

                                        EtrackerexistagentListData items = new EtrackerexistagentListData();
                                        items.MEETINGTYPE = strMEETINGTYPE;
                                        items.PEOPLEMET = strPEOPLEMET;
                                        items.VENUE = strVENUE;
                                        items.ROLE = strROLE;
                                        items.VERTICAL = strVERTICAL;
                                        items.SUBVERTICAL = strSUBVERTICAL;
                                        items.MINUTESOFMEETING = strMOM;
                                        items.DATEOFMEETING = strDATEOFMEETING;


                                        dataList.add(items.PEOPLEMET);
                                        ExistAgentItemlist.add(items);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                //  TAG_Soap_Response = "0";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("s:=>" + s);
            if (s.equals("Error")) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
            } else {

                if (s.equals("0")) {
                    swipeRefreshLayout.setRefreshing(false);
                    //Toast.makeText(context, "Invalid response", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setVisibility(View.GONE);

                    txtNoData.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");
                }  else {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    txtNoData.setVisibility(View.GONE);

                    if (ExistAgentArrayList != null) {
                        ExistAgentArrayList.clear();
                    }
                    if (ExistAgentItemlist.size() != 0) {

                        for (EtrackerexistagentListData obj : ExistAgentItemlist)
                            ExistAgentArrayList.add(obj);
                    } else{
                        swipeRefreshLayout.setVisibility(View.GONE);

                        txtNoData.setVisibility(View.VISIBLE);
                        txtNoData.setText("No Data Available");
                    }
                    System.out.println("ExistAgentArrayList:=>" + ExistAgentArrayList.size());
                    System.out.println("dataList:=>" + dataList.size());

                    loaddata();

                }
            }

        }
    }
    //end Pooja

}


