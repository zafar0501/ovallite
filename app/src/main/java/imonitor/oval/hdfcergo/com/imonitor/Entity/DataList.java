package imonitor.oval.hdfcergo.com.imonitor.Entity;

import android.graphics.drawable.Drawable;

/**
 * Created by Zafar.Hussain on 19/06/2017.
 */

public class DataList {
    public Drawable img;
    public String title;
    public String desc;

    public DataList(Drawable img, String title, String desc) {
        this.img = img;
        this.title = title;
        this.desc = desc;
    }

    public Drawable getImg() {
        return img;
    }

    public void setImg(Drawable img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
