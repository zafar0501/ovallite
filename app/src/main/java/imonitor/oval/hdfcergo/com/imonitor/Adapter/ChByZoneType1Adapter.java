package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import imonitor.oval.hdfcergo.com.imonitor.Entity.ChannelByZoneDataModel;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 25/09/2017.
 */

public class ChByZoneType1Adapter extends RecyclerView.Adapter<ChByZoneType1Adapter.ViewHolder> {
    Context mContext;
    ArrayList<ChannelByZoneDataModel> mListItems;
    //  ArrayList<String> mListItems;
    LinkedHashMap<String, String> headers;
    List<LinkedHashMap> mList;
    Object keyss;
    // Object[] values;
    String from;
    int[] s;
    LinearLayout linearlayout;
    TextView txtKey, txtValue;
    String[] keys, values;
    String key, value;
    //private String[] header = {"EAST", "WEST", "CENTRAL", "NORTH", "SOUTH"};
   // private String[] valuess = {"192%", "-", "-", "29%", "10%"};
    //   private String[] vertical = {"AGENCY1", "BANCASSURANCEB6", "DIRECTD1", "GRAND TOTAL"};


    public ChByZoneType1Adapter(Context mContext, ArrayList<ChannelByZoneDataModel> mListItems, String from, LinkedHashMap<String, String> headers, List<LinkedHashMap> mList) {
        this.mContext = mContext;
        this.mListItems = mListItems;
        this.from = from;
        this.headers = headers;
        this.mList = mList;
        Log.v("", "headers.size() " + headers.size());

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_list_item, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

            holder.txt_vertical.setText(mListItems.get(position).VERTICAL);
            holder.txt_year.setText(mListItems.get(position).YEAR);

            for (Map.Entry<String, String> mapEntry : headers.entrySet()) {
                key = mapEntry.getKey();
                value = mapEntry.getValue();

                //  holder.txt_vertical.setText((CharSequence) mList.get(position).get(key));
                //Creating LinearLayout
                linearlayout = new LinearLayout(mContext);
                //Setting up LinearLayout Orientation
                linearlayout.setOrientation(LinearLayout.VERTICAL);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                linearlayout.setLayoutParams(lp);
                LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                txtKey = new TextView(mContext);
                txtKey.setLayoutParams(LayoutParamsview);
                txtKey.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                txtKey.setText(key);

                // txtKey.setText((CharSequence) mList.get(position).get(key)); //headers.keySet().toArray() headers.keySet().toArray()[i].toString()
                // headers.get(mList.get(i))
                //lbl11.setText(mListItems.get(position).);

                //String.valueOf(mList.get(position).get(key)
                // float val= Float.parseFloat(String.valueOf(mList.get(position).get(key)));
                // Log.v("","StringVal: "+String.valueOf(mList.get(position).get(v)));
                // Log.v("","FloatVAl: "+Float.parseFloat(value));
//            Log.v("","FloatValues: "+new DecimalFormat("##").format(Float.parseFloat(String.valueOf(mList.get(position).get(key)))) + " %");

                txtValue = new TextView(mContext);
                txtValue.setLayoutParams(LayoutParamsview);
                txtValue.setTextColor(mContext.getResources().getColor(R.color.Black));
              //  txtValue.setText(value);// replace value with hashmap values
                // txtValue.setText(new DecimalFormat("##").format(Float.parseFloat(String.valueOf(mList.get(position).get(key)))) + " %");// replace value with hashmap values
                // txtValue.setText(valuess[i]);// replace value with hashmap values
                // txtValue.setText((CharSequence) mList.get(position).get(value));
                    if (String.valueOf(mList.get(position).get(key)).equals("null") ||
                            String.valueOf(mList.get(position).get(key)).equals("0") ||
                            String.valueOf(mList.get(position).get(key)).equals("NaN") ||
                            String.valueOf(mList.get(position).get(key)).equals("Infinity") ||
                            String.valueOf(mList.get(position).get(key)).equals("-Infinity") ||
                            String.valueOf(mList.get(position).get(key)).equals("INF")) {
                        Log.v("", "value= 0");
                        txtValue.setText("-");
                    Log.v("","value= 0");
                    txtValue.setText("-");
                }else{
                    txtValue.setText(new DecimalFormat("##").format(Float.parseFloat(String.valueOf(mList.get(position).get(key)))) + " %");// replace value with hashmap values
                }
                linearlayout.addView(txtKey);
                linearlayout.addView(txtValue);
                holder.ll_dynamic.addView(linearlayout);
        }




    }


    @Override
    public int getItemCount() {
            return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_dynamic;
        TextView lbl_vertical, txt_vertical,txt_year;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_dynamic = (LinearLayout) itemView.findViewById(R.id.ll_dynamic);

            lbl_vertical = (TextView) itemView.findViewById(R.id.lbl_vertical);
            txt_vertical = (TextView) itemView.findViewById(R.id.txt_vertical);
            txt_year = (TextView) itemView.findViewById(R.id.txt_Year);


        }
    }
}
