package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.DailySourceGetDataAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.DailySourceDataAddList;
import imonitor.oval.hdfcergo.com.imonitor.Entity.DailySourceGetData;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getDailySourcingList.METHOD_GET_SOURCE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getDailySourcingList.SOAP_ACTION_GET_SOURCE;

/**
 * Created by Pooja Patil on 21/06/2017.
 */

public class GetDailySourcingListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";

    ConsUrl consUrl;
    Context context;
    String venueAddr, ssid, Loginname, Loginntid;
    Toolbar mToolbar;
    TextView mTitle, txt_ttproducts, txt_policies, txt_premium, txt_label, txtNoData;
    LinearLayout ll_no_data;
    RecyclerView mRecyclerView;
    DividerItemDecoration dividerItemDecoration;
    LinearLayoutManager mLayoutManager;
    SoapObject result;
    FrameLayout listLayout;
    String TAG_Soap_Response;
    DailySourceGetData dataList;
    ArrayList<DailySourceGetData> mArrList;
    SharedPreferences sharedPreferences;
    DailySourceGetDataAdapter mAdapter;
    //    DailySourceDetailsData detailsList;
    DailySourceDataAddList detailsList;
    ArrayList<DailySourceDataAddList> detailsArrList;
    Date createdDate;
    Typeface calibriFont;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.daily_sourcing_list_activity);

        calibriFont = Typeface.createFromAsset(getAssets(), "fonts/calibri.ttf");

        consUrl = new ConsUrl();
        context = this;
        setAppToolbar();
        getSharedpreferebnceData();
        initializeComponents();
        callWebService();
        setUpRecyclerView();
        addFabButton();
    }

    private void callWebService() {
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        getDailySourceList();
                    }
                }
        );
    }

    private void initializeComponents() {
        mArrList = new ArrayList<>();
        detailsArrList = new ArrayList<>();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        txtNoData = (TextView) findViewById(R.id.tv_no_data);
        ll_no_data = (LinearLayout) findViewById(R.id.ll_no_data);
        listLayout = (FrameLayout) findViewById(R.id.outerLayout);

        //aded on 29/06/17
        txt_ttproducts = (TextView) findViewById(R.id.txt_ttproducts);
        txt_policies = (TextView) findViewById(R.id.txt_policies);
        txt_premium = (TextView) findViewById(R.id.txt_premium);
        txt_label = (TextView) findViewById(R.id.txt_label);

        txt_premium.setTextColor(Color.BLUE);
        txt_policies.setTextColor(Color.BLUE);
        txt_ttproducts.setTextColor(Color.BLUE);

        txt_premium.setTypeface(calibriFont);
        txt_policies.setTypeface(calibriFont);
        txt_ttproducts.setTypeface(calibriFont);
        txt_label.setTypeface(calibriFont);

        swipeRefreshLayout.setOnRefreshListener(this);


    }

    private void getDailySourceList() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(GetDailySourcingListActivity.this)) {
            try {
                new GetDailySourceList().execute(ssid);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void setUpRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.d_listRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Daily Sourcing");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname 11:=>" + Loginname);
        System.out.println("Loginssid 11:=>" + ssid);
        System.out.println("Loginntid 11:=>" + Loginntid);
    }

    private void addFabButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Constants.callIntent(context, AddDailySourcingActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loaddata() {
        mAdapter = new DailySourceGetDataAdapter(context, mArrList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //int pos = (int) Newagentadapter.getItemId(position);

                LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                parentLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("position 11: " + position);
                        String createdDate = mArrList.get(position).getCreatedDate();

                        Intent i = new Intent(GetDailySourcingListActivity.this, GetDailySourceDetailsListActivity.class);
                        i.putExtra("CREATED_DATE", createdDate);
                        startActivity(i);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));


    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Constants.callIntent(GetDailySourcingListActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);

    }

    public class GetDailySourceList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_SOURCE);
            request.addProperty("SSE_ID", params[0]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_SOURCE, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }

            if (soap_error.equals("")) {

                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "Response DailySource:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }


                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strTOT_PRODUCTS = "", strTOT_POLICIES = "", strTOT_PREMIUM = "", strCREATED_DATE = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strTOT_PRODUCTS = soapObject3.getPropertyAsString("TOT_PRODUCTS");
                                        strTOT_POLICIES = soapObject3.getPropertyAsString("TOT_POLICIES");
                                        strTOT_PREMIUM = soapObject3.getPropertyAsString("TOT_PREMIUM");
                                        strCREATED_DATE = soapObject3.getPropertyAsString("CREATED_DATE");

                                        sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();

                                        editor.putString("TOT_PRODUCTS" + i, strTOT_PRODUCTS);
                                        editor.putString("TOT_POLICIES" + i, strTOT_POLICIES);
                                        editor.putString("TOT_PREMIUM" + i, strTOT_PREMIUM);
                                        editor.putString("CREATED_DATE" + i, strCREATED_DATE);
                                        editor.commit();


                                        dataList = new DailySourceGetData();
                                        dataList.setTotalProducts(strTOT_PRODUCTS);
                                        dataList.setTotalPolicies(strTOT_POLICIES);
                                        dataList.setTotalPremium(strTOT_PREMIUM);
                                        dataList.setCreatedDate(strCREATED_DATE);

                                        mArrList.add(dataList);

                                        Log.v("", "mArrList: " + mArrList.size() + "" + mArrList);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            int totProducts = 0, totPolicies = 0, totPremium = 0;
            if (s.equals("Error")) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");

                } else if (s.equals("Message")) {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getApplicationContext(), "Error while fetching Daily Sourcing data !!", Toast.LENGTH_SHORT).show();
                    txt_ttproducts.setText("0");
                    txt_policies.setText("0");
                    txt_premium.setText("0L");
                } else {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    ll_no_data.setVisibility(View.GONE);

                    // adding/setting data to recycler view
                    loaddata();

                    //added on 29/06/17
                    for (int i = 0; i < mArrList.size(); i++) {
                        totProducts += Integer.parseInt(mArrList.get(i).getTotalProducts());
                        totPolicies += Integer.parseInt(mArrList.get(i).getTotalPolicies());
                        totPremium += Integer.parseInt(mArrList.get(i).getTotalPremium());

                    }
                    System.out.println("Product Total: " + totProducts);
                    System.out.println("Policies Total: " + totPolicies);
                    System.out.println("Premium Total: " + totPremium);


//                double i2=i/60000;
//                tv.setText(new DecimalFormat("##.##").format(i2));
                    float totPremm = totPremium / 100000f;
                    System.out.println("22 Premium Total: " + totPremm);


                    txt_ttproducts.setText(String.valueOf(totProducts));
                    txt_policies.setText(String.valueOf(totPolicies));
                    // txt_premium.setText(String.valueOf(totPremium) + "");
                    txt_premium.setText(new DecimalFormat("##.##").format(totPremm) + " L");

                    Log.i("status:=>", "1");
                }
            }

        }
    }
}
