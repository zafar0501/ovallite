package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import imonitor.oval.hdfcergo.com.imonitor.Entity.ChannelByZoneDataModel;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 29/09/2017.
 */

public class ChByZoneType4Adapter extends RecyclerView.Adapter<ChByZoneType4Adapter.ViewHolder> {
    Context mContext;
    ArrayList<ChannelByZoneDataModel> mChZoneType4ArrList;
    String from;
    LinkedHashMap<String, String> headers;
    List<LinkedHashMap> mChZoneType4List;
    LinearLayout linearlayout;
    TextView txtKey, txtValue;
    String key, value;

    public ChByZoneType4Adapter(Context mContext, ArrayList<ChannelByZoneDataModel> mChZoneType4ArrList, String from, LinkedHashMap<String, String> headers, List<LinkedHashMap> mChZoneType4List) {
        this.mContext = mContext;
        this.mChZoneType4ArrList = mChZoneType4ArrList;
        this.from = from;
        this.headers = headers;
        this.mChZoneType4List = mChZoneType4List;
        Log.v("ChByZoneType4Adapter ", "headers: " + headers.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_list_item, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public int getItemCount() {
        return mChZoneType4ArrList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChannelByZoneDataModel data = mChZoneType4ArrList.get(position);
        if (from.equals("Channel By Month")|| from.equals("Channel By Zone")) {
            holder.txt_vertical.setText(data.VERTICAL);
            setDynamicLayout(holder,position);
            holder.setIsRecyclable(false);
        } else if (from.equals("Zone By Month")) {
            holder.txt_vertical.setText(data.ZONE);
            setDynamicLayout(holder,position);
            holder.setIsRecyclable(false);
        } else if (from.equals("Seg By Month")) {
            holder.txt_vertical.setText(data.SEGMENT);
            setDynamicLayout(holder, position);
            holder.setIsRecyclable(false);
        } else if (from.equals("City By Month")) {
            holder.txt_vertical.setText(data.CITY);
            setDynamicLayout(holder,position);
            holder.setIsRecyclable(false);
        }
        holder.txt_Year.setText(data.YEAR);
      /*  if (from.equals("Channel By Month") || from.equals("Channel By Zone")) {
            holder.txt_vertical.setText(mChZoneType4ArrList.get(position).VERTICAL);
        } else if (from.equals("Zone By Month")) {
            holder.txt_vertical.setText(mChZoneType4ArrList.get(position).ZONE);
        }else if(from.equals("Seg By Month")){
            holder.txt_vertical.setText(mChZoneType4ArrList.get(position).SEGMENT);
        }else if(from.equals("City By Month")){
            holder.txt_vertical.setText(mChZoneType4ArrList.get(position).CITY);
        }
        holder.txt_Year.setText(mChZoneType4ArrList.get(position).YEAR);
*/

    }

    private void setDynamicLayout(ViewHolder holder, int position) {
        for (Map.Entry<String, String> mapEntry : headers.entrySet()) {
            key = mapEntry.getKey();
            value = mapEntry.getValue();

            //Creating LinearLayout
            linearlayout = new LinearLayout(mContext);
            //Setting up LinearLayout Orientation
            linearlayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
            lp.setMargins(5, 0, 5, 0);
            linearlayout.setLayoutParams(lp);
            LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            txtKey = new TextView(mContext);
            txtKey.setLayoutParams(LayoutParamsview);
            txtKey.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            txtKey.setText(key);

            txtValue = new TextView(mContext);
            txtValue.setGravity(Gravity.CENTER);
            txtValue.setLayoutParams(LayoutParamsview);

            txtValue.setTextColor(mContext.getResources().getColor(R.color.Black));
            //  txtValue.setText(new DecimalFormat("##").format(Float.parseFloat(String.valueOf(mChZoneType4List.get(position).get(key)))) + " %");// replace value with hashmap values
            if (String.valueOf(mChZoneType4List.get(position).get(key)).equals("null") ||
                    String.valueOf(mChZoneType4List.get(position).get(key)).equals("0") ||
                    String.valueOf(mChZoneType4List.get(position).get(key)).equals("NaN") ||
                    String.valueOf(mChZoneType4List.get(position).get(key)).equals("Infinity") ||
                    String.valueOf(mChZoneType4List.get(position).get(key)).equals("-Infinity") ||
                    String.valueOf(mChZoneType4List.get(position).get(key)).equals("INF")) {
                Log.v("", "value= 0");
                txtValue.setText("-");
            } else {
                txtValue.setText(new DecimalFormat("##").format(Float.parseFloat(String.valueOf(mChZoneType4List.get(position).get(key)))) + " %");// replace value with hashmap values
            }
            linearlayout.addView(txtKey);
            linearlayout.addView(txtValue);
            holder.ll_dynamic.addView(linearlayout);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_dynamic;
        TextView lbl_vertical, txt_vertical, txt_Year;


        public ViewHolder(View itemView) {
            super(itemView);
            ll_dynamic = (LinearLayout) itemView.findViewById(R.id.ll_dynamic);

            lbl_vertical = (TextView) itemView.findViewById(R.id.lbl_vertical);
            txt_vertical = (TextView) itemView.findViewById(R.id.txt_vertical);
            txt_Year = (TextView) itemView.findViewById(R.id.txt_Year);

        }
    }
}
