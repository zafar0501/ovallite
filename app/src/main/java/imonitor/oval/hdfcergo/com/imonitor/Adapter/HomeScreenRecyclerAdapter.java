package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;


import com.uncopt.android.widget.text.justify.JustifiedTextView;

import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Entity.DataList;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Pooja Patil on 19/06/2017.
 */

public class HomeScreenRecyclerAdapter extends RecyclerView.Adapter<HomeScreenRecyclerAdapter.ViewHolder> {
    Context context;
    ArrayList<DataList> items;

    public HomeScreenRecyclerAdapter(Context context, ArrayList<DataList> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.titleTextView.setText(items.get(position).title);
       // String justify =holder.justifiedTextView.justifyLine(items.get(position).desc);
       // System.out.println("justify:  "+justify);
        // holder.descTextView.setText(items.get(position).desc);
        holder.descTextView.setText(items.get(position).desc);
        holder.holderImg.setImageDrawable(items.get(position).img);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        //return super.getItemId(position);
        return position;
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        JustifiedTextView descTextView;
        ImageView holderImg;
        Typeface font, calibriFont,halveticaFont,arialFont,robotoFont;

        public ViewHolder(View itemView) {
            super(itemView);
            font = Typeface.createFromAsset(context.getAssets(), "fonts/LoraRegular.ttf");
            calibriFont = Typeface.createFromAsset(context.getAssets(), "fonts/calibri.ttf");
            halveticaFont = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueM.ttf");
            arialFont = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
            robotoFont = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoMedium.ttf");

            //justifiedTextView=new JustifiedTextView(context);

            titleTextView = (TextView) itemView.findViewById(R.id.title_txtView);
            descTextView = (JustifiedTextView) itemView.findViewById(R.id.desc_txtView);
            holderImg = (ImageView) itemView.findViewById(R.id.holder_img);

            titleTextView.setTypeface(calibriFont);
            descTextView.setTypeface(calibriFont);
        }
    }
}
