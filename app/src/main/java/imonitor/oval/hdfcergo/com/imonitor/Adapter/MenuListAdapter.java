package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Entity.DataList;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 07/09/2017.
 */

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ViewHolder> {
    Context mContext;
    ArrayList<DataList> mMenuListItems;

    public MenuListAdapter(Context mContext, ArrayList<DataList> mMenuListItems) {
        this.mContext = mContext;
        this.mMenuListItems = mMenuListItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_row_item, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_menuItem.setText(mMenuListItems.get(position).title);
        holder.img_menuIcon.setImageDrawable(mMenuListItems.get(position).img);

    }
    @Override
    public long getItemId(int position) {
        //return super.getItemId(position);
        return position;
    }
    @Override
    public int getItemCount() {
        return mMenuListItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tv_menuItem;
        ImageView img_menuIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_menuItem=(TextView)itemView.findViewById(R.id.menu_item);
            img_menuIcon=(ImageView)itemView.findViewById(R.id.menu_icon);
            img_menuIcon.setColorFilter(mContext.getResources().getColor(R.color.colorPrimary));
        }
    }
}
