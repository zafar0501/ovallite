package imonitor.oval.hdfcergo.com.imonitor.Entity;

/**
 * Created by Zafar.Hussain on 21/06/2017.
 */

public class DailySourceGetData {

    public String totalProducts;
    public String totalPolicies;
    public String totalPremium;
    public String createdDate;

    public String getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(String totalProducts) {
        this.totalProducts = totalProducts;
    }

    public String getTotalPolicies() {
        return totalPolicies;
    }

    public void setTotalPolicies(String totalPolicies) {
        this.totalPolicies = totalPolicies;
    }

    public String getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(String totalPremium) {
        this.totalPremium = totalPremium;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
