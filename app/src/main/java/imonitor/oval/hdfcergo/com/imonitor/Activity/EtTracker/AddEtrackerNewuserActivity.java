package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.GPS.LocationAddress;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.CustomOnItemSelectedListener;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertNewUser.METHOD_NAME_NEWAGENT_SAVE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertNewUser.SOAP_ACTION_NEWAGENT_SAVE;

/**
 * Created by Meenakshi Aher on 24/05/2017.
 */

public class AddEtrackerNewuserActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";

    String TAG_Soap_Response;
    Context context;
    Spinner meetingoutcomespinner;
    String meetingoutcomename;
    EditText ed_agentname, ed_contactperson, ed_newuservenue, ed_gpsaddress, ed_newusergpsaddr, ed_contactnumber, ed_emailid, ed_meetingdetail;
    Button btn_newusersave;
    ConsUrl consUrl;
    GPSTracker gps;
    String ssid, Loginname, Loginntid;
    String lattitude, longitude;
    RelativeLayout newreliu;
    String venueAddr;
    TextView mTitle;
    private Toolbar toolbar;
    private TextInputLayout text_input_layout_nagent, text_input_layout_emailid, text_input_layout_contactnumber,
            text_input_layout_contactperson, text_input_layout_newuservenue, text_input_layout_meetingdetail;

    public static boolean isValidPhone(String phone) {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        return matcher.matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_etrackernewuser);
        consUrl = new ConsUrl();
        context = this;
        getSharedpreferebnceData();
        Getlocation();
        // GetAddressbyLatlong();
        setAppToolbar();
        findViews();
        setViews();
        init();
        getSharedprefLatLong();
    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_barn);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        SetActionBarTitle("Add New Agent");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                //  overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

    }

    private void Getlocation() {
        gps = new GPSTracker(AddEtrackerNewuserActivity.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
         /*

            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation( latitude, longitude,
                    getApplicationContext(), new GeocoderHandler() );*/
            if (latitude == 0.0 && longitude == 0.0) {
                Toast.makeText(getApplicationContext(), "Cant get Location !! ", Toast.LENGTH_SHORT).show();

            } else {
                venueAddr = LocationAddress.getCompleteAddressString(context, latitude, longitude);

                System.out.println("NewagentLat:=>" + latitude + "\n" + "NewagentLng:=>" + longitude);
                // \n is for new line
                //  Toast.makeText( getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG ).show();

            }
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }
   /* private void GetAddressbyLatlong() {
        Double Lattitude = Double.valueOf( "19.111126" );
        Double Longitude = Double.valueOf( "72.873750" );
        Geocoder geocoder = new Geocoder( this, Locale.getDefault() );
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation( Lattitude, Longitude, 1 );
            String cityName = addresses.get( 0 ).getAddressLine( 0 );
            String stateName = addresses.get( 0 ).getAddressLine( 1 );
            String countryName = addresses.get( 0 ).getAddressLine( 2 );
            Toast.makeText(AddEtrackerNewuserActivity.this, cityName + stateName + countryName, Toast.LENGTH_SHORT ).show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void getSharedprefLatLong() {
        String TAG = "Inside getSharedprefLatLong ";
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        lattitude = preferences.getString("Latitude", "");
        longitude = preferences.getString("Longitude", "");

        System.out.println(TAG + " Latitude: =>" + lattitude);
        System.out.println(TAG + " Longitude: =>" + longitude);
    }

    private void setViews() {
        ed_newuservenue.setText(venueAddr);
    }

    private void init() {

        // Creating adapter for spinner
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.meetingoutcome));

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        meetingoutcomespinner.setAdapter(dataAdapter);
    }

    private void findViews() {
        newreliu = (RelativeLayout) findViewById(R.id.newreliu);


        meetingoutcomespinner = (Spinner) findViewById(R.id.meetingoutcomespinner);

        text_input_layout_nagent = (TextInputLayout) findViewById(R.id.text_input_layout_nagent);
        text_input_layout_contactperson = (TextInputLayout) findViewById(R.id.text_input_layout_ncontactpersn);
        text_input_layout_newuservenue = (TextInputLayout) findViewById(R.id.text_input_layout_nvenue);
        text_input_layout_contactnumber = (TextInputLayout) findViewById(R.id.text_input_layout_ncontactnumber);
        text_input_layout_emailid = (TextInputLayout) findViewById(R.id.text_input_layout_nemailid);
        text_input_layout_meetingdetail = (TextInputLayout) findViewById(R.id.text_input_layout_nmeetingdetail);


        ed_agentname = (EditText) findViewById(R.id.ed_agentname);
        ed_contactperson = (EditText) findViewById(R.id.ed_contactperson);
        ed_newuservenue = (EditText) findViewById(R.id.ed_newuservenue);
        // ed_gpsaddress = (EditText) findViewById(R.id.ed_gpsaddress);
        ed_contactnumber = (EditText) findViewById(R.id.ed_contactnumber);
        ed_emailid = (EditText) findViewById(R.id.ed_emailid);
        ed_meetingdetail = (EditText) findViewById(R.id.ed_meetingdetail);


        btn_newusersave = (Button) findViewById(R.id.btn_newusersave);
        btn_newusersave.setOnClickListener(this);

        try {
            meetingoutcomespinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    meetingoutcomename = meetingoutcomespinner.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    @Override
    public void onClick(View v) {
        if (v == btn_newusersave) {
            if (ed_agentname.getText().toString().equals("") || ed_agentname.getText().toString() == null) {
                ed_agentname.setError(getString(R.string.agentname));
            } else if (ed_contactperson.getText().toString().equals("") || ed_contactperson.getText().toString() == null) {
                ed_contactperson.setError(getString(R.string.contactpersnerr));
            } else if (ed_newuservenue.getText().toString().equals("") || ed_newuservenue.getText().toString() == null) {
                ed_newuservenue.setError(getString(R.string.venuerr));
            } else if (!isValidMobile(ed_contactnumber.getText().toString())) {
                ed_contactnumber.setError("Invalid Contact Number.");
            } else if (!isValidEmaillId(ed_emailid.getText().toString().trim())) {
                ed_emailid.setError("Invalid Email Address.");
            } else if (ed_meetingdetail.getText().toString().equals("") || ed_meetingdetail.getText().toString() == null) {
                ed_meetingdetail.setError(getString(R.string.metngerr));
            } else if (meetingoutcomespinner.getSelectedItem().toString().equals("Meeting Outcome")) {
                ((TextView) meetingoutcomespinner.getSelectedView()).setError(getString(R.string.meetingouterr));
            } else {
                if (Constants.isNetworkInfo(AddEtrackerNewuserActivity.this)) {
                    try {
                        String ROLE = "Agent";
                        String AGENTNAME = ed_agentname.getText().toString();
                        String CONTACTPERSON = ed_contactperson.getText().toString();
                        String MEETINGATTENDEDBY = Loginname;
                        String DATEOFMEETING = "null";
                        String TIME = "null";
                        String VENUE = ed_newuservenue.getText().toString();
                        String GPSADDRESS = venueAddr;
                        String CONTACTNUMBER = ed_contactnumber.getText().toString();
                        String EMAILID = ed_emailid.getText().toString();
                        String MINUTESOFMEETING = ed_meetingdetail.getText().toString();
                        String MEETINGOUTCOME = meetingoutcomename;
                        String ISACTIVE = "true";
                        String CREATEDBY = Loginname;
                        String CREATEDDATE = "null";
                        String UPDATEDBY = Loginname;
                        String UPDATEDDATE = Loginname;
                        String TRAINEECODE = "null";
                        String SSE_ID = ssid;
                        String LATTITUDE = lattitude;
                        String LONGITUDE = longitude;

                        System.out.println("ParamsRole:=>" + ROLE);
                        System.out.println("ParamsAGENTNAME:=>" + AGENTNAME);
                        System.out.println("ParamsCONTACTPERSON:=>" + CONTACTPERSON);
                        System.out.println("ParamsMEETINGATTENDEDBY:=>" + MEETINGATTENDEDBY);
                        System.out.println("ParamsDATEOFMEETING:=>" + DATEOFMEETING);
                        System.out.println("ParamsTIME:=>" + TIME);
                        System.out.println("ParamsVENUE:=>" + VENUE);
                        System.out.println("ParamsGPSADDRESS:=>" + GPSADDRESS);
                        System.out.println("ParamsCONTACTNUMBER:=>" + CONTACTNUMBER);
                        System.out.println("ParamsEMAILID:=>" + EMAILID);
                        System.out.println("ParamsMINUTESOFMEETING:=>" + MINUTESOFMEETING);
                        System.out.println("ParamsMEETINGOUTCOME:=>" + MEETINGOUTCOME);
                        System.out.println("ParamsISACTIVE:=>" + ISACTIVE);
                        System.out.println("ParamsCREATEDBY:=>" + CREATEDBY);
                        System.out.println("ParamsICREATEDDATE:=>" + CREATEDDATE);
                        System.out.println("ParamsUPDATEDBY:=>" + UPDATEDBY);
                        System.out.println("ParamsUPDATEDDATE:=>" + UPDATEDDATE);
                        System.out.println("ParamsTRAINEECODE:=>" + TRAINEECODE);
                        System.out.println("ParamsSSE_ID:=>" + SSE_ID);
                        System.out.println("ParamsLATTITUDE:=>" + LATTITUDE);
                        System.out.println("ParamsLONGITUDE:=>" + LONGITUDE);

                       /* CallSaveExistingUser( ROLE, AGENTNAME, CONTACTPERSON, MEETINGATTENDEDBY, DATEOFMEETING, TIME, VENUE,
                                GPSADDRESS, CONTACTNUMBER, EMAILID, MINUTESOFMEETING, MEETINGOUTCOME, ISACTIVE, CREATEDBY, CREATEDDATE, UPDATEDBY,
                                UPDATEDDATE, TRAINEECODE, SSE_ID );*/

                        // edited by Pooja Patil at 12-06-17
                        new CallSaveNewUser().execute(ROLE, AGENTNAME, CONTACTPERSON, MEETINGATTENDEDBY, DATEOFMEETING, TIME, VENUE,
                                GPSADDRESS, CONTACTNUMBER, EMAILID, MINUTESOFMEETING, MEETINGOUTCOME, ISACTIVE, CREATEDBY, CREATEDDATE, UPDATEDBY,
                                UPDATEDDATE, TRAINEECODE, SSE_ID, LATTITUDE, LONGITUDE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Constants.snackbar(newreliu, Constants.offline_msg);
                }
            }
        }
    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    //end Pooja
    @Override
    public void onBackPressed() {

        Constants.callIntent(this, EtrackerNewAgentListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    /*private void CallSaveExistingUser(String ROLE, String AGENTNAME, String CONTACTPERSON, String MEETINGATTENDEDBY, String DATEOFMEETING, String TIME, String VENUE,
                                      String GPSADDRESS, String CONTACTNUMBER, String EMAILID, String MINUTESOFMEETING, String MEETINGOUTCOME, String ISACTIVE, String CREATEDBY,
                                      String CREATEDDATE, String UPDATEDBY, String UPDATEDDATE, String TRAINEECODE, String SSE_ID) {
        String API = consUrl.WebUrl;

        Retrofit retrofit = new Retrofit.Builder().baseUrl( API ).addConverterFactory( GsonConverterFactory.create() ).build();

        Webservices webservices = retrofit.create( Webservices.class );

        Call call = webservices.addNewUser( ROLE, AGENTNAME, CONTACTPERSON, MEETINGATTENDEDBY, DATEOFMEETING, TIME, VENUE,
                GPSADDRESS, CONTACTNUMBER, EMAILID, MINUTESOFMEETING, MEETINGOUTCOME, ISACTIVE, CREATEDBY, CREATEDDATE, UPDATEDBY,
                UPDATEDDATE, TRAINEECODE, SSE_ID );

        call.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit.Response<ResponseBody> response, Retrofit retrofit) {

                ResponseBody body = response.body();
                if (response.body() == null) {
                    Toast.makeText( getApplicationContext(), "serverError:" + response.message(), Toast.LENGTH_SHORT ).show();
                    return;
                }
                try {
                    BufferedReader reader = new BufferedReader( new InputStreamReader( body.byteStream() ) );
                    StringBuilder out = new StringBuilder();
                    String newline = System.getProperty( "line.separator" );
                    String line;
                    while ((line = reader.readLine()) != null) {
                        out.append( line );
                        out.append( newline );
                    }
                    JSONObject jsonobject = new JSONObject( out.toString() );
                    Log.i( "JSON:=>", jsonobject.toString() );
                    System.out.println( "NewAgentSaveresponce:=>" + jsonobject.toString() );

                    Constants.callIntent( AddEtrackerNewuserActivity.this, EtrackerNewAgentListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left );

                  *//*  JSONArray jsonArrayHot = jsonobject.getJSONArray( "NameValueData" );
                    for (int i = 0; i < jsonArrayHot.length(); i++) {
                        JSONObject myJSON = jsonArrayHot.getJSONObject( i );

                        Peoplemeetllistdata items = new Peoplemeetllistdata();
                        items.Name = myJSON.getString( "Name" );

                        items.Description = myJSON.getString( "Description" );
                        items.Value = myJSON.getString( "Value" );
                        items.OtherValue = myJSON.getString( "OtherValue" );
                        items.ShortDesc = myJSON.getString( "ShortDesc" );
                        peoplemeetItemlist.add( items );

                        Log.i( "peoplemeetItemlist:=>", peoplemeetItemlist.toString() );

                    }
                    System.out.println( "peoplemeetItemlist:=>" + peoplemeetItemlist.toString() );

                *//*

                    Log.i( "status:=>", "1" );

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Throwable t) {
                Log.i( "FailureError:", "," + t.getMessage() );
            }
        } );

    }
*/
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            System.out.println("locationAddress:=>" + locationAddress);
            ed_newuservenue.setText(locationAddress);
        }
    }

    // edited by Pooja Patil at 12-06-17
    //start Pooja
    private class CallSaveNewUser extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_NEWAGENT_SAVE);

            request.addProperty("ROLE", params[0]);
            request.addProperty("AGENTNAME", params[1]);
            request.addProperty("CONTACTPERSON", params[2]);
            request.addProperty("MEETINGATTENDEDBY", params[3]);
            request.addProperty("DATEOFMEETING", params[4]);
            request.addProperty("TIME", params[5]);
            request.addProperty("VENUE", params[6]);
            request.addProperty("GPSADDRESS", params[7]);
            request.addProperty("CONTACTNUMBER", params[8]);
            request.addProperty("EMAILID", params[9]);
            request.addProperty("MINUTESOFMEETING", params[10]);
            request.addProperty("MEETINGOUTCOME", params[11]);
            request.addProperty("ISACTIVE", params[12]);
            request.addProperty("CREATEDBY", params[13]);
            request.addProperty("CREATEDDATE", params[14]);
            request.addProperty("UPDATEDBY", params[15]);
            request.addProperty("UPDATEDDATE", params[16]);
            request.addProperty("TRAINEECODE", params[17]);
            request.addProperty("SSE_ID", params[18]);
            request.addProperty("latitude", params[19]);
            request.addProperty("longitude", params[20]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_NEWAGENT_SAVE, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {
                SoapPrimitive result = null;
                try {
                    // getting response
                    result = (SoapPrimitive) envelope.getResponse();
                    System.out.println("Response SaveNewUser:=>" + result.toString()); // response =0
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }

            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("")) {
                Constants.callIntent(AddEtrackerNewuserActivity.this, EtrackerNewAgentListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            } else {
                JSONObject jsonobject = null;
                try {
                    jsonobject = new JSONObject(s.toString());

                    Log.i("JSON:=>", jsonobject.toString());
                    System.out.println("NewAgentSaveresponse:=>" + jsonobject.toString());

                    Constants.callIntent(AddEtrackerNewuserActivity.this, EtrackerNewAgentListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
