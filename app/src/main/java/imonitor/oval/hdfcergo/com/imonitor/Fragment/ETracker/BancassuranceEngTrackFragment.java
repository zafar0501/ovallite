package imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.AddBankAssuranceActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.EtrackerExistingAgentAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackerexistagentListData;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static android.content.Context.MODE_PRIVATE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getExistingUser.METHOD_GET_EX_USER;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getExistingUser.SOAP_ACTION_GET_EX_USER;

/**
 * Created by Zafar.Hussain on 19/07/2017.
 */

public class BancassuranceEngTrackFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";
    private static final String TAG = "BancassuranceEngTrackFragment";
    SharedPreferences sharedPreferences;
    LinearLayoutManager mLayoutManager;
    private Context context;
    private ConsUrl consUrl;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String ssid, Loginname, Loginntid;
    private RecyclerView mRecyclerView;
    private ProgressDialog progressDialog;
    private ArrayList<String> dataList;
    private ArrayList<EtrackerexistagentListData> ExistAgentItemlist;
    private ArrayList<EtrackerexistagentListData> ExistAgentArrayList;
    private EtrackerExistingAgentAdapter existagentadapter;
    TextView txtNoData;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bancassurance_layout, container, false);
        context = getActivity();
        consUrl = new ConsUrl();

        sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        getSharedpreferebnceData();
        findViews(view);

        //to add new bank details
        addFabButton(view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_banca);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        //get Existing Agent List from service
                        WebserviceCall();
                    }
                }
        );
    }

    private void WebserviceCall() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(getActivity())) {
            try {
               // String parameter = ssid + "_" + Loginname;
                String parameter = ssid + "_" +Loginname+ "_500";
               // String parameter = "1637" + "_" +"ankur boharey"+ "_500";
                //String parameter = "1637" + "_" +"ankur boharey";
                System.out.println(TAG + "parameter:=>" + parameter);

                // edited by Pooja Patil at 19-07-17
                new GetExistingAgentList().execute(parameter);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Constants.snackbar(reliu, Constants.offline_msg);

            Snackbar.make(getView(), Constants.offline_msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }


    private void addFabButton(View view) {
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Click action
                Constants.callIntent(context, AddBankAssuranceActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
    }

    private void findViews(View view) {
        dataList = new ArrayList<>();
        ExistAgentItemlist = new ArrayList<>();
        ExistAgentArrayList = new ArrayList<>();
        txtNoData = (TextView) view.findViewById(R.id.tv_no_data);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.banc_recycler);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(context);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loaddata() {
        if (ExistAgentArrayList.isEmpty() || ExistAgentArrayList == null || ExistAgentArrayList.equals("")) {
            System.out.println(TAG + "EmptySize:=>" + ExistAgentArrayList.size());
        } else {
            existagentadapter = new EtrackerExistingAgentAdapter(context, ExistAgentArrayList, mRecyclerView, R.layout.template_existagentuser, dataList);
            mRecyclerView.setAdapter(existagentadapter);
            existagentadapter.notifyDataSetChanged();

        }
    }

    public class GetExistingAgentList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_EX_USER);
            request.addProperty("sseId", params[0]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_EX_USER, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strPEOPLEMET = "", strMEETINGTYPE = "", strVENUE = "", strROLE = "", strVERTICAL = "";
                        String strSUBVERTICAL = "", strDATEOFMEETING = "", strMOM = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");
                                    strPEOPLEMET = soapObject3.getPropertyAsString("PEOPLEMET");
                                    strROLE = soapObject3.getPropertyAsString("ROLE");
                                    strSUBVERTICAL = soapObject3.getPropertyAsString("SUBVERTICAL");
                                    strDATEOFMEETING = soapObject3.getPropertyAsString("DATEOFMEETING");
                                    strVENUE = soapObject3.getPropertyAsString("VENUE");
                                    strMOM = soapObject3.getPropertyAsString("MINUTESOFMEETING");
                                    strMEETINGTYPE = soapObject3.getPropertyAsString("MEETINGTYPE");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (strVERTICAL.equals("B1") ||
                                        strVERTICAL.equals("B2") ||
                                        strVERTICAL.equals("B3") ||
                                        strVERTICAL.equals("B4") ||
                                        strVERTICAL.equals("B5") ||
                                        strVERTICAL.equals("B6") ||
                                        strVERTICAL.equals("B7") ||
                                        strVERTICAL.equals("B8") ||
                                        strVERTICAL.equals("B9") ||
                                        strVERTICAL.equals("CSC") ||
                                        strVERTICAL.equals("RABG")) {


                                    sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();

                                    editor.putString("MEETINGTYPE" + i, strMEETINGTYPE);
                                    editor.putString("VENUE" + i, strVENUE);
                                    editor.putString("PEOPLEMET" + i, strPEOPLEMET);
                                    editor.putString("ROLE" + i, strROLE);
                                    editor.putString("VERTICAL" + i, strVERTICAL);
                                    editor.putString("SUBVERTICAL" + i, strSUBVERTICAL);
                                    editor.putString("DATEOFMEETING" + i, strDATEOFMEETING);
                                    editor.putString("MINUTESOFMEETING" + i, strMOM);
                                    // Log.i("TAG", "i :" + i + " MEETING_CLIENT_NAME :" + strCLIENT_NAME);
                                    editor.commit();

                                    EtrackerexistagentListData items = new EtrackerexistagentListData();
                                    items.MEETINGTYPE = strMEETINGTYPE;
                                    items.PEOPLEMET = strPEOPLEMET;
                                    items.VENUE = strVENUE;
                                    items.ROLE = strROLE;
                                    items.VERTICAL = strVERTICAL;
                                    items.SUBVERTICAL = strSUBVERTICAL;
                                    items.MINUTESOFMEETING = strMOM;
                                    items.DATEOFMEETING = strDATEOFMEETING;


                                    dataList.add(items.PEOPLEMET);
                                    ExistAgentItemlist.add(items);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                // TAG_Soap_Response = "0";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println(TAG + "s:=>" + s);

            if (s.equals("Error")) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show();
            } else {

                if (s.equals("0")) {
                   // Toast.makeText(context, "Invalid response", Toast.LENGTH_SHORT).show();

                    swipeRefreshLayout.setRefreshing(false);
                    // Toast.makeText(mContext, "Data not available", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setVisibility(View.GONE);

                    txtNoData.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");

                } else {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    txtNoData.setVisibility(View.GONE);

                    if (ExistAgentArrayList != null) {
                        ExistAgentArrayList.clear();
                    }

                    if (ExistAgentItemlist.size() != 0)
                        for (EtrackerexistagentListData obj : ExistAgentItemlist)
                            ExistAgentArrayList.add(obj);

                    System.out.println("ExistAgentArrayList:=>" + ExistAgentArrayList.size());
                    System.out.println("dataList:=>" + dataList.size());

                    loaddata();
                }
            }
        }
    }
}
