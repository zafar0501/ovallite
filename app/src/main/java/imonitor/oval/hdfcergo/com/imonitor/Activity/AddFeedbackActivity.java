package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.AddEtrackerExistinguserActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.ETrackerHomeNewActivity;
import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.InsertMobileFeedback.METHOD_INSERT_FEEDBACK;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.InsertMobileFeedback.SOAP_ACTION_INSERT_FFEDBACK;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;

public class AddFeedbackActivity extends AppCompatActivity {
    Spinner spinner_feedback;
    EditText edt_feedback;
    Button btn_feedback;
    String feedbackType, appName, appVersion, createdBy, ssid, remarks,version;
    ConsUrl consUrl;
    Toolbar mToolbar;
    TextView mTitle,txt_info;
    Typeface calibriFont;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_feedback);
        consUrl = new ConsUrl();
        calibriFont = Typeface.createFromAsset(getAssets(), "fonts/calibri.ttf");
        getSharedpreferenceData();
        setAppToolbar();
        attachViews();
        setListeners();
        getAppVersionDetails();

    }

    private void getAppVersionDetails() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version = pInfo.versionName;
        appName = getResources().getString(R.string.app_name);
        Log.v("","appName "+appName);
    }
    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Add Feedback");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }
    private void getSharedpreferenceData() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AddFeedbackActivity.this);
       // appName = pref.getString("APP_NAME", "");
        //appVersion = pref.getString("VERSION", "");
       // createdBy = pref.getString("CREATED_BY", "");
        ssid = pref.getString("loginssid", "");
    }

    private void attachViews() {
        spinner_feedback = (Spinner) findViewById(R.id.spin_feedback);
        edt_feedback = (EditText) findViewById(R.id.edt_feedback);
        btn_feedback = (Button) findViewById(R.id.btn_send_feedback);

        txt_info=(TextView)findViewById(R.id.txt_info);
       // txt_info.setTypeface(calibriFont);

        WebView view = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = view.getSettings();
        webSettings.setDefaultFontSize(13);
        view.setBackgroundColor(Color.parseColor("#FFFFFF"));
        String text;
        text = "<html><body><p align=\"justify\">";
        text += "\n" + "<font color='#0041B4'>\n" +
                getResources().getString(R.string.feedback_info)+"</font>";
        text += "</p></body></html>";

        view.loadData(text, "text/html", "utf-8");


    }

    private void setListeners() {
        ArrayAdapter<CharSequence> adapter_Spin_Search_Policy;
        String[] feedback = {"Select Feedback Type","Feedback", "Query"};

        adapter_Spin_Search_Policy = new ArrayAdapter<CharSequence>(AddFeedbackActivity.this, R.layout.spinner_text, feedback);
       // adapter_Spin_Search_Policy.setDropDownViewResource(R.layout.spinner_text);
        spinner_feedback.setAdapter(adapter_Spin_Search_Policy);

        //spinner onitem click lisner
        spinner_feedback.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                feedbackType = spinner_feedback.getSelectedItem().toString();
                //   Toast.makeText( getApplicationContext(), spinner_feedback.getSelectedItem().toString(), Toast.LENGTH_SHORT ).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkInfo(AddFeedbackActivity.this)) {
                    try {
                        if(spinner_feedback.getSelectedItem().toString().equals("Select Feedback Type")){
                            Toast.makeText(getApplicationContext(),"Please Select Feedback Type",Toast.LENGTH_SHORT).show();
                        }
                        else if(edt_feedback.getText().toString().trim().isEmpty()){
                            Toast.makeText(getApplicationContext(),"Please Enter Remarks",Toast.LENGTH_SHORT).show();
                        }else {
                            new InsertMobileFeedback().execute();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private class InsertMobileFeedback extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            remarks = edt_feedback.getText().toString();
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_INSERT_FEEDBACK);
            request.addProperty("APP_NAME", appName);
            request.addProperty("VERSION", version);
            request.addProperty("TYPE", feedbackType);
            request.addProperty("REMARKS",remarks);
            request.addProperty("CREATED_BY", "");
            request.addProperty("SSE_ID", ssid);

            System.out.println("ParametersAsObject:=>" + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_INSERT_FFEDBACK, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }

            if (soap_error.equals("")) {

                SoapPrimitive result = null;
                try {
                    result = (SoapPrimitive) envelope.getResponse();
                    Log.v("", "insertFeedback:=>" + result.toString());
                    soap_error = result.toString();
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (Exception e) {
                    soap_error = "Error";
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.v("onPostExecute ","result: "+s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();

            } else {
                if (s.equals("1")) {
                    Toast.makeText(getApplicationContext(), "Record Inserted Successfully.", Toast.LENGTH_SHORT).show();
                    Constants.callIntent(AddFeedbackActivity.this,FeedbackActivity.class,R.anim.slide_in_left, R.anim.slide_out_left);
                } else {
                    Toast.makeText(getApplicationContext(), "Data Not Insert Successfully !!!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

       Constants.callIntent(AddFeedbackActivity.this,FeedbackActivity.class,R.anim.slide_in_left, R.anim.slide_out_left);
    }
}
