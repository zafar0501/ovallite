package imonitor.oval.hdfcergo.com.imonitor.Entity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Zafar.Hussain on 26/09/2017.
 */

public class ChannelByZoneDataModel {
    public String VERTICAL;
    public String YEAR;
    public String ZONE;
    public String SEGMENT;
    public String CITY;
    public String EAST;
    public String WEST;
    public String CENTRAL;
    public String NORTH;
    public String SOUTH;
//    private List<CityByMonthModel> citybymonth_details;
//    private List<SegByMonthModel> segbymonth_details;
 //   private List<ZoneByMonthModel> zonebymonth_details;
  /*  public String JAN17;
    public String FEB17;
    public String MAR17;
    public String APR17;
    public String MAY17;
    public String JUN17;
    public String JUL17;
    public String AUG17;
    public String SEP17;
    public String OCT17;
    public String NOV17;
    public String DEC17;*/

    @Override
    public String toString() {
        return "ChannelByZoneDataModel{" +
                "VERTICAL='" + VERTICAL + '\'' +
                ", EAST='" + EAST + '\'' +
                ", WEST='" + WEST + '\'' +
                ", CENTRAL='" + CENTRAL + '\'' +
                ", NORTH='" + NORTH + '\'' +
                ", SOUTH='" + SOUTH + '\'' +
              /*  ", JAN17='" + JAN17 + '\'' +
                ", FEB17='" + FEB17 + '\'' +
                ", MAR17='" + MAR17 + '\'' +
                ", APR17='" + APR17 + '\'' +
                ", MAY17='" + MAY17 + '\'' +
                ", JUN17='" + JUN17 + '\'' +
                ", JUL17='" + JUL17 + '\'' +
                ", AUG17='" + AUG17 + '\'' +
                ", SEP17='" + SEP17 + '\'' +
                ", OCT17='" + OCT17 + '\'' +
                ", NOV17='" + NOV17 + '\'' +
                ", DEC17='" + DEC17 + '\'' +*/
                '}';
    }
}
/*
class CityByMonthModel{
    public String CITY;
    public String YEAR;
}
class ZoneByMonthModel{
    public String YEAR;
    public String ZONE;
}
class SegByMonthModel{
    public String SEGMENT;
    public String YEAR;
}*/
