package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.METHOD_NAME_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.SOAP_ACTION_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getLossRatio.METHOD_NAME_LOSS_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getLossRatio.SOAP_ACTION_LOSS_Type;

public class Loss_Ratio_Home_Activity extends AppCompatActivity implements View.OnClickListener{

    private SharedPreferences sharedPreferences;
    public static final String MY_PREFS_NAME = "HdfcErgo";






    //Filter NAME AND VALUE
    String FLT_Name="";
    String FLT_Value="";

    Dialog dialog;

    ConsUrl con_url;
    PieChart pieChart,pieChart1;
    ProgressDialog progressDialog;
    ImageView btnPrevious;
    String TotalValue="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_loss__ratio__home_);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        con_url = new ConsUrl();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplication(),MainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        pieChart = (PieChart) findViewById(R.id.chart);
        pieChart1 = (PieChart) findViewById(R.id.chart1);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);
        check_internet_connection();

    }


    private void check_internet_connection() {
        ConnectivityManager cManager=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(),"Please Check Internet Connection",Toast.LENGTH_SHORT).show();
        } else {
            new Gwp_Service().execute(SOAP_ACTION_LOSS_Type,METHOD_NAME_LOSS_Type,"200177035877","");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        menu.getItem(5).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.action_toolbar_1:
                Toast.makeText( this, item.getTitle(), Toast.LENGTH_SHORT ).show();
                FLT_Name = "";
                FLT_Value = "";
                new Gwp_Filter().execute( SOAP_ACTION_GWP_FLT, METHOD_NAME_GWP_FLT, "Agents", "POL_AGENT_CODE" );

            default:
                return super.onOptionsItemSelected( item );
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(),MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_previous:
                Constants.callIntent(Loss_Ratio_Home_Activity.this, Inward_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;

            case R.id.btn_next:
                Constants.callIntent(Loss_Ratio_Home_Activity.this, Claims_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }

    private class Gwp_Service extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Loss_Ratio_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("agentCode",params[2] );
            request.addProperty("sseId",sharedPreferences.getString("NTID", "0") );
            request.addProperty("freezeMonth","" );
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error="";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e)
            {
                e.printStackTrace();
            }



            // Check Network Exception

            if(soap_error.equals(""))
            {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error=soapFault.toString();
                    soapFault.printStackTrace();
                }
                try  {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);


                    for (int i=0;i<soapObject2.getPropertyCount();i++)
                    {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        int x=0;
                        if (pi.getName().equals( "LossRatio" ))
                        {

                            Object property1 = propertyTest.getProperty("NetIncurred");
                            Object property2 = propertyTest.getProperty("NEP");

                            TotalValue=TotalValue+property1.toString()+","+property2.toString()+",";



                        }
                        x++;
                    }

                }catch (Exception  e)
                {
                    e.printStackTrace();
                }
                soap_error=TotalValue;
            }else {

                soap_error="Error";
            }




            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            if(progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (s.equals("Error"))
            {
                Toast.makeText(Loss_Ratio_Home_Activity.this,"Error in Response",Toast.LENGTH_SHORT ).show();


            }else
            {
                Log.i( "TAG",s );

                try
                {
                    String value[]=s.split( "," );
                    OpenBarchart(value[0],value[1]);
                    OpenBarchart1(value[2],value[3]);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }





    }
    }

    private void OpenBarchart(String s1, String s2) {

        pieChart = (PieChart) findViewById( R.id.chart );

        ArrayList<Entry> entries = new ArrayList<>();
        entries.add( new Entry( (int)Float.parseFloat(s1), 0 ) );
        entries.add( new Entry( (int)Float.parseFloat(s2), 1 ) );


        PieDataSet dataset = new PieDataSet( entries, "# of Calls" );

        ArrayList<String> labels = new ArrayList<String>();
        labels.add( "NetIncurred" );
        labels.add( "NEP" );


        PieData data = new PieData( labels, dataset );
        dataset.setColors(ColorTemplate.COLORFUL_COLORS); //


        try
        {
            pieChart1.setDescription( "" );
            pieChart.setData(data);
        }catch (Exception e )
        {

        }
        pieChart.animateY( 2000 );
        pieChart.setOnChartValueSelectedListener( new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {


            }

            @Override
            public void onNothingSelected() {

            }
        } );
    }

    private void OpenBarchart1(String s1, String s2) {
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add( new Entry( (int)Float.parseFloat(s1), 0 ) );
        entries.add( new Entry( (int)Float.parseFloat(s2), 1 ) );


        PieDataSet dataset = new PieDataSet(entries, "# of Calls");

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("NetIncurred");
        labels.add("NEP");

        PieData data = new PieData(labels, dataset);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS); //

        try
        {
            pieChart1.setDescription( "" );
            pieChart1.setData(data);
        }catch (Exception e )
        {

        }

        pieChart1.animateY(2000);
        pieChart1.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private class Gwp_Filter extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Loss_Ratio_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("sseId",sharedPreferences.getString("NTID", "0") );
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error="";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error=e.toString();
            } catch (XmlPullParserException e)
            {
                soap_error=e.toString();
                e.printStackTrace();
            }



            if(soap_error.equals(""))
            {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error=soapFault.toString();
                    soapFault.printStackTrace();
                    soap_error=soapFault.toString();
                }

                try  {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);


                    for (int i=0;i<soapObject2.getPropertyCount();i++)
                    {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals(params[2]))
                        {

                            Object property1 = propertyTest.getProperty("Name");
                            FLT_Name=FLT_Name+property1.toString()+",";
                            if(params[2].equals("Agents"))
                            {
                                Object property2 = propertyTest.getProperty("Value");
                                FLT_Value=FLT_Value+property2.toString()+",";
                            }else {

                                if(params[2].equals("Reportiees"))
                                {
                                    Object property2 = propertyTest.getProperty("Value");
                                    FLT_Value=FLT_Value+property2.toString()+",";
                                }else {
                                    FLT_Value=FLT_Name+property1.toString()+",";

                                }

                            }




                        }
                    }

                }catch (Exception  e)
                {
                    e.printStackTrace();
                }
                soap_error=params[2]+":"+params[3];
            }else
            {
                soap_error="Error";
            }




            return soap_error;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            final String Res[]= s.split(":");
            if (progressDialog.isShowing())
            {
                progressDialog.dismiss();
                progressDialog= null;

            }
            if(s.equals( "Error" ))
            {
                Toast.makeText(Loss_Ratio_Home_Activity.this,"Error in Response",Toast.LENGTH_SHORT ).show();
            }else {
                final String names[] =FLT_Name.split(",");
                final String values[] =FLT_Value.split(",");

                // Create custom dialog object
                dialog = new Dialog(Loss_Ratio_Home_Activity.this);
                // Include dialog.xml file
                dialog.setContentView(R.layout.custom);
                // Set dialog title
                dialog.setTitle(Res[0]);
                ListView lv = (ListView) dialog.findViewById(R.id.listView);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Loss_Ratio_Home_Activity.this,android.R.layout.simple_list_item_1,names);
                lv.setAdapter(adapter);
                lv.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        TotalValue="";
                        new Gwp_Service().execute(SOAP_ACTION_LOSS_Type,METHOD_NAME_LOSS_Type,values[position],"");
                        dialog.dismiss();

                    }
                } );
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = 500;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.RIGHT;

                dialog.getWindow().setAttributes(lp);
                dialog.show();
            }
        }
    }
}
