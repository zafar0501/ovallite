package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.Entity.NotificationListData;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 11/09/2017.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat sdf4 = new SimpleDateFormat("hh:mm aaa");
    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
    private Context mContext;
    private ArrayList<NotificationListData> mList;
    private ArrayList<String> letterArraylist;
    private String letter, dateString, newTime;
    private Date createdDate, dateFormat;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    private Typeface calibriFont;

    public NotificationListAdapter(Context mContext, ArrayList<NotificationListData> mList, ArrayList<String> letterArraylist) {
        this.mContext = mContext;
        this.mList = mList;
        this.letterArraylist = letterArraylist;
        calibriFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/calibri.ttf");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        try {
            createdDate = sdf3.parse(mList.get(position).CREATED_DATE);
            //  Log.v("", "createdDate 11 : " + createdDate);
            dateString = sdf2.format(createdDate);

            //  tv_meetingdate.setText(dateString);
            // Log.v("", "dateString: " + dateString);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        letter = String.valueOf(letterArraylist.get(position).toUpperCase().charAt(0));
        TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());
        holder.letter.setImageDrawable(drawable);
        String input = mList.get(position).MESSAGE;
        String output = input.substring(0, 1).toUpperCase() + input.substring(1);
        holder.txt_userName.setText(mList.get(position).USERNAME.toUpperCase());
       // holder.txt_message.setText(mList.get(position).MESSAGE);
        holder.txt_message.setText(output);
        holder.txt_date.setText(dateString);
        holder.txt_view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setMsgPopUpView(position);
            }
        });
    }

    private void setMsgPopUpView(int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View messagePopUpView = inflater.inflate(R.layout.popup_message_layout, null, false);
        messagePopUpView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        final Dialog messageDialog = new Dialog(mContext);

        TextView tv_title, tv_time, tv_date, tv_message, btn_exit;


        tv_title = (TextView) messagePopUpView.findViewById(R.id.txt_title);
        tv_time = (TextView) messagePopUpView.findViewById(R.id.txt_time);
        tv_date = (TextView) messagePopUpView.findViewById(R.id.txt_date);
        tv_message = (TextView) messagePopUpView.findViewById(R.id.txt_message);
        btn_exit = (TextView) messagePopUpView.findViewById(R.id.btn_exit);

        tv_title.setTypeface(calibriFont);
        tv_time.setTypeface(calibriFont);
        tv_date.setTypeface(calibriFont);
        btn_exit.setTypeface(calibriFont);
        //tv_message.setTypeface(calibriFont);
        try {
            createdDate = sdf3.parse(mList.get(position).CREATED_DATE);
            Log.v("", "createdDate 11 : " + createdDate);


            dateString = sdf2.format(createdDate);

            //  tv_meetingdate.setText(dateString);
            Log.v("", "dateString: " + dateString);

            //  sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
            dateFormat = sdf1.parse(mList.get(position).CREATED_DATE);
            Log.v("", "createdDate 22 : " + dateFormat);

            newTime = sdf4.format(dateFormat);
            Log.v("", "createdTime : " + newTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        tv_title.setText("Oval Lite");
        tv_time.setText(newTime);
        tv_date.setText(dateString);
        tv_message.setText(mList.get(position).MESSAGE);

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        messageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(messagePopUpView);

      /*  WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(connectiondialogue.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;*/
        //connectiondialogue.getWindow().setAttributes(lp);
        // connectiondialogue.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // connectiondialogue.getWindow().setGravity(Gravity.CENTER);
        messageDialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
        messageDialog.getWindow().setGravity(Gravity.CENTER);
        messageDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        messageDialog.setCancelable(true);
        messageDialog.show();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_userName, txt_message, txt_date, txt_view;
        ImageView letter;


        public ViewHolder(View itemView) {
            super(itemView);

            txt_userName = (TextView) itemView.findViewById(R.id.txt_userName);
            txt_message = (TextView) itemView.findViewById(R.id.txt_message);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_view = (TextView) itemView.findViewById(R.id.view);
            letter = (ImageView) itemView.findViewById(R.id.gmailitem_letter);


            txt_userName.setTypeface(calibriFont);
            txt_message.setTypeface(calibriFont);
            txt_date.setTypeface(calibriFont);
            txt_view.setTypeface(calibriFont);
        }
    }
}
