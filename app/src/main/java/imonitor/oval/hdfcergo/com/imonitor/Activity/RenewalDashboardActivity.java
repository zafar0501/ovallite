package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByMonType1Adaper;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByZoneType1Adapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByZoneType2Adapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByZoneType3Adapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByZoneType4Adapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByZoneType5Adapter;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ChByZoneType6Adapter;
import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Entity.ChannelByZoneDataModel;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.MultiSpinner;


import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetRenewalFilteredData.METHOD_GETRENEWFILDATA;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetRenewalFilteredData.SOAP_ACTION_GETRENEWFILDATA;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetRenewalFilteredSelection.METHOD_GET_RENEWFILTER;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetRenewalFilteredSelection.SOAP_ACTION_GET_RENEWFILTER;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;

public class RenewalDashboardActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";
    private static final int FROM_DATE_DIALOG_ID = 1;
    private static final int TO_DATE_DIALOG_ID = 2;
    private static String TAG = "RenewalDashboard";
    List<String> list;
    private ArrayList<String> list1, mVerticalList, mSegmentList,mZoneList,mGeoVerList,mCityList,mPolicyAgeList,mVehicleAgeList;
    private ArrayList<String> mMakeFinalList,mMakeModelList,mRenewalMonthList,mOveridingAgentList,mOveridingAgentCodeList;
    private ArrayList<String> mFuelTypeList,mSseNameList;
    String strSpinFinal;
    Menu mMenu;
    ImageView btn_filter;
    private boolean menuShow = false;
    private ConsUrl consUrl;
    private String chartType, typeOfDisplay;
    private SharedPreferences sharedPreferences, getFromMonthPref, getToMonthPref;
   // private RackMonthPicker fromMonthDialog, toMonthDialog;
    private SharedPreferences.Editor editor, editor1;
    private Toolbar mToolbar;
    private TextView mToolbarTitle, txtFromDate, txtToDate, txtChannelByZone, txtChannelByMonth, btnGetResult;
    private TextView txtZoneByMonth, txtSegByMonth, txtCityByMonth;
    private TextView lbl_type1, lbl_zone_type2, lbl_zone_type3, lbl_zone_type4, lbl_zone_type5, lbl_zone_type6;
    private TextView txtNoData;
    private LinearLayout ll_no_data, renewal_filter_layout;
    private RecyclerView mChByZoneRecycler, mChByMonRecycler, mZoneByMonRecycler, mSegByMonRecycler, mCityByMonRecycler;
    private RecyclerView mRenewRecycler, mAchievedNOPRecycler, mBaseNOPRecycler, mDateRenewRecycler, mBaseOnDateRecycler, mAchievedAsDateRecycler;
    private LinearLayoutManager mLayoutManager;
    private ArrayAdapter singleSelSpinnerAdapter;
    private Spinner singleSelcSpinner;
    private ChByZoneType1Adapter mChByZoneAdapter;
    private ChByMonType1Adaper mChByMonType1Adaper;
    private ChByZoneType2Adapter mChByZoneType2Adapter;
    private ChByZoneType3Adapter mChByZoneType3Adapter;
    private ChByZoneType4Adapter mChByZoneType4Adapter;
    private ChByZoneType5Adapter mChByZoneType5Adapter;
    private ChByZoneType6Adapter mChByZoneType6Adapter;
    private ArrayList mListItems;
    private List<LinkedHashMap> mChZoneType1List, mChByMonthType1List, mZoneByMonthList, mSegByMonthList, mCityByMonthList;
    private List<LinkedHashMap> mType2List, mType3List, mType4List, mType5List, mType6List;
    private ArrayList<ChannelByZoneDataModel> NewArrayList, mChZoneType1ArrList, mChByMonthType1ArrList, mZoneByMonthArrList, mSegByMonthArrList, mCityByMonthArrList;
    private ArrayList<ChannelByZoneDataModel> mType2ArrList, mType3ArrList, mType4ArrList, mType5ArrList, mType6ArrList;
    private LinkedHashMap headers;
    private LinearLayout ll_chByzone, ll_zone_type2, ll_zone_type3, ll_zone_type4, ll_zone_type5, ll_zone_type6;
    private MultiSpinner spinner, sp_renewal_filter;
    private ArrayAdapter<String> multiSpinnerAdapter, multiFilterSpAdapter;
    private TableLayout tbChByZone;
    private HorizontalScrollView hl_ch_by_zone;
    private ScrollView parent_scv;
    private TableRow tr;
    private String[] header = {"VERTICAL", "EAST", "WEST", "CENTRAL", "NORTH", "SOUTH"};
    private String[] vertical = {"AGENCY1", "BANCASSURANCEB6", "DIRECTD1", "GRAND TOTAL"};
    private String[] east = {"182%", "-", "42%", "70%"};
    private String[] west = {"8%", "-", "32%", "25%"};
    private String[] central = {"-", "-", "-", "-"};
    private String[] north = {"18%", "-", "39%", "33%"};
    private String[] south = {"18%", "-", "31%", "33%"};
    private String[] row1 = {"AGENCY1", "182%", "8%", "-", "18%", "18%"};
    private String[] row2 = {"BANCASSURANCEB6", "-", "-", "-", "-", "-"};
    private String[] row3 = {"DIRECTD1", "42%", "32%", "-", "39%", "31%"};
    private String[] row4 = {"GRAND TOTAL", "70%", "25%", "-", "33%", "33%"};
    private int fromMonth, toMonth;
    private ProgressDialog progressDialog = null;
    private String sseid;

    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            // Do something here with the selected items
            strSpinFinal = "";
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(multiSpinnerAdapter.getItem(i)).append(" ");
                    if (multiSpinnerAdapter.getItem(i).equals("Total Renewed (%)")) {
                        typeOfDisplay = "1";
                    } else if (multiSpinnerAdapter.getItem(i).equals("Total Achieved NOP")) {
                        typeOfDisplay = "2";
                    } else if (multiSpinnerAdapter.getItem(i).equals("Total Base NOP")) {
                        typeOfDisplay = "3";
                    } else if (multiSpinnerAdapter.getItem(i).equals("As on Date Renewed (%)")) {
                        typeOfDisplay = "4";
                    } else if (multiSpinnerAdapter.getItem(i).equals("Base as on date")) {
                        typeOfDisplay = "5";
                    } else if (multiSpinnerAdapter.getItem(i).equals("Achieved as on date")) {
                        typeOfDisplay = "6";
                    }
                    strSpinFinal += typeOfDisplay + ",";
                    Log.v("onSelectedListener", "strFinal: " + strSpinFinal);
                }
            }
            // Log.v("", "SpinnerItemsSelected: " + spinner.getText().toString());
            callWebServices(strSpinFinal);
        }
    };
    private MultiSpinner.MultiSpinnerListener onSelectedRenwlFilterListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            // Do something here with the selected items
            strSpinFinal = "";
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(multiFilterSpAdapter.getItem(i)).append(" ");
                }
            }
        }
    };

    private void callWebServices(String strSpinFinal) {
        if (!strSpinFinal.equals("")) {
            List<String> spinValueList = new ArrayList<String>(Arrays.asList(strSpinFinal.split(",")));
            Log.v("", "spinValueList " + spinValueList);
            for (int i = 0; i < spinValueList.size(); i++) {
                if (typeOfDisplay.equals(spinValueList.get(0))) {
                    Log.v("get_result:11 ", "typeOfDisplay: " + typeOfDisplay);
                    callServiceForSingleType(typeOfDisplay, chartType);
                } else {
                    callServiceForMultipleType(spinValueList.get(i), chartType);
                    Log.v("get_result:22 ", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay + " strSpinFinal: " + strSpinFinal);
                }
            }

        } else {

            Log.v("", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay);
            boolean[] selectedItems = new boolean[multiSpinnerAdapter.getCount()];
            selectedItems[0] = true; // select second item
            spinner.setSelected(selectedItems);
            typeOfDisplay = "1";

            ll_zone_type2.setVisibility(View.GONE);
            ll_zone_type3.setVisibility(View.GONE);
            ll_zone_type4.setVisibility(View.GONE);
            ll_zone_type5.setVisibility(View.GONE);
            ll_zone_type6.setVisibility(View.GONE);

            callServiceForSingleType(typeOfDisplay, chartType);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_renewal_dashboard);

        consUrl = new ConsUrl();
        setAppToolbar();
        initializeViews();
        setListeners();

        setDefaultValues();
        setFromMonSharedPrefVals();
        setToMonSharedPrefVals();


    }

    private void setToMonthDialog() {
        Log.v("", "setToMonthDialog");

    }

    private void setToMonSharedPrefVals() {
        String newDate = txtToDate.getText().toString();
        Log.v("setToMonSharedPrefVals ", " newDate " + newDate);
        String[] val = newDate.split("-");
        String newYear = val[0];
        String newMonth = val[1];
        String finalMon = "";
        //  String finalMonth=newMonth.substring(1);
        int lengthMonth = String.valueOf(newMonth).length();
        Log.v("setSharedPrefVals ", " lengthMonth " + lengthMonth);
        if (lengthMonth == 1) {
            finalMon = "0" + newMonth;
        } else {
            if ((newMonth.equals("01")
                    || newMonth.equals("02")
                    || newMonth.equals("03")
                    || newMonth.equals("04")
                    || newMonth.equals("05")
                    || newMonth.equals("06")
                    || newMonth.equals("07")
                    || newMonth.equals("08")
                    || newMonth.equals("09"))) {
                finalMon = newMonth.substring(1);
            } else {
                finalMon = newMonth;
            }
        }
        Log.v("setToMonSharedPrefVals ", " finalMon " + finalMon);
        getToMonthPref = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        editor1 = getToMonthPref.edit();
        // editor1.clear();
        editor1.putString("toMonth", String.valueOf(finalMon));
        editor1.putString("toYear", String.valueOf(newYear));
        editor1.putString("to_id", String.valueOf(TO_DATE_DIALOG_ID));
        editor1.commit();
    }

    private void setFromMonSharedPrefVals() {
        String newDate = txtFromDate.getText().toString();
        Log.v("setFmMonSharedPrefVals ", " newDate " + newDate);
        String[] val = newDate.split("-");
        String newYear = val[0];
        String newMonth = val[1];
        String finalMon = "";
        //  String finalMonth=newMonth.substring(1);
        int lengthMonth = String.valueOf(newMonth).length();
        Log.v("setSharedPrefVals ", " lengthMonth " + lengthMonth);
        if (lengthMonth == 1) {
            // finalMon = "0" + newMonth;
        } else {
            if ((newMonth.equals("01")
                    || newMonth.equals("02")
                    || newMonth.equals("03")
                    || newMonth.equals("04")
                    || newMonth.equals("05")
                    || newMonth.equals("06")
                    || newMonth.equals("07")
                    || newMonth.equals("08")
                    || newMonth.equals("09"))) {
                finalMon = newMonth.substring(1);
            } else {
                finalMon = newMonth;
            }
        }
        Log.v("setFmMonSharedPrefVals ", " finalMon " + finalMon);

        getFromMonthPref = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        editor = getFromMonthPref.edit();
        //   editor.clear();
        editor.putString("fromMonth", String.valueOf(finalMon));
        editor.putString("fromYear", String.valueOf(newYear));
        editor.putString("from_id", String.valueOf(FROM_DATE_DIALOG_ID));
        editor.commit();
    }

    private void setDefaultValues() {


        txtFromDate.setText("2017-09");
        txtToDate.setText("2017-10");

        chartType = "1";
        typeOfDisplay = "1";
        strSpinFinal = "1,";

        boolean[] selectedItems = new boolean[multiSpinnerAdapter.getCount()];
        selectedItems[0] = true; // select second item
        spinner.setSelected(selectedItems);

        //chartType=1=Channel By Zone
        //typeOfDiaplay=1=Total Renewed %

        callWebServiceByZone(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), typeOfDisplay);

        txtChannelByZone.setBackgroundDrawable(getResources().getDrawable(R.drawable.renewal_filter_btn_bg));
        txtChannelByZone.setTextColor(getResources().getColor(R.color.White));

        txtCityByMonth.setBackgroundDrawable(null);
        txtCityByMonth.setTextColor(getResources().getColor(R.color.Black));
        txtChannelByMonth.setBackgroundDrawable(null);
        txtChannelByMonth.setTextColor(getResources().getColor(R.color.Black));
        txtZoneByMonth.setBackgroundDrawable(null);
        txtZoneByMonth.setTextColor(getResources().getColor(R.color.Black));
        txtSegByMonth.setBackgroundDrawable(null);
        txtSegByMonth.setTextColor(getResources().getColor(R.color.Black));

    }

    private void setChannelByZoneRecyclerView() {
        mChByZoneRecycler.setHasFixedSize(true);
        mChByZoneRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mChByZoneRecycler.setLayoutManager(mLayoutManager);

        mChByZoneAdapter = new ChByZoneType1Adapter(RenewalDashboardActivity.this, mChZoneType1ArrList, "zonewise", headers, mChZoneType1List);
        mChByZoneRecycler.setAdapter(mChByZoneAdapter);
        mChByZoneAdapter.notifyDataSetChanged();

    }

    private void setListeners() {

        ViewCompat.setNestedScrollingEnabled(parent_scv, true);

        txtFromDate.setOnClickListener(this);
        txtToDate.setOnClickListener(this);
        txtChannelByZone.setOnClickListener(this);
        txtChannelByMonth.setOnClickListener(this);
        txtZoneByMonth.setOnClickListener(this);
        txtSegByMonth.setOnClickListener(this);
        txtCityByMonth.setOnClickListener(this);

        btnGetResult.setOnClickListener(this);
        btn_filter.setOnClickListener(this);
    }

    private void initializeViews() {
        headers = new LinkedHashMap<String, String>();

        NewArrayList = new ArrayList<>();

        mChZoneType1List = new ArrayList<>();
        mChZoneType1ArrList = new ArrayList<>();

        mChByMonthType1List = new ArrayList<>();
        mChByMonthType1ArrList = new ArrayList<>();

        mZoneByMonthList = new ArrayList<>();
        mZoneByMonthArrList = new ArrayList<>();

        mSegByMonthList = new ArrayList<>();
        mSegByMonthArrList = new ArrayList<>();

        mCityByMonthList = new ArrayList<>();
        mCityByMonthArrList = new ArrayList<>();

        mVerticalList = new ArrayList<>();
        mSegmentList = new ArrayList<>();
        mZoneList = new ArrayList<>();
        mGeoVerList = new ArrayList<>();
        mCityList = new ArrayList<>();
        mPolicyAgeList = new ArrayList<>();
        mVehicleAgeList = new ArrayList<>();
        mMakeFinalList = new ArrayList<>();
        mMakeModelList = new ArrayList<>();
        mRenewalMonthList = new ArrayList<>();
        mOveridingAgentList = new ArrayList<>();
        mOveridingAgentCodeList = new ArrayList<>();
        mFuelTypeList = new ArrayList<>();
        mSseNameList = new ArrayList<>();

       /* mType2List = new ArrayList<>();
        mType2ArrList = new ArrayList<>();*/
       /* mType3List = new ArrayList<>();
        mType3ArrList = new ArrayList<>();
        mType4ArrList = new ArrayList<>();
        mType4List = new ArrayList<>();
        mType5ArrList = new ArrayList<>();
        mType5List = new ArrayList<>();
        mType6ArrList = new ArrayList<>();
        mType6List = new ArrayList<>();*/

        txtNoData = (TextView) findViewById(R.id.tv_no_data);
        ll_no_data = (LinearLayout) findViewById(R.id.ll_no_data);

        renewal_filter_layout = (LinearLayout) findViewById(R.id.renewal_filter_layout);
        singleSelcSpinner = (Spinner) findViewById(R.id.main_selection_spinner);

        list1 = new ArrayList<>();
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        sseid = sharedPreferences.getString("NTID", "0");
        Log.v("sharedPreferences ", "sseid " + sseid);

        //layouts where recyclerview & lables are added
        ll_chByzone = (LinearLayout) findViewById(R.id.ll_chByzone);
        ll_zone_type2 = (LinearLayout) findViewById(R.id.ll_zone_type2);
        ll_zone_type3 = (LinearLayout) findViewById(R.id.ll_zone_type3);
        ll_zone_type4 = (LinearLayout) findViewById(R.id.ll_zone_type4);
        ll_zone_type5 = (LinearLayout) findViewById(R.id.ll_zone_type5);
        ll_zone_type6 = (LinearLayout) findViewById(R.id.ll_zone_type6);

        //labels for each o/p, for recyclerview
        lbl_type1 = (TextView) findViewById(R.id.lbl_type1);
        lbl_zone_type2 = (TextView) findViewById(R.id.lbl_zone_type2);
        lbl_zone_type3 = (TextView) findViewById(R.id.lbl_zone_type3);
        lbl_zone_type4 = (TextView) findViewById(R.id.lbl_zone_type4);
        lbl_zone_type5 = (TextView) findViewById(R.id.lbl_zone_type5);
        lbl_zone_type6 = (TextView) findViewById(R.id.lbl_zone_type6);

        btn_filter=(ImageView)findViewById(R.id.btn_filter);
        tbChByZone = (TableLayout) findViewById(R.id.tb_ch_by_zone);
        //  tbChByZone.setColumnStretchable(6, true);
        tbChByZone.setStretchAllColumns(true);
        tbChByZone.setShrinkAllColumns(true);

        parent_scv = (ScrollView) findViewById(R.id.parent_scv);

        //By ZOne  Type 1 recyclerview
        mChByZoneRecycler = (RecyclerView) findViewById(R.id.renew_recyclerViewZone);
        //By month type1 recyclerview
        mChByMonRecycler = (RecyclerView) findViewById(R.id.renew_recyclerViewMon);
        //Zone By Month type1 recyclerview
        mZoneByMonRecycler = (RecyclerView) findViewById(R.id.zone_by_mon_recycler);
        //Segment By Month type1 recyclerview
        mSegByMonRecycler = (RecyclerView) findViewById(R.id.seg_by_mon_recycler);
        //City By Month type1 recyclerview
        mCityByMonRecycler = (RecyclerView) findViewById(R.id.city_by_mon_recycler);
        //type 2 recyclerview
        mAchievedNOPRecycler = (RecyclerView) findViewById(R.id.achievedNOP_recyclerView);
        // Type 3 recyclerview
        mBaseNOPRecycler = (RecyclerView) findViewById(R.id.baseNOP_recyclerView);
        //type 4 recyclerview
        mDateRenewRecycler = (RecyclerView) findViewById(R.id.dateRenew_recyclerView);
        //type 5 recyclerview
        mBaseOnDateRecycler = (RecyclerView) findViewById(R.id.baseOnDate_recyclerView);
        //type 6 recyclerview
        mAchievedAsDateRecycler = (RecyclerView) findViewById(R.id.achievedAsDate_recyclerView);

        txtFromDate = (TextView) findViewById(R.id.txt_from_date);
        txtToDate = (TextView) findViewById(R.id.txt_to_date);
        btnGetResult = (TextView) findViewById(R.id.btn_get_result);

        //For selecting single option chart Type
        txtChannelByZone = (TextView) findViewById(R.id.txt_channel_by_zone);
        txtChannelByMonth = (TextView) findViewById(R.id.txt_channel_by_month);
        txtZoneByMonth = (TextView) findViewById(R.id.txt_zone_by_month);
        txtSegByMonth = (TextView) findViewById(R.id.txt_seg_by_month);
        txtCityByMonth = (TextView) findViewById(R.id.txt_city_by_month);

        //For selecting multiple option typeofdisplay
        multiSpinnerAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text);
        multiSpinnerAdapter.setDropDownViewResource(R.layout.spinner_text);
        // multiSpinnerAdapter.add("Select values from below");
        multiSpinnerAdapter.add("Total Renewed (%)");
        multiSpinnerAdapter.add("Total Achieved NOP");
        multiSpinnerAdapter.add("Total Base NOP");
        multiSpinnerAdapter.add("As on Date Renewed (%)");
        multiSpinnerAdapter.add("Base as on date");
        multiSpinnerAdapter.add("Achieved as on date");

        spinner = (MultiSpinner) findViewById(R.id.sp_multi_selections);
        spinner.setAdapter(multiSpinnerAdapter, false, onSelectedListener);


        sp_renewal_filter = (MultiSpinner) findViewById(R.id.sp_renewal_filter);


    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mToolbarTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Renewal Dashboard");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void SetActionBarTitle(String title) {
        mToolbarTitle.setText(title);
        mToolbarTitle.setTextColor(Color.WHITE);

    }

    public void setFromMonthDialog() {
        Log.v("", "setFromMonthDialog");

    }

    public String stringToDate(String startDateString) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-mm");
        Date startDate;
        String newDateString = "";
        try {
            startDate = df.parse(startDateString);
            newDateString = df.format(startDate);
            System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDateString;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.callIntent(RenewalDashboardActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_filter:
                List<String> filterList = new ArrayList<String>(Arrays.asList(sp_renewal_filter.getText().toString().split(",")));
                Log.v("", "filterList " + filterList);

                /*if(sp_renewal_filter.getText().toString().equals("")){

                }*/
                break;

            case R.id.txt_from_date:

                /*new RackMonthPicker(RenewalDashboardActivity.this)*/
                //  setFromMonthDialog();
                openFromDateDialog();
                break;
            case R.id.txt_to_date:
                // setToMonthDialog();
                openToDateDialog();
                break;

            case R.id.btn_get_result:
                Log.v("", "fromMonth " + fromMonth + " toMonth " + toMonth);
                //if (fromMonth > toMonth) {
                //    Toast.makeText(getApplicationContext(), "Please Select Proper Date", Toast.LENGTH_SHORT).show();
                //} else {
                if (!strSpinFinal.equals("")) {
                    List<String> spinValueList = new ArrayList<String>(Arrays.asList(strSpinFinal.split(",")));
                    Log.v("", "spinValueList " + spinValueList);
                    for (int i = 0; i < spinValueList.size(); i++) {
                        if (typeOfDisplay.equals(spinValueList.get(0))) {
                            Log.v("get_result:11 ", "typeOfDisplay: " + typeOfDisplay);
                            callServiceForSingleType(typeOfDisplay, chartType);
                        } else {
                            callServiceForMultipleType(spinValueList.get(i), chartType);
                            Log.v("get_result:22 ", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay + " strSpinFinal: " + strSpinFinal);
                        }
                    }

                } else {
                    // setDefaultValues();
                    Log.v("", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay);
                    boolean[] selectedItems = new boolean[multiSpinnerAdapter.getCount()];
                    selectedItems[0] = true; // select second item
                    spinner.setSelected(selectedItems);
                    typeOfDisplay = "1";

                    ll_zone_type2.setVisibility(View.GONE);
                    ll_zone_type3.setVisibility(View.GONE);
                    ll_zone_type4.setVisibility(View.GONE);
                    ll_zone_type5.setVisibility(View.GONE);
                    ll_zone_type6.setVisibility(View.GONE);

                    callServiceForSingleType(typeOfDisplay, chartType);

                    /*if(chartType.equals("1")){
                        mChByMonRecycler.setVisibility(View.GONE);
                        callServiceForSingleType(typeOfDisplay, chartType);
                    }else if(chartType.equals("2")){
                        mChByZoneRecycler.setVisibility(View.GONE);
                        callServiceForSingleType(typeOfDisplay, chartType);
                    }else if(chartType.equals("3")){
                        mChByZoneRecycler.setVisibility(View.GONE);
                        callServiceForSingleType(typeOfDisplay, chartType);
                    }else if(chartType.equals("4")){
                        mChByZoneRecycler.setVisibility(View.GONE);
                        callServiceForSingleType(typeOfDisplay, chartType);
                    }else if(chartType.equals("5")){
                        mChByZoneRecycler.setVisibility(View.GONE);
                        callServiceForSingleType(typeOfDisplay, chartType);
                    }*/
                    //  setDefaultValues();
                }
                break;
            case R.id.txt_channel_by_zone:
                chartType = "1";
                ll_chByzone.setVisibility(View.GONE);
                ll_zone_type2.setVisibility(View.GONE);
                ll_zone_type3.setVisibility(View.GONE);
                ll_zone_type4.setVisibility(View.GONE);
                ll_zone_type5.setVisibility(View.GONE);
                ll_zone_type6.setVisibility(View.GONE);

                //mChByMonthType1ArrList.clear();
                mChByMonRecycler.setVisibility(View.GONE);
                mChByZoneRecycler.setVisibility(View.GONE);
                mAchievedNOPRecycler.setVisibility(View.GONE);
                mBaseNOPRecycler.setVisibility(View.GONE);
                mDateRenewRecycler.setVisibility(View.GONE);
                mBaseOnDateRecycler.setVisibility(View.GONE);
                mAchievedAsDateRecycler.setVisibility(View.GONE);


                Log.v("txt_channel_by_zone: ", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay);

                txtChannelByZone.setBackgroundDrawable(getResources().getDrawable(R.drawable.renewal_filter_btn_bg));
                txtChannelByZone.setTextColor(getResources().getColor(R.color.White));

                txtCityByMonth.setBackgroundDrawable(null);
                txtCityByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtChannelByMonth.setBackgroundDrawable(null);
                txtChannelByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtZoneByMonth.setBackgroundDrawable(null);
                txtZoneByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtSegByMonth.setBackgroundDrawable(null);
                txtSegByMonth.setTextColor(getResources().getColor(R.color.Black));
                break;
            case R.id.txt_channel_by_month:
                chartType = "2";
                ll_chByzone.setVisibility(View.GONE);
                ll_zone_type2.setVisibility(View.GONE);
                ll_zone_type3.setVisibility(View.GONE);
                ll_zone_type4.setVisibility(View.GONE);
                ll_zone_type5.setVisibility(View.GONE);
                ll_zone_type6.setVisibility(View.GONE);
                lbl_type1.setVisibility(View.GONE);

                mChByMonRecycler.setVisibility(View.GONE);
                mChByZoneRecycler.setVisibility(View.GONE);
                mAchievedNOPRecycler.setVisibility(View.GONE);
                mBaseNOPRecycler.setVisibility(View.GONE);
                mDateRenewRecycler.setVisibility(View.GONE);
                mBaseOnDateRecycler.setVisibility(View.GONE);
                mAchievedAsDateRecycler.setVisibility(View.GONE);

                Log.v("txt_channel_by_zone: ", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay);

                txtChannelByMonth.setBackgroundDrawable(getResources().getDrawable(R.drawable.renewal_filter_btn_bg));
                txtChannelByMonth.setTextColor(getResources().getColor(R.color.White));

                txtChannelByZone.setBackgroundDrawable(null);
                txtChannelByZone.setTextColor(getResources().getColor(R.color.Black));
                txtZoneByMonth.setBackgroundDrawable(null);
                txtZoneByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtSegByMonth.setBackgroundDrawable(null);
                txtSegByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtCityByMonth.setBackgroundDrawable(null);
                txtCityByMonth.setTextColor(getResources().getColor(R.color.Black));

                break;
            case R.id.txt_zone_by_month:
                chartType = "3";

                ll_chByzone.setVisibility(View.GONE);
                ll_zone_type2.setVisibility(View.GONE);
                ll_zone_type3.setVisibility(View.GONE);
                ll_zone_type4.setVisibility(View.GONE);
                ll_zone_type5.setVisibility(View.GONE);
                ll_zone_type6.setVisibility(View.GONE);

                mChByMonRecycler.setVisibility(View.GONE);
                mChByZoneRecycler.setVisibility(View.GONE);
                mAchievedNOPRecycler.setVisibility(View.GONE);
                mBaseNOPRecycler.setVisibility(View.GONE);
                mDateRenewRecycler.setVisibility(View.GONE);
                mBaseOnDateRecycler.setVisibility(View.GONE);
                mAchievedAsDateRecycler.setVisibility(View.GONE);
                Log.v("txt_zone_by_month: ", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay);
                //  callWebServiceZoneByMonth(sseid, "123", "", "3", txtFromDate.getText().toString(), txtToDate.getText().toString(), "1");

                txtZoneByMonth.setBackgroundDrawable(getResources().getDrawable(R.drawable.renewal_filter_btn_bg));
                txtZoneByMonth.setTextColor(getResources().getColor(R.color.White));

                txtChannelByZone.setBackgroundDrawable(null);
                txtChannelByZone.setTextColor(getResources().getColor(R.color.Black));
                txtChannelByMonth.setBackgroundDrawable(null);
                txtChannelByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtSegByMonth.setBackgroundDrawable(null);
                txtSegByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtCityByMonth.setBackgroundDrawable(null);
                txtCityByMonth.setTextColor(getResources().getColor(R.color.Black));

                break;
            case R.id.txt_seg_by_month:
                chartType = "4";

                ll_chByzone.setVisibility(View.GONE);
                ll_zone_type2.setVisibility(View.GONE);
                ll_zone_type3.setVisibility(View.GONE);
                ll_zone_type4.setVisibility(View.GONE);
                ll_zone_type5.setVisibility(View.GONE);
                ll_zone_type6.setVisibility(View.GONE);

                mChByMonRecycler.setVisibility(View.GONE);
                mChByZoneRecycler.setVisibility(View.GONE);
                mAchievedNOPRecycler.setVisibility(View.GONE);
                mBaseNOPRecycler.setVisibility(View.GONE);
                mDateRenewRecycler.setVisibility(View.GONE);
                mBaseOnDateRecycler.setVisibility(View.GONE);
                mAchievedAsDateRecycler.setVisibility(View.GONE);
                Log.v("txt_seg_by_month: ", "chartType: " + chartType + " typeOfDisplay: " + typeOfDisplay);
                txtSegByMonth.setBackgroundDrawable(getResources().getDrawable(R.drawable.renewal_filter_btn_bg));
                txtSegByMonth.setTextColor(getResources().getColor(R.color.White));

                txtChannelByZone.setBackgroundDrawable(null);
                txtChannelByZone.setTextColor(getResources().getColor(R.color.Black));
                txtChannelByMonth.setBackgroundDrawable(null);
                txtChannelByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtZoneByMonth.setBackgroundDrawable(null);
                txtZoneByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtCityByMonth.setBackgroundDrawable(null);
                txtCityByMonth.setTextColor(getResources().getColor(R.color.Black));
                break;
            case R.id.txt_city_by_month:
                chartType = "5";
                ll_chByzone.setVisibility(View.GONE);
                ll_zone_type2.setVisibility(View.GONE);
                ll_zone_type3.setVisibility(View.GONE);
                ll_zone_type4.setVisibility(View.GONE);
                ll_zone_type5.setVisibility(View.GONE);
                ll_zone_type6.setVisibility(View.GONE);
                mChByMonRecycler.setVisibility(View.GONE);
                mChByZoneRecycler.setVisibility(View.GONE);
                mAchievedNOPRecycler.setVisibility(View.GONE);
                mBaseNOPRecycler.setVisibility(View.GONE);
                mDateRenewRecycler.setVisibility(View.GONE);
                mBaseOnDateRecycler.setVisibility(View.GONE);
                mAchievedAsDateRecycler.setVisibility(View.GONE);
                txtCityByMonth.setBackgroundDrawable(getResources().getDrawable(R.drawable.renewal_filter_btn_bg));
                txtCityByMonth.setTextColor(getResources().getColor(R.color.White));

                txtChannelByZone.setBackgroundDrawable(null);
                txtChannelByZone.setTextColor(getResources().getColor(R.color.Black));
                txtChannelByMonth.setBackgroundDrawable(null);
                txtChannelByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtZoneByMonth.setBackgroundDrawable(null);
                txtZoneByMonth.setTextColor(getResources().getColor(R.color.Black));
                txtSegByMonth.setBackgroundDrawable(null);
                txtSegByMonth.setTextColor(getResources().getColor(R.color.Black));
                break;


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_renewal_filter, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if (id == R.id.filter) {
            Log.v("", "onclick of filter");
            new GetRenewalFilterSelection().execute(sseid, "123", "");
            // item.setIcon(R.drawable.ic_close);
            //  setActionIcon(false);

          /*  mMenu.getItem(1).setVisible(true);
            mMenu.getItem(0).setVisible(false);*/
            menuShow = true;
        } else if (id == R.id.close) {
            Log.v("", "onclick of close");
            renewal_filter_layout.setVisibility(View.GONE);
            //setActionIcon(true);
            //  item.setIcon(R.drawable.ic_filter);
          /*  mMenu.getItem(1).setVisible(false);
            mMenu.getItem(0).setVisible(true);*/
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menuShow) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        } /*else {
            menu.getItem(1).setVisible(false);
            menu.getItem(0).setVisible(true);
        }*/
        return true;
    }

    private void openToDateDialog() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // Launch Date Picker Dialog

        Date today = new Date();
        c.setTime(today);
        c.add(Calendar.DAY_OF_MONTH, -4); // Subtract 4 days
        long minDate = c.getTime().getTime();

        DatePickerDialog dpd = new DatePickerDialog(RenewalDashboardActivity.this, R.style.PickerTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int month = monthOfYear + 1;
                        String Mon = null;
                        int lengthMonth = String.valueOf(month).length();

                        if (lengthMonth == 1) {
                            Mon = "0" + month;
                        } else {
                            Mon = month + "";
                        }
                        String Day = null;
                        int lengthDay = String.valueOf(dayOfMonth).length();

                        if (lengthDay == 1) {
                            Day = "0" + dayOfMonth;
                        } else {
                            Day = dayOfMonth + "";
                        }
                        txtToDate.setText(year + "-" + Mon);
                    }
                }, mYear, mMonth, mDay);

        //  Log.v("DatePickerDialog ",""+Resources.getSystem().getIdentifier("day", "id", "android"));
        //   dpd.findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
        //((ViewGroup) dpd.getDatePicker()).findViewById(getResources().getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
        dpd.show();
    }

    private void openFromDateDialog() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // Launch Date Picker Dialog

        Date today = new Date();
        c.setTime(today);
        c.add(Calendar.DAY_OF_MONTH, -4); // Subtract 4 days
        long minDate = c.getTime().getTime();

        //android.app.AlertDialog.THEME_HOLO_DARK for month & year

        DatePickerDialog dpd = new DatePickerDialog(RenewalDashboardActivity.this, R.style.PickerTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int month = monthOfYear + 1;
                        String Mon = null;
                        int lengthMonth = String.valueOf(month).length();

                        if (lengthMonth == 1) {
                            Mon = "0" + month;
                        } else {
                            Mon = month + "";
                        }
                        String Day = null;
                        int lengthDay = String.valueOf(dayOfMonth).length();

                        if (lengthDay == 1) {
                            Day = "0" + dayOfMonth;
                        } else {
                            Day = dayOfMonth + "";
                        }
                        txtFromDate.setText(year + "-" + Mon);
                    }
                }, mYear, mMonth, mDay);
        //((ViewGroup) dpd.getDatePicker()).findViewById(getResources().getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
        dpd.show();
    }

    public void forChart1Type1(String type) {
        if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
            ll_zone_type2.setVisibility(View.GONE);
            ll_zone_type3.setVisibility(View.GONE);
            ll_zone_type4.setVisibility(View.GONE);
            ll_zone_type5.setVisibility(View.GONE);
            ll_zone_type6.setVisibility(View.GONE);
            //  callWebServiceByZone(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
            new GetChByZoneRenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void forChartNTypeN(String chartType, String type) {
        switch (type) {
            case "2":
                if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
                    ll_chByzone.setVisibility(View.GONE);
                    ll_zone_type3.setVisibility(View.GONE);
                    ll_zone_type4.setVisibility(View.GONE);
                    ll_zone_type5.setVisibility(View.GONE);
                    ll_zone_type6.setVisibility(View.GONE);
                    new GetChartNType2RenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case "3":
                if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
                    ll_chByzone.setVisibility(View.GONE);
                    ll_zone_type2.setVisibility(View.GONE);
                    ll_zone_type4.setVisibility(View.GONE);
                    ll_zone_type5.setVisibility(View.GONE);
                    ll_zone_type6.setVisibility(View.GONE);
                    new GetChartNType3RenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case "4":
                if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
                    ll_chByzone.setVisibility(View.GONE);
                    ll_zone_type3.setVisibility(View.GONE);
                    ll_zone_type2.setVisibility(View.GONE);
                    ll_zone_type5.setVisibility(View.GONE);
                    ll_zone_type6.setVisibility(View.GONE);
                    new GetChartNType4RenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case "5":
                if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
                    ll_chByzone.setVisibility(View.GONE);
                    ll_zone_type3.setVisibility(View.GONE);
                    ll_zone_type4.setVisibility(View.GONE);
                    ll_zone_type2.setVisibility(View.GONE);
                    ll_zone_type6.setVisibility(View.GONE);
                    new GetChartNType5RenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case "6":
                if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
                    ll_chByzone.setVisibility(View.GONE);
                    ll_zone_type3.setVisibility(View.GONE);
                    ll_zone_type4.setVisibility(View.GONE);
                    ll_zone_type5.setVisibility(View.GONE);
                    ll_zone_type2.setVisibility(View.GONE);
                    new GetChartNType6RenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void callServiceForSingleType(String type, String chartType) {
        Log.v("ForSingleType ", "typeOfDisplay " + type + " chartType " + chartType);
        if (chartType.equals("1") && type.equals("1")) {
            mChByMonRecycler.setVisibility(View.GONE);
            forChart1Type1(type);

        } else if (chartType.equals("1") && !type.equals("1")) {
            mChByMonRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }

        if (chartType.equals("2") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart2Type1(type);

        } else if (chartType.equals("2") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }

        if (chartType.equals("3") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart3Type1(chartType, type);
        } else if (chartType.equals("3") && !type.equals("1")) {
            forChartNTypeN(chartType, type);
        }
        if (chartType.equals("4") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart4Type1(chartType, type);
        } else if (chartType.equals("4") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }
        if (chartType.equals("5") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart5Type1(chartType, type);
        } else if (chartType.equals("5") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }

    }

    private void forChart3Type1(String chartType, String type) {
        if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
            ll_zone_type2.setVisibility(View.GONE);
            ll_zone_type3.setVisibility(View.GONE);
            ll_zone_type4.setVisibility(View.GONE);
            ll_zone_type5.setVisibility(View.GONE);
            ll_zone_type6.setVisibility(View.GONE);
            new GetZoneByMonthRenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void forChart2Type1(String type) {
        if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
            //  ll_chByzone.setVisibility(View.GONE);
            ll_zone_type2.setVisibility(View.GONE);
            ll_zone_type3.setVisibility(View.GONE);
            ll_zone_type4.setVisibility(View.GONE);
            ll_zone_type5.setVisibility(View.GONE);
            ll_zone_type6.setVisibility(View.GONE);
            new GetChByMonthRenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void callServiceForMultipleType(String type, String chartType) {
        Log.v("ForMultipleType", "type " + type + " chartType " + chartType);

        if (chartType.equals("1") && type.equals("1")) {
            mChByMonRecycler.setVisibility(View.GONE);
            forChart1Type1(type);
        } else if (chartType.equals("1") && !type.equals("1")) {
            mChByMonRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }
        if (chartType.equals("2") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart2Type1(type);
        } else if (chartType.equals("2") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }
        if (chartType.equals("3") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart3Type1(chartType, type);

        } else if (chartType.equals("3") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }
        if (chartType.equals("4") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart4Type1(chartType, type);
        } else if (chartType.equals("4") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }
        if (chartType.equals("5") && type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChart5Type1(chartType, type);
        } else if (chartType.equals("5") && !type.equals("1")) {
            mChByZoneRecycler.setVisibility(View.GONE);
            forChartNTypeN(chartType, type);
        }
    }

    private void forChart5Type1(String chartType, String type) {
        if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
            ll_zone_type2.setVisibility(View.GONE);
            ll_zone_type3.setVisibility(View.GONE);
            ll_zone_type4.setVisibility(View.GONE);
            ll_zone_type5.setVisibility(View.GONE);
            ll_zone_type6.setVisibility(View.GONE);
            new GetCityByMonthRenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void forChart4Type1(String chartType, String type) {
        if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
            ll_zone_type2.setVisibility(View.GONE);
            ll_zone_type3.setVisibility(View.GONE);
            ll_zone_type4.setVisibility(View.GONE);
            ll_zone_type5.setVisibility(View.GONE);
            ll_zone_type6.setVisibility(View.GONE);
            new GetSegByMonthRenewalFilteredData().execute(sseid, "123", "", chartType, txtFromDate.getText().toString(), txtToDate.getText().toString(), type);
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void callWebServiceByZone(String sseid, String userId, String condition, String chartType,
                                      String startDate, String endDate, String typeOfDisplay) {
        if (Constants.isNetworkInfo(RenewalDashboardActivity.this)) {
            try {
                new GetChByZoneRenewalFilteredData().execute(sseid, userId, condition, chartType, startDate, endDate, typeOfDisplay);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void setChannelByMonthRecyclerView() {
        // lbl_type1.setText("Channel By Month(%)");
        mChByMonRecycler.setHasFixedSize(true);
        mChByMonRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mChByMonRecycler.setLayoutManager(mLayoutManager);

        mChByMonType1Adaper = new ChByMonType1Adaper(RenewalDashboardActivity.this, mChByMonthType1ArrList, "Channel By Month", headers, mChByMonthType1List);
        mChByMonRecycler.setAdapter(mChByMonType1Adaper);
        mChByMonType1Adaper.notifyDataSetChanged();
    }

    private void setChannelByZoneType2RecyclerView() {
        mAchievedNOPRecycler.setHasFixedSize(true);
        mAchievedNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType2Adapter = new ChByZoneType2Adapter(RenewalDashboardActivity.this, mType2ArrList, "Channel By Zone", headers, mType2List);
        mAchievedNOPRecycler.setAdapter(mChByZoneType2Adapter);
        mChByZoneType2Adapter.notifyDataSetChanged();
    }

    private void setChannelByMonthType2RecyclerView() {
        mAchievedNOPRecycler.setHasFixedSize(true);
        mAchievedNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType2Adapter = new ChByZoneType2Adapter(RenewalDashboardActivity.this, mType2ArrList, "Channel By Month", headers, mType2List);
        mAchievedNOPRecycler.setAdapter(mChByZoneType2Adapter);
        mChByZoneType2Adapter.notifyDataSetChanged();
    }

    private void setChannelByZoneType3RecyclerView() {
        mBaseNOPRecycler.setHasFixedSize(true);
        mBaseNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType3Adapter = new ChByZoneType3Adapter(RenewalDashboardActivity.this, mType3ArrList, "Channel By Zone", headers, mType3List);
        mBaseNOPRecycler.setAdapter(mChByZoneType3Adapter);
        mChByZoneType3Adapter.notifyDataSetChanged();
    }

    private void setChannelByZoneType4RecyclerView() {
        mDateRenewRecycler.setHasFixedSize(true);
        mDateRenewRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mDateRenewRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType4Adapter = new ChByZoneType4Adapter(RenewalDashboardActivity.this, mType4ArrList, "Channel By Zone", headers, mType4List);
        mDateRenewRecycler.setAdapter(mChByZoneType4Adapter);
        mChByZoneType4Adapter.notifyDataSetChanged();
    }

    private void setChannelByMonthType4RecyclerView() {
        mDateRenewRecycler.setHasFixedSize(true);
        mDateRenewRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mDateRenewRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType4Adapter = new ChByZoneType4Adapter(RenewalDashboardActivity.this, mType4ArrList, "Channel By Month", headers, mType4List);
        mDateRenewRecycler.setAdapter(mChByZoneType4Adapter);
        mChByZoneType4Adapter.notifyDataSetChanged();
    }

    private void setZoneByMonthType4RecyclerView() {
        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mDateRenewRecycler.setLayoutParams(LayoutParamsview);

        mDateRenewRecycler.setHasFixedSize(true);
        mDateRenewRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mDateRenewRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType4Adapter = new ChByZoneType4Adapter(RenewalDashboardActivity.this, mType4ArrList, "Zone By Month", headers, mType4List);
        mDateRenewRecycler.setAdapter(mChByZoneType4Adapter);
        mChByZoneType4Adapter.notifyDataSetChanged();
    }

    private void setChannelByZoneType5RecyclerView() {
        mBaseOnDateRecycler.setHasFixedSize(true);
        mBaseOnDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseOnDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType5Adapter = new ChByZoneType5Adapter(RenewalDashboardActivity.this, mType5ArrList, "Channel By Zone", headers, mType5List);
        mBaseOnDateRecycler.setAdapter(mChByZoneType5Adapter);
        mChByZoneType5Adapter.notifyDataSetChanged();
    }

    private void setChannelByMonthType5RecyclerView() {
        mBaseOnDateRecycler.setHasFixedSize(true);
        mBaseOnDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseOnDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType5Adapter = new ChByZoneType5Adapter(RenewalDashboardActivity.this, mType5ArrList, "Channel By Month", headers, mType5List);
        mBaseOnDateRecycler.setAdapter(mChByZoneType5Adapter);
        mChByZoneType5Adapter.notifyDataSetChanged();
    }

    private void setZoneByMonthType5RecyclerView() {
        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBaseOnDateRecycler.setLayoutParams(LayoutParamsview);

        mBaseOnDateRecycler.setHasFixedSize(true);
        mBaseOnDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseOnDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType5Adapter = new ChByZoneType5Adapter(RenewalDashboardActivity.this, mType5ArrList, "Zone By Month", headers, mType5List);
        mBaseOnDateRecycler.setAdapter(mChByZoneType5Adapter);
        mChByZoneType5Adapter.notifyDataSetChanged();
    }

    private void setChannelByZoneType6RecyclerView() {
        mAchievedAsDateRecycler.setHasFixedSize(true);
        mAchievedAsDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedAsDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType6Adapter = new ChByZoneType6Adapter(RenewalDashboardActivity.this, mType6ArrList, "Channel By Zone", headers, mType6List);
        mAchievedAsDateRecycler.setAdapter(mChByZoneType6Adapter);
        mChByZoneType6Adapter.notifyDataSetChanged();
    }

    private void setChannelByMonthType6RecyclerView() {
        mAchievedAsDateRecycler.setHasFixedSize(true);
        mAchievedAsDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedAsDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType6Adapter = new ChByZoneType6Adapter(RenewalDashboardActivity.this, mType6ArrList, "Channel By Month", headers, mType6List);
        mAchievedAsDateRecycler.setAdapter(mChByZoneType6Adapter);
        mChByZoneType6Adapter.notifyDataSetChanged();
    }

    private void setZoneByMonthType6RecyclerView() {
        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mAchievedAsDateRecycler.setLayoutParams(LayoutParamsview);

        mAchievedAsDateRecycler.setHasFixedSize(true);
        mAchievedAsDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedAsDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType6Adapter = new ChByZoneType6Adapter(RenewalDashboardActivity.this, mType6ArrList, "Zone By Month", headers, mType6List);
        mAchievedAsDateRecycler.setAdapter(mChByZoneType6Adapter);
        mChByZoneType6Adapter.notifyDataSetChanged();
    }

    private void setZoneByMonthRecyclerView() {
        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mChByMonRecycler.setLayoutParams(LayoutParamsview);

        mChByMonRecycler.setHasFixedSize(true);
        mChByMonRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mChByMonRecycler.setLayoutManager(mLayoutManager);

        mChByMonType1Adaper = new ChByMonType1Adaper(RenewalDashboardActivity.this, mZoneByMonthArrList, "Zone By Month", headers, mZoneByMonthList);
        mChByMonRecycler.setAdapter(mChByMonType1Adaper);
        mChByMonType1Adaper.notifyDataSetChanged();

    }

    private void setZoneByMonthType2RecyclerView() {
        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mAchievedNOPRecycler.setLayoutParams(LayoutParamsview);

        mAchievedNOPRecycler.setHasFixedSize(true);
        mAchievedNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType2Adapter = new ChByZoneType2Adapter(RenewalDashboardActivity.this, mType2ArrList, "Zone By Month", headers, mType2List);
        mAchievedNOPRecycler.setAdapter(mChByZoneType2Adapter);
        mChByZoneType2Adapter.notifyDataSetChanged();
    }

    private void setChannelByMonthType3RecyclerView() {
        //Channel By Month
        mBaseNOPRecycler.setHasFixedSize(true);
        mBaseNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType3Adapter = new ChByZoneType3Adapter(RenewalDashboardActivity.this, mType3ArrList, "Channel By Month", headers, mType3List);
        mBaseNOPRecycler.setAdapter(mChByZoneType3Adapter);
        mChByZoneType3Adapter.notifyDataSetChanged();
    }

    private void setZoneByMonthType3RecyclerView() {
        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBaseNOPRecycler.setLayoutParams(LayoutParamsview);

        mBaseNOPRecycler.setHasFixedSize(true);
        mBaseNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType3Adapter = new ChByZoneType3Adapter(RenewalDashboardActivity.this, mType3ArrList, "Zone By Month", headers, mType3List);
        mBaseNOPRecycler.setAdapter(mChByZoneType3Adapter);
        mChByZoneType3Adapter.notifyDataSetChanged();
    }

    private void setSegByMonthRecyclerView() {

        //  if (mSegByMonthArrList.isEmpty() || mSegByMonthArrList == null || mSegByMonthArrList.equals("")) {
        //    System.out.println("EmptySize:=>" + mSegByMonthArrList.size());
        //  } else {
        mChByMonRecycler.setHasFixedSize(true);
        mChByMonRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mChByMonRecycler.setLayoutManager(mLayoutManager);

        mChByMonType1Adaper = new ChByMonType1Adaper(RenewalDashboardActivity.this, mSegByMonthArrList, "Seg By Month", headers, mSegByMonthList);
        mChByMonRecycler.setAdapter(mChByMonType1Adaper);
        mChByMonType1Adaper.notifyDataSetChanged();
        //  }
    }

    private void setCityByMonthRecyclerView() {
        mChByMonRecycler.setHasFixedSize(true);
        mChByMonRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mChByMonRecycler.setLayoutManager(mLayoutManager);

        mChByMonType1Adaper = new ChByMonType1Adaper(RenewalDashboardActivity.this, mCityByMonthArrList, "City By Month", headers, mCityByMonthList);
        mChByMonRecycler.setAdapter(mChByMonType1Adaper);
        mChByMonType1Adaper.notifyDataSetChanged();
    }

    private void setSegByMonthType2RecyclerView() {
        mAchievedNOPRecycler.setHasFixedSize(true);
        mAchievedNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType2Adapter = new ChByZoneType2Adapter(RenewalDashboardActivity.this, mType2ArrList, "Seg By Month", headers, mType2List);
        mAchievedNOPRecycler.setAdapter(mChByZoneType2Adapter);
        mChByZoneType2Adapter.notifyDataSetChanged();
    }

    private void setCityByMonthType2RecyclerView() {
        mAchievedNOPRecycler.setHasFixedSize(true);
        mAchievedNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType2Adapter = new ChByZoneType2Adapter(RenewalDashboardActivity.this, mType2ArrList, "City By Month", headers, mType2List);
        mAchievedNOPRecycler.setAdapter(mChByZoneType2Adapter);
        mChByZoneType2Adapter.notifyDataSetChanged();
    }

    private void setSegByMonthType3RecyclerView() {
        mBaseNOPRecycler.setHasFixedSize(true);
        mBaseNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType3Adapter = new ChByZoneType3Adapter(RenewalDashboardActivity.this, mType3ArrList, "Seg By Month", headers, mType3List);
        mBaseNOPRecycler.setAdapter(mChByZoneType3Adapter);
        mChByZoneType3Adapter.notifyDataSetChanged();
    }

    private void setCityByMonthType3RecyclerView() {
        mBaseNOPRecycler.setHasFixedSize(true);
        mBaseNOPRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseNOPRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType3Adapter = new ChByZoneType3Adapter(RenewalDashboardActivity.this, mType3ArrList, "City By Month", headers, mType3List);
        mBaseNOPRecycler.setAdapter(mChByZoneType3Adapter);
        mChByZoneType3Adapter.notifyDataSetChanged();
    }

    private void setSegByMonthType4RecyclerView() {
        mDateRenewRecycler.setHasFixedSize(true);
        mDateRenewRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mDateRenewRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType4Adapter = new ChByZoneType4Adapter(RenewalDashboardActivity.this, mType4ArrList, "Seg By Month", headers, mType4List);
        mDateRenewRecycler.setAdapter(mChByZoneType4Adapter);
        mChByZoneType4Adapter.notifyDataSetChanged();
    }

    private void setCityByMonthType4RecyclerView() {
        mDateRenewRecycler.setHasFixedSize(true);
        mDateRenewRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mDateRenewRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType4Adapter = new ChByZoneType4Adapter(RenewalDashboardActivity.this, mType4ArrList, "City By Month", headers, mType4List);
        mDateRenewRecycler.setAdapter(mChByZoneType4Adapter);
        mChByZoneType4Adapter.notifyDataSetChanged();
    }

    private void setSegByMonthType5RecyclerView() {
        mBaseOnDateRecycler.setHasFixedSize(true);
        mBaseOnDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseOnDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType5Adapter = new ChByZoneType5Adapter(RenewalDashboardActivity.this, mType5ArrList, "Seg By Month", headers, mType5List);
        mBaseOnDateRecycler.setAdapter(mChByZoneType5Adapter);
        mChByZoneType5Adapter.notifyDataSetChanged();
    }

    private void setCityByMonthType5RecyclerView() {
        mBaseOnDateRecycler.setHasFixedSize(true);
        mBaseOnDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mBaseOnDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType5Adapter = new ChByZoneType5Adapter(RenewalDashboardActivity.this, mType5ArrList, "City By Month", headers, mType5List);
        mBaseOnDateRecycler.setAdapter(mChByZoneType5Adapter);
        mChByZoneType5Adapter.notifyDataSetChanged();
    }

    private void setCityByMonthType6RecyclerView() {
        mAchievedAsDateRecycler.setHasFixedSize(true);
        mAchievedAsDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedAsDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType6Adapter = new ChByZoneType6Adapter(RenewalDashboardActivity.this, mType6ArrList, "City By Month", headers, mType6List);
        mAchievedAsDateRecycler.setAdapter(mChByZoneType6Adapter);
        mChByZoneType6Adapter.notifyDataSetChanged();
    }

    private void setSegByMonthType6RecyclerView() {
        mAchievedAsDateRecycler.setHasFixedSize(true);
        mAchievedAsDateRecycler.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(RenewalDashboardActivity.this);
        mAchievedAsDateRecycler.setLayoutManager(mLayoutManager);

        mChByZoneType6Adapter = new ChByZoneType6Adapter(RenewalDashboardActivity.this, mType6ArrList, "Seg By Month", headers, mType6List);
        mAchievedAsDateRecycler.setAdapter(mChByZoneType6Adapter);
        mChByZoneType6Adapter.notifyDataSetChanged();
    }

    // For example - Call when you need to change icon
    private void setActionIcon(boolean showFilter) {
        MenuItem item = mMenu.findItem(R.id.filter);

        if (mMenu != null) {
            if (showFilter) {
                item.setIcon(R.drawable.ic_filter);

                // renewal_filter_layout.setVisibility(View.GONE);
                mMenu.getItem(0).setVisible(true);
                mMenu.getItem(1).setVisible(false);
                showFilter = false;
            } else {
                mMenu.getItem(0).setVisible(false);
                mMenu.getItem(1).setVisible(true);

                item.setIcon(R.drawable.ic_close);
                showFilter = true;
                // renewal_filter_layout.setVisibility(View.GONE);
                //setColorFilter(getResources().getColor(R.color.blue));
            }
        }
    }

    private void setRenewalFilterMultiSelectOptions() {
        // setActionIcon(false);
        singleSelcSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = singleSelcSpinner.getSelectedItem().toString();
                if (type.equals("VERTICAL")) {
                    multiFilterSpAdapter = new ArrayAdapter<String>(RenewalDashboardActivity.this, R.layout.spinner_text, mVerticalList);
                    multiFilterSpAdapter.setDropDownViewResource(R.layout.spinner_text);
                    sp_renewal_filter.setAdapter(multiFilterSpAdapter, false, onSelectedRenwlFilterListener);
                } else if (type.equals("SEGMENT TYPE")) {
                    multiFilterSpAdapter = new ArrayAdapter<String>(RenewalDashboardActivity.this, R.layout.spinner_text, mSegmentList);
                    multiFilterSpAdapter.setDropDownViewResource(R.layout.spinner_text);
                    sp_renewal_filter.setAdapter(multiFilterSpAdapter, false, onSelectedRenwlFilterListener);
                } else if(type.equals("ZONE")){
                    multiFilterSpAdapter = new ArrayAdapter<String>(RenewalDashboardActivity.this, R.layout.spinner_text, mZoneList);
                    multiFilterSpAdapter.setDropDownViewResource(R.layout.spinner_text);
                    sp_renewal_filter.setAdapter(multiFilterSpAdapter, false, onSelectedRenwlFilterListener);
                } else if(type.equals("CITY")){
                    multiFilterSpAdapter = new ArrayAdapter<String>(RenewalDashboardActivity.this, R.layout.spinner_text, mCityList);
                    multiFilterSpAdapter.setDropDownViewResource(R.layout.spinner_text);
                    sp_renewal_filter.setAdapter(multiFilterSpAdapter, false, onSelectedRenwlFilterListener);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    //For Chart Type 1 & typeOfDisplay 1
    private class GetChByZoneRenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mChZoneType1ArrList != null) {
                mChZoneType1ArrList.clear();
            }
            if (mChZoneType1List != null) {
                mChZoneType1List.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("", "parameters: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChByZone Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        String strYear = "2017", strVERTICAL = "", strEAST = "", strWEST = "", strCENTRAL = "", strNORTH = "", strSOUTH = "";
                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            if (soapObject3.toString().contains("Message")) {
                                soap_error = "Message";
                            } else {
                                try {
                                    headers = new LinkedHashMap();
                                    strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");
                                    if (soapObject3.toString().contains("EAST")) {
                                        strEAST = soapObject3.getPropertyAsString("EAST");
                                        headers.put("EAST", strEAST);
                                    }
                                    if (soapObject3.toString().contains("WEST")) {
                                        strWEST = soapObject3.getPropertyAsString("WEST");
                                        headers.put("WEST", strWEST);
                                    }
                                    if (soapObject3.toString().contains("CENTRAL")) {
                                        strCENTRAL = soapObject3.getPropertyAsString("CENTRAL");
                                        headers.put("CENTRAL", strCENTRAL);
                                    }
                                    if (soapObject3.toString().contains("NORTH")) {
                                        strNORTH = soapObject3.getPropertyAsString("NORTH");
                                        headers.put("NORTH", strNORTH);
                                    }
                                    if (soapObject3.toString().contains("SOUTH")) {
                                        strSOUTH = soapObject3.getPropertyAsString("SOUTH");
                                        headers.put("SOUTH", strSOUTH);
                                    }
                                    mChZoneType1List.add(headers);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                items.VERTICAL = strVERTICAL;
                                items.YEAR = strYear;
                                items.EAST = strEAST;
                                items.WEST = strWEST;
                                items.CENTRAL = strCENTRAL;
                                items.NORTH = strNORTH;
                                items.SOUTH = strSOUTH;
                                mChZoneType1ArrList.add(items);


                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }

            System.out.println("onPostExecChByZOne: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    ll_chByzone.setVisibility(View.GONE);
                    lbl_type1.setVisibility(View.GONE);
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    ll_chByzone.setVisibility(View.GONE);
                    lbl_type1.setVisibility(View.GONE);
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("Error while fetching Channel By Zone data");
                    Toast.makeText(getApplicationContext(), "Error while fetching Channel By Zone data !!", Toast.LENGTH_SHORT).show();
                } else {
                    ll_chByzone.setVisibility(View.VISIBLE);
                    lbl_type1.setVisibility(View.VISIBLE);
                    lbl_type1.setText("Channel By Zone(%)");
                    mChByZoneRecycler.setVisibility(View.VISIBLE);
                    mChByMonRecycler.setVisibility(View.GONE);

                    // System.out.println("ChannelByZoneArrayList:=>" + mChZoneType1ArrList.size());
                    //  System.out.println("ChannelByZoneArrayData:=>" + mChZoneType1ArrList.toString());
                    //System.out.println("mChZoneType1List.size:=>" + mChZoneType1List.size() + "" + mChZoneType1List.toString());

                    setChannelByZoneRecyclerView();
                }
            }
        }
    }

    //For Chart Type 2 & typeOfDisplay 1
    private class GetChByMonthRenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mChByMonthType1List != null) {
                mChByMonthType1List.clear();
            }
            if (mChByMonthType1ArrList != null) {
                mChByMonthType1ArrList.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("", "parametersByMonth: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChannelByMonth Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        String strYear = "2017", strVERTICAL = "";
                        String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                        String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            if (soapObject3.toString().contains("Message")) {
                                soap_error = "Message";
                            } else {
                                try {
                                    strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");
                                /*if(soapObject3.toString().startsWith("JAN")){
                                    headers = new LinkedHashMap();
                                    headers.put("EAST", strEAST);

                                    *//**//*for(String cs : (SoapObject) soapObject2.getProperty(i))
                                    {
                                        prjList.add(cs);
                                    }
                                }*/

                              /*  for (int k = 0; k < soapObject3.getPropertyCount(); k++) {
                                    list1.add(String.valueOf(soapObject3.getProperty(k)));

                                    Log.v("", "list:" + list1.toString());
                                }*/
                                    headers = new LinkedHashMap();

                                    if (soapObject3.toString().contains("JAN17")) {
                                        strJAN = soapObject3.getPropertyAsString("JAN17");
                                        headers.put("JAN", strJAN);
                                    }
                                    if (soapObject3.toString().contains("FEB17")) {
                                        strFEB = soapObject3.getPropertyAsString("FEB17");
                                        headers.put("FEB", strFEB);
                                    }
                                    if (soapObject3.toString().contains("MAR17")) {
                                        strMAR = soapObject3.getPropertyAsString("MAR17");
                                        headers.put("MAR", strMAR);
                                    }
                                    if (soapObject3.toString().contains("APR17")) {
                                        strAPR = soapObject3.getPropertyAsString("APR17");
                                        headers.put("APR", strAPR);
                                    }
                                    if (soapObject3.toString().contains("MAY17")) {
                                        strMAY = soapObject3.getPropertyAsString("MAY17");
                                        headers.put("MAY", strMAY);
                                    }
                                    if (soapObject3.toString().contains("JUN17")) {
                                        strJUN = soapObject3.getPropertyAsString("JUN17");
                                        headers.put("JUN", strJUN);
                                    }
                                    if (soapObject3.toString().contains("JUL17")) {
                                        strJUL = soapObject3.getPropertyAsString("JUL17");
                                        headers.put("JUL", strJUL);
                                    }
                                    if (soapObject3.toString().contains("AUG17")) {
                                        strAUG = soapObject3.getPropertyAsString("AUG17");
                                        headers.put("AUG", strAUG);
                                    }
                                    if (soapObject3.toString().contains("SEP17")) {
                                        strSEP = soapObject3.getPropertyAsString("SEP17");
                                        headers.put("SEP", strSEP);
                                    }
                                    if (soapObject3.toString().contains("OCT17")) {
                                        strOCT = soapObject3.getPropertyAsString("OCT17");
                                        headers.put("OCT", strOCT);
                                    }
                                    if (soapObject3.toString().contains("NOV17")) {
                                        strNOV = soapObject3.getPropertyAsString("NOV17");
                                        headers.put("NOV", strNOV);
                                    }
                                    if (soapObject3.toString().contains("DEC17")) {
                                        strDEC = soapObject3.getPropertyAsString("DEC17");
                                        headers.put("DEC", strDEC);
                                    }



                               /* strJAN = soapObject3.getPropertyAsString("JAN17");
                                strFEB = soapObject3.getPropertyAsString("FEB17");
                                strMAR = soapObject3.getPropertyAsString("MAR17");
                                strAPR = soapObject3.getPropertyAsString("APR17");
                                strMAY = soapObject3.getPropertyAsString("MAY17");
                                strJUN = soapObject3.getPropertyAsString("JUN17");
                                strJUL = soapObject3.getPropertyAsString("JUL17");
                                strAUG = soapObject3.getPropertyAsString("AUG17");
                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                *//*strNOV = soapObject3.getPropertyAsString("NOV17");
                                strDEC = soapObject3.getPropertyAsString("DEC17");*/

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                           /* headers.put("FEB17", strFEB);
                            headers.put("MAR17", strMAR);
                            headers.put("APR17", strAPR);
                            headers.put("MAY17", strMAY);
                            headers.put("JUN17", strJUN);
                            headers.put("JUL17", strJUL);
                            headers.put("AUG17", strAUG);*/
                                //   headers.put("SEP17", strSEP);
                                //   headers.put("OCT17", strOCT);
                          /*  headers.put("NOV17", strNOV);
                            headers.put("DEC17", strDEC);*/

                                mChByMonthType1List.add(headers);

                                ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                items.VERTICAL = strVERTICAL;
                                items.YEAR = strYear;

                                mChByMonthType1ArrList.add(items);

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    ll_chByzone.setVisibility(View.GONE);
                    lbl_type1.setVisibility(View.GONE);
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.GONE);

                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Channel By Month data !!", Toast.LENGTH_SHORT).show();
                } else {
                    ll_chByzone.setVisibility(View.VISIBLE);
                    lbl_type1.setVisibility(View.VISIBLE);
                    lbl_type1.setText("Channel By Month(%)");
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByMonthArrayList:=>" + mChByMonthType1ArrList.size());
                    System.out.println("ChannelByMonthArrayData:=>" + mChByMonthType1ArrList.toString());
                    System.out.println("mChByMonthType1List.size:=>" + mChByMonthType1List.size() + "" + mChByMonthType1List.toString());

                    setChannelByMonthRecyclerView();
                }
            }
        }
    }

    //For Chart Type 3 & typeOfDisplay 1
    private class GetZoneByMonthRenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mZoneByMonthList != null) {
                mZoneByMonthList.clear();
            }
            if (mZoneByMonthArrList != null) {
                mZoneByMonthArrList.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);


            Log.v("parameters ", "ZoneByMon: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("Response ", "ZoneByMonth:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        String strYear = "2017", strZONE = "";
                        String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                        String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            if (soapObject3.toString().contains("Message")) {
                                soap_error = "Message";
                            } else {
                                try {
                                    strZONE = soapObject3.getPropertyAsString("ZONE");

                                    headers = new LinkedHashMap();

                                    if (soapObject3.toString().contains("JAN17")) {
                                        strJAN = soapObject3.getPropertyAsString("JAN17");
                                        headers.put("JAN", strJAN);
                                    }
                                    if (soapObject3.toString().contains("FEB17")) {
                                        strFEB = soapObject3.getPropertyAsString("FEB17");
                                        headers.put("FEB", strFEB);
                                    }
                                    if (soapObject3.toString().contains("MAR17")) {
                                        strMAR = soapObject3.getPropertyAsString("MAR17");
                                        headers.put("MAR", strMAR);
                                    }
                                    if (soapObject3.toString().contains("APR17")) {
                                        strAPR = soapObject3.getPropertyAsString("APR17");
                                        headers.put("APR", strAPR);
                                    }
                                    if (soapObject3.toString().contains("MAY17")) {
                                        strMAY = soapObject3.getPropertyAsString("MAY17");
                                        headers.put("MAY", strMAY);
                                    }
                                    if (soapObject3.toString().contains("JUN17")) {
                                        strJUN = soapObject3.getPropertyAsString("JUN17");
                                        headers.put("JUN", strJUN);
                                    }
                                    if (soapObject3.toString().contains("JUL17")) {
                                        strJUL = soapObject3.getPropertyAsString("JUL17");
                                        headers.put("JUL", strJUL);
                                    }
                                    if (soapObject3.toString().contains("AUG17")) {
                                        strAUG = soapObject3.getPropertyAsString("AUG17");
                                        headers.put("AUG", strAUG);
                                    }
                                    if (soapObject3.toString().contains("SEP17")) {
                                        strSEP = soapObject3.getPropertyAsString("SEP17");
                                        headers.put("SEP", strSEP);
                                    }
                                    if (soapObject3.toString().contains("OCT17")) {
                                        strOCT = soapObject3.getPropertyAsString("OCT17");
                                        headers.put("OCT", strOCT);
                                    }
                                    if (soapObject3.toString().contains("NOV17")) {
                                        strNOV = soapObject3.getPropertyAsString("NOV17");
                                        headers.put("NOV", strNOV);
                                    }
                                    if (soapObject3.toString().contains("DEC17")) {
                                        strDEC = soapObject3.getPropertyAsString("DEC17");
                                        headers.put("DEC", strDEC);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                mZoneByMonthList.add(headers);

                                ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                items.ZONE = strZONE;
                                items.YEAR = strYear;

                                mZoneByMonthArrList.add(items);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            Log.v("onPostExec ", " ZonByMon: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Zone By Month data !!", Toast.LENGTH_SHORT).show();
                } else {
                    ll_chByzone.setVisibility(View.VISIBLE);
                    lbl_type1.setVisibility(View.VISIBLE);
                    lbl_type1.setText("Zone By Month(%)");
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ZonByArrayList:=>" + mZoneByMonthArrList.size());
                    //System.out.println("ZonByMonthArrayData:=>" + mChByMonthType1ArrList.toString());
                    System.out.println("ZonMonthType1List.size:=>" + mZoneByMonthList.size() + "" + mZoneByMonthList.toString());

                    setZoneByMonthRecyclerView();
                }
            }
        }

    }

    //For Chart Type 4 & typeOfDisplay 1
    private class GetSegByMonthRenewalFilteredData extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mSegByMonthList != null) {
                mSegByMonthList.clear();
            }
            if (mSegByMonthArrList != null) {
                mSegByMonthArrList.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);


            Log.v("parameters ", "SegByMon: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("Response ", "SegByMonth:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        String strYear = "2017", strSEGMENT = "";
                        String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                        String strSEP = "", strOCT = "", strNOV = "", strDEC = "";


                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            if (soapObject3.toString().contains("Message")) {
                                soap_error = "Message";
                            } else {
                                try {
                                    strSEGMENT = soapObject3.getPropertyAsString("SEGMENT");

                                    headers = new LinkedHashMap();

                                    if (soapObject3.toString().contains("JAN17")) {
                                        strJAN = soapObject3.getPropertyAsString("JAN17");
                                        headers.put("JAN", strJAN);
                                    }
                                    if (soapObject3.toString().contains("FEB17")) {
                                        strFEB = soapObject3.getPropertyAsString("FEB17");
                                        headers.put("FEB", strFEB);
                                    }
                                    if (soapObject3.toString().contains("MAR17")) {
                                        strMAR = soapObject3.getPropertyAsString("MAR17");
                                        headers.put("MAR", strMAR);
                                    }
                                    if (soapObject3.toString().contains("APR17")) {
                                        strAPR = soapObject3.getPropertyAsString("APR17");
                                        headers.put("APR", strAPR);
                                    }
                                    if (soapObject3.toString().contains("MAY17")) {
                                        strMAY = soapObject3.getPropertyAsString("MAY17");
                                        headers.put("MAY", strMAY);
                                    }
                                    if (soapObject3.toString().contains("JUN17")) {
                                        strJUN = soapObject3.getPropertyAsString("JUN17");
                                        headers.put("JUN", strJUN);
                                    }
                                    if (soapObject3.toString().contains("JUL17")) {
                                        strJUL = soapObject3.getPropertyAsString("JUL17");
                                        headers.put("JUL", strJUL);
                                    }
                                    if (soapObject3.toString().contains("AUG17")) {
                                        strAUG = soapObject3.getPropertyAsString("AUG17");
                                        headers.put("AUG", strAUG);
                                    }
                                    if (soapObject3.toString().contains("SEP17")) {
                                        strSEP = soapObject3.getPropertyAsString("SEP17");
                                        headers.put("SEP", strSEP);
                                    }
                                    if (soapObject3.toString().contains("OCT17")) {
                                        strOCT = soapObject3.getPropertyAsString("OCT17");
                                        headers.put("OCT", strOCT);
                                    }
                                    if (soapObject3.toString().contains("NOV17")) {
                                        strNOV = soapObject3.getPropertyAsString("NOV17");
                                        headers.put("NOV", strNOV);
                                    }
                                    if (soapObject3.toString().contains("DEC17")) {
                                        strDEC = soapObject3.getPropertyAsString("DEC17");
                                        headers.put("DEC", strDEC);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                mSegByMonthList.add(headers);

                                ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                items.SEGMENT = strSEGMENT;
                                items.YEAR = strYear;

                                mSegByMonthArrList.add(items);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            Log.v("onPostExec ", " SegByMon: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Segment By Month data !!", Toast.LENGTH_SHORT).show();
                } else {
                    ll_chByzone.setVisibility(View.VISIBLE);
                    lbl_type1.setVisibility(View.VISIBLE);
                    lbl_type1.setText("Segment By Month(%)");
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.VISIBLE);

                   /* if (NewArrayList != null) {
                        NewArrayList.clear();
                    }
                    if (mSegByMonthArrList.size() != 0) {

                        for (ChannelByZoneDataModel obj : mSegByMonthArrList)
                            NewArrayList.add(obj);
                    }*/
                    System.out.println("SegByArrayList:=>" + mSegByMonthArrList.size());
                    System.out.println("SegByList.size:=>" + mSegByMonthList.size() + "" + mSegByMonthList.toString());

                    setSegByMonthRecyclerView();
                }
            }
        }
    }

    //For Chart Type 5 & typeOfDisplay 1
    private class GetCityByMonthRenewalFilteredData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mCityByMonthList != null) {
                mCityByMonthList.clear();
            }
            if (mCityByMonthArrList != null) {
                mCityByMonthArrList.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);


            Log.v("parameters ", "CityByMon: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("Response ", "CityByMonth:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        String strYear = "2017", strCITY = "";
                        String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                        String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            if (soapObject3.toString().contains("Message")) {
                                soap_error = "Message";
                            } else {
                                try {
                                    strCITY = soapObject3.getPropertyAsString("CITY");

                                    headers = new LinkedHashMap();

                                    if (soapObject3.toString().contains("JAN17")) {
                                        strJAN = soapObject3.getPropertyAsString("JAN17");
                                        Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                        headers.put("JAN", strJAN);
                                    }
                                    if (soapObject3.toString().contains("FEB17")) {
                                        strFEB = soapObject3.getPropertyAsString("FEB17");
                                        headers.put("FEB", strFEB);
                                    }
                                    if (soapObject3.toString().contains("MAR17")) {
                                        strMAR = soapObject3.getPropertyAsString("MAR17");
                                        headers.put("MAR", strMAR);
                                    }
                                    if (soapObject3.toString().contains("APR17")) {
                                        strAPR = soapObject3.getPropertyAsString("APR17");
                                        headers.put("APR", strAPR);
                                    }
                                    if (soapObject3.toString().contains("MAY17")) {
                                        strMAY = soapObject3.getPropertyAsString("MAY17");
                                        headers.put("MAY", strMAY);
                                    }
                                    if (soapObject3.toString().contains("JUN17")) {
                                        strJUN = soapObject3.getPropertyAsString("JUN17");
                                        headers.put("JUN", strJUN);
                                    }
                                    if (soapObject3.toString().contains("JUL17")) {
                                        strJUL = soapObject3.getPropertyAsString("JUL17");
                                        headers.put("JUL", strJUL);
                                    }
                                    if (soapObject3.toString().contains("AUG17")) {
                                        strAUG = soapObject3.getPropertyAsString("AUG17");
                                        headers.put("AUG", strAUG);
                                    }
                                    if (soapObject3.toString().contains("SEP17")) {
                                        strSEP = soapObject3.getPropertyAsString("SEP17");
                                        headers.put("SEP", strSEP);
                                    }
                                    if (soapObject3.toString().contains("OCT17")) {
                                        strOCT = soapObject3.getPropertyAsString("OCT17");
                                        headers.put("OCT", strOCT);
                                    }
                                    if (soapObject3.toString().contains("NOV17")) {
                                        strNOV = soapObject3.getPropertyAsString("NOV17");
                                        headers.put("NOV", strNOV);
                                    }
                                    if (soapObject3.toString().contains("DEC17")) {
                                        strDEC = soapObject3.getPropertyAsString("DEC17");
                                        headers.put("DEC", strDEC);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                mCityByMonthList.add(headers);

                                ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                items.CITY = strCITY;
                                items.YEAR = strYear;

                                mCityByMonthArrList.add(items);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            Log.v("onPostExec ", " CityByMon: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching City By Month data !!", Toast.LENGTH_SHORT).show();
                } else {
                    ll_chByzone.setVisibility(View.VISIBLE);
                    lbl_type1.setVisibility(View.VISIBLE);
                    lbl_type1.setText("City By Month(%)");
                    mChByZoneRecycler.setVisibility(View.GONE);
                    mChByMonRecycler.setVisibility(View.VISIBLE);

                    System.out.println("CityByMonthArrayList:=>" + mCityByMonthArrList.size());
                    System.out.println("CityByMonthList.size:=>" + mCityByMonthList.size() + "" + mCityByMonthList.toString());

                    setCityByMonthRecyclerView();
                }
            }
        }
    }

    //For Chart Type N & typeOfDisplay 2
    private class GetChartNType2RenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mType2ArrList != null) {
                mType2ArrList.clear();
            }
            if (mType2List != null) {
                mType2List.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("GetChByZoneType2 ", "parameters: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "", setChartType = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {


                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChByZoneType2 Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                if (params[3].equals("1")) { // Channel By Zone
                    mType2List = new ArrayList<>();
                    mType2ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strVERTICAL = "", strEAST = "", strWEST = "", strCENTRAL = "", strNORTH = "", strSOUTH = "";
                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {

                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("EAST")) {
                                            strEAST = soapObject3.getPropertyAsString("EAST");
                                            headers.put("EAST", strEAST);
                                        }
                                        if (soapObject3.toString().contains("WEST")) {
                                            strWEST = soapObject3.getPropertyAsString("WEST");
                                            headers.put("WEST", strWEST);
                                        }
                                        if (soapObject3.toString().contains("CENTRAL")) {
                                            strCENTRAL = soapObject3.getPropertyAsString("CENTRAL");
                                            headers.put("CENTRAL", strCENTRAL);
                                        }
                                        if (soapObject3.toString().contains("NORTH")) {
                                            strNORTH = soapObject3.getPropertyAsString("NORTH");
                                            headers.put("NORTH", strNORTH);
                                        }
                                        if (soapObject3.toString().contains("SOUTH")) {
                                            strSOUTH = soapObject3.getPropertyAsString("SOUTH");
                                            headers.put("SOUTH", strSOUTH);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType2List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType2ArrList.add(items);

                                    soap_error = "1";// + "," + params[3];
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else if (params[3].equals("2")) { //Channel By Month
                    mType2List = new ArrayList<>();
                    mType2ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);

                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strVERTICAL = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "", strYear = "2017";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                PropertyInfo pi = new PropertyInfo();
                                soapObject2.getPropertyInfo(i, pi);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");
                                        //strYear = "2017";
                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            //  strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //  strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //  strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //   strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //  strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //  strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType2List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType2ArrList.add(items);
                                    soap_error = "2";// + "," + params[3];
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("3")) { //Zone By Month
                    mType2List = new ArrayList<>();
                    mType2ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strZONE = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "",
                                    strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                PropertyInfo pi = new PropertyInfo();
                                soapObject2.getPropertyInfo(i, pi);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strZONE = soapObject3.getPropertyAsString("ZONE");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            //  strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //  strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //  strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //   strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //  strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //  strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType2List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.ZONE = strZONE;
                                    items.YEAR = strYear;

                                    mType2ArrList.add(items);
                                    soap_error = "3";// + "," + params[3];
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("4")) { //Segment By Month
                    mType2List = new ArrayList<>();
                    mType2ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strSEGMENT = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strSEGMENT = soapObject3.getPropertyAsString("SEGMENT");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType2List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.SEGMENT = strSEGMENT;
                                    items.YEAR = strYear;

                                    mType2ArrList.add(items);

                                    soap_error = "4";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("5")) { // City By Month
                    mType2List = new ArrayList<>();
                    mType2ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strCITY = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strCITY = soapObject3.getPropertyAsString("CITY");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType2List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.CITY = strCITY;
                                    items.YEAR = strYear;

                                    mType2ArrList.add(items);
                                    soap_error = "5";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;

                }
            }
            System.out.println("Type 2 " + " onPostExc: " + s);

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Total Achieved NOP data !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("1")) {
                    ll_zone_type2.setVisibility(View.VISIBLE);
                    lbl_zone_type2.setVisibility(View.VISIBLE);
                    mAchievedNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByZoneType2ArrayList:=>" + mType2ArrList.size());
                    // System.out.println("ChannelByZoneType2ArrayData:=>" + mType2ArrList.toString());
                    System.out.println("mType2List:=>" + mType2List.size() + "" + mType2List.toString());

                    //  setChannelByZoneRecyclerView();
                    setChannelByZoneType2RecyclerView();
                } else if (s.equals("2")) {
                    ll_zone_type2.setVisibility(View.VISIBLE);
                    lbl_zone_type2.setVisibility(View.VISIBLE);
                    mAchievedNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChByMonthType2ArrayList:=>" + mType2ArrList.size());
                    System.out.println("headers:=>" + headers.size());
                    System.out.println("mChByMonthType2List:=>" + mType2List.size() + "" + mType2List.toString());

                    setChannelByMonthType2RecyclerView();
                } else if (s.equals("3")) {
                    ll_zone_type2.setVisibility(View.VISIBLE);
                    lbl_zone_type2.setVisibility(View.VISIBLE);
                    mAchievedNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ZonByMonType2ArrayList:=>" + mType2ArrList.size());
                    System.out.println("headers:=>" + headers.size());
                    System.out.println("ZonByMonType2List:=>" + mType2List.size() + "" + mType2List.toString());

                    setZoneByMonthType2RecyclerView();

                } else if (s.equals("4")) {
                    ll_zone_type2.setVisibility(View.VISIBLE);
                    lbl_zone_type2.setVisibility(View.VISIBLE);
                    mAchievedNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("SegByMonType2ArrayList:=>" + mType2ArrList.size());
                    System.out.println("headers:=>" + headers.size());
                    System.out.println("SegByMonType2List:=>" + mType2List.size() + "" + mType2List.toString());

                    setSegByMonthType2RecyclerView();
                } else if (s.equals("5")) {
                    ll_zone_type2.setVisibility(View.VISIBLE);
                    lbl_zone_type2.setVisibility(View.VISIBLE);
                    mAchievedNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("cityByMonType2ArrayList:=>" + mType2ArrList.size());
                    System.out.println("headers:=>" + headers.size());
                    System.out.println("cityByMonType2List:=>" + mType2List.size() + "" + mType2List.toString());

                    setCityByMonthType2RecyclerView();
                }
            }
        }
    }

    //For Chart Type N & typeOfDisplay 3
    private class GetChartNType3RenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mType3ArrList != null) {
                mType3ArrList.clear();
            }
            if (mType3List != null) {
                mType3List.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("GetChByZoneType3 ", "parameters: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChByZoneType3 Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                if (params[3].equals("1")) {  // Channel By Zone
                    mType3List = new ArrayList<>();
                    mType3ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strVERTICAL = "", strEAST = "", strWEST = "", strCENTRAL = "", strNORTH = "", strSOUTH = "";
                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {

                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("EAST")) {
                                            strEAST = soapObject3.getPropertyAsString("EAST");
                                            headers.put("EAST", strEAST);
                                        }
                                        if (soapObject3.toString().contains("WEST")) {
                                            strWEST = soapObject3.getPropertyAsString("WEST");
                                            headers.put("WEST", strWEST);
                                        }
                                        if (soapObject3.toString().contains("CENTRAL")) {
                                            strCENTRAL = soapObject3.getPropertyAsString("CENTRAL");
                                            headers.put("CENTRAL", strCENTRAL);
                                        }
                                        if (soapObject3.toString().contains("NORTH")) {
                                            strNORTH = soapObject3.getPropertyAsString("NORTH");
                                            headers.put("NORTH", strNORTH);
                                        }
                                        if (soapObject3.toString().contains("SOUTH")) {
                                            strSOUTH = soapObject3.getPropertyAsString("SOUTH");
                                            headers.put("SOUTH", strSOUTH);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType3List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType3ArrList.add(items);


                                    soap_error = "1";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("2")) {// Channel By Month
                    mType3List = new ArrayList<>();
                    mType3ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strVERTICAL = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "", strYear = "2017";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            // strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            // strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            // strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            // strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            // strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //   strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                //strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                //  strYear = strOCT.substring(3, 4);
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType3List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType3ArrList.add(items);
                                    soap_error = "2";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("3")) { //Zone By Month
                    mType3List = new ArrayList<>();
                    mType3ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strZONE = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "",
                                    strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                PropertyInfo pi = new PropertyInfo();
                                soapObject2.getPropertyInfo(i, pi);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strZONE = soapObject3.getPropertyAsString("ZONE");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            //  strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //  strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //  strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //   strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //  strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //  strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType3List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.ZONE = strZONE;
                                    items.YEAR = strYear;

                                    mType3ArrList.add(items);
                                    soap_error = "3";// + "," + params[3];
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("4")) { //Segment By Month
                    mType3List = new ArrayList<>();
                    mType3ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strSEGMENT = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strSEGMENT = soapObject3.getPropertyAsString("SEGMENT");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType3List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.SEGMENT = strSEGMENT;
                                    items.YEAR = strYear;

                                    mType3ArrList.add(items);

                                    soap_error = "4";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("5")) { // City By Month
                    mType3List = new ArrayList<>();
                    mType3ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strCITY = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strCITY = soapObject3.getPropertyAsString("CITY");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType3List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.CITY = strCITY;
                                    items.YEAR = strYear;

                                    mType3ArrList.add(items);
                                    soap_error = "5";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Total Base NOP data !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("1")) {
                    ll_zone_type3.setVisibility(View.VISIBLE);
                    lbl_zone_type3.setVisibility(View.VISIBLE);
                    mBaseNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByZoneType3ArrayList:=>" + mType3ArrList.size());
                    System.out.println("ChannelByZoneType3ArrayData:=>" + mType3ArrList.toString());
                    //   System.out.println("mType3List:=>" + mType3List.size() + "" + mType3List.toString());

                    setChannelByZoneType3RecyclerView();
                } else if (s.equals("2")) {
                    ll_zone_type3.setVisibility(View.VISIBLE);
                    lbl_zone_type3.setVisibility(View.VISIBLE);
                    mBaseNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByMonthType3ArrayList:=>" + mType3ArrList.size());
                    // System.out.println("ChannelByMonType3ArrayData:=>" + mType3ArrList.toString());
                    System.out.println("mChMonthType3List:=>" + mType3List.size() + "" + mType3List.toString());

                    setChannelByMonthType3RecyclerView();
                } else if (s.equals("3")) {
                    ll_zone_type3.setVisibility(View.VISIBLE);
                    lbl_zone_type3.setVisibility(View.VISIBLE);
                    mBaseNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ZonByMonType3ArrayList:=>" + mType3ArrList.size());
                    // System.out.println("ChannelByMonType3ArrayData:=>" + mType3ArrList.toString());
                    System.out.println("ZonByMonType3List:=>" + mType3List.size() + "" + mType3List.toString());

                    setZoneByMonthType3RecyclerView();
                } else if (s.equals("4")) {
                    ll_zone_type3.setVisibility(View.VISIBLE);
                    lbl_zone_type3.setVisibility(View.VISIBLE);
                    mBaseNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("SegByMonType3ArrayList:=>" + mType3ArrList.size());
                    System.out.println("SegByMonType3List:=>" + mType3List.size() + "" + mType3List.toString());

                    setSegByMonthType3RecyclerView();
                } else if (s.equals("5")) {
                    ll_zone_type3.setVisibility(View.VISIBLE);
                    lbl_zone_type3.setVisibility(View.VISIBLE);
                    mBaseNOPRecycler.setVisibility(View.VISIBLE);

                    System.out.println("CityByMonType3ArrayList:=>" + mType3ArrList.size());
                    System.out.println("CityByMonType3List:=>" + mType3List.size() + "" + mType3List.toString());

                    setCityByMonthType3RecyclerView();
                }
            }
        }
    }

    //For Chart Type N & typeOfDisplay 4
    private class GetChartNType4RenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mType4ArrList != null) {
                mType4ArrList.clear();
            }
            if (mType4List != null) {
                mType4List.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("GetChByZoneType3 ", "parameters: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChByZoneType4 Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                if (params[3].equals("1")) { // Channel By Zone
                    mType4ArrList = new ArrayList<>();
                    mType4List = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strVERTICAL = "", strEAST = "", strWEST = "", strCENTRAL = "", strNORTH = "", strSOUTH = "";
                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {

                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("EAST")) {
                                            strEAST = soapObject3.getPropertyAsString("EAST");
                                            headers.put("EAST", strEAST);
                                        }
                                        if (soapObject3.toString().contains("WEST")) {
                                            strWEST = soapObject3.getPropertyAsString("WEST");
                                            headers.put("WEST", strWEST);
                                        }
                                        if (soapObject3.toString().contains("CENTRAL")) {
                                            strCENTRAL = soapObject3.getPropertyAsString("CENTRAL");
                                            headers.put("CENTRAL", strCENTRAL);
                                        }
                                        if (soapObject3.toString().contains("NORTH")) {
                                            strNORTH = soapObject3.getPropertyAsString("NORTH");
                                            headers.put("NORTH", strNORTH);
                                        }
                                        if (soapObject3.toString().contains("SOUTH")) {
                                            strSOUTH = soapObject3.getPropertyAsString("SOUTH");
                                            headers.put("SOUTH", strSOUTH);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType4List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;
                                    mType4ArrList.add(items);


                                    soap_error = "1";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("2")) { // Channel By Month
                    mType4ArrList = new ArrayList<>();
                    mType4List = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strVERTICAL = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "", strYear = "2017";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            // strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            // strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            // strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            // strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //   strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            // strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            // strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                //   strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                //   strYear = strOCT.substring(3, 4);
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType4List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;
                                    mType4ArrList.add(items);
                                    soap_error = "2";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("3")) { //Zone By Month
                    mType4List = new ArrayList<>();
                    mType4ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strZONE = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "",
                                    strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                PropertyInfo pi = new PropertyInfo();
                                soapObject2.getPropertyInfo(i, pi);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strZONE = soapObject3.getPropertyAsString("ZONE");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            //  strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //  strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //  strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //   strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //  strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //  strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType4List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.ZONE = strZONE;
                                    items.YEAR = strYear;

                                    mType4ArrList.add(items);
                                    soap_error = "3";// + "," + params[3];
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("4")) { //Segment By Month
                    mType4List = new ArrayList<>();
                    mType4ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strSEGMENT = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strSEGMENT = soapObject3.getPropertyAsString("SEGMENT");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType4List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.SEGMENT = strSEGMENT;
                                    items.YEAR = strYear;

                                    mType4ArrList.add(items);

                                    soap_error = "4";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("5")) { // City By Month
                    mType4List = new ArrayList<>();
                    mType4ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strCITY = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strCITY = soapObject3.getPropertyAsString("CITY");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType4List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.CITY = strCITY;
                                    items.YEAR = strYear;

                                    mType4ArrList.add(items);
                                    soap_error = "5";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching As on Date Renewed data !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("1")) {
                    ll_zone_type4.setVisibility(View.VISIBLE);
                    lbl_zone_type4.setVisibility(View.VISIBLE);
                    mDateRenewRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByZoneType4ArrayList:=>" + mType4ArrList.size());
                    System.out.println("ChannelByZoneType4ArrayData:=>" + mType4ArrList.toString());
                    System.out.println("mType4List:=>" + mType4List.size() + "" + mType4List.toString());

                    setChannelByZoneType4RecyclerView();
                } else if (s.equals("2")) {
                    ll_zone_type4.setVisibility(View.VISIBLE);
                    lbl_zone_type4.setVisibility(View.VISIBLE);
                    mDateRenewRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByMonType4ArrayList:=>" + mType4ArrList.size());
                    System.out.println("ChannelByMonType4ArrayData:=>" + mType4ArrList.toString());
                    System.out.println("mChZoneMonList:=>" + mType4List.size() + "" + mType4List.toString());

                    setChannelByMonthType4RecyclerView();
                } else if (s.equals("3")) {
                    ll_zone_type4.setVisibility(View.VISIBLE);
                    lbl_zone_type4.setVisibility(View.VISIBLE);
                    mDateRenewRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ZoneByMonType4ArrayList:=>" + mType4ArrList.size());
                    System.out.println("ZoneByMonList:=>" + mType4List.size() + "" + mType4List.toString());

                    setZoneByMonthType4RecyclerView();
                } else if (s.equals("4")) {
                    ll_zone_type4.setVisibility(View.VISIBLE);
                    lbl_zone_type4.setVisibility(View.VISIBLE);
                    mDateRenewRecycler.setVisibility(View.VISIBLE);

                    System.out.println("SegByMonType4ArrayList:=>" + mType4ArrList.size());
                    System.out.println("SegByMonList:=>" + mType4List.size() + "" + mType4List.toString());

                    setSegByMonthType4RecyclerView();
                } else if (s.equals("5")) {
                    ll_zone_type4.setVisibility(View.VISIBLE);
                    lbl_zone_type4.setVisibility(View.VISIBLE);
                    mDateRenewRecycler.setVisibility(View.VISIBLE);

                    System.out.println("CityByMonType4ArrayList:=>" + mType4ArrList.size());
                    System.out.println("CityByMonList:=>" + mType4List.size() + "" + mType4List.toString());

                    setCityByMonthType4RecyclerView();
                }
            }
        }
    }

    //For Chart Type N & typeOfDisplay 5
    private class GetChartNType5RenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mType5ArrList != null) {
                mType5ArrList.clear();
            }
            if (mType5List != null) {
                mType5List.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("GetChByZoneType5 ", "parameters: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChByZoneType5 Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                if (params[3].equals("1")) { //Channel By Zone
                    mType5ArrList = new ArrayList<>();
                    mType5List = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strVERTICAL = "", strEAST = "", strWEST = "", strCENTRAL = "", strNORTH = "", strSOUTH = "";
                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {

                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("EAST")) {
                                            strEAST = soapObject3.getPropertyAsString("EAST");
                                            headers.put("EAST", strEAST);
                                        }
                                        if (soapObject3.toString().contains("WEST")) {
                                            strWEST = soapObject3.getPropertyAsString("WEST");
                                            headers.put("WEST", strWEST);
                                        }
                                        if (soapObject3.toString().contains("CENTRAL")) {
                                            strCENTRAL = soapObject3.getPropertyAsString("CENTRAL");
                                            headers.put("CENTRAL", strCENTRAL);
                                        }
                                        if (soapObject3.toString().contains("NORTH")) {
                                            strNORTH = soapObject3.getPropertyAsString("NORTH");
                                            headers.put("NORTH", strNORTH);
                                        }
                                        if (soapObject3.toString().contains("SOUTH")) {
                                            strSOUTH = soapObject3.getPropertyAsString("SOUTH");
                                            headers.put("SOUTH", strSOUTH);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType5List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType5ArrList.add(items);

                                    soap_error = "1";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("2")) { //Channel BY Month
                    mType5ArrList = new ArrayList<>();
                    mType5List = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strVERTICAL = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "", strYear = "2017";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            // strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            // strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //  strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            // strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //    strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                //  strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                //    strYear = strOCT.substring(3, 4);
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType5List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType5ArrList.add(items);
                                    soap_error = "2";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("3")) { //Zone By Month
                    mType5List = new ArrayList<>();
                    mType5ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strZONE = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "",
                                    strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                PropertyInfo pi = new PropertyInfo();
                                soapObject2.getPropertyInfo(i, pi);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strZONE = soapObject3.getPropertyAsString("ZONE");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            //  strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //  strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //  strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //   strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //  strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //  strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType5List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.ZONE = strZONE;
                                    items.YEAR = strYear;

                                    mType5ArrList.add(items);
                                    soap_error = "3";// + "," + params[3];
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("4")) { //Segment By Month
                    mType5List = new ArrayList<>();
                    mType5ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strSEGMENT = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strSEGMENT = soapObject3.getPropertyAsString("SEGMENT");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType5List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.SEGMENT = strSEGMENT;
                                    items.YEAR = strYear;

                                    mType5ArrList.add(items);

                                    soap_error = "4";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("5")) { // City By Month
                    mType5List = new ArrayList<>();
                    mType5ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strCITY = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strCITY = soapObject3.getPropertyAsString("CITY");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType5List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.CITY = strCITY;
                                    items.YEAR = strYear;

                                    mType5ArrList.add(items);
                                    soap_error = "5";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Base as on date data !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("1")) {
                    ll_zone_type5.setVisibility(View.VISIBLE);
                    lbl_zone_type5.setVisibility(View.VISIBLE);
                    mBaseOnDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByZoneType5ArrayList:=>" + mType5ArrList.size());
                    //  System.out.println("ChannelByZoneType5ArrayData:=>" + mType5ArrList.toString());
                    System.out.println("mType5List:=>" + mType5List.size() + "" + mType5List.toString());

                    setChannelByZoneType5RecyclerView();
                } else if (s.equals("2")) {
                    ll_zone_type5.setVisibility(View.VISIBLE);
                    lbl_zone_type5.setVisibility(View.VISIBLE);
                    mBaseOnDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByMonthType5ArrayList:=>" + mType5ArrList.size());
                    //   System.out.println("ChannelByMonthType5ArrayList:=>" + mType5ArrList.toString());
                    System.out.println("mChMonthType5List:=>" + mType5List.size() + "" + mType5List.toString());

                    setChannelByMonthType5RecyclerView();
                } else if (s.equals("3")) {
                    ll_zone_type5.setVisibility(View.VISIBLE);
                    lbl_zone_type5.setVisibility(View.VISIBLE);
                    mBaseOnDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ZoneByMonthType5ArrayList:=>" + mType5ArrList.size());
                    //   System.out.println("ChannelByMonthType5ArrayList:=>" + mType5ArrList.toString());
                    System.out.println("zoneByMonthType5List:=>" + mType5List.size() + "" + mType5List.toString());

                    setZoneByMonthType5RecyclerView();
                } else if (s.equals("4")) {
                    ll_zone_type5.setVisibility(View.VISIBLE);
                    lbl_zone_type5.setVisibility(View.VISIBLE);
                    mBaseOnDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("SegByMonthType5ArrayList:=>" + mType5ArrList.size());
                    //   System.out.println("ChannelByMonthType5ArrayList:=>" + mType5ArrList.toString());
                    System.out.println("SegByMonthType5List:=>" + mType5List.size() + "" + mType5List.toString());

                    setSegByMonthType5RecyclerView();
                } else if (s.equals("5")) {
                    ll_zone_type5.setVisibility(View.VISIBLE);
                    lbl_zone_type5.setVisibility(View.VISIBLE);
                    mBaseOnDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("CityByMonType5ArrayList:=>" + mType5ArrList.size());
                    //   System.out.println("ChannelByMonthType5ArrayList:=>" + mType5ArrList.toString());
                    System.out.println("CityByMonType5List:=>" + mType5List.size() + "" + mType5List.toString());

                    setCityByMonthType5RecyclerView();
                }
            }
        }
    }

    //For Chart Type N & typeOfDisplay 6
    private class GetChartNType6RenewalFilteredData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(RenewalDashboardActivity.this, R.style.AlertDialogCustom);
                progressDialog.setMessage("Loading..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            if (mType6ArrList != null) {
                mType6ArrList.clear();
            }
            if (mType6List != null) {
                mType6List.clear();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GETRENEWFILDATA);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);
            request.addProperty("chartType", params[3]);
            request.addProperty("startDate", params[4]);
            request.addProperty("endDate", params[5]);
            request.addProperty("typeOfDisplay", params[6]);

            Log.v("GetChByZoneType6 ", "parameters: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GETRENEWFILDATA, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "ChByZoneType6 Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                if (params[3].equals("1")) { //Channel By ZOne
                    mType6ArrList = new ArrayList<>();
                    mType6List = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strVERTICAL = "", strEAST = "", strWEST = "", strCENTRAL = "", strNORTH = "", strSOUTH = "";
                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {

                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("EAST")) {
                                            strEAST = soapObject3.getPropertyAsString("EAST");
                                            headers.put("EAST", strEAST);
                                        }
                                        if (soapObject3.toString().contains("WEST")) {
                                            strWEST = soapObject3.getPropertyAsString("WEST");
                                            headers.put("WEST", strWEST);
                                        }
                                        if (soapObject3.toString().contains("CENTRAL")) {
                                            strCENTRAL = soapObject3.getPropertyAsString("CENTRAL");
                                            headers.put("CENTRAL", strCENTRAL);
                                        }
                                        if (soapObject3.toString().contains("NORTH")) {
                                            strNORTH = soapObject3.getPropertyAsString("NORTH");
                                            headers.put("NORTH", strNORTH);
                                        }
                                        if (soapObject3.toString().contains("SOUTH")) {
                                            strSOUTH = soapObject3.getPropertyAsString("SOUTH");
                                            headers.put("SOUTH", strSOUTH);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType6List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;
                                    mType6ArrList.add(items);
                                    soap_error = "1";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("2")) { //Channel By Month
                    mType6List = new ArrayList<>();
                    mType6ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strVERTICAL = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "", strYear = "2017";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            // strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            // strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            // strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            // strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            // strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            // strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                //  strYear = strOCT.substring(3, 4);
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            // strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mType6List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.VERTICAL = strVERTICAL;
                                    items.YEAR = strYear;

                                    mType6ArrList.add(items);
                                    soap_error = "2";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("3")) { //Zone By Month
                    mType6List = new ArrayList<>();
                    mType6ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strZONE = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "",
                                    strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                PropertyInfo pi = new PropertyInfo();
                                soapObject2.getPropertyInfo(i, pi);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strZONE = soapObject3.getPropertyAsString("ZONE");

                                        headers = new LinkedHashMap();
                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            //  strYear = strJAN.substring(3, 4);
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            //strYear = strFEB.substring(3, 4);
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            //  strYear = strMAR.substring(3, 4);
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            //  strYear = "20" + strAPR.substring(3, 4);
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            //  strYear = strMAY.substring(3, 4);
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            //   strYear = strJUN.substring(3, 4);
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            //  strYear = strJUL.substring(3, 4);
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            //  strYear = strAUG.substring(3, 4);
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            if (!soapObject3.getPropertyAsString("SEP17").equals("null")) {
                                                strSEP = soapObject3.getPropertyAsString("SEP17");
                                                // strYear = "20" + strSEP.substring(3, 4);
                                                headers.put("SEP", strSEP);
                                            }
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            if (!soapObject3.getPropertyAsString("OCT17").equals("null")) {
                                                strOCT = soapObject3.getPropertyAsString("OCT17");
                                                headers.put("OCT", strOCT);
                                            }
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            // strYear = strNOV.substring(3, 4);
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            //  strYear = strDEC.substring(3, 4);
                                            headers.put("DEC", strDEC);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType6List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.ZONE = strZONE;
                                    items.YEAR = strYear;

                                    mType6ArrList.add(items);
                                    soap_error = "3";// + "," + params[3];
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("4")) { //Segment By Month
                    mType6List = new ArrayList<>();
                    mType6ArrList = new ArrayList<>();
                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strSEGMENT = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strSEGMENT = soapObject3.getPropertyAsString("SEGMENT");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType6List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.SEGMENT = strSEGMENT;
                                    items.YEAR = strYear;

                                    mType6ArrList.add(items);

                                    soap_error = "4";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (params[3].equals("5")) { // City By Month
                    mType6List = new ArrayList<>();
                    mType6ArrList = new ArrayList<>();

                    try {
                        SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                        if (soapObject1.toString().equals("anyType{}")) {
                            soap_error = "0";
                        } else {
                            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                            String strYear = "2017", strCITY = "";
                            String strJAN = "", strFEB = "", strMAR = "", strAPR = "", strMAY = "", strJUN = "", strJUL = "", strAUG = "";
                            String strSEP = "", strOCT = "", strNOV = "", strDEC = "";

                            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strCITY = soapObject3.getPropertyAsString("CITY");

                                        headers = new LinkedHashMap();

                                        if (soapObject3.toString().contains("JAN17")) {
                                            strJAN = soapObject3.getPropertyAsString("JAN17");
                                            Log.v("strJAN ", " strJAN " + soapObject3.getPropertyAsString("JAN17"));
                                            headers.put("JAN", strJAN);
                                        }
                                        if (soapObject3.toString().contains("FEB17")) {
                                            strFEB = soapObject3.getPropertyAsString("FEB17");
                                            headers.put("FEB", strFEB);
                                        }
                                        if (soapObject3.toString().contains("MAR17")) {
                                            strMAR = soapObject3.getPropertyAsString("MAR17");
                                            headers.put("MAR", strMAR);
                                        }
                                        if (soapObject3.toString().contains("APR17")) {
                                            strAPR = soapObject3.getPropertyAsString("APR17");
                                            headers.put("APR", strAPR);
                                        }
                                        if (soapObject3.toString().contains("MAY17")) {
                                            strMAY = soapObject3.getPropertyAsString("MAY17");
                                            headers.put("MAY", strMAY);
                                        }
                                        if (soapObject3.toString().contains("JUN17")) {
                                            strJUN = soapObject3.getPropertyAsString("JUN17");
                                            headers.put("JUN", strJUN);
                                        }
                                        if (soapObject3.toString().contains("JUL17")) {
                                            strJUL = soapObject3.getPropertyAsString("JUL17");
                                            headers.put("JUL", strJUL);
                                        }
                                        if (soapObject3.toString().contains("AUG17")) {
                                            strAUG = soapObject3.getPropertyAsString("AUG17");
                                            headers.put("AUG", strAUG);
                                        }
                                        if (soapObject3.toString().contains("SEP17")) {
                                            strSEP = soapObject3.getPropertyAsString("SEP17");
                                            headers.put("SEP", strSEP);
                                        }
                                        if (soapObject3.toString().contains("OCT17")) {
                                            strOCT = soapObject3.getPropertyAsString("OCT17");
                                            headers.put("OCT", strOCT);
                                        }
                                        if (soapObject3.toString().contains("NOV17")) {
                                            strNOV = soapObject3.getPropertyAsString("NOV17");
                                            headers.put("NOV", strNOV);
                                        }
                                        if (soapObject3.toString().contains("DEC17")) {
                                            strDEC = soapObject3.getPropertyAsString("DEC17");
                                            headers.put("DEC", strDEC);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mType6List.add(headers);

                                    ChannelByZoneDataModel items = new ChannelByZoneDataModel();
                                    items.CITY = strCITY;
                                    items.YEAR = strYear;

                                    mType6ArrList.add(items);
                                    soap_error = "5";
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Found !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while fetching Achieved as on date data !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("1")) {
                    ll_zone_type6.setVisibility(View.VISIBLE);
                    lbl_zone_type6.setVisibility(View.VISIBLE);
                    mAchievedAsDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByZoneType6ArrayList:=>" + mType6ArrList.size());
                    System.out.println("ChannelByZoneType6ArrayList:=>" + mType6ArrList.toString());
                    System.out.println("mType6List:=>" + mType6List.size() + "" + mType6List.toString());

                    setChannelByZoneType6RecyclerView();
                } else if (s.equals("2")) {
                    ll_zone_type6.setVisibility(View.VISIBLE);
                    lbl_zone_type6.setVisibility(View.VISIBLE);
                    mAchievedAsDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ChannelByMonthType6ArrayList:=>" + mType6ArrList.size());
                    System.out.println("ChannelByMonthType6ArrayList:=>" + mType6ArrList.toString());
                    System.out.println("mChMonthType6List:=>" + mType6List.size() + "" + mType6List.toString());

                    setChannelByMonthType6RecyclerView();
                } else if (s.equals("3")) {
                    ll_zone_type6.setVisibility(View.VISIBLE);
                    lbl_zone_type6.setVisibility(View.VISIBLE);
                    mAchievedAsDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("ZoneByMonthType6ArrayList:=>" + mType6ArrList.size());
                    System.out.println("ZoneByMonthType6ArrayList:=>" + mType6ArrList.toString());
                    System.out.println("ZoneByMonthType6List:=>" + mType6List.size() + "" + mType6List.toString());

                    setZoneByMonthType6RecyclerView();
                } else if (s.equals("4")) {
                    ll_zone_type6.setVisibility(View.VISIBLE);
                    lbl_zone_type6.setVisibility(View.VISIBLE);
                    mAchievedAsDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("SegByMonType6ArrayList:=>" + mType6ArrList.size());
                    System.out.println("SegByMonType6List:=>" + mType6List.size() + "" + mType6List.toString());

                    setSegByMonthType6RecyclerView();
                } else if (s.equals("5")) {
                    ll_zone_type6.setVisibility(View.VISIBLE);
                    lbl_zone_type6.setVisibility(View.VISIBLE);
                    mAchievedAsDateRecycler.setVisibility(View.VISIBLE);

                    System.out.println("CityByMonthType6ArrayList:=>" + mType6ArrList.size());
                    System.out.println("CityByMonthType6List:=>" + mType6List.size() + "" + mType6List.toString());

                    setCityByMonthType6RecyclerView();
                }
            }
        }
    }

    private class GetRenewalFilterSelection extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_RENEWFILTER);
            request.addProperty("sseId", params[0]);
            request.addProperty("userId", params[1]);
            request.addProperty("condition", params[2]);

            Log.v("GetRenewFiltrSelection ", "params: " + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_RENEWFILTER, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "GetRenewFiltrSelection Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    soap_error = "Error";
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject AgentData = null;
                        SoapObject root;
                        for (int i = 1; i < result.getPropertyCount(); i++) {


                            root = (SoapObject) result.getProperty(i);

                            AgentData = (SoapObject) root.getProperty(0);

                            for (int prop = 0; prop < AgentData.getPropertyCount(); prop++) {

                                PropertyInfo pi = new PropertyInfo();

                                AgentData.getPropertyInfo(prop, pi);
                                if (pi.getName().equals("Vertical")) {
                                    SoapObject propertyTest = (SoapObject) pi.getValue();
                                    try {
                                        Object property1 = "";
                                        //= propertyTest.getProperty("Customer_Proposer");
                                        if (propertyTest.toString().contains("EXPD_DEAL_VERTICAL_DESC")) {
                                            property1 = propertyTest.getProperty("EXPD_DEAL_VERTICAL_DESC");
                                        } else {
                                            property1 = "NA";
                                        }

                                        mVerticalList.add(property1.toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                                if (pi.getName().equals("Segment_x0020_Type")) {
                                    SoapObject propertyTest = (SoapObject) pi.getValue();
                                    try {
                                        Object property1 = "";
                                        //= propertyTest.getProperty("Customer_Proposer");
                                        if (propertyTest.toString().contains("SEGMENT_TYPE")) {
                                            property1 = propertyTest.getProperty("SEGMENT_TYPE");
                                        } else {
                                            property1 = "NA";
                                        }

                                        mSegmentList.add(property1.toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (pi.getName().equals("Zone")) {
                                    SoapObject propertyTest = (SoapObject) pi.getValue();
                                    try {
                                        Object property1 = "";
                                        //= propertyTest.getProperty("Customer_Proposer");
                                        if (propertyTest.toString().contains("EXPD_BRANCH_REGION")) {
                                            property1 = propertyTest.getProperty("EXPD_BRANCH_REGION");
                                        } else {
                                            property1 = "NA";
                                        }

                                        mZoneList.add(property1.toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                if (pi.getName().equals("City")) {
                                    SoapObject propertyTest = (SoapObject) pi.getValue();
                                    try {
                                        Object property1 = "";
                                        //= propertyTest.getProperty("Customer_Proposer");
                                        if (propertyTest.toString().contains("EXPD_CITY_NAME")) {
                                            property1 = propertyTest.getProperty("EXPD_CITY_NAME");
                                        } else {
                                            property1 = "NA";
                                        }

                                        mCityList.add(property1.toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else if (s.equals("0")) {
                Toast.makeText(getApplicationContext(), "No Details Found", Toast.LENGTH_SHORT).show();
            } else {
                renewal_filter_layout.setVisibility(View.VISIBLE);

                HashMap<String, List<String>> mDropDownList = new HashMap<>();


                if (mVerticalList != null) {
                    mDropDownList.put("VERTICAL", mVerticalList);
                }
                if (mSegmentList != null) {
                    mDropDownList.put("SEGMENT TYPE", mSegmentList);
                }
                if (mZoneList != null) {
                    mDropDownList.put("ZONE", mZoneList);
                }
                if (mCityList != null) {
                    mDropDownList.put("CITY", mCityList);
                }

                List<String> mStringList = new ArrayList<>(mDropDownList.keySet());
                mStringList.add(0, "Select Type");

                singleSelSpinnerAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.renewl_filter_spinner_item, mStringList);
                singleSelSpinnerAdapter.setDropDownViewResource(R.layout.renewal_spinner_dropdown_text);
                singleSelcSpinner.setAdapter(singleSelSpinnerAdapter);
                singleSelSpinnerAdapter.notifyDataSetChanged();

                setRenewalFilterMultiSelectOptions();
            }
        }
    }
}
