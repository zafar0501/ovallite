package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.EtrackerHomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.EtrackerExistingAgentAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackerexistagentListData;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.VerticalSpaceItemDecorartion;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getExistingUser.METHOD_GET_EX_USER;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getExistingUser.SOAP_ACTION_GET_EX_USER;

/**
 * Created by Meenakshi Aher on 29/05/2017.
 */

public class ExistingAgentListActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";

    String TAG_Soap_Response;
    RelativeLayout reliu;
    ConsUrl consUrl;
    ArrayList<EtrackerexistagentListData> ExistAgentItemlist = new ArrayList<EtrackerexistagentListData>();
    ArrayList<EtrackerexistagentListData> ExistAgentArrayList = new ArrayList<EtrackerexistagentListData>();
    RecyclerView existagentrecyclerview;
    LinearLayoutManager mLayoutManager;
    EtrackerExistingAgentAdapter existagentadapter;
    ArrayList<String> dataList = new ArrayList<String>();
    TextView mTitle;
    ImageView addagent;
    String ssid, Loginname, Loginntid;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_existagentlist);
        consUrl = new ConsUrl();
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        getSharedpreferebnceData();
        setAppToolbar();
        findViews();
      //  WebserviceCall();
    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        addagent = (ImageView) toolbar.findViewById(R.id.addagent);
        addagent.setVisibility(View.VISIBLE);

        SetActionBarTitle("Existing Agent List");
        addagent.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
               /* finish();
                //overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
                overridePendingTransition(  R.anim.slide_in_left, R.anim.slide_out_left);*/
            }
        } );

    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void WebserviceCall() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(ExistingAgentListActivity.this)) {
            try {
                 String existagentparameter = ssid + "_" + Loginname;
               // String existagentparameter = "1968__sandeep shewale";
                System.out.println("existagentparameter:=>" + existagentparameter);

                // edited by Pooja Patil at 08-06-17
                new GetExistingAgentList().execute(existagentparameter);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }



    private void findViews() {
        reliu = (RelativeLayout) findViewById(R.id.reliu);



        existagentrecyclerview = (RecyclerView) findViewById(R.id.existagentList);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        existagentrecyclerview.setHasFixedSize(true);
        existagentrecyclerview.setLayoutManager(new LinearLayoutManager(this));
        existagentrecyclerview.setItemAnimator(new DefaultItemAnimator());
        existagentrecyclerview.addItemDecoration(new VerticalSpaceItemDecorartion(10));
        mLayoutManager = new LinearLayoutManager(this);
        existagentrecyclerview.setLayoutManager(mLayoutManager);

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        WebserviceCall();
                    }
                }
        );

       /* existagentrecyclerview.setOnClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        // do it
                    }
                }
        );*/




    }


    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);


    }

    @Override
    public void onClick(View v) {
        if (v == addagent) {

            Constants.callIntent(this, AddEtrackerExistinguserActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        }
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
       // WebserviceCall();
        swipeRefreshLayout.setRefreshing( false );
    }



  /*  private void getExistingAgentList(String params) {

        if (ExistAgentItemlist != null) {
            ExistAgentItemlist.clear();
        }

        String API = consUrl.WebUrl;

        Retrofit retrofit = new Retrofit.Builder().baseUrl( API ).addConverterFactory( GsonConverterFactory.create() ).build();

        Webservices webservices = retrofit.create( Webservices.class );
        Call call = webservices.getExistingAgentlist( params );
        call.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit.Response<ResponseBody> response, Retrofit retrofit) {


                ResponseBody body = response.body();
                if (response.body() == null) {
                    Toast.makeText( getApplicationContext(), "Error:" + response.message(), Toast.LENGTH_SHORT ).show();
                    return;
                }
                try {
                    BufferedReader reader = new BufferedReader( new InputStreamReader( body.byteStream() ) );
                    StringBuilder out = new StringBuilder();
                    String newline = System.getProperty( "line.separator" );
                    String line;
                    while ((line = reader.readLine()) != null) {
                        out.append( line );
                        out.append( newline );
                    }
                    JSONObject jsonobject = new JSONObject( out.toString() );
                    Log.i( "JSON:=>", jsonobject.toString());
                    System.out.println( "Response:=>" + jsonobject.toString() );
                   *//* if (jsonobject.length()==0 || jsonobject.toString().equals("{}")) {
*//**//*
                        JSONArray jsonArrayHot = jsonobject.getJSONArray( "ETDetails" );
                        for (int i = 0; i < jsonArrayHot.length(); i++) {
                            JSONObject myJSON = jsonArrayHot.getJSONObject( i );

                            EtrackerexistagentListData items = new EtrackerexistagentListData();
                            items.ID = myJSON.getString( "ID" );
                            items.ROLE = myJSON.getString( "ROLE" );
                            items.VERTICAL = myJSON.getString( "VERTICAL" );
                            items.SUBVERTICAL = myJSON.getString( "SUBVERTICAL" );
                            items.ACTIONTAKENREPORT = myJSON.getString( "ACTIONTAKENREPORT" );
                            items.BRANCHEMPCODE = myJSON.getString( "BRANCHEMPCODE" );
                            items.CREATEDBY = myJSON.getString( "CREATEDBY" );
                            items.DATEOFMEETING = myJSON.getString( "DATEOFMEETING" );
                            items.MEETINGATTENDEDBY = myJSON.getString( "MEETINGATTENDEDBY" );
                            items.MEETINGTYPE = myJSON.getString( "MEETINGTYPE" );
                            items.MINUTESOFMEETING = myJSON.getString( "MINUTESOFMEETING" );
                            items.PEOPLEMET = myJSON.getString( "PEOPLEMET" );
                            items.TIME = myJSON.getString( "TIME" );
                            items.VENUE = myJSON.getString( "VENUE" );
                            items.ISACTIVE = myJSON.getString( "ISACTIVE" );
                            items.RN = myJSON.getString( "RN" );

                            dataList.add( items.PEOPLEMET );
                            ExistAgentItemlist.add( items );

                            Log.i( "ExistAgentItemlist:=>", ExistAgentItemlist.toString() );

                        }
                        swipeRefreshLayout.setRefreshing( false );

                        System.out.println( "ExistAgentItemlist:=>" + ExistAgentItemlist.toString() );
                        System.out.println( "ExistAgentItemlistSizzzze:=>" + ExistAgentItemlist.size() );

                    if(ExistAgentArrayList!=null){
                        ExistAgentArrayList.clear();
                    }

                        if (ExistAgentItemlist.size() != 0)
                            for (EtrackerexistagentListData obj : ExistAgentItemlist)
                                ExistAgentArrayList.add( obj );

                        System.out.println( "ExistAgentArrayList:=>" + ExistAgentArrayList.size() );

                        loaddata();

                        Log.i( "status:=>", "1" );

                  *//**//*  } else {
                        Constants.snackbar( reliu, "No Record Found." );
                    }*//**//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i( "Error:", "," + t.getMessage() );
                swipeRefreshLayout.setRefreshing( false );
            }
        } );
    }*/

    private void loaddata() {

        if (ExistAgentArrayList.isEmpty() || ExistAgentArrayList == null || ExistAgentArrayList.equals("")) {
            System.out.println("EmptySize:=>" + ExistAgentArrayList.size());
        } else {
            existagentadapter = new EtrackerExistingAgentAdapter(this, ExistAgentArrayList, existagentrecyclerview, R.layout.template_existagentuser, dataList);
            existagentrecyclerview.setAdapter(existagentadapter);
            existagentadapter.notifyDataSetChanged();


            existagentrecyclerview.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), existagentrecyclerview, new ClickListener() {
                @Override
                public void onClick(View view,final  int position) {
                    LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                    parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("position 11: "+position);
                            existagentadapter.setPopUpView(position);
                        }
                    });

                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
        }
    }

    @Override
    public void onBackPressed() {

        Constants.callIntent(this, EtrackerHomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

 /*   private void voidinitpopupwindow(View v) {
        LayoutInflater inflater = (LayoutInflater) getSystemService( this.LAYOUT_INFLATER_SERVICE );
        popuplayout1 = inflater.inflate( R.layout.popup_existsagentdetail, null );
        connectiondialogue = new Dialog( this );
        popuplayout1.setFocusable( false );
        TextView tv;
        Button exit;
        exit = (Button) popuplayout1.findViewById( R.id.btn_exit );
        tv = (TextView) popuplayout1.findViewById( R.id.tv_actionreport );
        try {
            exit.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectiondialogue.dismiss();
                }
            } );

        } catch (Exception e) {
            e.printStackTrace();

        }
        connectiondialogue.getWindow().requestFeature( Window.FEATURE_NO_TITLE );
        connectiondialogue.setContentView( popuplayout1 );
        connectiondialogue.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN );
        connectiondialogue.setCancelable( true );
        connectiondialogue.show();

    }*/
 // edited by Pooja Patil at 08-06-17
    //start pooja
    public class GetExistingAgentList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog = new ProgressDialog( ExistingAgentListActivity.this );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
           /* SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("type", params[2]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            request.addProperty("condition", params[3]);*/

            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_EX_USER);
            request.addProperty("sseId", params[0]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_GET_EX_USER, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        TAG_Soap_Response = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strPEOPLEMET = "",strMEETINGTYPE = "",strVENUE = "",strROLE="",strVERTICAL="";
                        String strSUBVERTICAL="",strDATEOFMEETING="";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strPEOPLEMET = soapObject3.getPropertyAsString("PEOPLEMET");
                                    strVENUE = soapObject3.getPropertyAsString("VENUE");
                                    strMEETINGTYPE = soapObject3.getPropertyAsString("MEETINGTYPE");
                                    strROLE = soapObject3.getPropertyAsString("ROLE");
                                    strVERTICAL = soapObject3.getPropertyAsString("VERTICAL");
                                    strSUBVERTICAL = soapObject3.getPropertyAsString("SUBVERTICAL");
                                    strDATEOFMEETING = soapObject3.getPropertyAsString("DATEOFMEETING");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("MEETINGTYPE" + i, strMEETINGTYPE);
                                editor.putString("VENUE" + i, strVENUE);
                                editor.putString("PEOPLEMET" + i, strPEOPLEMET);
                                editor.putString("ROLE" + i, strROLE);
                                editor.putString("VERTICAL" + i, strVERTICAL);
                                editor.putString("SUBVERTICAL" + i, strSUBVERTICAL);
                                editor.putString("DATEOFMEETING" + i, strDATEOFMEETING);

                                // Log.i("TAG", "i :" + i + " MEETING_CLIENT_NAME :" + strCLIENT_NAME);
                                editor.commit();

                                EtrackerexistagentListData items = new EtrackerexistagentListData();
                                items.MEETINGTYPE = strMEETINGTYPE;
                                items.PEOPLEMET = strPEOPLEMET;
                                items.VENUE = strVENUE;
                                items.ROLE=strROLE;
                                items.VERTICAL=strVERTICAL;
                                items.SUBVERTICAL=strSUBVERTICAL;
                                items.DATEOFMEETING=strDATEOFMEETING;

                                dataList.add(items.PEOPLEMET);
                                ExistAgentItemlist.add(items);



                             /*    String str_NAME = "";
                                // String[] splited = strCLIENT_NAME.split("\\s+");


                               if (splited.length == 1) {
                                    String p1 = splited[0];
                                    str_NAME = String.valueOf(p1.charAt(0));
                                } else

                                {
                                    String p1 = splited[0];
                                    String p2 = splited[splited.length - 1];
                                    str_NAME = String.valueOf(p1.charAt(0)) + String.valueOf(p2.charAt(0));
                                }*/
                              /*  Name = Name + str_NAME + "!";
                                CLIENT_NAME = CLIENT_NAME + strCLIENT_NAME + "!";
                                MEETING_ADDRESS = MEETING_ADDRESS + strMEETING_ADDRESS + "!";
                                MEETING_STATUS = MEETING_STATUS + strMEETING_STATUS + "!";


                                TAG_Soap_Response = Name + "!!" + CLIENT_NAME + "!!" + MEETING_ADDRESS + "!!" + MEETING_STATUS;
*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("0")) {

            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (ExistAgentArrayList != null) {
                    ExistAgentArrayList.clear();
                }

                if (ExistAgentItemlist.size() != 0)
                    for (EtrackerexistagentListData obj : ExistAgentItemlist)
                        ExistAgentArrayList.add(obj);

                System.out.println("ExistAgentArrayList:=>" + ExistAgentArrayList.size());
                System.out.println("dataList:=>" + dataList.size());

                loaddata();

                Log.i("status:=>", "1");
            }
           /* if(progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (s.equals(""))
            {
                barDataSet1 = new BarDataSet(group1, "CurrentYear");
                barDataSet1.setColors(new int[]{Color.rgb( 102,180,216 )});
                if(group1.isEmpty())
                {
                    barChart.setVisibility( View.INVISIBLE );
                    Toast.makeText(Claims_Home_Activity.this,"No Business Yet",Toast.LENGTH_SHORT ).show();
                }
                ArrayList<BarDataSet> dataset = new ArrayList<>();
                dataset.add(barDataSet1);



            }else
            {
                Toast.makeText(ExistingAgentListActivity.this,"No Business Yet",Toast.LENGTH_SHORT ).show();
            }*/


        }
    }
    //end Pooja

}
