package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.NotificationListAdapter;
import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Entity.NotificationListData;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetMobileNotifications.METHOD_GET_NOTIFICATIONS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.GetMobileNotifications.SOAP_ACTION_GET_NOTIFICATIONS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;

public class NotificationListActivity extends AppCompatActivity {
    public static final String MY_PREFS_NAME = "HdfcErgo";
    private String ssid;
    private ConsUrl consUrl;
    private RecyclerView mNotificationRecycler;
    private LinearLayoutManager mLayoutManager;
    private SharedPreferences sharedPreferences;
    private ArrayList<NotificationListData> notifItemList;
    private ArrayList<NotificationListData> notifArrayList;
    private ArrayList<String> dataList;
    private NotificationListAdapter mAdapter;
    private Toolbar mToolbar;
    private TextView mTitle, txtNoData;
    private LinearLayout ll_no_data;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_notification_list);

        consUrl = new ConsUrl();
        getLoginData();
        setAppToolbar();
        getMobileNotifications();
        attachViews();
    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Notifications");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    private void attachViews() {
        notifItemList = new ArrayList<>();
        notifArrayList = new ArrayList<>();
        dataList = new ArrayList<>();

        txtNoData = (TextView) findViewById(R.id.tv_no_data);
        ll_no_data = (LinearLayout) findViewById(R.id.ll_no_data);

        mNotificationRecycler = (RecyclerView) findViewById(R.id.notification_recyclerview);
        mNotificationRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(NotificationListActivity.this);
        mNotificationRecycler.setLayoutManager(mLayoutManager);

    }

    private void getMobileNotifications() {
        if (Constants.isNetworkInfo(NotificationListActivity.this)) {
            try {
                new GetMobileNotifications().execute(ssid);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getLoginData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(NotificationListActivity.this);
        ssid = pref_securitypin.getString("loginssid", "");
        System.out.println("Loginssid:=>" + ssid);

    }

    private void loaddata() {
        if (notifArrayList.isEmpty() || notifArrayList == null || notifArrayList.equals("")) {
            System.out.println("EmptySize:=>" + notifArrayList.size());
        } else {
            mAdapter = new NotificationListAdapter(NotificationListActivity.this, notifArrayList, dataList);
            mNotificationRecycler.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.callIntent(NotificationListActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private class GetMobileNotifications extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NotificationListActivity.this, R.style.AlertDialogCustom);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_NOTIFICATIONS);

            request.addProperty("SSE_ID", params[0]);
            Log.v("GetMobileNotifications "," SSE_ID "+params[0]);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_NOTIFICATIONS, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }

            if (soap_error.equals("")) {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "Response getMobileNotifications:=>" + result.toString());
                    soap_error = result.toString();
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    soap_error = "Error";
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        String strUSERNAME = "", strMESSAGE = "", strID = "", strCREATED_BY = "";
                        String strCREATED_DATE = "", strSSE_ID = "";
                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                            try {
                                if (soapObject3.getProperty(0).equals("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {
                                        strSSE_ID = soapObject3.getPropertyAsString("SSEID");
                                        strUSERNAME = soapObject3.getPropertyAsString("USERNAME");
                                        strMESSAGE = soapObject3.getPropertyAsString("NOTIFYTEXT");
                                        strCREATED_BY = soapObject3.getPropertyAsString("CREATEDBY");
                                        strCREATED_DATE = soapObject3.getPropertyAsString("CREATEDDATE");
                                        strID = soapObject3.getPropertyAsString("ID");

                                        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();

                                        editor.putString("USERNAME" + i, strUSERNAME);
                                        editor.putString("MESSAGE" + i, strMESSAGE);
                                        editor.putString("ID" + i, strID);
                                        editor.putString("CREATED_BY" + i, strCREATED_BY);
                                        editor.putString("CREATED_DATE" + i, strCREATED_DATE);
                                        editor.putString("SSEID" + i, strSSE_ID);
                                        editor.commit();


                                        NotificationListData items = new NotificationListData();
                                        items.USERNAME = strUSERNAME;
                                        items.MESSAGE = strMESSAGE;
                                        items.ID = strID;
                                        items.CREATED_BY = strCREATED_BY;
                                        items.CREATED_DATE = strCREATED_DATE;
                                        items.SSEID = strSSE_ID;


                                        dataList.add(items.MESSAGE);
                                        notifItemList.add(items);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                soap_error = "Error";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;

            }
            System.out.println("onPostExecNotification: result " + s);

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    mNotificationRecycler.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");
                    Toast.makeText(getApplicationContext(), "No Data Available", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    mNotificationRecycler.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.VISIBLE);
                    txtNoData.setText("Error while fetching Notifications in Oval Lite !!");
                    Toast.makeText(getApplicationContext(), "Error while fetching Notifications in Oval Lite !!", Toast.LENGTH_SHORT).show();
                } else {
                    ll_no_data.setVisibility(View.GONE);
                    mNotificationRecycler.setVisibility(View.VISIBLE);
                    if (notifItemList != null) {
                        notifArrayList.clear();
                    }
                    if (notifItemList.size() != 0)
                        for (NotificationListData obj : notifItemList)
                            notifArrayList.add(obj);

                    System.out.println("NotificationArrayList:=>" + notifArrayList.size());
                    System.out.println("dataList:=>" + dataList.size());

                    loaddata();
                }
            }

        }
    }
}
