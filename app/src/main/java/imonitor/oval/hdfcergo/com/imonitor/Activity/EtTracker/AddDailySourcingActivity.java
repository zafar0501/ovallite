package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.DailySourceListAddAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.DailySourceDataAddList;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.GPS.LocationAddress;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertDailySource.METHOD_INSERT_SOURCE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertDailySource.SOAP_ACTION_SOURCE;

/**
 * Created by Pooja Patil on 20/06/2017.
 */

/*Methods used in AddDailySourcingActivity
*
*
*
*
* */
public class AddDailySourcingActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";

    Spinner sp_prod_name;
    EditText edtPolicySrcd, edtPremium;
    ArrayAdapter<String> prodAdapter;
    Button btnAdd, btnSubmit;
    RecyclerView mRecyclerView;
    DailySourceListAddAdapter mAdapter;
    LinearLayoutManager mLayoutManager;
    DividerItemDecoration dividerItemDecoration;
    ArrayList<DailySourceDataAddList> mListItems;
    DailySourceDataAddList list;
    ConsUrl consUrl;
    Context context;
    GPSTracker gps;
    String venueAddr, ssid, Loginname, Loginntid;
    String productName, policySourced, premium;
    SharedPreferences sharedPreferences;
    RecyclerView.ViewHolder holder;
    String prodName = "", policy, premn;
    Toolbar toolbar;
    TextView mTitle;
    TextView txtView;
    String lattitude, longitude;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_daily_sourcing);

        consUrl = new ConsUrl();
        context = this;
        setAppToolbar();
        // getLocation();
        getSharedpreferebnceData();
        initializeComponents();
        setUpRecyclerView();
        setClickListeners();
        getSharedprefLatLong();


    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        SetActionBarTitle("Add Daily Sourcing");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setClickListeners() {

        sp_prod_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String productName = sp_prod_name.getSelectedItem().toString();
            //    System.out.println("productNameSelected:=>" + productName.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnAdd.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);


    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    private void initializeComponents() {
        mListItems = new ArrayList<>();
        sp_prod_name = (Spinner) findViewById(R.id.sp_product_name);
        edtPolicySrcd = (EditText) findViewById(R.id.edt_policy_srcd);
        edtPremium = (EditText) findViewById(R.id.edt_premium);
        btnAdd = (Button) findViewById(R.id.btn_add);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        // txtView = (TextView) findViewById(R.id.txtView);


        prodAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, getResources().getStringArray(R.array.d_prod_name));
        prodAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp_prod_name.setAdapter(prodAdapter);

    }

    private void getSharedprefLatLong() {
        String TAG = "Inside getSharedprefLatLong ";
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        lattitude = preferences.getString("Latitude", "");
        longitude = preferences.getString("Longitude", "");

    //    System.out.println(TAG + " Latitude: =>" + lattitude);
     //   System.out.println(TAG + " Longitude: =>" + longitude);
    }

    private void getLocation() {
    //    System.out.println("InsideGetLocation");
        gps = new GPSTracker(AddDailySourcingActivity.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {


            // check if GPS enabled
            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

               /* LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(latitude, longitude,
                        getApplicationContext(), new GeocoderHandler());

                */
                if (latitude == 0.0 && longitude == 0.0) {
                    Toast.makeText(getApplicationContext(), "Cant get Location !! ", Toast.LENGTH_SHORT).show();

                } else {
                    venueAddr = LocationAddress.getCompleteAddressString(context, latitude, longitude);

                    // \n is for new line
               /* Toast.makeText( getApplicationContext(), "Your Location is - \nLat: "
                        + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG ).show();*/

                   // System.out.println("Mlatitude:=>" + latitude + "\n " + "Mlaongotude:=>" + longitude);
                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

//        System.out.println("Loginname 11:=>" + Loginname);
//        System.out.println("Loginssid 11:=>" + ssid);
//        System.out.println("Loginntid 11:=>" + Loginntid);
    }

    private void setUpRecyclerView() {

        mRecyclerView = (RecyclerView) findViewById(R.id.d_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // mRecyclerView.addItemDecoration(new VerticalSpaceItemDecorartion(10));

        //   mAdapter = new DailySourceListAddAdapter(this, mListItems);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        // addListItems();
        mRecyclerView.setLayoutManager(mLayoutManager);
        //  mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_add:
                if (sp_prod_name.getSelectedItem().toString().equals("Select Product")) {
                    Toast.makeText(getApplicationContext(), "Please select Product", Toast.LENGTH_SHORT).show();
                } else if (edtPolicySrcd.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter no. of policies", Toast.LENGTH_SHORT).show();
                } else if (edtPremium.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter premium", Toast.LENGTH_SHORT).show();
                } else {
                    policySourced = edtPolicySrcd.getText().toString();
                    premium = edtPremium.getText().toString();
                    productName = sp_prod_name.getSelectedItem().toString();

                 //   System.out.println("policySourced " + policySourced + " premium " + premium + " prodName " + productName);

                    //  addDataToList(productName, policySourced, premium);
                    //list = new DailySourceDataAddList(productName, policySourced, premium);
                    //for(int i=0; i<mListItems.size(); i++){
                    list = new DailySourceDataAddList();
                    list.setProduct_name(productName);
                    list.setPolicy_sourced(policySourced);
                    list.setPremium(premium);

                    mListItems.add(list);

                    mAdapter = new DailySourceListAddAdapter(context, mListItems);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                    edtPremium.setText("");
                    edtPolicySrcd.setText("");
                    sp_prod_name.setSelection(0);

                }

                break;

            case R.id.btn_submit:
           /*     DailySourceDataAddList listItems = new DailySourceDataAddList();
                for (int childCount = mRecyclerView.getChildCount(), i = 0; i < childCount; ++i) {
                    holder = mRecyclerView.getChildViewHolder(mRecyclerView.getChildAt(i));
                }*/

              /*  for (DailySourceDataAddList list : mListItems) {
                    prodName = list.getProduct_name();
                    policy = list.getPolicy_sourced();
                    premn = list.getPremium();

                    //  mListItems.get(0);
                    //  System.out.println(list.getProduct_name());
                    // System.out.println(prodName + " | "+list.getProduct_name());
                    System.out.println("11 ProdName: " + prodName);


                }*/

                if (mListItems.isEmpty()) {
               /* if((sp_prod_name.getSelectedItem().toString().equals("Select Product") &&
                        (edtPolicySrcd.getText().toString().equals("")) &&
                        (edtPremium.getText().toString().equals("")))){*/
                    Toast.makeText(getApplicationContext(), "Please select product,policies,premium and add it", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < mListItems.size(); i++) {

                        //if i=0
                        //prodName = mListItems.get(i).getProduct_name();
                        //else
                        //prodName = prodName +" | "+ mListItems.get(i).getProduct_name();

                        if (i == 0) {
                            prodName = mListItems.get(i).getProduct_name();
                            policy = mListItems.get(i).getPolicy_sourced();
                            premn = mListItems.get(i).getPremium();
                        } else {
                            prodName = prodName + "|" + mListItems.get(i).getProduct_name();
                            policy = policy + "|" + mListItems.get(i).getPolicy_sourced();
                            premn = premn + "|" + mListItems.get(i).getPremium();
                        }
                        //  prodName = prodName +" | "+ mListItems.get(i).getProduct_name();
                        //   policy = mListItems.get(i).getPolicy_sourced();
                        // premn = mListItems.get(i).getPremium();

                        //   System.out.println("22 ProdName: " + prodName +" | ");

                  /*  arr = new ArrayList<>();
                    arr.add(policy);
                    System.out.println("arr " + arr);*/

                        //String s = mListItems.get(i);
                    }

//                    System.out.println("23 ProdName: " + prodName);
//                    System.out.println("23 policy: " + policy);
//                    System.out.println("23 Premium: " + premn);

                    confirmSubmit();

                }
                break;
        }

    }

    private void confirmSubmit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddDailySourcingActivity.this,R.style.AlertDialogCustom);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to send the data ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        System.out.println("24 ProdName: " + prodName);
//                        System.out.println("24 policy: " + policy);
//                        System.out.println("24 Premium: " + premn);
                        // finishAffinity();
                        new InsertDailySource().execute(prodName, policy, premn, ssid);
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Constants.callIntent(AddDailySourcingActivity.this, GetDailySourcingListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public class InsertDailySource extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddDailySourcingActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait.. Data is submitting");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_INSERT_SOURCE);
            request.addProperty("PRODUCTNAME", params[0]);
            request.addProperty("POLICYSOURCED", params[1]);
            request.addProperty("PREMIUM", params[2]);
            request.addProperty("CREATEDBY_SSEID", params[3]);
            request.addProperty("LATITUDE", lattitude);
            request.addProperty("LONGITUDE", longitude);
            request.addProperty("CREATEDBY_NAME", Loginname);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SOURCE, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }

            if (soap_error.equals("")) {

                SoapPrimitive result = null;
                try {
                    result = (SoapPrimitive) envelope.getResponse();
                    System.out.println("Response Insert DailySourcing:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }
            } else {
                soap_error = "Error";
            }
            return soap_error;


        }

        @Override
        protected void onPostExecute(String s) {
            //super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "Data Not Inserted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Data Inserted Succesfully", Toast.LENGTH_SHORT).show();
                Constants.callIntent(AddDailySourcingActivity.this, GetDailySourcingListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }

        }
    }
}