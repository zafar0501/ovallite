package imonitor.oval.hdfcergo.com.imonitor.AwesomeSplashLibrary.utils;


import imonitor.oval.hdfcergo.com.imonitor.AwesomeSplashLibrary.cnst.Flags;
import imonitor.oval.hdfcergo.com.imonitor.AwesomeSplashLibrary.model.ConfigSplash;

/**
 * Created by varsovski on 27-Sep-15.
 */
public class ValidationUtil {

    public static int hasPath(ConfigSplash cs) {
        if (cs.getPathSplash().isEmpty())
            return Flags.WITH_LOGO;
        else
            return Flags.WITH_PATH;
    }
}
