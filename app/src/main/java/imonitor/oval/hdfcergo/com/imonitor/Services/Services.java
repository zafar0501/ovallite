package imonitor.oval.hdfcergo.com.imonitor.Services;

/**
 * Created by Zafar.Hussain on 15/06/2017.
 */

public class Services {
    public static final String NAMESPACE = "http://tempuri.org/";

    public class updateLead {
        // when saving  customer
        public static final String SOAP_ACTION_UPDATE_LEAD = NAMESPACE + "UpdateNewETDetails";
        public static final String METHOD_UPDATE_LEAD = "UpdateNewETDetails";
    }

    public class getStatusDetails {
        public static final String METHOD_STATUS = "GetStatusDetails";
        public static final String SOAP_ACTION_STATUS = NAMESPACE + "GetStatusDetails";
    }

    public class insertNewETDetails {
        public static final String SOAP_ACTION_INSERT = NAMESPACE + "insertNewETDetails";
        public static final String METHOD_INSERT = "insertNewETDetails";

    }

    public class insertNewUser {
        // when adding new agent
        public static final String SOAP_ACTION_NEWAGENT_SAVE = NAMESPACE + "insertNewUser";
        public static final String METHOD_NAME_NEWAGENT_SAVE = "insertNewUser";
    }

    public class saveExistingUser {
        public static final String SOAP_ACTION_SAVE = NAMESPACE + "SaveEngagementTracker";
        public static final String METHOD_NAME_SAVE = "SaveEngagementTracker";
    }

    public class getVertical {
        public static final String SOAP_ACTION_VERTICAL = NAMESPACE + "GetVertical";
        public static final String METHOD_NAME_VERTICAL = "GetVertical";
    }

    public class getSubVertical {
        public static final String SOAP_ACTION_SUBVERTICAL = NAMESPACE + "GetCascadingETData";
        public static final String METHOD_NAME_SUBVERTICAL = "GetCascadingETData";
    }

    public class getSubStatus {
        public static final String SOAP_ACTION_SUB_STATUS = NAMESPACE + "GetSubStatusDetails";
        public static final String METHOD_SUB_STATUS = "GetSubStatusDetails";

    }

    public class saveNewCustomer {
        public static final String SOAP_ACTION_NEWCUST_SAVE = NAMESPACE + "insertNewUser";
        public static final String METHOD_NAME_NEWCUST_SAVE = "insertNewUser";
    }

    public class getNewAgentList {
        public static final String SOAP_ACTION_GET_NEW_AGENT = NAMESPACE + "getNewETrackerData";
        public static final String METHOD_GET_NEW_AGENT = "getNewETrackerData";
    }

    public class getNewCustomer {
        public static final String SOAP_ACTION_GET_NEW_CUST = NAMESPACE + "getNewETDetails";
        public static final String METHOD_GET_NEW_CUST = "getNewETDetails";
    }

    public class getExistingUser {
        public static final String SOAP_ACTION_GET_EX_USER = NAMESPACE + "getExistingETrackerData";
        public static final String METHOD_GET_EX_USER = "getExistingETrackerData";
    }

    public class authenticateUser { // login action

        //UAT
        public static final String SOAP_ACTION_LOGIN = NAMESPACE + "AuthenticateUser";
        public static final String METHOD_LOGIN = "AuthenticateUser";
    }

    public class authenticateNewUser {
        //PROD
        public static final String SOAP_ACTION_LOGIN_PROD = NAMESPACE + "AuthenticateUserNew";
        public static final String METHOD_LOGIN_PROD = "AuthenticateUserNew";


    }

    public class getMobileAppDetails { // version details
        public static final String SOAP_ACTION_GET_APPDETAILS = NAMESPACE + "GetMobileAppDetails";
        public static final String METHOD_GET_APPDETAILS = "GetMobileAppDetails";
    }

    public class insertFosData { // login action
        public static final String SOAP_ACTION_INSERT_FOS = NAMESPACE + "insertFOSData";
        public static final String METHOD_INSERT_FOS = "insertFOSData";
    }

    public class GetMobileNotifications {
        public static final String SOAP_ACTION_GET_NOTIFICATIONS = NAMESPACE + "getMobileNotifications";
        public static final String METHOD_GET_NOTIFICATIONS = "getMobileNotifications";
    }

    public class GetMobileFeedback {
        public static final String SOAP_ACTION_GET_FEEDBACK = NAMESPACE + "getMobileFeedback";
        public static final String METHOD_GET_FEEDBACK = "getMobileFeedback";
    }

    public class InsertMobileFeedback {
        public static final String SOAP_ACTION_INSERT_FFEDBACK = NAMESPACE + "insertMobileFeedback";
        public static final String METHOD_INSERT_FEEDBACK = "insertMobileFeedback";
    }


    public class insertDailySource {
        public static final String SOAP_ACTION_SOURCE = NAMESPACE + "insertDailySourcing";
        public static final String METHOD_INSERT_SOURCE = "insertDailySourcing";
    }


    public class getDailySourcingList {
        //getDailySourcing
        public static final String SOAP_ACTION_GET_SOURCE = NAMESPACE + "getDailySourcing";
        public static final String METHOD_GET_SOURCE = "getDailySourcing";
    }

    public class getDailySourcingDetails {

        public static final String SOAP_ACTION_GET_SOURCE_DETAILS = NAMESPACE + "getDailySourcingDetails";
        public static final String METHOD_GET_SOURCE_DETAILS = "getDailySourcingDetails";

    }

    // REN GraphData Type
    public class getClamis {
        public static final String SOAP_ACTION_CLAIM_Type = NAMESPACE + "GetClaims";
        public static final String METHOD_NAME_CLAIM_Type = "GetClaims";
    }

    public class getGWPFilter {
        //REN Filter Wise  & //GWP Filter Wise
        public static final String SOAP_ACTION_GWP_FLT = NAMESPACE + "GetGWPFilter";
        public static final String METHOD_NAME_GWP_FLT = "GetGWPFilter";
    }


    public class getGWPGraphData {
        // GWPGraphData Type
        public static final String SOAP_ACTION_GWP_Type = NAMESPACE + "GetGWPGraphData";
        public static final String METHOD_NAME_GWP_Type = "GetGWPGraphData";
    }

    public class getGWPPeriodWise {
        //GWPGraphData Period Wise
        public static final String SOAP_ACTION_GWP_PW_Type = NAMESPACE + "GetPeriodWiseGWP";
        public static final String METHOD_NAME_GWP_PW_Type = "GetPeriodWiseGWP";
    }

    public class getFilteredGWP {
        //GWPGraphData Filter Wise
        public static final String SOAP_ACTION_GWP_FLT_Type = NAMESPACE + "GetFilteredGWP";
        public static final String METHOD_NAME_GWP_FLT_Type = "GetFilteredGWP";
    }

    public class getInteractionFilterData {
        // REN GraphData Type
        public static final String SOAP_ACTION_INT_Type = NAMESPACE + "GetInteractionFilterData";
        public static final String METHOD_NAME_INT_Type = "GetInteractionFilterData";
    }

    public class getInwardGraph {
        // REN GetInwardGraph Type
        public static final String SOAP_ACTION_INW_Type = NAMESPACE + "GetInwardGraph";
        public static final String METHOD_NAME_INW_Type = "GetInwardGraph";

    }

    public class getPeriodWiseInward {
        // REN PeriodWiseInward
        public static final String SOAP_ACTION_INW_PRD_Type = NAMESPACE + "GetPeriodWiseInward";
        public static final String METHOD_NAME_INW_PRD_Type = "GetPeriodWiseInward";

    }

    public class getLossRatio {
        // REN GetInwardGraph Type
        public static final String SOAP_ACTION_LOSS_Type = NAMESPACE + "GetLossRatio";
        public static final String METHOD_NAME_LOSS_Type = "GetLossRatio";
    }

    public class getPostGraph {
        // REN GetInwardGraph Type
        public static final String SOAP_ACTION_POSS_Type = NAMESPACE + "GetPosGraph";
        public static final String METHOD_NAME_POS_Type = "GetPosGraph";
    }

    public class getRenewalGraph {
        // REN GraphData Type
        public static final String SOAP_ACTION_REN_Type = NAMESPACE + "GetRenewalGrpah";
        public static final String METHOD_NAME_REN_Type = "GetRenewalGrpah";
    }

    public class getProductRenewal {
        // REN GraphData Type
        public static final String SOAP_ACTION_REN_PRD_Type = NAMESPACE + "GetProductRenewal";
        public static final String METHOD_NAME_REN_PRD_Type = "GetProductRenewal";
    }

    public class getFilteredRenewal {
        //REN GraphData Filter Wise
        public static final String SOAP_ACTION_REN_FLT_Type = NAMESPACE + "GetFilteredRenewal";
        public static final String METHOD_NAME_REN_FLT_Type = "GetFilteredRenewal";
    }

    public class getSurveyGraph {
        // REN GraphData Type
        public static final String SOAP_ACTION_SUR_Type = NAMESPACE + "GetSurveyGraph";
        public static final String METHOD_NAME_SUR_Type = "GetSurveyGraph";
    }

    public class getFilteredSurvey {
        //REN GraphData Filter Wise
        public static final String SOAP_ACTION_SUR_FLT_Type = NAMESPACE + "GetFilteredSurvey";
        public static final String METHOD_NAME_SUR_FLT_Type = "GetFilteredSurvey";
    }

    public class getVerticalSubVertical {
        public static final String SOAP_ACTION_VSV = NAMESPACE + "GetVSVByCode";
        public static final String METHOD_NAME_VSV = "GetVSVByCode";
    }

    public class getProfileImage {
        public static final String SOAP_ACTION_GETINMAGE = NAMESPACE + "getImage";
        public static final String METHOD_GETINMAGE = "getImage";
    }

    public class GetRenewalFilteredData{
        public static final String SOAP_ACTION_GETRENEWFILDATA = NAMESPACE + "GetRenewalFilteredData";
        public static final String METHOD_GETRENEWFILDATA = "GetRenewalFilteredData";
    }
    public class GetRenewalFilteredSelection{
        public static final String SOAP_ACTION_GET_RENEWFILTER = NAMESPACE + "GetRenewalFilters";
        public static final String METHOD_GET_RENEWFILTER = "GetRenewalFilters";
    }
}
