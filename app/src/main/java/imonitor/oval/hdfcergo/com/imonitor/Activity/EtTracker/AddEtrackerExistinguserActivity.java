package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.ETrackerHomeNewActivity;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.GPS.LocationAddress;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.MultiSpinner;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.METHOD_NAME_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.SOAP_ACTION_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getVerticalSubVertical.METHOD_NAME_VSV;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getVerticalSubVertical.SOAP_ACTION_VSV;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.saveExistingUser.METHOD_NAME_SAVE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.saveExistingUser.SOAP_ACTION_SAVE;

//import imonitor_old.oval.hdfcergo.com.imonitor_old.GPS.GPSTracker;


/**
 * Created by Meenakshi Aher on 24/05/2017.
 */

public class AddEtrackerExistinguserActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String MY_PREFS_NAME = "HdfcErgo";
    public static final String TAG = "Add Existing Agent";

    String TAG_Soap_Response;
    String strVERTICAL, strSUBVERTICAL;
    Context context;
    boolean valid = false;

    RelativeLayout exreliu;
    // LinearLayout exreliu;
    String FLT_Name = "", FLT_Value = "", FLT_Desc = "";
    List<String> nameArrList, valueArrayList, descArrList, nameDescArrList;
    HashMap<String, String> nameValueList;
    ArrayAdapter<String> adapter;
    TextView txt_appoint_date, txt_appoint_time;
    CheckBox cb_yes, cb_no;
    EditText ed_venue, ed_minutsofmeeting, ed_remarks, edt_others_op;
    EditText txtSubVertical, txtVertical, txt_gccode;
    ProgressDialog pDialog;
    ConsUrl consUrl;
    Button btn_save;
    String mom;
    String lattitude, longitude;

    GPSTracker gps;
    String venueAddr, meetingtypename, peoplemeetname, branchname;
    ArrayList<String> mbranchcodelist = new ArrayList<String>();
    String branchcodeid, ssid, Loginname, Loginntid, traineeCode;
    TextView mTitle;
    AutoCompleteTextView edt_agent;
    ProgressDialog progressDialog;
    MultiSpinner spinner;
    List<CheckBox> items = new ArrayList<CheckBox>();
    View.OnClickListener checkboxClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean checked = ((CheckBox) view).isChecked();

            switch (view.getId()) {
                case R.id.cb_yes:
                    cb_yes.setChecked(true);
                    cb_no.setChecked(false);
                    items.add(cb_yes);
                    break;
                case R.id.cb_no:
                    cb_no.setChecked(true);
                    cb_yes.setChecked(false);
                    items.add(cb_no);
                    break;

            }
        }
    };
    String spStatus;
    private int mYear, mMonth, mDay;
    private SharedPreferences sharedPreferences;
    private Toolbar toolbar;
    private ArrayAdapter<String> multiSpinnerAdapter;
    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            // Do something here with the selected items
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(multiSpinnerAdapter.getItem(i)).append(" ");
                    if (multiSpinnerAdapter.getItem(i).equals("Others")) {
                        edt_others_op.setVisibility(View.VISIBLE);
                        // mom = "Others: " + edt_others_op.getText().toString();
                        spStatus = "Others";

                    } else {
                        edt_others_op.setVisibility(View.GONE);
                        // mom = spinner.getText().toString();
                        spStatus = "";

                    }
                }
            }

            // Toast.makeText(AddEtrackerExistinguserActivity.this, builder.toString(), Toast.LENGTH_SHORT).show();

            String mom = spinner.getText().toString();
            //   System.out.println(TAG + " minutsofmeeting " + mom);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // setContentView(R.layout.activity_etrackerexistusernew);
        //edited on 27-06-17
        setContentView(R.layout.add_extagent_new);

        consUrl = new ConsUrl();
        context = this;
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        setAppToolbar();

        getSharedpreferebnceData();
        getSharedprefLatLong();
        findViews();

        getLocation();

        //  getVerticalDataWebCall();

        //getAgent
        getUsersWebCall();
    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        SetActionBarTitle("Add Existing Agent");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  finish();
                onBackPressed();
            }
        });
    }

    private void getUsersWebCall() {
        if (Constants.isNetworkInfo(AddEtrackerExistinguserActivity.this)) {
            try {
                getSharedpreferebnceData();
                new Gwp_Filter().execute(SOAP_ACTION_GWP_FLT, METHOD_NAME_GWP_FLT, "Agents", "POL_AGENT_CODE");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

//        System.out.println("Loginname 11:=>" + Loginname);
//        System.out.println("Loginssid 11:=>" + ssid);
//        System.out.println("Loginntid 11:=>" + Loginntid);
    }

    private void getSharedprefLatLong() {
        String TAG = "Inside getSharedprefLatLong ";
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences preferences = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        lattitude = preferences.getString("Latitude", "");
        longitude = preferences.getString("Longitude", "");

//        System.out.println(TAG + " Latitude: =>" + lattitude);
//        System.out.println(TAG + " Longitude: =>" + longitude);
    }

    private void setViews() {
        ed_venue.setText(venueAddr.trim());
        // ed_venue.setText(getString(R.string.eng_desc));
    }

    private void getLocation() {
        //   System.out.println("InsideGetLocation");
        gps = new GPSTracker(AddEtrackerExistinguserActivity.this);

        // check if GPS enabled
         if (gps.canGetLocation()) {

             double newLatitude = gps.getLatitude();
             double newLongitude = gps.getLongitude();

             if (newLatitude == 0.0 && newLongitude == 0.0) {
                 Toast.makeText(getApplicationContext(), "Cant get Location !! ", Toast.LENGTH_SHORT).show();
             } else {
              //   double lat = Double.parseDouble(lattitude);
              //   double longi = Double.parseDouble(longitude);
                 venueAddr = LocationAddress.getCompleteAddressString(context, newLatitude, newLongitude);
                 Log.v("", "venueAddr " + venueAddr);
              //   Toast.makeText(getApplicationContext(), "latitude " + newLatitude + "\n longitude " + newLongitude, Toast.LENGTH_SHORT).show();

                 setViews();
             }
         } else {
             // can't get location
             // GPS or Network is not enabled
             // Ask user to enable GPS/network in settings
             gps.showSettingsAlert();
         }

    }

    private void findViews() {
        nameValueList = new HashMap<>();
        nameArrList = new ArrayList<>();
        valueArrayList = new ArrayList<>();
        descArrList = new ArrayList<>();
        nameDescArrList = new ArrayList<>();

        exreliu = (RelativeLayout) findViewById(R.id.exreliu);
        ed_venue = (EditText) findViewById(R.id.ed_venue);
        edt_agent = (AutoCompleteTextView) findViewById(R.id.edt_agent);
        ed_minutsofmeeting = (EditText) findViewById(R.id.ed_minutsofmeeting);
        edt_others_op = (EditText) findViewById(R.id.edt_others_op);
        ed_remarks = (EditText) findViewById(R.id.ed_remarks); //action taken changed to remarks 19/07
        txtVertical = (EditText) findViewById(R.id.txtVertical);
        txtSubVertical = (EditText) findViewById(R.id.txt_subVertical);
        txt_gccode = (EditText) findViewById(R.id.txt_gccode);
        txt_appoint_date = (TextView) findViewById(R.id.txt_appoint_date);
        txt_appoint_time = (TextView) findViewById(R.id.txt_appoint_time);

        cb_yes = (CheckBox) findViewById(R.id.cb_yes);
        cb_no = (CheckBox) findViewById(R.id.cb_no);

       /* verticalspinner = (Spinner) findViewById(R.id.verticalspinner);
        subverticalspinner = (Spinner) findViewById(R.id.subverticalspinner);
        meetingtypespinner = (Spinner) findViewById(R.id.meetingtypespinner);
        peoplemeetspinner = (Spinner) findViewById(R.id.peoplemeetspinner);
        branchspinner = (Spinner) findViewById(R.id.branchspinner);*/

        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        txt_appoint_date.setOnClickListener(this);
        txt_appoint_time.setOnClickListener(this);

        cb_yes.setOnClickListener(checkboxClickListener);
        cb_no.setOnClickListener(checkboxClickListener);

        // get spinner and set adapter
        spinner = (MultiSpinner) findViewById(R.id.spinnerMulti);
     //   ScrollView scroller = (ScrollView)findViewById(R.id.textAreaScroller);
     //   scroller.addView(spinner);


        // create spinner list elements
        multiSpinnerAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text);
        multiSpinnerAdapter.setDropDownViewResource(R.layout.spinner_text);
        multiSpinnerAdapter.add("Business discussion");
        multiSpinnerAdapter.add("Channel performance feedback");
        multiSpinnerAdapter.add("Commission");
        multiSpinnerAdapter.add("R&R intimation & update");
        multiSpinnerAdapter.add("New initiatives update");
        multiSpinnerAdapter.add("POS Update");
        multiSpinnerAdapter.add("Claims discussion");
        multiSpinnerAdapter.add("Proposal tracker");
        multiSpinnerAdapter.add("Renewal discussion");
        multiSpinnerAdapter.add("Training & Development");
        multiSpinnerAdapter.add("Others");

        spinner.setMovementMethod(new ScrollingMovementMethod());
        spinner.setAdapter(multiSpinnerAdapter, false, onSelectedListener);


        // set initial selection
      /*  boolean[] selectedItems = new boolean[adapter.getCount()];
        selectedItems[1] = true; // select second item
        spinner.setSelected(selectedItems);*/


    }


    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    saveUserDetails();
                }
                break;

            case R.id.txt_appoint_date:

                openDateDialog();

                break;

            case R.id.txt_appoint_time:

                openTimeDialog();
                break;

        }

    }

    private void saveUserDetails() {
        if (Constants.isNetworkInfo(AddEtrackerExistinguserActivity.this)) {
            try {

                String Venueaddr = ed_venue.getText().toString().trim();
                // String mom = ed_minutsofmeeting.getText().toString();
                // String mom = spinner.getText().toString();
                String remarks = ed_remarks.getText().toString().trim(); //action taken changed to remarks 19/07
                String peoplemeetname = edt_agent.getText().toString().trim();
                String verticalname = txtVertical.getText().toString().trim();
                String subverticalname = txtSubVertical.getText().toString().trim();
                String dateOfMet = txt_appoint_date.getText().toString().trim();
                String timeOfMet = txt_appoint_time.getText().toString().trim();
                if (spStatus.equals("Others")) {
                    mom = "Others: " + edt_others_op.getText().toString().trim();
                } else {
                    mom = spinner.getText().toString();
                }
                String actionTaken = "";
                for (CheckBox item : items) {
                    if (item.isChecked())
                        actionTaken = "Support Required: " + item.getText().toString();
                }
                //      System.out.println("Checkbox item:=>" + actionTaken);
                //traineeCode = "Support Required: " + ;

//                            System.out.println("Paramsssid:=>" + ssid);
//                            System.out.println("Paramssverticalname:=>" + verticalname);
//                            System.out.println("Paramssubverticalname:=>" + subverticalname);
//                            System.out.println("Paramsmeetingtypename:=>" + meetingtypename);
//                            System.out.println("ParamsVenue:=>" + Venueaddr);
//                            System.out.println("ParamsPeopleMet:=>" + peoplemeetname);
//                            System.out.println("ParamsMinutesOfMeeting:=>" + mom);
//                            System.out.println("ParamsIsActive:=>" + "true");
//                            System.out.println("ParamsCreatedBy:=>" + Loginname);
//                            System.out.println("ParamsUserName:=>" + Loginntid);


                // edited by Pooja Patil at 12-06-17
                new CallSaveExistingUser().execute(ssid, "Sales", verticalname, subverticalname, "NA",
                        "Self", "", "", Venueaddr, "", peoplemeetname, "", mom, remarks,
                        "true", Loginname, Loginntid, actionTaken, dateOfMet, timeOfMet);

                //action taken changed to remarks 19/07
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(exreliu, Constants.offline_msg);
        }
    }

    private boolean validateFields() {
        if (ed_venue.getText().toString().trim().equals("") || ed_venue.getText().toString() == null) {
            //  ed_venue.setError(getString(R.string.venuerr));
            Toast.makeText(getApplicationContext(), "Enter Venue", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (txt_appoint_date.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter a Meeting Date", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (txt_appoint_time.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter a Meeting Time", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (spinner.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Select Meeting Status", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if ((edt_others_op.getVisibility() == View.VISIBLE) && (edt_others_op.getText().toString().trim().equals(""))) {
            Toast.makeText(getApplicationContext(), "Enter Meeting Status", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (ed_remarks.getText().toString().trim().equals("") || ed_remarks.getText().toString() == null) {
            Toast.makeText(getApplicationContext(), "Enter Remarks", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (txt_gccode.getText().toString().trim().equals("") && txtVertical.getText().toString().trim().equals("") && txtSubVertical.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Please select Agent Name from Existing Agent List", Toast.LENGTH_SHORT).show();
            return valid;
        }

        else {
            valid = true;
        }
        return valid;
    }

    private void openTimeDialog() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddEtrackerExistinguserActivity.this,R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int lengthHour = String.valueOf(selectedHour).length();
                int lengthMinutes = String.valueOf(selectedMinute).length();

                String Hour = null;

                if (lengthHour == 1) {
                    Hour = "0" + selectedHour;
                } else {
                    Hour = selectedHour + "";
                }
                String Minutes = null;


                if (lengthMinutes == 1) {
                    Minutes = "0" + selectedMinute;
                } else {
                    Minutes = selectedMinute + "";
                }
                txt_appoint_time.setText(Hour + ":" + Minutes);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    private void openDateDialog() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        // Launch Date Picker Dialog

        Date today = new Date();
        c.setTime(today);
        c.add(Calendar.DAY_OF_MONTH, -4); // Subtract 4 days
        long minDate = c.getTime().getTime();

        DatePickerDialog dpd = new DatePickerDialog(AddEtrackerExistinguserActivity.this,R.style.PickerTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int month = monthOfYear + 1;
                        String Mon = null;
                        int lengthMonth = String.valueOf(month).length();

                        if (lengthMonth == 1) {
                            Mon = "0" + month;
                        } else {
                            Mon = month + "";
                        }
                        String Day = null;
                        int lengthDay = String.valueOf(dayOfMonth).length();

                        if (lengthDay == 1) {
                            Day = "0" + dayOfMonth;
                        } else {
                            Day = dayOfMonth + "";
                        }
                        txt_appoint_date.setText(year + "-" + Mon + "-" + Day);
                    }
                }, mYear, mMonth, mDay);

        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.getDatePicker().setMinDate(minDate);
        dpd.show();
    }


    @Override
    public void onBackPressed() {

        // Constants.callIntent(this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        Intent newIntent = new Intent(AddEtrackerExistinguserActivity.this, ETrackerHomeNewActivity.class);
        newIntent.putExtra("addFrom", "existing user");
        startActivity(newIntent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }


    private void getVerticalSubVertical() {
        String gcCode = txt_gccode.getText().toString();

        //    System.out.println("22 gccode: " + gcCode);


        new GetVerticalSub().execute(gcCode);
    }

    private int getPos(String name) {
        return nameDescArrList.indexOf(name);
    }

    // edited by Pooja Patil at 12-06-17
    //start Pooja
    private class CallSaveExistingUser extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddEtrackerExistinguserActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait.. Data is submitting");
            progressDialog.setCancelable(false);
            progressDialog.show();

            btn_save.setVisibility(View.INVISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            String parameter = "{  \n" +
                    "   \"ID\":" + "0" + ", \n" +
                    "   \"SSEID\":\"" + params[0] + "\", \n" +
                    "   \"Role\":\"Sales\", \n" +
                    "   \"Vertical\":\"" + params[2] + "\",\n" +
                    "   \"SubVertical\":\"" + params[3] + "\",\n" +
                    "   \"MeetingType\":\"" + params[4] + "\",\n" +
                    "   \"MeetingAttendedBy\":\"Self\", \n" +
                    "   \"DateOfMeeting\":\"" + params[18] + "\", \n" + //date of meeting
                    "   \"Time\":\"" + params[19] + "\", \n" + // meeting time
                    "   \"Venue\":\"" + params[8] + "\",\n" +
                    "   \"BranchEmpCode\":\"" + params[9] + "\",\n" +
                    "   \"PeopleMet\":\"" + params[10] + "\",\n" +
                    "   \"SelectedPeople\":null, \n" +
                    "   \"MinutesOfMeeting\":\"" + params[12] + "\",\n" +
                    "   \"ActionTakenReport\":\"" + params[13] + "\",\n" +
                    "   \"IsActive\":true, \n" +
                    "   \"CreatedBy\":\"" + params[15] + "\",\n" +
                    "   \"UserName\":\"" + params[16] + "\", \n" +
                    "   \"TraineeCode\":\"" + params[17] + "\"\n" +
                    "}";
            /* "   \"DateOfMeeting\":null,\n" +
            "   \"Time\":null,\n" +*/

            System.out.println("ParametersAsObject:=>" + parameter);
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SAVE);
            //jsonResult

            request.addProperty("jsonResult", parameter);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_SAVE, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }
            if (soap_error.equals("")) {
               // SoapPrimitive result = null;
                SoapObject result = null;
                try {
                    // getting response
                    result = (SoapObject) envelope.getResponse();
                    Log.v("", "Response SaveExisting:=>" + result.toString()); //response = "1"

                    soap_error = result.toString();
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                soap_error = "Error";
            }

            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;

            }
            //    System.out.println("onPostExc: result" + s);
            if (s.equals("Error") || s.equals("")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();

            } else {
                if (s.equals("\"1\"")) {
                    Toast.makeText(getApplicationContext(), "Record Inserted Successfully.", Toast.LENGTH_SHORT).show();

                    Intent newIntent = new Intent(AddEtrackerExistinguserActivity.this, ETrackerHomeNewActivity.class);
                    newIntent.putExtra("addFrom", "existing user");
                    startActivity(newIntent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    //  Constants.callIntent(AddEtrackerExistinguserActivity.this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                } else {
                    Toast.makeText(getApplicationContext(), "Data Not Insert Successfully !!!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //end Pooja
/*    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            System.out.println("locationAddress:=>" + locationAddress);
            ed_venue.setText(locationAddress);
        }
    }*/

    private class Gwp_Filter extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddEtrackerExistinguserActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();


        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            String ntid = sharedPreferences.getString("NTID", "0");
            //     System.out.println("11 ntid: " + ntid);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            // request.addProperty( "sseId", Loginntid );
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(ConsUrl.UAT);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                soap_error = e.toString();
                e.printStackTrace();
            }


            if (soap_error.equals("")) {

                SoapObject result = null;

                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println(TAG + " Response " + result);
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";

                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            PropertyInfo pi = new PropertyInfo();
                            soapObject2.getPropertyInfo(i, pi);
                            SoapObject propertyTest = (SoapObject) pi.getValue();
                            if (pi.getName().equals(params[2])) {

                                Object property1 = propertyTest.getProperty("Name");
                                String strName = property1.toString();
                                nameArrList.add(strName);
                                FLT_Name = FLT_Name + property1.toString() + ",";

                                Object property3 = propertyTest.getProperty("Description");
                                String mobNo = property3.toString();
                                descArrList.add(mobNo);
                                if (params[2].equals("Agents")) {
                                    Object property2 = propertyTest.getProperty("Value");
                                    valueArrayList.add(property2.toString());
                                    FLT_Value = FLT_Value + property2.toString() + ",";

                                } else {
                                    if (params[2].equals("Reportiees")) {
                                        Object property2 = propertyTest.getProperty("Value");
                                        //  valueArrayList.add(property2.toString());
                                        FLT_Value = FLT_Value + property2.toString() + ",";
                                    } else {
                                        valueArrayList.add(property1.toString());
                                        FLT_Value = FLT_Name + property1.toString() + ",";

                                    }
                                }


                            }
                        }
                        soap_error = params[2] + ":" + params[3];
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                soap_error = "Error";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
               System.out.println("GetAgentList" + " onPostExec: result: " + s);
            final String Res[] = s.split(":");
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;

            }

            if (s.equals("Error")) {
                Toast.makeText(AddEtrackerExistinguserActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                setValuesOnTextChanged();
            }
        }

        private void setValuesOnTextChanged() {


            // System.out.println("ValueArrList: " + valueArrayList);
            //  System.out.println("NamesArrList: " + nameArrList);


            for (int i = 0; i < nameArrList.size() && i < descArrList.size(); i++) {

                nameDescArrList.add(nameArrList.get(i) + " (" + descArrList.get(i) + ")");

            }
            //  System.out.println("NameDescArrList: " + nameDescArrList);
            adapter = new ArrayAdapter<String>(AddEtrackerExistinguserActivity.this, R.layout.spinner_item, nameDescArrList);

            edt_agent.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
                    AddEtrackerExistinguserActivity.this.adapter.getFilter().filter(cs);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    if (edt_agent.getText().toString().equals("")) {
                        txt_gccode.setText("");
                        txtSubVertical.setText("");
                        txtVertical.setText("");
                    }
                }
            });
            edt_agent.setAdapter(adapter);
            edt_agent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    //    System.out.println("11 position: " + position);
                    String foundItem = adapterView.getItemAtPosition(position).toString();
                    //   System.out.println("You have selected --->" + foundItem);

                    int pos = getPos(foundItem);
                    //    System.out.println("Pos--->" + pos);

                    String gcCode = valueArrayList.get(pos);
                    //   System.out.println("value 12 " + gcCode);
                    txt_gccode.setText(gcCode);

                    getVerticalSubVertical();

                }
            });

        }
    }

    public class GetVerticalSub extends AsyncTask<String, Void, String> {
        public String TAG = "GetVerticalSub";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddEtrackerExistinguserActivity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_VSV);
            request.addProperty("gcCode", params[0]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_VSV, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }

            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                       System.out.println("Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {


                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);


                                try {
                                    strVERTICAL = soapObject3.getPropertyAsString("VERTICAL_DESC");
                                    strSUBVERTICAL = soapObject3.getPropertyAsString("SUB_VERTICAL_DESC");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }
            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;

            }
            // System.out.println(TAG + "onPostExecute: result: " + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "Response Error", Toast.LENGTH_SHORT).show();
                } else {
                    txtVertical.setText(strVERTICAL);
                    txtSubVertical.setText(strSUBVERTICAL);
                }
            }
        }
    }
}
