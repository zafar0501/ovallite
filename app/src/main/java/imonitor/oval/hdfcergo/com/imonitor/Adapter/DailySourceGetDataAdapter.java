package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.Entity.DailySourceGetData;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 21/06/2017.
 */

public class DailySourceGetDataAdapter extends RecyclerView.Adapter<DailySourceGetDataAdapter.ViewHolder> {
    Context mContext;
    ArrayList<DailySourceGetData> mItems;
    ColorGenerator generator = ColorGenerator.MATERIAL;
    String letter, dateString;
    Date createdDate;

    public DailySourceGetDataAdapter(Context mContext, ArrayList<DailySourceGetData> mItems) {
        this.mContext = mContext;
        this.mItems = mItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_get_list_item, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        /*holder.txtProdName.setText(mItems.get(position).product_name);
        holder.txtPolSrcd.setText(mItems.get(position).policy_sourced);
        holder.txtPremium.setText(mItems.get(position).premium);*/
        try {
            SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); // first example
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy"); // first example

            String strCreatedDate = mItems.get(position).getCreatedDate();
            try {

                //sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

                // createdDate = sdf.parse(crDate);
                createdDate = sdf3.parse(strCreatedDate);
                System.out.println("createdDate 11 : " + createdDate);


                dateString = sdf2.format(createdDate);

                //  tv_meetingdate.setText(dateString);
                System.out.println("dateString: " + dateString);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            holder.txtProdName.setText("Total Products: " + mItems.get(position).getTotalProducts());
            holder.txtPolSrcd.setText("Total Policies: " + mItems.get(position).getTotalPolicies());
            holder.txtPremium.setText("Total Premium: " + mItems.get(position).getTotalPremium());
            holder.txtDate.setText("Date: " + dateString);

            letter = "DS";
            TextDrawable drawable = TextDrawable.builder().buildRound(letter, generator.getRandomColor());

            holder.letter.setImageDrawable(drawable);


            holder.itemView.setTag(holder);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        //return super.getItemId(position);
        return mItems.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPremium, txtPolSrcd, txtProdName;
        public TextView btnRemove, txtDate;
        ImageView letter;
        Typeface calibriFont;

        public ViewHolder(View itemView) {
            super(itemView);

            calibriFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/calibri.ttf");
            txtProdName = (TextView) itemView.findViewById(R.id.txt_prod_name);
            txtPolSrcd = (TextView) itemView.findViewById(R.id.txt_pol_srcd);
            txtPremium = (TextView) itemView.findViewById(R.id.txt_premium);
            btnRemove = (TextView) itemView.findViewById(R.id.txt_remove);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date);
            letter = (ImageView) itemView.findViewById(R.id.gmailitem_letter);
            txtPremium.setTextColor(Color.BLUE);
            btnRemove.setVisibility(View.GONE);


            txtProdName.setTypeface(calibriFont);
            txtPolSrcd.setTypeface(calibriFont);
            txtPremium.setTypeface(calibriFont);
            txtDate.setTypeface(calibriFont);


        }


    }
}
