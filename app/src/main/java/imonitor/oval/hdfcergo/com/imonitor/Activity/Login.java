package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Entity.LoginDataList;
import imonitor.oval.hdfcergo.com.imonitor.GPS.AppLocationService;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.authenticateNewUser.METHOD_LOGIN_PROD;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.authenticateNewUser.SOAP_ACTION_LOGIN_PROD;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getMobileAppDetails.METHOD_GET_APPDETAILS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getMobileAppDetails.SOAP_ACTION_GET_APPDETAILS;


public class Login extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "HdfcErgo";

    private static final int PERMISSION_REQUEST_CODE = 200;

    private static final String TAG = "IMonitorLogin";
    EditText edt_agent, edt_password;
    String str_UserName, str_Password, rp_SSEID;
    TextView txt_version;
    ImageView img_show_pwd, img_hide_pwd;
    Button btn_submit;
    ProgressDialog pDialog;
    String soap_result;
    CheckBox cb;
    ConsUrl consUrl;
    ArrayList<LoginDataList> LoginItemlist = new ArrayList<LoginDataList>();
    SoapPrimitive response;
    Context context;
    GPSTracker gps;
    double latitude, longitude;
    String agentEmail = "", rmdEmail = "", mobNo = "", LName = "", FName = "", userId = "";
    AppLocationService appLocationService;
    String imeiNo;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        consUrl = new ConsUrl();
        context = this;


        //  getDeviceScreenSize();
        setAppToolbar();
        checkPermisions();

        getMobileAppDetails();

        ComponetIntial();
        setClickListners();
    }

    private void getDeviceScreenSize() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        String toastMsg;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                toastMsg = "Large screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                toastMsg = "Normal screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                toastMsg = "Small screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                toastMsg = "Extra Large screen";
            default:
                toastMsg = "Screen size is neither large, normal or small";
        }
        Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
    }

    private void getRegistrationToken() {
        //sender-id==project number of app registered on firebase console == 453954366792

        // Get token
        String token = FirebaseInstanceId.getInstance().getToken();
        // Log and toast
        String msg = getString(R.string.msg_token_fmt, token);
        Log.d(TAG, msg);
        Toast.makeText(Login.this, msg, Toast.LENGTH_SHORT).show();
    }

    private void checkPermisions() {
        requestPermission();
    }

    private void requestPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);
        if (result != PackageManager.PERMISSION_GRANTED && result1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        } else {
            getLocation();

            imeiNo = getDeviceImei();
            setIMEINOPref(imeiNo);
        }
    }

    private void getLocation() {
        gps = new GPSTracker(Login.this);
        // Check if GPS enabled
        if (gps.canGetLocation()) {
            gps.getLocation();

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            setLatLongPreferences(latitude, longitude, "11");
            // Toast.makeText(getApplicationContext(), "11 lat " + latitude + " long " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            gps.showSettingsAlert();
        }
    }

    private void getMobileAppDetails() {
        if (Constants.isNetworkInfo(Login.this)) {
            try {
                new GetMobileAppDetails().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //  Constants.snackbar(exreliu, Constants.offline_msg);
            Toast.makeText(getApplicationContext(), "Please Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private String getDeviceImei() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String imeiNo = "OvalLite:" + String.valueOf(telephonyManager.getDeviceId());
        return imeiNo;
    }

    public void setLatLongPreferences(double latitude, double longitude, String tt) {
        String TAG = "setLatLongPreferences ";

        Log.i(TAG, "Inside setLatLongPreferences ");
        System.out.println(TAG + tt + " latitude : " + latitude + " longitude: " + longitude);

        // SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Login.this);
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Latitude", latitude + "");
        editor.putString("Longitude", longitude + "");
        //  editor.putString("imeiNo", imeiNo);
        editor.commit();


    }

    public void setIMEINOPref(String imeiNo) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("imeiNo", imeiNo);
        editor.commit();

    }


    // added by Pooja P at 14/06/17
    private void setAppToolbar() {
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Login</font>"));
    }


    private void ComponetIntial() {

        cb = (CheckBox) findViewById(R.id.checkbox);
        edt_agent = (EditText) findViewById(R.id.edt_agent_code);

        // edt_agent.setText("sandeep.shewale@hdfcergo.com");
        // edt_agent.setText("4345");
        edt_agent.setText("5128");
        // edt_agent.setText("pravat.sharma@hdfcergo.com");
        edt_password = (EditText) findViewById(R.id.edt_pass);
        // edt_password.setText("July@2017");
        // edt_password.setText("Ergo@123");
        //edt_password.setText("Aug@2017");
         edt_password.setText("Sep@2017");
        btn_submit = (Button) findViewById(R.id.btn_submit);
        txt_version = (TextView) findViewById(R.id.txt_ver);
        img_show_pwd = (ImageView) findViewById(R.id.img_show_pwd);
        img_hide_pwd = (ImageView) findViewById(R.id.img_hide_pwd);


    }

    private void setClickListners() {
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                inter_check();

                //   getRegistrationToken();


            }
        });
        img_show_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_show_pwd.setVisibility(View.GONE);
                img_hide_pwd.setVisibility(View.VISIBLE);
                edt_password.setTransformationMethod(null);
            }
        });
        img_hide_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_show_pwd.setVisibility(View.VISIBLE);
                img_hide_pwd.setVisibility(View.GONE);
                edt_password.setTransformationMethod(new PasswordTransformationMethod());
            }
        });
        if (sharedPreferences.getString("NTID", "0").equals("0")) {

        } else {

            if (sharedPreferences.getString("SaveLogin", "0").equals("0")) {

            } else {
                //   Constants.callIntent(Login.this, OvalHomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                Constants.callIntent(Login.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
        }

        if (cb.isChecked()) {
            cb.setChecked(false);
        }

        cb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {

                    str_UserName = edt_agent.getText().toString().trim();
                    str_Password = edt_password.getText().toString().trim();
                    if (str_UserName.equals("") || str_Password.equals("")) {
                        cb.setChecked(false);
                        Toast.makeText(getApplicationContext(), "Please Enter  User ID & Password", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Login.this, "Login Details Are Saved", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("SaveLogin", "1");
                        editor.putString("Email", str_UserName);
                        editor.commit();
                    }
                } else {
                    Toast.makeText(Login.this, "Login Details are Cleared", Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("USER_ID", "");
                    editor.putString("USER_PASS", "");
                    editor.commit();
                }
            }
        });

    }

    private void inter_check() {
        ConnectivityManager cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
        } else {
            str_UserName = edt_agent.getText().toString().trim();
            //String to read the user input
            int length = str_UserName.length();  //change the string characters to length
            for (int i = 0; i < length; i++)  //to check the characters of string..
            {
                char character = str_UserName.charAt(i);
                if (Character.isLowerCase(character)) {
                    str_UserName = str_UserName.toUpperCase();
                }

            }

            Log.i("TAG", "S_upper : inter " + str_UserName);

            str_Password = edt_password.getText().toString().trim();

            if (str_UserName.equals("") || str_Password.equals("")) {
                Toast.makeText(getApplicationContext(), "Please Enter Correct User ID & Password", Toast.LENGTH_SHORT).show();
            } else {

                Log.i("CORE", "str_UserName " + str_UserName);


                // added by Pooja Patil at 08-06-17
                new SlowOperation().execute();

            }

        }
    }

    private void getAmsIntent() {
        try {


            // Get intent, action and MIME type
            Intent intent = getIntent();
            String action = intent.getAction();
            String type = intent.getType();

            // if (Intent.ACTION_SEND.equals(action) && type != null) {
            //      if ("text/plain".equals(type)) {
            handleSendText(intent); // Handle text being sent
            //    }
            // }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void handleSendText(Intent intent) {
        Bundle bundle = intent.getExtras().getBundle("details");
        if (bundle != null) {
            // Update UI to reflect text being shared
            System.out.println("if bundle not null");
            userId = bundle.getString("userId");
            FName = bundle.getString("fName");
            LName = bundle.getString("lName");
            agentEmail = bundle.getString("emailId");
            rmdEmail = bundle.getString("rmemailId");
            mobNo = bundle.getString("mobileNo");

            // Toast.makeText(getApplicationContext(), "In Login " + "mobNo" + mobNo, Toast.LENGTH_LONG).show();
        }
    }

    public void setLoginSharedPreferences(String usrssid, String userntid, String username) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Login.this);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("loginssid", usrssid);
        editor.putString("loginntid", userntid);
        editor.putString("loginname", username);
        editor.commit();
    }

    //end Pooja
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this, R.style.AlertDialogCustom);
        builder.setTitle("Confirm Please...");
        builder.setMessage("Do you want to close the app ?");
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  finishAffinity();

                        ActivityCompat.finishAffinity(Login.this);
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder.create();
        alert11.show();
    }


    // added by Pooja Patil at 15/06/17
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean phoneStateAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && phoneStateAccepted) {

                        getLocation();

                        imeiNo = getDeviceImei();
                        setIMEINOPref(imeiNo);

                        // Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    } else {
                        // Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE},
                                                            PERMISSION_REQUEST_CODE);


                                                }
                                            }
                                        });
                                // return;
                            }
                        } else {
                            //to get Location
                            getLocation();

                            // to get imei no
                            imeiNo = getDeviceImei();
                            setIMEINOPref(imeiNo);
                        }

                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Login.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public class GetMobileAppDetails extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_APPDETAILS);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String strResult = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_APPDETAILS, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                strResult = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                strResult = e.toString();
            }

            if (strResult.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    //System.out.println("GetMobileAppDetails Response: " + result.toString());
                    // Log.v("GetMobileAppDetails Res:", "" + result.toString());
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                }

                String app_name = "", App_Version = "";
                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        strResult = "0";
                    } else {

                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                            SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                            if (soapObject3.toString().contains("Message")) {
                                strResult = "Message";
                            } else {
                                app_name = soapObject3.getPropertyAsString("APP_NAME");


                                // soap_result = App_Version;
                                if (app_name.equals("OvalLite")) {
                                    App_Version = soapObject3.getPropertyAsString("APP_VERSION");
                                    strResult = App_Version;
                                }
                            }
                        }
                    }
                    // }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                strResult = "Error";
            }
            return strResult;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("onPostExecute: result " + s);

            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Poor Internet Connectivity", Toast.LENGTH_SHORT).show();
                //txt_version.setText("");
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "No Data Available !!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("Message")) {
                    Toast.makeText(getApplicationContext(), "Error while accessing Oval Lite !!", Toast.LENGTH_SHORT).show();
                } else {

                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    String ser_verssion = s;
                    String version = pInfo.versionName;
                    txt_version.setText("Version " + version);


                    if (ser_verssion.equals(version)) {
                        Log.i("TAG", "Version Match");
                    } else {
                        Log.i("TAG", "Version not Match");
                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this, R.style.AlertDialogCustom);
                        builder.setTitle("Confirm Please...");
                        builder.setMessage("Do you want to update App ? ")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        String url = "http://muat.hdfcergo.com/dnld/Oval/OvalLite.apk";
                                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                        startActivity(i);
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //  Action for 'NO' Button
                                        dialog.cancel();
                                    }
                                });

                        //Creating dialog box
                        AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("Oval Lite Update Available");
                        alert.show();

                    }
                }

            }
        }


    }

    // added by Pooja Patil at 08-06-17
    //start Pooja
    private class SlowOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(Login.this, R.style.AlertDialogCustom);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

            str_UserName = edt_agent.getText().toString().trim();

            //String to read the user input
            int length = str_UserName.length();  //change the string characters to length
            for (int i = 0; i < length; i++)  //to check the characters of string..
            {
                char character = str_UserName.charAt(i);
                if (Character.isLowerCase(character)) {
                    str_UserName = str_UserName.toUpperCase();
                }

            }

            Log.i("TAG", "S_upper : inter " + str_UserName);


        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_LOGIN_PROD);
            //   SoapObject request = new SoapObject(NAMESPACE, METHOD_LOGIN);
            request.addProperty("userName", str_UserName);
            request.addProperty("password", str_Password);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "", resp_error = "";
            try {
                httpTransport.call(SOAP_ACTION_LOGIN_PROD, envelope); //send request
                //     httpTransport.call(SOAP_ACTION_LOGIN, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {


                try {
                    // getting response
                    response = (SoapPrimitive) envelope.getResponse();
                    // response = (SoapObject) envelope.getResponse();
                    Log.v("", "Login Response: " + response.toString());
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                    resp_error = "Error";
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    resp_error = "Error";
                    Log.v("11 Login Response:", "" + resp_error);
                } catch (Exception e) {
                    e.printStackTrace();
                    resp_error = "Error";
                    Log.v("22 Login Response:", "" + resp_error);
                }

                if (resp_error.equals("Error")) {
                    soap_result = "Error";
                } else if (response.toString().equals("{}") || response.toString().equals("") || response.toString() == null) {
                    rp_SSEID = "0";
                    soap_result = "0" + "," + rp_SSEID;

                } else {

                    JSONObject jsonobject = null;
                    try {
                        jsonobject = new JSONObject(response.toString());

                        Log.i("JSON:=>", jsonobject.toString());
                        System.out.println("LoginResponse:=>" + jsonobject.toString());

                        JSONArray jsonArrayHot = jsonobject.getJSONArray("UserDetail");
                        for (int i = 0; i < jsonArrayHot.length(); i++) {
                            JSONObject myJSON = jsonArrayHot.getJSONObject(i);

                            LoginDataList items = new LoginDataList();
                            items.SSEId = myJSON.getString("SSEId");
                            System.out.println("0 LOGIN==> " + " SSEID " + items.SSEId.toString());
                            if (items.SSEId.equals("") || items.SSEId.equals("null")) {
                                rp_SSEID = "0";
                            } else {
                                System.out.println("2 LOGIN==> " + " SSEID " + items.SSEId.toString());

                                items.NTID = myJSON.getString("NT ID");
                                items.Name = myJSON.getString("Name");
                                items.Email = myJSON.getString("Email Id");

                                LoginItemlist.add(items);

                                rp_SSEID = items.SSEId;

                                System.out.println("LoginItemlist:=>" + LoginItemlist.toString());

                                Log.i("LoginItemlist:=>", LoginItemlist.toString());

                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("NTID", items.SSEId);
                                if (items.SSEId.equals("5128")) {
                                    editor.putString("NTID", "1968");
                                } else {
                                    editor.putString("NTID", items.SSEId);
                                }
                                //editor.putString("NTID","1968");
                                editor.putString("User_Name", items.Name);
                                editor.putString("Email", items.Email);
                                editor.commit();
                            }

                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }

                    soap_result = "1" + "," + rp_SSEID;
                }
                // }
           /* } else {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
            }*/
            } else {
                soap_result = "Error";
            }

            return soap_result;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pDialog.isShowing())
                pDialog.dismiss();

            System.out.println("on post execute " + "s:=>" + s);
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                String[] parts = s.split(",");
                String res_check = parts[0];
                String sseid = parts[1];

                // System.out.println("on post execute " + "sseid:=>" + sseid);

                if (res_check.equals("0") || sseid.equals("0")) {
                    Toast.makeText(getApplicationContext(), "Enter Correct Staff id & NT Password", Toast.LENGTH_SHORT).show();
                    pDialog.cancel();

                } else {
                    Toast.makeText(getApplicationContext(), "Login Successfully", Toast.LENGTH_SHORT).show();
                    pDialog.cancel();

                    setLoginSharedPreferences(LoginItemlist.get(0).SSEId, LoginItemlist.get(0).NTID, LoginItemlist.get(0).Name);
                    // Toast.makeText(Login.this, "Welcome - " + LoginItemlist.get(0).Name, Toast.LENGTH_SHORT).show();
                    pDialog.hide();

                    Constants.callIntent(Login.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                }
            }
        }
    }
}
