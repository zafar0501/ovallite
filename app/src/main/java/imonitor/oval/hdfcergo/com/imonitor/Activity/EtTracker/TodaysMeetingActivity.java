package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 13/06/2017.
 */

public class TodaysMeetingActivity extends AppCompatActivity{

    Toolbar toolbar;
    GoogleMap googleMap;
    ListView lv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        setAppToolbar();
        setMeetingLocation();
    }

    private void setMeetingLocation() {
        // Getting reference to the SupportMapFragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        // Getting reference to the Google Map
        googleMap = mapFragment.getMap();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);

       /* GPSTracker mGPS = new GPSTracker(TodaysMeetingActivity.this);
        if (mGPS.canGetLocation) {
            mGPS.getLocation();
            MarkerOptions marker = new MarkerOptions().position(new LatLng(mGPS.getLatitude(), mGPS.getLongitude())).title("Your Current Position");
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            googleMap.addMarker(marker);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mGPS.getLatitude(), mGPS.getLongitude())).zoom(10).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        } else {
            Toast.makeText(getApplicationContext(), "Not Detect GPS", Toast.LENGTH_SHORT).show();
        }

        // Calling Webservice
        //new GetMobileAppDetails().execute();

        lv = (ListView) findViewById(list);
        // calling listview Onclick Event
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


                googleMap.clear();
                GPSTracker mGPS = new GPSTracker(Todays_Meeting.this);
                if (mGPS.canGetLocation) {
                    mGPS.getLocation();
                    MarkerOptions marker = new MarkerOptions().position(new LatLng(mGPS.getLatitude(), mGPS.getLongitude())).title("Your Current Position");
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    googleMap.addMarker(marker);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(mGPS.getLatitude(), mGPS.getLongitude())).zoom(10).build();
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                } else {
                    Toast.makeText(getApplicationContext(), "Not Detect GPS", Toast.LENGTH_SHORT).show();
                }
                String ADD=sharedPreferences.getString("POS_MS"+position,"");
                Log.i("TAG","ADD :"+ADD);
                GeocodingLocation locationAddress = new GeocodingLocation();
                locationAddress.getAddressFromLocation(ADD,
                        getApplicationContext(), new GeocoderHandler());
            }
        });
//First Time Sync
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String toDate=dateFormat.format(cal.getTime());
        cal.add(Calendar.DATE, -1);

        String from_Date =dateFormat.format(cal.getTime());
        new synWebservice().execute(from_Date,toDate);*/
    }

    private void setAppToolbar() {
        //Setting toolbar
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}
