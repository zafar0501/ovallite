package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.R.id.chart;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getClamis.METHOD_NAME_CLAIM_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getClamis.SOAP_ACTION_CLAIM_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.METHOD_NAME_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.SOAP_ACTION_GWP_FLT;

public class Claims_Home_Activity extends AppCompatActivity implements View.OnClickListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";
    BarDataSet barDataSet1, barDataSet2;
    //Filter NAME AND VALUE
    String FLT_Name = "";
    String FLT_Value = "";
    // Barchart Parameter
    String C_Name = "";
    String C_Value = "";
    String P_Name = "";
    String P_Value = "";
    TextView Claim_Type, Claim_IC, Claim_PC, Claim_OC;
    ImageView btnNext, btnPrevious;
    ProgressDialog progressDialog;
    ConsUrl con_url;
    BarChart barChart;
    Dialog dialog;
    ArrayAdapter<String> adapter;
    ArrayList<String> labels = new ArrayList<String>();
    ArrayList<BarEntry> group1 = new ArrayList<>();
    ArrayList<BarEntry> group2 = new ArrayList<>();
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_claims__home_);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        con_url = new ConsUrl();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplication(), MainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        // all Compnent Value initialize
        ViewInitilize();

        check_internet_connection();


    }

    private void ViewInitilize() {

        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);

        //BarChart of Gwp
        barChart = (BarChart) findViewById(chart);

        //TextView of Gwp
        Claim_Type = (TextView) findViewById(R.id.claim_type);
        Claim_IC = (TextView) findViewById(R.id.claim_ic);
        Claim_PC = (TextView) findViewById(R.id.claim_pc);
        Claim_OC = (TextView) findViewById(R.id.claim_oc);

        //
        Claim_Type.setText("Incurred  (in Thousands)");
    }

    private void check_internet_connection() {
        ConnectivityManager cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
        } else {
            //Open BarChart
            OpenBarchart();
            // Calling GWP webserivce
            new Gwp_Service().execute(SOAP_ACTION_CLAIM_Type, METHOD_NAME_CLAIM_Type, "PAID", "");
        }
    }

    private void OpenBarchart() {

        //BarDataSet dataset = new BarDataSet(entries, "# of Calls");

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                String value = barChart.getBarData().getXVals().get(e.getXIndex()) + " = " + String.valueOf(e.getVal());
                Toast toast = Toast.makeText(getApplication(),
                        value, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();


            }

            @Override
            public void onNothingSelected() {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case R.id.action_toolbar_1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                FLT_Name = "";
                FLT_Value = "";
                new Claim_Filter().execute(SOAP_ACTION_GWP_FLT, METHOD_NAME_GWP_FLT, "Agents", "NEW_DEALER_CODE");


                return true;
            case R.id.action_toolbar_6:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                FLT_Name = "";
                FLT_Value = "";

                new Claim_Filter().execute(SOAP_ACTION_GWP_FLT, METHOD_NAME_GWP_FLT, "Reportiees", "product_name");
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(), MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.claim_ic:
                barChart.clear();
                labels.clear();
                group1.clear();
                Claim_IC.setTextColor(Color.RED);
                Claim_PC.setTextColor(Color.BLACK);
                Claim_OC.setTextColor(Color.BLACK);
                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_CLAIM_Type, METHOD_NAME_CLAIM_Type, "PAID", "");
                barChart.setVisibility(View.VISIBLE);
                break;

            case R.id.claim_pc:
                barChart.clear();
                labels.clear();
                group1.clear();
                Claim_IC.setTextColor(Color.BLACK);
                Claim_PC.setTextColor(Color.RED);
                Claim_OC.setTextColor(Color.BLACK);
                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_CLAIM_Type, METHOD_NAME_CLAIM_Type, "OS", "");
                barChart.setVisibility(View.VISIBLE);
                Claim_Type.setText("Paid  (in Thousands)");
                break;


            case R.id.claim_oc:
                barChart.clear();
                labels.clear();
                group1.clear();
                Claim_IC.setTextColor(Color.BLACK);
                Claim_PC.setTextColor(Color.BLACK);
                Claim_OC.setTextColor(Color.RED);
                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_CLAIM_Type, METHOD_NAME_CLAIM_Type, "INCURRED", "");
                barChart.setVisibility(View.VISIBLE);
                Claim_Type.setText("Outstanding  (in Thousands)");
                break;

            case R.id.btn_next:
                Constants.callIntent(Claims_Home_Activity.this, Poss_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                break;
            case R.id.btn_previous:
                Constants.callIntent(Claims_Home_Activity.this, Loss_Ratio_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;


        }
    }

    private class Gwp_Service extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Claims_Home_Activity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("type", params[2]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            request.addProperty("condition", params[3]);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    int index_x = 0;
                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo(i, pi);
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals("Claims")) {

                            Object property1 = propertyTest.getProperty("Name");
                            Object property2 = propertyTest.getProperty("ValueInDec");
                            C_Name = property1.toString();
                            C_Value = property2.toString();

                            double value = Double.parseDouble(C_Value);
                            labels.add(C_Name);
                            group1.add(new BarEntry((float) value / 1000, index_x));
                            Log.i("TAG", index_x + "" + ": " + value);
                            index_x = index_x + 1;

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                soap_error = "Error";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

                if (s.equals("")) {
                    barDataSet1 = new BarDataSet(group1, "CurrentYear");
                    barDataSet1.setColors(new int[]{Color.rgb(102, 180, 216)});
                    if (group1.isEmpty()) {
                        barChart.setVisibility(View.INVISIBLE);
                        Toast.makeText(Claims_Home_Activity.this, "No Business Yet", Toast.LENGTH_SHORT).show();
                    }
                    ArrayList<BarDataSet> dataset = new ArrayList<>();
                    dataset.add(barDataSet1);


                    try {
                        BarData data = new BarData(labels, dataset);
                        barChart.setData(data);
                        barChart.setDescription("");
                        barChart.animateY(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(Claims_Home_Activity.this, "No Business Yet", Toast.LENGTH_SHORT).show();
                    barChart.setVisibility(View.GONE);
                }

        }
    }

    private class Claim_Filter extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Claims_Home_Activity.this,R.style.AlertDialogCustom);
            progressDialog.setMessage("Please Wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                soap_error = e.toString();
                e.printStackTrace();
            }


            if (soap_error.equals("")) {
                try {
                    SoapObject result = null;
                    try {
                        result = (SoapObject) envelope.getResponse();
                    } catch (SoapFault soapFault) {
                        soap_error = "Error";
                        soapFault.printStackTrace();
                    }

                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);


                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo(i, pi);
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals(params[2])) {

                            Object property1 = propertyTest.getProperty("Name");
                            FLT_Name = FLT_Name + property1.toString() + ",";

                            if (params[2].equals("Agents")) {
                                Object property2 = propertyTest.getProperty("Value");
                                FLT_Value = FLT_Value + property2.toString() + ",";
                            } else {

                                if (params[2].equals("Product")) {
                                    FLT_Value = FLT_Name + property1.toString() + ",";
                                } else {
                                    Object property2 = propertyTest.getProperty("Value");
                                    FLT_Value = FLT_Value + property2.toString() + ",";
                                }

                            }


                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                soap_error = params[2] + ":" + params[3];
            } else {
                soap_error = "Error";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            final String Res[] = s.split(":");
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (s.equals("Error")) {
                Toast.makeText(Claims_Home_Activity.this, "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                final String names[] = FLT_Name.split(",");
                final String values[] = FLT_Value.split(",");

                // Create custom dialog object
                dialog = new Dialog(Claims_Home_Activity.this);
                // Include dialog.xml file
                dialog.setContentView(R.layout.custom);
                // Set dialog title
                dialog.setTitle(Res[0]);
                ListView lv = (ListView) dialog.findViewById(R.id.listView);
                adapter = new ArrayAdapter<String>(Claims_Home_Activity.this, android.R.layout.simple_list_item_1, names);
                lv.setAdapter(adapter);
                EditText inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
                inputSearch.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        Claims_Home_Activity.this.adapter.getFilter().filter(cs);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = 500;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.RIGHT;

                dialog.getWindow().setAttributes(lp);
                dialog.show();

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        barChart.clear();
                        labels.clear();
                        group1.clear();
                        group2.clear();

                        if (Res[0].equals("Agents")) {


                            new Gwp_Service().execute(SOAP_ACTION_CLAIM_Type, METHOD_NAME_CLAIM_Type, "PAID", "POL_AGENT_CODE =" + "'" + values[position] + "'");
                        } else {

                            new Gwp_Service().execute(SOAP_ACTION_CLAIM_Type, METHOD_NAME_CLAIM_Type, "PAID", "POL_GEO_BRANCH_MGR_EMPNO =" + "'" + values[position] + "'");
                        }

                        barChart.setVisibility(View.VISIBLE);
                        Claim_Type.setText("OS  (in Thousands) " + names[position]);
                        dialog.dismiss();
                    }
                });
            }
        }
    }


}
