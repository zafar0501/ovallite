package imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.ViewPagerAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker.BancassuranceEngTrackFragment;
import imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker.ExistingAgentListFragment;
import imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker.NewProspectFragment;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Pooja Patil on 16/06/2017.
 */

public class ETrackerHomeNewActivity extends AppCompatActivity {


    public ViewPager mViewPager;
    TabLayout mTabLayout;
    ViewPagerAdapter mAdapter;
    Toolbar mToolbar;
    TextView mTitle;
    ImageView addagent;
    String addFrom;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.etracker_home_activity_new);

        setAppToolbar();
        initalizeComponents();
        getIntentData();


    }

    private void getIntentData() {
        try {
            addFrom = getIntent().getExtras().getString("addFrom");

            if (addFrom == null || addFrom.equals("")) {
                //Toast.makeText(ETrackerHomeNewActivity.this, "if null", Toast.LENGTH_SHORT).show();
            } else {
                if(addFrom.equals("new prospect")){
                    mViewPager.setCurrentItem(1);
                }else if(addFrom.equals("existing user")){
                    mViewPager.setCurrentItem(0);
                }else{
                    mViewPager.setCurrentItem(2);
                }
               // Toast.makeText(ETrackerHomeNewActivity.this, "if not null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initalizeComponents() {


        mViewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(mViewPager);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mViewPager);

        setupCustomTab();


    }

    private void setupCustomTab() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Existing Agent");
        mTabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("New Prospect");
        mTabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Banca");
        mTabLayout.getTabAt(2).setCustomView(tabThree);
    }

    private void setupViewPager(ViewPager mViewPager) {
        //setting up view pager
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new ExistingAgentListFragment(), "Existing Agent");
        mAdapter.addFragment(new NewProspectFragment(), "New Prospect"); // new cust & agent list
        mAdapter.addFragment(new BancassuranceEngTrackFragment(), "Banca"); // new cust & agent list
        mViewPager.setAdapter(mAdapter);
    }

    private void setAppToolbar() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar_home);
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        addagent = (ImageView) mToolbar.findViewById(R.id.addagent);
        addagent.setVisibility(View.INVISIBLE);

        SetActionBarTitle("Engagement Tracker");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Constants.callIntent(this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }


}
