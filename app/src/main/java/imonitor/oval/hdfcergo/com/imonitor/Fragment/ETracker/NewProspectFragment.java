package imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.LeadActivity;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.EtrackerNewCustomerAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackernewcustomerListData;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.VerticalSpaceItemDecorartion;

import static android.content.Context.MODE_PRIVATE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getNewCustomer.METHOD_GET_NEW_CUST;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getNewCustomer.SOAP_ACTION_GET_NEW_CUST;

/**
 * Created by Zafar.Hussain on 16/06/2017.
 */

public class NewProspectFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String MY_PREFS_NAME = "HdfcErgo";

    String TAG_Soap_Response;
    RelativeLayout reliu;
    ConsUrl consUrl;
    ArrayList<EtrackernewcustomerListData> NewCustomerItemlist = new ArrayList<EtrackernewcustomerListData>();
    ArrayList<EtrackernewcustomerListData> NewCustomerArrayList = new ArrayList<EtrackernewcustomerListData>();
    RecyclerView Newagentrecyclerview;
    LinearLayoutManager mLayoutManager;
    EtrackerNewCustomerAdapter Newcustomeradapter;
    ArrayList<String> dataList = new ArrayList<String>();
    View popuplayout1;
    Dialog connectiondialogue;
    ImageView addagent;
    String branchcodeid, ssid, Loginname, Loginntid;
    TextView mTitle;
    Context mContext;
    private SharedPreferences sharedPreferences;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;
    TextView txtNoData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_customer_list, container, false);
        consUrl = new ConsUrl();
        mContext = getActivity();
        getSharedpreferenceData();
        findViews(view);
        addFabButton(view);
        // WebserviceCall();
        return view;

    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layoutnewcustomer);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        WebserviceCall();
                    }
                }
        );
    }

    private void addFabButton(View view) {
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_prospect);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Constants.callIntent(mContext, LeadActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
    }

    private void getSharedpreferenceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(mContext);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void WebserviceCall() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(getActivity())) {
            try {
                String newagentparameter = ssid;
                System.out.println("newagentparameter:=>" + newagentparameter);
                String role = "Customer";
                //  getNewCustomerList( newagentparameter,role );


                // edited by Pooja Patil at 09-06-17
                new GetNewCustomerList().execute(newagentparameter, role);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //  Constants.snackbar(reliu, Constants.offline_msg);
            Snackbar.make(getView(), Constants.offline_msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }


    private void findViews(View view) {
        txtNoData = (TextView)view.findViewById(R.id.tv_no_data);
        reliu = (RelativeLayout) view.findViewById(R.id.newreliu);


        Newagentrecyclerview = (RecyclerView) view.findViewById(R.id.newcustomerList);


        Newagentrecyclerview.setHasFixedSize(true);
        Newagentrecyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        Newagentrecyclerview.setItemAnimator(new DefaultItemAnimator());
        Newagentrecyclerview.addItemDecoration(new VerticalSpaceItemDecorartion(10));
        mLayoutManager = new LinearLayoutManager(mContext);
        Newagentrecyclerview.setLayoutManager(mLayoutManager);


    }

    private void loaddata() {
        if (NewCustomerArrayList.isEmpty()) {
            System.out.println("EmptySize:=>" + NewCustomerArrayList.size());
            Constants.snackbar(reliu, "No Record Found..");

        } else {
            Newcustomeradapter = new EtrackerNewCustomerAdapter(mContext, NewCustomerArrayList, Newagentrecyclerview, R.layout.template_newcustomer, dataList);
            Newagentrecyclerview.setAdapter(Newcustomeradapter);
            Newcustomeradapter.notifyDataSetChanged();
            Newagentrecyclerview.addOnItemTouchListener(new RecyclerTouchListener(mContext, Newagentrecyclerview, new ClickListener() {
                @Override
                public void onClick(View view, final int position) {
                    LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                    parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("position 11: " + position);
                            Newcustomeradapter.setPopUpView(position);
                        }
                    });
                }

                @Override
                public void onLongClick(View view, int position) {
                    // Newcustomeradapter.setPopUpView(position);
                }
            }));
        }
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        //   WebserviceCall();
        swipeRefreshLayout.setRefreshing(false);
    }

    // edited by Pooja Patil at 16-06-17
    //start Pooja
    public class GetNewCustomerList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog = new ProgressDialog( ExistingAgentListActivity.this );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
           /* SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("type", params[2]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            request.addProperty("condition", params[3]);*/

            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_NEW_CUST);
            request.addProperty("SSE_ID", params[0]);
            request.addProperty("CUST_ROLE", params[1]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call(SOAP_ACTION_GET_NEW_CUST, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }catch (Exception e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response newProspect:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = "Error";
                    soapFault.printStackTrace();
                }catch(Exception e){
                    soap_error = "Error";
                    e.printStackTrace();
                }

                try {
                    //customer name, product name,address 1
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        soap_error = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strCONTACTPERSON = "", strROW_ID = "";
                        String strAGENTNAME = "";
                        String strVENUE = "", strROLE = "", strCONTACTNUMBER = "", strMEETINGOUTCOME = "", strEMAILID = "";
                        String strCUST_ROLE = "", strPRODUCTNAME = "", strAPPOINTDATE = "", strMOBILE_NO = "", strADDRESS1 = "", strPINCODE = "", strSTATUS = "", strREMARKS = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                //more view
                                // cust_role, product name, appoint date, cust_name, mob , address1 status, remarks
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                                if (soapObject3.toString().contains("Message")) {
                                    soap_error = "Message";
                                } else {
                                    try {

                                        //strVENUE = soapObject3.getPropertyAsString("VENUE");
                                        // strAGENTNAME = soapObject3.getPropertyAsString("AGENTNAME");
                                        // strROLE = soapObject3.getPropertyAsString("ROLE");
                                        //strCONTACTNUMBER = soapObject3.getPropertyAsString("CONTACTNUMBER");
                                        // strMEETINGOUTCOME = soapObject3.getPropertyAsString("MEETINGOUTCOME");
                                        //strEMAILID = soapObject3.getPropertyAsString("EMAILID");
                                        strROW_ID = soapObject3.getPropertyAsString("ROW_ID");// customer_name
                                        strCONTACTPERSON = soapObject3.getPropertyAsString("CUSTOMER_NAME");// customer_name
                                        strCUST_ROLE = soapObject3.getPropertyAsString("CUST_ROLE");// channel
                                        strPRODUCTNAME = soapObject3.getPropertyAsString("PRODUCTNAME");
                                        strAPPOINTDATE = soapObject3.getPropertyAsString("APPOINTMENT_DATE");
                                        strMOBILE_NO = soapObject3.getPropertyAsString("MOBILE_NO");
                                        strADDRESS1 = soapObject3.getPropertyAsString("ADDRESS1");
                                        strSTATUS = soapObject3.getPropertyAsString("STATUS");
                                        strREMARKS = soapObject3.getPropertyAsString("REMARKS");
                                        strPINCODE = soapObject3.getPropertyAsString("PINCODE");


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    sharedPreferences = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();

                                    editor.putString("ROW_ID" + i, strROW_ID);
                                    editor.putString("CUSTOMER_NAME" + i, strCONTACTPERSON); // customer_name
                                    editor.putString("ADDRESS1" + i, strADDRESS1);
                                    editor.putString("CUST_ROLE" + i, strCUST_ROLE); //role= customer
                                    editor.putString("MOBILE_NO" + i, strMOBILE_NO);
                                    editor.putString("PRODUCTNAME" + i, strPRODUCTNAME);
                                    editor.putString("APPOINTMENT_DATE" + i, strAPPOINTDATE);
                                    editor.putString("STATUS" + i, strSTATUS);
                                    editor.putString("REMARKS" + i, strREMARKS);
                                    editor.putString("PINCODE" + i, strPINCODE);

                                    editor.commit();

                                    EtrackernewcustomerListData items = new EtrackernewcustomerListData();
                                    items.CUSTOMER_NAME = strCONTACTPERSON;
                                    items.CUST_ROLE = strCUST_ROLE;
                                    items.ADDRESS1 = strADDRESS1;
                                    items.PRODUCTNAME = strPRODUCTNAME;
                                    items.APPOINTMENT_DATE = strAPPOINTDATE;
                                    items.STATUS = strSTATUS;
                                    items.REMARKS = strREMARKS;
                                    items.MOBILE_NO = strMOBILE_NO;
                                    items.ROW_ID = strROW_ID;
                                    items.PINCODE = strPINCODE;

                                    // added to remove null contact person
                                /*if (strCONTACTPERSON.equals("") || strCONTACTPERSON == null) {

                                } else {*/
                                    dataList.add(items.CUSTOMER_NAME); // contact_person== customer_name
                                    // }


                                    NewCustomerItemlist.add(items);
                                    Log.i("NewAgentItemlist:=>", NewCustomerItemlist.toString());
                                }

                                } catch(Exception e){
                                    e.printStackTrace();
                                }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("onPostExecute: result "+s);

            if (s.equals("Error")) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(mContext, "Server Error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    swipeRefreshLayout.setRefreshing(false);
                   // Toast.makeText(mContext, "Data not available", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setVisibility(View.GONE);

                    txtNoData.setVisibility(View.VISIBLE);
                    txtNoData.setText("No Data Available");

                }else if (s.equals("Message")) {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(mContext, "Error while fetching Engagement Tracker data !!", Toast.LENGTH_SHORT).show();
                } else {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);


                    swipeRefreshLayout.setRefreshing(false);
                    txtNoData.setVisibility(View.GONE);

                    if (NewCustomerArrayList != null) {
                        NewCustomerArrayList.clear();
                    }

                    if (NewCustomerItemlist.size() != 0)
                        for (EtrackernewcustomerListData obj : NewCustomerItemlist)
                            NewCustomerArrayList.add(obj);

                    System.out.println("NewAgentArrayList:=>" + NewCustomerArrayList.size());
                    System.out.println("dataList:=>" + dataList.size());

                    loaddata();

                }
            }
        }
    }

    //end Pooja
}
