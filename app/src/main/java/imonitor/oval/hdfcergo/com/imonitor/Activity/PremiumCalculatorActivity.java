package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.HomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;


public class PremiumCalculatorActivity extends AppCompatActivity implements View.OnClickListener {


    Button btn_search;
    EditText edt_Search_Type;
    ProgressDialog pDialog;
    TextView txt;
    Spinner spin_link, spin_title;
    WebView webview;
    LinearLayout ll_home_tab, ll_travel_tab, ll_motor_tab, ll_health_tab;
    RelativeLayout rl_webview;
    TextView txtNoInternet;
    ConnectivityManager cManager;
    NetworkInfo info;
    String url;
    ImageView travel_image, home_image, motor_image, health_image;

    ProgressBar prgs;
    Boolean showProgressOnSplashScreen = true;
    RelativeLayout splash;
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.MATCH_PARENT);
        setContentView(R.layout.activity_premium_calculator);

        setAppToolbar();
        initializeComponents();
        setListeners();
    }

    private void setListeners() {
        ll_home_tab.setOnClickListener(this);
        ll_travel_tab.setOnClickListener(this);
        ll_motor_tab.setOnClickListener(this);
        ll_health_tab.setOnClickListener(this);
    }


    private void initializeComponents() {


        webview = (WebView) findViewById(R.id.webview_link);

        prgs = (ProgressBar) findViewById(R.id.progressBar);
        // splash screen View

        txtNoInternet = (TextView) findViewById(R.id.txtNoInternet);
        rl_webview = (RelativeLayout) findViewById(R.id.rl_webview);

        ll_home_tab = (LinearLayout) findViewById(R.id.ll_home_tab);
        ll_travel_tab = (LinearLayout) findViewById(R.id.ll_travel_tab);
        ll_motor_tab = (LinearLayout) findViewById(R.id.ll_motor_tab);
        ll_health_tab = (LinearLayout) findViewById(R.id.ll_health_tab);

        health_image = (ImageView) findViewById(R.id.health_image);
        travel_image = (ImageView) findViewById(R.id.travel_image);
        motor_image = (ImageView) findViewById(R.id.motor_image);
        home_image = (ImageView) findViewById(R.id.home_image);
        home_image = (ImageView) findViewById(R.id.home_image);


        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);

        setDefaultSelected();
    }

    private void setDefaultSelected() {
        ll_travel_tab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


        ll_home_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
        ll_health_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
        ll_motor_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));

        cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        info = cManager.getActiveNetworkInfo();
        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            txtNoInternet.setVisibility(View.VISIBLE);
            webview.setVisibility(View.GONE);
            rl_webview.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
        } else {

            webview.setVisibility(View.VISIBLE);
            rl_webview.setVisibility(View.VISIBLE);
            txtNoInternet.setVisibility(View.GONE);

            url = getResources().getString(R.string.travel_online);
            Log.d("", "print values" + url);

            webview.setWebViewClient(new MyBrowser());
            webview.loadUrl(url);
        }
    }

    private void setAppToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_link);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Constants.callIntent(PremiumCalculatorActivity.this, HomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_travel_tab:
                ll_travel_tab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                ll_home_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_health_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_motor_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));

                cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = cManager.getActiveNetworkInfo();

                if ((info == null || !info.isConnected() || !info.isAvailable())) {
                    txtNoInternet.setVisibility(View.VISIBLE);
                    webview.setVisibility(View.GONE);
                    rl_webview.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                } else {

                    webview.setVisibility(View.VISIBLE);
                    rl_webview.setVisibility(View.VISIBLE);
                    txtNoInternet.setVisibility(View.GONE);

                    url = getResources().getString(R.string.travel_online);
                    Log.d("", "print values" + url);
                    webview.setWebViewClient(new MyBrowser());
                    webview.loadUrl(url);
                }

                break;
            case R.id.ll_health_tab:
                ll_health_tab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


                ll_home_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_travel_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_motor_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));

                cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = cManager.getActiveNetworkInfo();

                if ((info == null || !info.isConnected() || !info.isAvailable())) {
                    txtNoInternet.setVisibility(View.VISIBLE);
                    webview.setVisibility(View.GONE);
                    rl_webview.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                } else {


                    webview.setVisibility(View.VISIBLE);
                    rl_webview.setVisibility(View.VISIBLE);
                    txtNoInternet.setVisibility(View.GONE);
                    url = getResources().getString(R.string.health_online);
                    Log.d("", "print values" + url);
                    webview.setWebViewClient(new MyBrowser());
                    webview.loadUrl(url);
                }
                break;

            case R.id.ll_motor_tab:
                ll_motor_tab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                ll_home_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_travel_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_health_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));

                cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = cManager.getActiveNetworkInfo();

                if ((info == null || !info.isConnected() || !info.isAvailable())) {
                    txtNoInternet.setVisibility(View.VISIBLE);
                    webview.setVisibility(View.GONE);
                    rl_webview.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();

                } else {
                    webview.setVisibility(View.VISIBLE);
                    rl_webview.setVisibility(View.VISIBLE);
                    txtNoInternet.setVisibility(View.GONE);
                    url = getResources().getString(R.string.motor_online);
                    Log.d("", "print values" + url);
                    webview.setWebViewClient(new MyBrowser());
                    webview.loadUrl(url);
                }
                break;

            case R.id.ll_home_tab:
                ll_home_tab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


                ll_motor_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_travel_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));
                ll_health_tab.setBackgroundColor(getResources().getColor(R.color.LightGrey));

                cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = cManager.getActiveNetworkInfo();

                if ((info == null || !info.isConnected() || !info.isAvailable())) {
                    txtNoInternet.setVisibility(View.VISIBLE);
                    webview.setVisibility(View.GONE);
                    rl_webview.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                } else {


                    webview.setVisibility(View.VISIBLE);
                    rl_webview.setVisibility(View.VISIBLE);
                    txtNoInternet.setVisibility(View.GONE);
                    url = getResources().getString(R.string.home_online);
                    Log.d("", "print values" + url);
                    webview.setWebViewClient(new MyBrowser());
                    webview.loadUrl(url);
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private class MyBrowser extends WebViewClient {


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (prgs.getVisibility() == View.GONE) {
                prgs.setVisibility(View.VISIBLE);
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (prgs.getVisibility() == View.GONE) {
                prgs.setVisibility(View.VISIBLE);
            }
            //  webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (prgs.getVisibility() == View.VISIBLE)
                prgs.setVisibility(View.GONE);


        }
    }


}
