package imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.EtrackerNewAgentListActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.EtrackerNewCustomerListActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.ExistingAgentListActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.OvalHomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.LeadActivity;
import imonitor.oval.hdfcergo.com.imonitor.R;

public class EtrackerHomeScreenActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout ovalhomerel;
    Button btn_etrackernewuser, btn_existinguser, btn_newcustomer, btn_today_meeting;
    Toolbar toolbar;
    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_etrackerhomescreen);

        findViewById();
        setAppToolbar();
        setClickEvents();
    }

    private void setClickEvents() {
        btn_etrackernewuser.setOnClickListener(this);
        btn_existinguser.setOnClickListener(this);
        btn_newcustomer.setOnClickListener(this);
        btn_today_meeting.setOnClickListener(this);
    }

    private void setAppToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_homescreen);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        SetActionBarTitle("Engagement Tracker");

        //  getSupportActionBar().setDefaultDisplayHomeAsUpEnabled( true );
        //  toolbar.setNavigationIcon( R.drawable.backarrow );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });
    }

    private void findViewById() {
        ovalhomerel = (RelativeLayout) findViewById(R.id.etrackerhomerel);
        btn_etrackernewuser = (Button) findViewById(R.id.btn_etrackernewuser);
        btn_existinguser = (Button) findViewById(R.id.btn_existinguser);
        btn_newcustomer = (Button) findViewById(R.id.btn_newcustomer);
        btn_today_meeting = (Button) findViewById(R.id.btn_todays_meeting);


    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    @Override
    public void onClick(View v) {
        if (v == btn_etrackernewuser) {
            Constants.callIntent(EtrackerHomeScreenActivity.this, EtrackerNewAgentListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        } else if (v == btn_existinguser) {
            Constants.callIntent(EtrackerHomeScreenActivity.this, ExistingAgentListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        } else if (v == btn_newcustomer) {
            // Constants.callIntent(EtrackerHomeScreenActivity.this, EtrackerNewCustomerListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
            Constants.callIntent(EtrackerHomeScreenActivity.this, LeadActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        } else if (v == btn_today_meeting) {
            Constants.callIntent(EtrackerHomeScreenActivity.this, EtrackerNewCustomerListActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        }
    }

    @Override
    public void onBackPressed() {

        Constants.callIntent(this, OvalHomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }
}
