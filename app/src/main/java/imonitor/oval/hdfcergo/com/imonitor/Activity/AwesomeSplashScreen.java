package imonitor.oval.hdfcergo.com.imonitor.Activity;

import android.content.Intent;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;

import imonitor.oval.hdfcergo.com.imonitor.AwesomeSplashLibrary.activity.AwesomeSplash;
import imonitor.oval.hdfcergo.com.imonitor.AwesomeSplashLibrary.cnst.Flags;
import imonitor.oval.hdfcergo.com.imonitor.AwesomeSplashLibrary.model.ConfigSplash;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 07/08/2017.
 */

public class AwesomeSplashScreen extends AwesomeSplash {


    @Override
    public void initSplash(final ConfigSplash configSplash) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        configSplash.setBackgroundColor(R.color.colorPrimary); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(2000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM);


        //Customize Logo
          // configSplash.setLogoSplash(R.mipmap.ic_launcher); //or any other drawable
//        configSplash.setAnimLogoSplashDuration(500); //int ms
//        configSplash.setAnimLogoSplashTechnique(Techniques.Landing);
//
        // configSplash.setTitleSplash("Oval Lite");
//        configSplash.setTitleTextColor(R.color.White);
//        configSplash.setTitleTextSize(20f); //float value
//        configSplash.setAnimTitleDuration(20); //int ms
//        configSplash.setAnimTitleTechnique(Techniques.SlideInUp);
      //  configSplash.setTitleFont("fonts/HelveticaNeueM.ttf");


        //Customize Title
//        configSplash.setTitleSplash2("Loading...");
//        configSplash.setTitleTextColor2(R.color.White);
//        configSplash.setTitleTextSize2(22f); //float value
//        configSplash.setAnimTitleDuration2(20); //int ms
//        configSplash.setAnimTitleTechnique2(Techniques.FlipInX);
      //  configSplash.setTitleFont2("fonts/streatwear.otf");

    }


    @Override
    public void animationsFinished() {
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(AwesomeSplashScreen.this, Login.class));

            }
        }, 1000);
    }
}
