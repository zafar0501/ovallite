package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackerexistagentListData;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Meenakshi Aher on 29/05/2017.
 */


public class EtrackerExistingAgentAdapter extends RecyclerView.Adapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    Context context;
    ColorGenerator generator = ColorGenerator.MATERIAL;
    String letter;
    View popuplayout1;
    Dialog connectiondialogue;
    Date dateOfMet;
    private ArrayList<EtrackerexistagentListData> items;
    private ArrayList<String> letterArraylist;
    private int itemLayout;


    public EtrackerExistingAgentAdapter(Context context, ArrayList<EtrackerexistagentListData> items, RecyclerView recyclerview, int itemLayout, ArrayList letterlistname) {
        this.items = items;
        this.itemLayout = itemLayout;
        this.context = context;
        this.letterArraylist = letterlistname;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
            vh = new ActivityViewHolder(v);


        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);
            vh = new ProgressViewHolder(v);

        }

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ActivityViewHolder) {
            try {
                EtrackerexistagentListData item = items.get(position);

               // System.out.println("Allnames:=>" + items.get(position).PEOPLEMET);

                if (items.get(position).PEOPLEMET != null || items.get(position).PEOPLEMET != "") {
                    String names = items.get(position).PEOPLEMET;

                    String names_list[] = names.split(",");
                    String name1 = names_list[0].toUpperCase();
                  //  String finalName = name1.substring(0,1).toUpperCase() + name1.substring(1);
                   // System.out.println("finalName: " +finalName);
                    ((ActivityViewHolder) holder).name.setText(name1);
                } else {
                    ((ActivityViewHolder) holder).name.setText("No Name Found");
                }


                //((ActivityViewHolder) holder).tv_meetingtype.setText(items.get(position).MEETINGTYPE);
                ((ActivityViewHolder) holder).tv_vertical.setText((items.get(position).VERTICAL).toUpperCase());
                ((ActivityViewHolder) holder).tv_subvertical.setText(items.get(position).SUBVERTICAL);
                ((ActivityViewHolder) holder).tv_venue.setText(items.get(position).VENUE);
                //        Get the first letter of list item

                letter = String.valueOf(items.get(position).PEOPLEMET.charAt(0));

                //      Create a new TextDrawable for our image's background
                TextDrawable drawable = TextDrawable.builder()
                        .buildRound(letter, generator.getRandomColor());

                ((ActivityViewHolder) holder).letter.setImageDrawable(drawable);
                // ((ActivityViewHolder) holder).letter = String.valueOf(items.get( position ).PEOPLEMET);


                ((ActivityViewHolder) holder).view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        setPopUpView(position);
                    }
                });
               /* holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setPopUpView(position);
                    }
                });*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    private String firstCharToUp(String name) {
        StringBuilder sb = new StringBuilder(name);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }


    public void setPopUpView(int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popuplayout1 = inflater.inflate(R.layout.popup_existsagentdetail, null, false);
        popuplayout1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        connectiondialogue = new Dialog(context);
        popuplayout1.setFocusable(false);

        TextView tv_plemet, tv_role, tv_venue, tv_pvertical, tv_psubvertical, tv_metngtype, tv_isactive, tv_createby, tv_mom,
                tv_branchempcode, tv_meetingdate, tv_actionreport;

        Button exit;
        exit = (Button) popuplayout1.findViewById(R.id.btn_exit);
        tv_plemet = (TextView) popuplayout1.findViewById(R.id.tv_plemet);

        tv_role = (TextView) popuplayout1.findViewById(R.id.tv_role);
        tv_venue = (TextView) popuplayout1.findViewById(R.id.tv_venue);
        tv_pvertical = (TextView) popuplayout1.findViewById(R.id.tv_pvertical);
        tv_psubvertical = (TextView) popuplayout1.findViewById(R.id.tv_psubvertical);
        tv_metngtype = (TextView) popuplayout1.findViewById(R.id.tv_metngtype);
        tv_meetingdate = (TextView) popuplayout1.findViewById(R.id.tv_meetingdate);

        tv_plemet.setText(items.get(position).PEOPLEMET);
        tv_role.setText(items.get(position).ROLE);
        tv_venue.setText(items.get(position).VENUE);
        tv_pvertical.setText(items.get(position).VERTICAL);
        tv_psubvertical.setText(items.get(position).SUBVERTICAL);
        tv_metngtype.setText(items.get(position).MINUTESOFMEETING);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // first example


        String dateMet = items.get(position).DATEOFMEETING;
        try {
            dateOfMet = sdf.parse(dateMet);
            String dateString = sdf.format(dateOfMet);
            tv_meetingdate.setText(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // tv_actionreport.setText(items.get(position).ACTIONTAKENREPORT);


        try {
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectiondialogue.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }


        connectiondialogue.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        connectiondialogue.setContentView(popuplayout1);

      /*  WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(connectiondialogue.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;*/
        //connectiondialogue.getWindow().setAttributes(lp);
        // connectiondialogue.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // connectiondialogue.getWindow().setGravity(Gravity.CENTER);
        connectiondialogue.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        connectiondialogue.setCancelable(true);
        connectiondialogue.show();

    }

    /* dialog = new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
     dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     dialog.setContentView(R.layout.loading_screen);
     Window window = dialog.getWindow();
     WindowManager.LayoutParams wlp = window.getAttributes();

     wlp.gravity = Gravity.CENTER;
     wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
     window.setAttributes(wlp);
     dialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.MATCH_PARENT);
     dialog.show();*/
    @Override
    public int getItemViewType(int position) {
        return items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    public class ActivityViewHolder extends RecyclerView.ViewHolder {
        public TextView name, tv_meetingtype, tv_venue, view, tv_vertical, tv_subvertical;
        ImageView letter;
        Typeface font, calibriFont, halveticaFont, arialFont, robotoFont;

        public ActivityViewHolder(View itemview) {
            super((itemview));
            name = (TextView) itemview.findViewById(R.id.from);
            //tv_meetingtype = (TextView) itemview.findViewById(R.id.tv_meetingtype);
            tv_vertical = (TextView) itemview.findViewById(R.id.tv_vertical);
            tv_subvertical = (TextView) itemview.findViewById(R.id.tv_subvertical);
            tv_venue = (TextView) itemview.findViewById(R.id.tv_venue);
            view = (TextView) itemview.findViewById(R.id.view);
            letter = (ImageView) itemView.findViewById(R.id.gmailitem_letter);

            font = Typeface.createFromAsset(context.getAssets(), "fonts/LoraRegular.ttf");
            calibriFont = Typeface.createFromAsset(context.getAssets(), "fonts/calibri.ttf");
            halveticaFont = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueM.ttf");
            arialFont = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
            robotoFont = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoMedium.ttf");


            name.setTypeface(calibriFont);
            tv_vertical.setTypeface(calibriFont);
            tv_subvertical.setTypeface(calibriFont);
            tv_venue.setTypeface(calibriFont);
            view.setTypeface(calibriFont);

            /*itemview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setPopUpView(getAdapterPosition());
                }
            });*/
        }
    }

}

