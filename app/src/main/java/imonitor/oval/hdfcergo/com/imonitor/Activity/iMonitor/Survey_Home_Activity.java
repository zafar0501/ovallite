package imonitor.oval.hdfcergo.com.imonitor.Activity.iMonitor;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.data.BarDataSet;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Calendar;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getFilteredSurvey.METHOD_NAME_SUR_FLT_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getFilteredSurvey.SOAP_ACTION_SUR_FLT_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.METHOD_NAME_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getGWPFilter.SOAP_ACTION_GWP_FLT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getSurveyGraph.METHOD_NAME_SUR_Type;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getSurveyGraph.SOAP_ACTION_SUR_Type;

public class Survey_Home_Activity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    public static final String MY_PREFS_NAME = "HdfcErgo";

    BarDataSet barDataSet1;


    //Filter NAME AND VALUE
    String FLT_Name="";
    String FLT_Value="";


    // Barchart Parameter
    String C_Name="";
    String C_Value="";

    TextView Sur_Type,Sur_Quar, Sur_Year;
    TextView Sur_From,Sur_To,Sur_Show;
    TextView tv1,tv2,tv3,tv4,tv5,tv6;
    ImageView btnNext, btnPrevious;
    ProgressDialog progressDialog;
    ConsUrl con_url;
    Dialog dialog;
    String Survey[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_survey__home_);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        con_url = new ConsUrl();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplication(),MainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        // all Compnent Value initialize
        ViewInitilize();

        check_internet_connection();


    }

    private void ViewInitilize() {
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);
        //

        tv1 =(TextView)findViewById( R.id.txt1 );
        tv2 =(TextView)findViewById( R.id.txt2 );
        tv3 =(TextView)findViewById( R.id.txt3 );
        tv4 =(TextView)findViewById( R.id.txt4 );
        tv5 =(TextView)findViewById( R.id.txt5 );
        tv6 =(TextView)findViewById( R.id.txt6 );
        //TextView of Gwp


                Sur_Type =(TextView)findViewById( R.id.sur_type );
        Sur_Quar =(TextView)findViewById( R.id.sur_qurterly );
        Sur_Year =(TextView)findViewById( R.id.sur_yearly );

        //
        Sur_From=(TextView)findViewById( R.id.sur_from);
        Sur_To=(TextView)findViewById( R.id.sur_to );
        Sur_Show=(TextView)findViewById( R.id.sur_result );
        Sur_Type.setText( "Customer Satisfication Survey (Current Freeze Month)" );
    }

    private void check_internet_connection() {
        ConnectivityManager cManager=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(),"Please Check Internet Connection",Toast.LENGTH_SHORT).show();
        } else {

            // Calling GWP webserivce
            new Gwp_Service().execute(SOAP_ACTION_SUR_Type,METHOD_NAME_SUR_Type,"1","");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        menu.getItem(5).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case R.id.action_toolbar_1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                FLT_Name="";
                FLT_Value="";
                new Survey_Filter().execute(SOAP_ACTION_GWP_FLT,METHOD_NAME_GWP_FLT,"Agents","");
                return true;




            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(),MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.sur_qurterly:
                Sur_Quar.setTextColor(Color.RED);
                Sur_Year.setTextColor(Color.BLACK);

                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_SUR_Type,METHOD_NAME_SUR_Type,"2","");
                Survey=null;
                break;

            case R.id.sur_yearly:

                Sur_Quar.setTextColor(Color.RED);
                Sur_Year.setTextColor(Color.BLACK);


                //Gwp Service Call
                new Gwp_Service().execute(SOAP_ACTION_SUR_Type,METHOD_NAME_SUR_Type,"3","");
                Survey=null;
                break;



            case R.id.sur_result:
                if(Sur_From.getText().toString().equals("From Month"))
                {
                    Toast.makeText(Survey_Home_Activity.this,"Select 'From Month First' ",Toast.LENGTH_SHORT).show();
                }else {
                    if(Sur_To.getText().toString().equals("To Month"))
                    {
                        Toast.makeText(Survey_Home_Activity.this,"Select 'To Month First' ",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        //Gwp Service Call
                        new Gwp_Service().execute(SOAP_ACTION_SUR_Type,METHOD_NAME_SUR_Type,"1","");

                    }
                }

                break;

            case R.id.sur_from:
                // Calender
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int  mMonth = c.get(Calendar.MONTH);
                int  mDay = c.get(Calendar.DAY_OF_MONTH);
                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(Survey_Home_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month= monthOfYear+1;
                                String Mon = null;
                                int lengthMonth = String.valueOf(month).length();
                                if (lengthMonth==1)
                                {
                                    Mon="0"+month;
                                }
                                else {
                                    Mon=month+"";
                                }
                                String Day = null;
                                int lengthDay = String.valueOf(dayOfMonth).length();

                                if (lengthDay==1)
                                {
                                    Day="0"+dayOfMonth;
                                }
                                else {
                                    Day=dayOfMonth+"";
                                }

                                Sur_From.setText(year+"-"+Mon);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();

                break;
            case R.id.sur_to:
                // Calender

                if(Sur_From.getText().toString().equals("From Month"))
                {
                    Toast.makeText(Survey_Home_Activity.this,"Select 'From Month' First ",Toast.LENGTH_SHORT).show();
                }else

                {
                    Calendar c2 = Calendar.getInstance();
                    int mYear2 = c2.get(Calendar.YEAR);
                    int  mMonth2 = c2.get(Calendar.MONTH);
                    int  mDay2 = c2.get(Calendar.DAY_OF_MONTH);
                    // Launch Date Picker Dialog
                    DatePickerDialog dpd2 = new DatePickerDialog(Survey_Home_Activity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int month= monthOfYear+1;
                                    String Mon = null;
                                    int lengthMonth = String.valueOf(month).length();
                                    if (lengthMonth==1)
                                    {
                                        Mon="0"+month;
                                    }
                                    else {
                                        Mon=month+"";
                                    }
                                    String Day = null;
                                    int lengthDay = String.valueOf(dayOfMonth).length();

                                    if (lengthDay==1)
                                    {
                                        Day="0"+dayOfMonth;
                                    }
                                    else {
                                        Day=dayOfMonth+"";
                                    }

                                    Sur_To.setText(year+"-"+Mon);
                                }
                            }, mYear2, mMonth2, mDay2);
                    dpd2.show();
                }



                break;
            case R.id.btn_next:
                Constants.callIntent(Survey_Home_Activity.this, Inward_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                break;
            case R.id.btn_previous:
                Constants.callIntent(Survey_Home_Activity.this, Renewal_Home_Activity.class, R.anim.slide_in_left, R.anim.slide_out_left);
                break;


        }
    }

    private class Gwp_Service extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Survey_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("filterType",params[2] );
            request.addProperty("fromDate",Sur_From.getText().toString() );
            request.addProperty("toDate",Sur_To.getText().toString() );

            request.addProperty("sseId",sharedPreferences.getString("NTID", "0") );

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error="";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error=e.toString();
            } catch (XmlPullParserException e)
            {
                e.printStackTrace();
                soap_error=e.toString();
            }


            // Check Network Exception

            if(soap_error.equals(""))
            {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error=soapFault.toString();
                    soapFault.printStackTrace();
                }

                try  {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    for (int i=0;i<soapObject2.getPropertyCount();i++)
                    {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals( "Survey" ))
                        {

                            Object FreezMonthId = propertyTest.getProperty("FreezMonthId");
                            Object TotalBooking = propertyTest.getProperty("TotalBooking");
                            Object CsatSentCount = propertyTest.getProperty("CsatSentCount");
                            Object FullSatisfied = propertyTest.getProperty("FullSatisfied");
                            Object NotSatisfied = propertyTest.getProperty("NotSatisfied");
                            Object PartiallySatisfied = propertyTest.getProperty("PartiallySatisfied");

                            soap_error=FreezMonthId.toString()+","+TotalBooking.toString()+","+CsatSentCount.toString()+","+FullSatisfied.toString()+","
                                    +PartiallySatisfied.toString()+","+NotSatisfied.toString()+",";

                        }



                    }

                }catch (Exception  e)
                {
                    e.printStackTrace();
                }
            }else {

                soap_error="Error";
            }




            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            if(progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (s.equals("Error"))
            {
                Toast.makeText(Survey_Home_Activity.this,"Error in Response",Toast.LENGTH_SHORT ).show();
            }else
            {

                String Survey[]=s.split(",");

                try {
                    tv1.setText( "Freeze Month"+"\n"+"\n"+Survey[0] );
                    tv2.setText( "Total Booking"+"\n"+"\n"+Survey[1] );
                    tv3.setText( "Email Sent"+"\n"+"\n"+Survey[2] );
                    tv4.setText( "Fully Satisfied"+"\n"+"\n"+Survey[3] );
                    tv5.setText("Partially Satisfied"+"\n"+"\n"+Survey[4] );
                    tv6.setText("Not Satisfied"+"\n"+"\n"+Survey[5] );
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }




        }
    }

    private class Survey_Filter extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Survey_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            SoapObject request = new SoapObject( NAMESPACE, params[1] );
            request.addProperty( "sseId", sharedPreferences.getString("NTID", "0") );
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11 );
            envelope.setOutputSoapObject( request );
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE( con_url.TAG_URL );
            httpTransport.debug = true;
            String soap_error = "";
            try {
                httpTransport.call( params[0], envelope ); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                soap_error = e.toString();
                e.printStackTrace();
            }




            if (soap_error.equals( "" )) {
                try {
                    SoapObject result = null;
                    try {
                        result = (SoapObject) envelope.getResponse();
                    } catch (SoapFault soapFault) {
                        soap_error = soapFault.toString();
                        soapFault.printStackTrace();
                        soap_error = soapFault.toString();
                    }
                    SoapObject soapObject1 = (SoapObject) result.getProperty( 1 );
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty( 0 );


                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals( params[2] )) {

                            Object property1 = propertyTest.getProperty( "Name" );
                            FLT_Name = FLT_Name + property1.toString() + ",";

                            if (params[2].equals( "Agents" )) {
                                Object property2 = propertyTest.getProperty( "Value" );
                                FLT_Value = FLT_Value + property2.toString() + ",";
                            } else {

                                if (params[2].equals( "Product" ))
                                {
                                    FLT_Value = FLT_Name + property1.toString() + ",";
                                }

                            }


                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                soap_error = params[2] + ":" + params[3];
            } else {
                soap_error = "Error";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute( s );
            final String Res[] = s.split( ":" );
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (s.equals( "Error" )) {
                Toast.makeText( Survey_Home_Activity.this, "Error in Response", Toast.LENGTH_SHORT ).show();
            } else {
                final String names[] = FLT_Name.split( "," );
                // Create custom dialog object
                dialog = new Dialog( Survey_Home_Activity.this );
                // Include dialog.xml file
                dialog.setContentView( R.layout.custom );
                // Set dialog title
                dialog.setTitle( Res[0] );
                ListView lv = (ListView) dialog.findViewById( R.id.listView );
                ArrayAdapter<String> adapter = new ArrayAdapter<String>( Survey_Home_Activity.this, android.R.layout.simple_list_item_1, names );
                lv.setAdapter( adapter );

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom( dialog.getWindow().getAttributes() );
                lp.width = 500;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.RIGHT;

                dialog.getWindow().setAttributes( lp );
                dialog.show();

                lv.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        dialog.dismiss();
                        new Survey_Service().execute(SOAP_ACTION_SUR_FLT_Type,METHOD_NAME_SUR_FLT_Type,names[position],"");
                    }
                } );
            }
        }
    }


    private class Survey_Service extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Survey_Home_Activity.this,R.style.AlertDialogCustom );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("filterValue",params[2] );
            request.addProperty("graphType","AGENT" );
            request.addProperty("monthType","1" );
            request.addProperty("startDate","2016-01" );
            request.addProperty("endDate","2016-10" );
            request.addProperty("sseId",sharedPreferences.getString("NTID", "0") );

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            String soap_error="";
            try {
                httpTransport.call(params[0], envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e)
            {
                e.printStackTrace();
            }


            // Check Network Exception

            if(soap_error.equals(""))
            {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soap_error=soapFault.toString();
                    soapFault.printStackTrace();
                }

                try  {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                    for (int i=0;i<soapObject2.getPropertyCount();i++)
                    {

                        PropertyInfo pi = new PropertyInfo();
                        soapObject2.getPropertyInfo( i, pi );
                        SoapObject propertyTest = (SoapObject) pi.getValue();
                        if (pi.getName().equals( "Survey" ))
                        {

                            Object FreezMonthId = propertyTest.getProperty("FreezMonthId");
                            Object TotalBooking = propertyTest.getProperty("TotalBooking");
                            Object CsatSentCount = propertyTest.getProperty("CsatSentCount");
                            Object FullSatisfied = propertyTest.getProperty("FullSatisfied");
                            Object NotSatisfied = propertyTest.getProperty("NotSatisfied");
                            Object PartiallySatisfied = propertyTest.getProperty("PartiallySatisfied");

                            soap_error=FreezMonthId.toString()+","+TotalBooking.toString()+","+CsatSentCount.toString()+","+FullSatisfied.toString()+","
                                    +PartiallySatisfied.toString()+","+NotSatisfied.toString()+",";

                        }



                    }

                }catch (Exception  e)
                {
                    e.printStackTrace();
                }
            }else {

                soap_error="Error";
            }




            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            if(progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (s.equals("Error"))
            {
                Toast.makeText(Survey_Home_Activity.this,"Error in Response",Toast.LENGTH_SHORT ).show();
            }else
            {
                Toast.makeText(Survey_Home_Activity.this,s,Toast.LENGTH_SHORT ).show();
                String Survey[]=s.split(",");
                try
                {
                    tv1.setText( "Freeze Month"+"\n"+"\n"+Survey[0] );
                    tv2.setText( "Total Booking"+"\n"+"\n"+Survey[1] );
                    tv3.setText( "Email Sent"+"\n"+"\n"+Survey[2] );
                    tv4.setText( "Fully Satisfied"+"\n"+"\n"+Survey[3] );
                    tv5.setText("Partially Satisfied"+"\n"+"\n"+Survey[4] );
                    tv6.setText("Not Satisfied"+"\n"+"\n"+Survey[5] );
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }




        }
    }
}
