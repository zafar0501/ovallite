package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.EtrackerHomeScreenActivity;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.EtrackerNewCustomerAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackernewcustomerListData;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.VerticalSpaceItemDecorartion;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getNewCustomer.METHOD_GET_NEW_CUST;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getNewCustomer.SOAP_ACTION_GET_NEW_CUST;

/**
 * Created by Meenakshi Aher on 29/05/2017.
 */

public class EtrackerNewCustomerListActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {


    public static final String MY_PREFS_NAME = "HdfcErgo";

    String TAG_Soap_Response;
    RelativeLayout reliu;
    ConsUrl consUrl;
    ArrayList<EtrackernewcustomerListData> NewCustomerItemlist = new ArrayList<EtrackernewcustomerListData>();
    ArrayList<EtrackernewcustomerListData> NewCustomerArrayList = new ArrayList<EtrackernewcustomerListData>();
    RecyclerView Newagentrecyclerview;
    LinearLayoutManager mLayoutManager;
    EtrackerNewCustomerAdapter Newcustomeradapter;
    ArrayList<String> dataList = new ArrayList<String>();
    View popuplayout1;
    Dialog connectiondialogue;
    ImageView addagent;
    String branchcodeid, ssid, Loginname, Loginntid;
    TextView mTitle;
    private SharedPreferences sharedPreferences;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_newcustomerlist);
        consUrl = new ConsUrl();
        getSharedpreferenceData();
        findViews();
     // WebserviceCall();
    }

    private void getSharedpreferenceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void WebserviceCall() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(EtrackerNewCustomerListActivity.this)) {
            try {
                String newagentparameter = ssid;
                System.out.println("newagentparameter:=>" + newagentparameter);
                String role = "Customer";
                //  getNewCustomerList( newagentparameter,role );


                // edited by Pooja Patil at 09-06-17
                new GetNewCustomerList().execute(newagentparameter, role);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(reliu, Constants.offline_msg);
        }
    }



    private void findViews() {
        reliu = (RelativeLayout) findViewById(R.id.newreliu);

        toolbar = (Toolbar) findViewById(R.id.tool_barnewcustomer);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        addagent = (ImageView) toolbar.findViewById(R.id.addagent);
        addagent.setVisibility(View.INVISIBLE);

        SetActionBarTitle("Todays Meeting");
        addagent.setOnClickListener(this);

        Newagentrecyclerview = (RecyclerView) findViewById(R.id.newcustomerList);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layoutnewcustomer);
        swipeRefreshLayout.setOnRefreshListener(this);

        Newagentrecyclerview.setHasFixedSize(true);
        Newagentrecyclerview.setLayoutManager(new LinearLayoutManager(this));
        Newagentrecyclerview.setItemAnimator(new DefaultItemAnimator());
        Newagentrecyclerview.addItemDecoration(new VerticalSpaceItemDecorartion(10));
        mLayoutManager = new LinearLayoutManager(this);
        Newagentrecyclerview.setLayoutManager(mLayoutManager);

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        WebserviceCall();
                    }
                }
        );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                /*finish();
                //overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);*/
            }
        });
    }


    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);
    }

    @Override
    public void onClick(View v) {
        if (v == addagent) {
            //  Constants.callIntent(this, AddEtrackerNewCustomerActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
           // Constants.callIntent(this, LeadActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        }
    }

/*    private void getNewCustomerList(String parameter,String role) {

        if (NewCustomerItemlist != null) {
            NewCustomerItemlist.clear();
        }

        String API = consUrl.WebUrl;

        Retrofit retrofit = new Retrofit.Builder().baseUrl( API ).addConverterFactory( GsonConverterFactory.create() ).build();

        Webservices webservices = retrofit.create( Webservices.class );
        Call call = webservices.getNewAgentlist( parameter,role );
        call.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit.Response<ResponseBody> response, Retrofit retrofit) {


                ResponseBody body = response.body();
                if (response.body() == null) {
                    Toast.makeText( getApplicationContext(), "Error:" + response.message(), Toast.LENGTH_SHORT ).show();
                    return;
                }
                try {
                    BufferedReader reader = new BufferedReader( new InputStreamReader( body.byteStream() ) );
                    StringBuilder out = new StringBuilder();
                    String newline = System.getProperty( "line.separator" );
                    String line;
                    while ((line = reader.readLine()) != null) {
                        out.append( line );
                        out.append( newline );
                    }
                    JSONObject jsonobject = new JSONObject( out.toString() );
                    Log.i( "JSON:=>", jsonobject.toString() );
                    System.out.println( "NewagentListResponse:=>" + jsonobject.toString() );

                    JSONArray jsonArrayHot = jsonobject.getJSONArray( "Table" );
                    for (int i = 0; i < jsonArrayHot.length(); i++) {
                        JSONObject myJSON = jsonArrayHot.getJSONObject( i );

                        EtrackernewcustomerListData items = new EtrackernewcustomerListData();

                        items.ID = myJSON.getString( "ID" );
                        items.ROLE = myJSON.getString( "ROLE" );
                        items.AGENTNAME = myJSON.getString( "AGENTNAME" );
                        items.CONTACTPERSON = myJSON.getString( "CONTACTPERSON" );
                        items.MEETINGATTENDEDBY = myJSON.getString( "MEETINGATTENDEDBY" );
                        items.DATEOFMEETING = myJSON.getString( "DATEOFMEETING" );
                        items.CREATEDDATE = myJSON.getString( "CREATEDDATE" );
                        items.EMAILID = myJSON.getString( "EMAILID" );
                        items.CONTACTNUMBER = myJSON.getString( "CONTACTNUMBER" );
                        items.UPDATEDBY = myJSON.getString( "UPDATEDBY" );
                        items.CREATEDBY = myJSON.getString( "CREATEDBY" );
                        items.TIME = myJSON.getString( "TIME" );
                        items.VENUE = myJSON.getString( "VENUE" );
                        items.ISACTIVE = myJSON.getString( "ISACTIVE" );
                        items.GPSADDRESS = myJSON.getString( "GPSADDRESS" );
                        items.SSE_ID = myJSON.getString( "SSE_ID" );
                        items.MEETINGOUTCOME = myJSON.getString( "MEETINGOUTCOME" );
                        items.MINUTESOFMEETING = myJSON.getString( "MINUTESOFMEETING" );
                        items.UPDATEDDATE = myJSON.getString( "UPDATEDDATE" );
                        items.TRAINEECODE = myJSON.getString( "TRAINEECODE" );

                        dataList.add( items.CONTACTPERSON );

                        NewCustomerItemlist.add( items );

                        Log.i( "NewAgentItemlist:=>", NewCustomerItemlist.toString() );

                    }
                    swipeRefreshLayout.setRefreshing( false );

                    System.out.println( "NewAgentItemlist:=>" + NewCustomerItemlist.toString() );
                    System.out.println( "NewAgentItemlistSizzzze:=>" +NewCustomerItemlist.size() );

                    if (NewCustomerArrayList != null) {
                        NewCustomerArrayList.clear();
                    }

                    if (NewCustomerItemlist.size() != 0)
                        for (EtrackernewcustomerListData obj : NewCustomerItemlist)
                            NewCustomerArrayList.add( obj );

                    System.out.println( "NewAgentArrayList:=>" + NewCustomerArrayList.size() );

                    loaddata();

                    Log.i( "status:=>", "1" );

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i( "Error:", "," + t.getMessage() );
                swipeRefreshLayout.setRefreshing( false );
            }
        } );
    }*/

    private void loaddata() {
        if (NewCustomerArrayList.isEmpty()) {
            System.out.println("EmptySize:=>" + NewCustomerArrayList.size());
            Constants.snackbar(reliu, "No Record Found..");

        } else {
            Newcustomeradapter = new EtrackerNewCustomerAdapter(this, NewCustomerArrayList, Newagentrecyclerview, R.layout.template_newcustomer, dataList);
            Newagentrecyclerview.setAdapter(Newcustomeradapter);
            Newcustomeradapter.notifyDataSetChanged();
            Newagentrecyclerview.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), Newagentrecyclerview, new ClickListener() {
                @Override
                public void onClick(View view,final int position) {
                    LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                    parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("position 11: "+position);
                            Newcustomeradapter.setPopUpView(position);
                        }
                    });
                }

                @Override
                public void onLongClick(View view, int position) {
                   // Newcustomeradapter.setPopUpView(position);
                }
            }));
        }
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        //   WebserviceCall();
        swipeRefreshLayout.setRefreshing(false);
    }

    /*  private void voidinitpopupwindow(View v) {
          LayoutInflater inflater = (LayoutInflater) getSystemService( this.LAYOUT_INFLATER_SERVICE );
          popuplayout1 = inflater.inflate( R.layout.popup_existsagentdetail, null );
          connectiondialogue = new Dialog( this );
          popuplayout1.setFocusable( false );
          TextView tv;
          Button exit;
          exit = (Button) popuplayout1.findViewById( R.id.btn_exit );
          tv = (TextView) popuplayout1.findViewById( R.id.tv_actionreport );
          try {
              exit.setOnClickListener( new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      connectiondialogue.dismiss();
                  }
              } );

          } catch (Exception e) {
              e.printStackTrace();

          }
          connectiondialogue.getWindow().requestFeature( Window.FEATURE_NO_TITLE );
          connectiondialogue.setContentView( popuplayout1 );
          connectiondialogue.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN );
          connectiondialogue.setCancelable( true );
          connectiondialogue.show();

      }*/

    @Override
    public void onBackPressed() {

        Constants.callIntent(this, EtrackerHomeScreenActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    // edited by Pooja Patil at 09-06-17
    //start Pooja
    public class GetNewCustomerList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog = new ProgressDialog( ExistingAgentListActivity.this );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
           /* SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("type", params[2]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            request.addProperty("condition", params[3]);*/

            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_NEW_CUST);
            request.addProperty("SSE_ID", params[0]);
            request.addProperty("CUST_ROLE", params[1]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_GET_NEW_CUST, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    //customer name, product name,address 1
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        TAG_Soap_Response = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strCONTACTPERSON = "",strROW_ID="";
                        String strAGENTNAME = "";
                        String strVENUE = "", strROLE = "", strCONTACTNUMBER = "", strMEETINGOUTCOME = "", strEMAILID = "";
                        String strCUST_ROLE="",strPRODUCTNAME="",strAPPOINTDATE="",strMOBILE_NO="",strADDRESS1="",strPINCODE="",strSTATUS="",strREMARKS="";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                //more view
                                // cust_role, product name, appoint date, cust_name, mob , address1 status, remarks
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {

                                    //strVENUE = soapObject3.getPropertyAsString("VENUE");
                                   // strAGENTNAME = soapObject3.getPropertyAsString("AGENTNAME");
                                   // strROLE = soapObject3.getPropertyAsString("ROLE");
                                    //strCONTACTNUMBER = soapObject3.getPropertyAsString("CONTACTNUMBER");
                                   // strMEETINGOUTCOME = soapObject3.getPropertyAsString("MEETINGOUTCOME");
                                    //strEMAILID = soapObject3.getPropertyAsString("EMAILID");
                                    strROW_ID = soapObject3.getPropertyAsString("ROW_ID");// customer_name
                                    strCONTACTPERSON = soapObject3.getPropertyAsString("CUSTOMER_NAME");// customer_name
                                    strCUST_ROLE = soapObject3.getPropertyAsString("CUST_ROLE");// role
                                    strPRODUCTNAME = soapObject3.getPropertyAsString("PRODUCTNAME");
                                    strAPPOINTDATE = soapObject3.getPropertyAsString("APPOINTMENT_DATE");
                                    strMOBILE_NO = soapObject3.getPropertyAsString("MOBILE_NO");
                                    strADDRESS1 = soapObject3.getPropertyAsString("ADDRESS1");
                                    strSTATUS = soapObject3.getPropertyAsString("STATUS");
                                    strREMARKS = soapObject3.getPropertyAsString("REMARKS");
                                    strPINCODE = soapObject3.getPropertyAsString("PINCODE");


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }



                                sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("ROW_ID" + i, strROW_ID);
                                editor.putString("CUSTOMER_NAME" + i, strCONTACTPERSON); // customer_name
                                editor.putString("ADDRESS1" + i, strADDRESS1);
                                editor.putString("CUST_ROLE" + i, strCUST_ROLE); //role= customer
                                editor.putString("MOBILE_NO" + i, strMOBILE_NO);
                                editor.putString("PRODUCTNAME" + i, strPRODUCTNAME);
                                editor.putString("APPOINTMENT_DATE" + i, strAPPOINTDATE);
                                editor.putString("STATUS" + i, strSTATUS);
                                editor.putString("REMARKS" + i, strREMARKS);
                                editor.putString("PINCODE" + i, strPINCODE);

                                editor.commit();

                                EtrackernewcustomerListData items = new EtrackernewcustomerListData();
                                items.CUSTOMER_NAME = strCONTACTPERSON;
                                items.CUST_ROLE = strCUST_ROLE;
                                items.ADDRESS1 = strADDRESS1;
                                items.PRODUCTNAME = strPRODUCTNAME;
                                items.APPOINTMENT_DATE = strAPPOINTDATE;
                                items.STATUS = strSTATUS;
                                items.REMARKS = strREMARKS;
                                items.MOBILE_NO = strMOBILE_NO;
                                items.ROW_ID=strROW_ID;
                                items.PINCODE = strPINCODE;

                                // added to remove null contact person
                                /*if (strCONTACTPERSON.equals("") || strCONTACTPERSON == null) {

                                } else {*/
                                    dataList.add(items.CUSTOMER_NAME); // contact_person== customer_name
                               // }


                                NewCustomerItemlist.add(items);
                                Log.i("NewAgentItemlist:=>", NewCustomerItemlist.toString());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("0")) {

            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (NewCustomerArrayList != null) {
                    NewCustomerArrayList.clear();
                }

                if (NewCustomerItemlist.size() != 0)
                    for (EtrackernewcustomerListData obj : NewCustomerItemlist)
                        NewCustomerArrayList.add(obj);

                System.out.println("NewAgentArrayList:=>" + NewCustomerArrayList.size());
                System.out.println("dataList:=>" + dataList.size());

                loaddata();

                Log.i("status:=>", "1");
            }
        }
    }

    //end Pooja
}
