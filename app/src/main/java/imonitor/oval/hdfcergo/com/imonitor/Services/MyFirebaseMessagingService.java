package imonitor.oval.hdfcergo.com.imonitor.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import imonitor.oval.hdfcergo.com.imonitor.Activity.Login;
import imonitor.oval.hdfcergo.com.imonitor.R;

/**
 * Created by Zafar.Hussain on 28/08/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    Bitmap bitmap;

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {

        Log.w("fcm", "received notification");
        // Log.d("fcm", "From: " + remoteMessage.getFrom());
        //  Log.d("fcm", "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //Log.d("fcm", "Notification Message: " + remoteMessage.getData());
        // Toast.makeText(this,"Message "+remoteMessage,Toast.LENGTH_SHORT).show();

        if (remoteMessage.getData().size() > 0) {
            Log.d("fcm", "Message data payload: " + remoteMessage.getData());
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("fcm", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //  String title = remoteMessage.getData().get("title");
        //message will contain the Push Message
        //  String message = remoteMessage.getData().get("message");

        //imageUri will contain URL of the image to be displayed with Notification
        //   String imageUri = remoteMessage.getData().get("image");
        if (remoteMessage.getNotification().getIcon() != null) {
            Log.d("fcm", "Message Notification Icon: " + remoteMessage.getNotification().getIcon());
        }

        String imageUri = remoteMessage.getNotification().getIcon();
        //To get a Bitmap image from the URL received
        bitmap = getBitmapfromUrl(imageUri);

        // sendNotification(message, title, bitmap);
        sendNotification(remoteMessage, bitmap);

    }

    private void sendNotification(RemoteMessage message, Bitmap bigPicture) {
        Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

        Intent intent = new Intent(this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (bigPicture != null) {
                notificationBuilder.setSmallIcon(R.drawable.ic_notification);
                notificationBuilder.setLargeIcon(bitmap);
                notificationBuilder.setContentTitle(message.getNotification().getTitle())
                        .setContentText(message.getNotification().getBody())
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bigPicture)
                                .setBigContentTitle(message.getNotification().getTitle())
                                .setSummaryText(message.getNotification().getBody()))
                        .setContentIntent(pendingIntent)
                        .setSound(defaultSoundUri);

            } else {
                notificationBuilder.setSmallIcon(R.drawable.ic_notification);
                notificationBuilder.setLargeIcon(bitmap);
                notificationBuilder.setContentTitle(message.getNotification().getTitle())
                        .setContentText(message.getNotification().getBody())
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setSound(defaultSoundUri);
            }

        } else {
            //  notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setContentTitle(message.getNotification().getTitle())
                    .setContentText(message.getNotification().getBody())
                    .setAutoCancel(true)
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bigPicture)
                            .setBigContentTitle(message.getNotification().getTitle())
                            .setSummaryText(message.getNotification().getBody()))
                    .setSound(defaultSoundUri);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(0, notificationBuilder.build());
    }

    private void sendNotification(String messageBody, String title, Bitmap aBigBitmap) {
        Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher);
        //Drawable imageNot= ContextCompat.getDrawable(this,R.drawable.imonitor_old);

        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        // Bitmap aBigBitmap = ((BitmapDrawable)imageNot).getBitmap();

        Intent intent = new Intent(this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_notification);
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setContentTitle(title)
                    .setContentText(messageBody)
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setAutoCancel(false)
                    .setContentIntent(pendingIntent)
                    .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(aBigBitmap)
                            .setBigContentTitle(title)
                            .setSummaryText(messageBody))
                    .setSound(defaultSoundUri);


        } else {
            //  notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(false)
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(aBigBitmap)
                            .setBigContentTitle(title)
                            .setSummaryText(messageBody))
                    .setSound(defaultSoundUri);
        }
        //  if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

        // notificationBuilder.setSmallIcon(R.drawable.ic_notification)
        // .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.launcher))

        /*} else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(bitmap)
                    .setContentTitle(messageBody)
                    .setAutoCancel(false)
                    .setContentIntent(pendingIntent)
                    .setSound(defaultSoundUri);
        }*/

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    /*
    *To get a Bitmap image from the URL received
    */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
