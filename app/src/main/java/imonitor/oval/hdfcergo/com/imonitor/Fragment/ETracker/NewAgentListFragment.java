package imonitor.oval.hdfcergo.com.imonitor.Fragment.ETracker;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.RecyclerTouchListener;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;
import imonitor.oval.hdfcergo.com.imonitor.Adapter.EtrackerNewAgentAdapter;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackernewagentListData;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.VerticalSpaceItemDecorartion;

import static android.content.Context.MODE_PRIVATE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getNewAgentList.METHOD_GET_NEW_AGENT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getNewAgentList.SOAP_ACTION_GET_NEW_AGENT;

/**
 * Created by Zafar.Hussain on 16/06/2017.
 */

public class NewAgentListFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{
    public static final String MY_PREFS_NAME = "HdfcErgo";


    String TAG_Soap_Response;
    RelativeLayout reliu;
    ConsUrl consUrl;
    ArrayList<EtrackernewagentListData> NewAgentItemlist = new ArrayList<EtrackernewagentListData>();
    ArrayList<EtrackernewagentListData> NewAgentArrayList = new ArrayList<EtrackernewagentListData>();
    RecyclerView Newagentrecyclerview;
    LinearLayoutManager mLayoutManager;
    EtrackerNewAgentAdapter Newagentadapter;
    ArrayList<String> dataList = new ArrayList<String>();
    View popuplayout1;
    Dialog connectiondialogue;
    TextView mTitle;
    ImageView addagent;
    String branchcodeid, ssid, Loginname, Loginntid;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences sharedPreferences;
    private Toolbar toolbar;
    Context mContext;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_agent_list, container, false);
        consUrl = new ConsUrl();
        mContext =getActivity();
        getSharedpreferebnceData();
        findViews(view);
        // WebserviceCall();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layoutnew);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        if (NewAgentArrayList != null) {
                            NewAgentArrayList.clear();
                        }
                        Log.i("NewAgentItemlist 33:=>", NewAgentItemlist.toString());
                        WebserviceCall();
                    }
                }
        );
    }

    private void getSharedpreferebnceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(mContext);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void WebserviceCall() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.isNetworkInfo(getActivity())) {
            try {
                String newagentparameter = ssid;
                String role = "Agent";
                System.out.println("newagentparameter:=>" + newagentparameter);
                //getNewingAgentList( newagentparameter ,role);

                // edited by Pooja Patil at 08-06-17
                new GetNewingAgentList().execute(newagentparameter, role);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Constants.snackbar(reliu, Constants.offline_msg);
        }
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        // WebserviceCall();
        swipeRefreshLayout.setRefreshing(false);
    }
    private void findViews(View v) {
        reliu = (RelativeLayout) v.findViewById(R.id.nreliu);
        /*toolbar = (Toolbar) findViewById(R.id.tool_barnew);
        setSupportActionBar(toolbar);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        addagent = (ImageView) toolbar.findViewById(R.id.addagent);
        addagent.setVisibility(View.VISIBLE);

        SetActionBarTitle("New Agent List");
        addagent.setOnClickListener(this);*/

        Newagentrecyclerview = (RecyclerView) v.findViewById(R.id.newagentagentList);


        Newagentrecyclerview.setHasFixedSize(true);
        Newagentrecyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        Newagentrecyclerview.setItemAnimator(new DefaultItemAnimator());
        Newagentrecyclerview.addItemDecoration(new VerticalSpaceItemDecorartion(10));
        mLayoutManager = new LinearLayoutManager(mContext);
        Newagentrecyclerview.setLayoutManager(mLayoutManager);



    }
    private void loaddata() {

        if (NewAgentArrayList.isEmpty()) {
            System.out.println("EmptySize:=>" + NewAgentArrayList.size());
            Constants.snackbar(reliu, "No Record Found..");
        } else {
            Newagentadapter = new EtrackerNewAgentAdapter(mContext, NewAgentArrayList, Newagentrecyclerview, R.layout.template_newagentuser, dataList);
            Newagentrecyclerview.setAdapter(Newagentadapter);
            Newagentadapter.notifyDataSetChanged();


            Newagentrecyclerview.addOnItemTouchListener(new RecyclerTouchListener(mContext, Newagentrecyclerview, new ClickListener() {
                @Override
                public void onClick(View view,final int position) {
                    LinearLayout parentLayout = (LinearLayout) view.findViewById(R.id.message_container);
                    parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("position 11: "+position);
                            Newagentadapter.setPopUpView(position);
                        }
                    });

                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
        }
    }
    // edited by Pooja Patil at 16-06-17
    //start Pooja
    public class GetNewingAgentList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog = new ProgressDialog( ExistingAgentListActivity.this );
            progressDialog.setMessage( "Please Wait.." );
            progressDialog.setCancelable( false );
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
           /* SoapObject request = new SoapObject(NAMESPACE, params[1]);
            request.addProperty("type", params[2]);
            request.addProperty("sseId", sharedPreferences.getString("NTID", "0"));
            request.addProperty("condition", params[3]);*/

            SoapObject request = new SoapObject(NAMESPACE, METHOD_GET_NEW_AGENT);
            request.addProperty("sseId", params[0]);
            request.addProperty("Role", params[1]);


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(consUrl.TAG_URL);
            httpTransport.debug = true;
            String soap_error = "";
            try {
                //  httpTransport.call(params[0], envelope); //send request
                httpTransport.call(SOAP_ACTION_GET_NEW_AGENT, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                soap_error = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                soap_error = e.toString();
            }


            // Check Network Exception

            if (soap_error.equals("")) {

                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    System.out.println("Response:=>" + result.toString());
                } catch (SoapFault soapFault) {
                    soap_error = soapFault.toString();
                    soapFault.printStackTrace();
                }

                try {
                    SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                    if (soapObject1.toString().equals("anyType{}")) {
                        TAG_Soap_Response = "0";
                    } else {
                        SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);

                        String strCONTACTPERSON = "";
                        String strAGENTNAME = "";
                        String strVENUE = "", strROLE = "", strCONTACTNUMBER = "", strMEETINGOUTCOME = "", strEMAILID = "";

                        for (int i = 0; i < soapObject2.getPropertyCount(); i++) {

                            try {
                                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);

                                try {
                                    strCONTACTPERSON = soapObject3.getPropertyAsString("CONTACTPERSON");
                                    strVENUE = soapObject3.getPropertyAsString("VENUE");
                                    strAGENTNAME = soapObject3.getPropertyAsString("AGENTNAME");
                                    strROLE = soapObject3.getPropertyAsString("ROLE");
                                    strCONTACTNUMBER = soapObject3.getPropertyAsString("CONTACTNUMBER");
                                    strMEETINGOUTCOME = soapObject3.getPropertyAsString("MEETINGOUTCOME");
                                    strEMAILID = soapObject3.getPropertyAsString("EMAILID");


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                sharedPreferences = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("CONTACTPERSON" + i, strCONTACTPERSON);
                                editor.putString("VENUE" + i, strVENUE);
                                editor.putString("AGENTNAME" + i, strAGENTNAME);
                                editor.putString("CONTACTNUMBER" + i, strCONTACTNUMBER);
                                editor.putString("ROLE" + i, strROLE);
                                editor.putString("MEETINGOUTCOME" + i, strMEETINGOUTCOME);
                                editor.putString("EMAILID" + i, strEMAILID);

                                editor.commit();

                                EtrackernewagentListData items = new EtrackernewagentListData();
                                items.CONTACTPERSON = strCONTACTPERSON;
                                items.AGENTNAME = strAGENTNAME;
                                items.VENUE = strVENUE;
                                items.ROLE = strROLE;
                                items.MEETINGOUTCOME = strMEETINGOUTCOME;
                                items.EMAILID = strEMAILID;
                                items.CONTACTNUMBER = strCONTACTNUMBER;
                                // dataList.add(items.CONTACTPERSON);

                                NewAgentItemlist.add(items);

                                Log.i("NewAgentItemlist 110:=>", NewAgentItemlist.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                soap_error = "Error";
                TAG_Soap_Response = "0";
            }


            return soap_error;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("0")) {
            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (NewAgentArrayList != null) {
                    NewAgentArrayList.clear();
                }

                if (NewAgentItemlist.size() != 0)
                    for (EtrackernewagentListData obj : NewAgentItemlist)
                        NewAgentArrayList.add(obj);

                System.out.println("NewAgentArrayList 22:=>" + NewAgentArrayList.size());

                System.out.println("dataList:=>" + dataList.size());

                loaddata();

                Log.i("status:=>", "1");
            }
        }
    }
}
