package imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.ETrackerHomeNewActivity;
import imonitor.oval.hdfcergo.com.imonitor.GPS.GPSTracker;
import imonitor.oval.hdfcergo.com.imonitor.R;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getStatusDetails.METHOD_STATUS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.getStatusDetails.SOAP_ACTION_STATUS;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertNewETDetails.METHOD_INSERT;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.insertNewETDetails.SOAP_ACTION_INSERT;


public class LeadActivity extends AppCompatActivity {


    public static final String MY_PREFS_NAME = "HdfcErgo";
    // private static final String SOAP_ACTION = "http://tempuri.org/insertNewLeadwithCheckSum";
    //private static final String METHOD_NAME = "insertNewLeadwithCheckSum";

    GPSTracker gps;
    String venueAddr;
    Typeface calibriFont;
    String address, city, state, country, postalCode, knownName;
    Spinner spin_pname, spin_status, spin_sub_status, spin_exp, spin_channel_type;
    EditText edt_add1, edt_add2, edt_city, edt_mobile, edt_remark, edt_customer_name, edt_pin, edt_email, edt_mtparticpnt, edt_others_op;
    TextView txt_appoint_date, txt_appoint_time;
    Button btn_submit;
    ProgressDialog pDialog;
    String Str_Product;
    String Str_Status, Str_Status_Id;
    String Str_Sub_Status, Str_Sub_Status_Id;
    String Str_Add1;
    String Str_Exp, expStatus;
    String Str_City;
    String Str_Mobile;
    String Str_Remark;
    String Str_Customer_Name;
    String Str_Channel;
    String Str_Pin;
    String Str_Email;
    String Str_App_Date;
    String Str_App_Time;
    String Str_Participants;
    ConsUrl con_url;
    String ssid, Loginname, Loginntid;
    String lattitude, longitude;
    TextView mTitle;
    ImageView addagent;
    Context context;
    private SharedPreferences sharedPreferences;
    private int mYear, mMonth, mDay;
    private boolean valid = false;

    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target.length() != 10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lead);
        con_url = new ConsUrl();
        context = this;
        // First Check Internet Connection
        check_internet_connection();
        getSharedpreferenceData();


        setAppToolbar();
        getSharedprefLatLong();
        findViews();
        getlocation();
        setAdapters();
        setClickListeners();

    }

    private void setViews() {
        //addres state, and country = null
        //address  & state = null

        //address & country = null
        //state & country =null

        if (address.equals("") || state.equals("") || country.equals("")) {
            edt_add1.setText("");
        } else {
            // edt_add1.setText(address + ", " + state + ", " + country);
            edt_add1.setText(address);
        }

        // edt_add1.setText(address + ", " + state + ", " + country);
        edt_city.setText(city);
        edt_pin.setText(postalCode);
    }

    private void getSharedpreferenceData() {
        SharedPreferences pref_securitypin = PreferenceManager.getDefaultSharedPreferences(this);
        ssid = pref_securitypin.getString("loginssid", "");
        Loginname = pref_securitypin.getString("loginname", "");
        Loginntid = pref_securitypin.getString("loginntid", "");

        System.out.println("Loginname:=>" + Loginname);
        System.out.println("Loginssid:=>" + ssid);
        System.out.println("Loginntid:=>" + Loginntid);
    }

    private void getSharedprefLatLong() {
        String TAG = "Inside getSharedprefLatLong ";
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        lattitude = preferences.getString("Latitude", "");
        longitude = preferences.getString("Longitude", "");

        System.out.println(TAG + " Latitude: =>" + lattitude);
        System.out.println(TAG + " Longitude: =>" + longitude);
    }

    private void setAdapters() {
        // SPIN Channel Type
        ArrayAdapter<CharSequence> adapter_Spin_Channel;
        String[] Channel_Type = {"Select Channel Type", "Broker", "Agent", "Dealer"};
        adapter_Spin_Channel = new ArrayAdapter<CharSequence>(LeadActivity.this, R.layout.spinner_text, Channel_Type);

        spin_channel_type.setAdapter(adapter_Spin_Channel);
        spin_channel_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Str_Channel = spin_channel_type.getSelectedItem().toString();

                System.out.println("Channel Type: " + Str_Channel);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // SPIN Status Expectation
        ArrayAdapter<CharSequence> adapter_Spin_Exptn;
        String[] st_exp = {"Select Expectation", "Club Program", "Underwriting Guidelines", "Discount", "Pricing", "Others"};
        adapter_Spin_Exptn = new ArrayAdapter<CharSequence>(LeadActivity.this, R.layout.spinner_text, st_exp);

        spin_exp.setAdapter(adapter_Spin_Exptn);

        spin_exp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (spin_exp.getSelectedItem().toString().equals("Others")) {
                    edt_others_op.setVisibility(View.VISIBLE);
                    //      Str_Exp = "Others: " + edt_others_op.getText().toString();
                    // System.out.println("Othr: StExpectation: " + Str_Exp);

                    expStatus = "Others";

                } else {
                    // Str_Exp = spin_exp.getSelectedItem().toString();
                    // System.out.println("StExpectation: " + Str_Exp);
                    edt_others_op.setVisibility(View.GONE);
                    expStatus = "";

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // SPIN PRODUCT NAME
        ArrayAdapter<CharSequence> adapter_Spin_Product_Name;
        String[] Product_Name = {"Select Profile", "Multi Line", "Health", "Motor", "SME"};

        adapter_Spin_Product_Name = new ArrayAdapter<CharSequence>(LeadActivity.this, R.layout.spinner_text, Product_Name);

        spin_pname.setAdapter(adapter_Spin_Product_Name);

        //spinner onitem click lisner

        spin_pname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                Str_Product = spin_pname.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getlocation() {
        gps = new GPSTracker(LeadActivity.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double newLatitude = gps.getLatitude();
            double newLongitude = gps.getLongitude();
         /*LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation( latitude, longitude,
                    getApplicationContext(), new GeocoderHandler() );*/

            if (newLatitude == 0.0 && newLongitude == 0.0) {
                Toast.makeText(getApplicationContext(), "Cant get Location !! ", Toast.LENGTH_SHORT).show();
                address = "";
                city = "";
                state = "";
                country = "";
                postalCode = "";
                setViews();
            } else {
                //  Toast.makeText(getApplicationContext(), "latitude " + newLatitude + "\n longitude " + newLongitude, Toast.LENGTH_SHORT).show();
                //  double lat = Double.parseDouble(lattitude);
                // double longi = Double.parseDouble(longitude);

                //    List<Address> addresses = LocationAddress.getSpecificAddressDetails(context, latitude, longitude);
                List<Address> addresses = null;
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                try {
                    //  addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    addresses = geocoder.getFromLocation(newLatitude, newLongitude, 1);
                    Log.v("", "ListAddress " + addresses.toString());
                    if (addresses == null || addresses.size() == 0) {

                        Log.v("Loction address", "No Address returned!");

                    } else {

                        address = addresses.get(0).getAddressLine(0) + ",\n" + addresses.get(0).getAddressLine(1) + ",\n" + addresses.get(0).getAddressLine(2); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        country = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                        setViews();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

    }

    private void setClickListeners() {
        txt_appoint_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                // Launch Date Picker Dialog

                Date today = new Date();
                c.setTime(today);
                c.add(Calendar.DAY_OF_MONTH, -4); // Subtract 4 days
                long minDate = c.getTime().getTime();
                DatePickerDialog dpd = new DatePickerDialog(LeadActivity.this, R.style.PickerTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String Mon = null;
                                int lengthMonth = String.valueOf(month).length();

                                if (lengthMonth == 1) {
                                    Mon = "0" + month;
                                } else {
                                    Mon = month + "";
                                }
                                String Day = null;
                                int lengthDay = String.valueOf(dayOfMonth).length();

                                if (lengthDay == 1) {
                                    Day = "0" + dayOfMonth;
                                } else {
                                    Day = dayOfMonth + "";
                                }
                                txt_appoint_date.setText(year + "-" + Mon + "-" + Day);
                            }
                        }, mYear, mMonth, mDay);

                dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
                dpd.getDatePicker().setMinDate(minDate);
                dpd.show();
            }


        });
        txt_appoint_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(LeadActivity.this, R.style.PickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        int lengthHour = String.valueOf(selectedHour).length();
                        int lengthMinutes = String.valueOf(selectedMinute).length();

                        String Hour = null;

                        if (lengthHour == 1) {
                            Hour = "0" + selectedHour;
                        } else {
                            Hour = selectedHour + "";
                        }
                        String Minutes = null;


                        if (lengthMinutes == 1) {
                            Minutes = "0" + selectedMinute;
                        } else {
                            Minutes = selectedMinute + "";
                        }
                        txt_appoint_time.setText(Hour + ":" + Minutes);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }


        });

        // SPIN SUB STATUS
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {
                    new GetSoapAction().execute();
                }
            }
        });
    }

    private boolean validateFields() {
        String Email_Valid = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (spin_channel_type.getSelectedItem().toString().equals("Select Channel Type")) {
            Toast.makeText(getApplicationContext(), "Select Channel Type", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (spin_pname.getSelectedItem().toString().equals("Select Profile")) {
            Toast.makeText(getApplicationContext(), "Select Profile", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (edt_customer_name.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Name", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (!isValidPhoneNumber(edt_mobile.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Enter Mobile Num", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (!(edt_email.getText().toString().trim().equals(""))) {
            if (!(edt_email.getText().toString().matches(Email_Valid))) {
                Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                return valid;
            }
        }
        if (txt_appoint_date.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Meeting Date", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (txt_appoint_time.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Meeting Time", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (edt_add1.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Address ", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (edt_city.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Customer City", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (spin_status.getSelectedItem().toString().equals("Select Status")) {
            Toast.makeText(getApplicationContext(), "Enter Meeting Status", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (spin_exp.getSelectedItem().toString().equals("Select Expectation")) {
            Toast.makeText(getApplicationContext(), "Select Expectation", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if ((edt_others_op.getVisibility() == View.VISIBLE) && (edt_others_op.getText().toString().trim().equals(""))) {
            Toast.makeText(getApplicationContext(), "Enter Expectation", Toast.LENGTH_SHORT).show();
            return valid;
        }
        if (edt_remark.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter Remarks/MOM", Toast.LENGTH_SHORT).show();
            return valid;
        } else {
            valid = true;
        }

        return valid;
    }

    private void findViews() {

        calibriFont = Typeface.createFromAsset(getAssets(), "fonts/calibri.ttf");

        // Id define of all view
        edt_add1 = (EditText) findViewById(R.id.edt_add_1);
        //edt_add2= (EditText) findViewById(R.id.edt_add_2);
        edt_city = (EditText) findViewById(R.id.edt_city);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile_num);
        edt_remark = (EditText) findViewById(R.id.edt_remarks);
        edt_customer_name = (EditText) findViewById(R.id.edt_customer_name);
        edt_pin = (EditText) findViewById(R.id.edt_pincode);
        edt_email = (EditText) findViewById(R.id.edt_email);

        edt_others_op = (EditText) findViewById(R.id.edt_others_op);

        edt_mtparticpnt = (EditText) findViewById(R.id.edt_mtparticpnt);
        txt_appoint_date = (TextView) findViewById(R.id.txt_appoint_date);
        txt_appoint_time = (TextView) findViewById(R.id.txt_appoint_time);

        spin_pname = (Spinner) findViewById(R.id.spin_product_name);
        spin_status = (Spinner) findViewById(R.id.spin_status);
        spin_sub_status = (Spinner) findViewById(R.id.spin_sub_status);
        spin_channel_type = (Spinner) findViewById(R.id.spin_channel_Type);
        spin_exp = (Spinner) findViewById(R.id.spin_status_exptn);


        btn_submit = (Button) findViewById(R.id.btn_save);


    }

    private void setAppToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_newcust);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        addagent = (ImageView) toolbar.findViewById(R.id.addagent);
        addagent.setVisibility(View.INVISIBLE);

        SetActionBarTitle("New Prospect");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
       /* if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }*/
        /*SetActionBarTitle("Lead");//*/
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                // startActivity(new Intent(getApplicationContext(), MainActivity.class));
                //finish();
            }
        });

    }

    private void check_internet_connection() {

        ConnectivityManager cManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cManager.getActiveNetworkInfo();

        if ((info == null || !info.isConnected() || !info.isAvailable())) {

            Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
        } else {
            // SPIN STATUS
            new StatusAction_Service().execute();
        }


    }

    private void SetActionBarTitle(String title) {
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(getApplicationContext(),MainActivity.class));
        //finish();
        //   Constants.callIntent(this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);
        Intent newIntent = new Intent(LeadActivity.this, ETrackerHomeNewActivity.class);
        newIntent.putExtra("addFrom", "new prospect");
        startActivity(newIntent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

    }

    private class GetSoapAction extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LeadActivity.this,R.style.AlertDialogCustom);
            pDialog.setMessage("Please Wait.. Data is submitting");
            pDialog.setCancelable(false);
            pDialog.show();

            if (expStatus.equals("Others")) {
                Str_Exp = "Others: " + edt_others_op.getText().toString().trim();
            } else {
                Str_Exp = spin_exp.getSelectedItem().toString();
            }
            Str_Add1 = edt_add1.getText().toString().trim();
            //   Str_Add2=edt_add2.getText().toString();
            Str_City = edt_city.getText().toString().trim();
            Str_Mobile = edt_mobile.getText().toString().trim();
            Str_Remark = edt_remark.getText().toString().trim();
            Str_Customer_Name = edt_customer_name.getText().toString().trim();
            Str_Pin = edt_pin.getText().toString().trim();
            Str_Email = edt_email.getText().toString().trim();
            Str_App_Date = txt_appoint_date.getText().toString().trim();
            Str_App_Time = txt_appoint_time.getText().toString().trim();

            Str_Participants = edt_mtparticpnt.getText().toString().trim();
            Str_Participants = Str_Participants.replace('\n', ',');
            System.out.println("Participants: " + Str_Participants);

            if (spin_status.getSelectedItem().toString().equals("Select Status")) {
                Str_Status = "";
            } else {
                Str_Status = spin_status.getSelectedItem().toString();
            }
            btn_submit.setVisibility(View.GONE);
        }


        @Override
        protected String doInBackground(String... params) {
            // sandeep sir imei no =865980025996148
            String fullAddress = Str_Add1 + " " + Str_City;

            sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

          /*  System.out.println("PRODUCTNAME:=>" + Str_Product);
            System.out.println("CUSTOMER_NAME:=>" + Str_Customer_Name);
            System.out.println("SSE_NAME:=>" + Loginname);
            System.out.println("NT_ID:=>" + Loginntid);
            System.out.println("ADDRESS1:=>" + fullAddress);
            System.out.println("PinCode:=>" + Str_Pin);
            System.out.println("MOBILE_NO:=>" + Str_Mobile);
            System.out.println("STATUS:=>" + Str_Status);
            System.out.println("Remarks:=>" + Str_Remark);
            System.out.println("SSE_ID:=>" + ssid);*/
            System.out.println("DoInBack Expectation:=>" + Str_Exp);

            SoapObject request = new SoapObject(NAMESPACE, METHOD_INSERT);
            request.addProperty("PRODUCTNAME", Str_Product); // profile
            request.addProperty("SSE_ID", ssid);
            request.addProperty("LEAD_NO", "");
            //cust role
            request.addProperty("CUST_ROLE", Str_Channel); // channel
            request.addProperty("CUSTOMER_NAME", Str_Customer_Name);
            //SSE_NAME
            request.addProperty("SSE_NAME", Loginname);
            //NT_ID
            request.addProperty("NT_ID", Loginntid);
            request.addProperty("ADDRESS1", fullAddress);
            request.addProperty("PinCode", Str_Pin);
            request.addProperty("MOBILE_NO", Str_Mobile);
            request.addProperty("APPOINTMENT_DATE", Str_App_Date + " " + Str_App_Time);
            request.addProperty("STATUS", Str_Status);
            request.addProperty("Remarks", Str_Remark);
            request.addProperty("Latitude", lattitude);
            request.addProperty("Longitude", longitude);
            request.addProperty("ADDRESS2", Str_Participants); //Str_Participants
            request.addProperty("SUBSTATUS", Str_Exp); //Str_Exp

            // System.out.println("APPOINTMENT_DATE:=>" + Str_App_Date + " " + Str_App_Time);

            System.out.println("Parameters:=>" + request.toString());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);

            envelope.dotNet = true;

            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);

            httpTransport.debug = true;
            String strREsult = "";
            try {
                httpTransport.call(SOAP_ACTION_INSERT, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                strREsult = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                strREsult = e.toString();
            }

            if (strREsult.equals("")) {
                SoapPrimitive result_new = null;
                try {
                    result_new = (SoapPrimitive) envelope.getResponse();
                    System.out.println("Lead Activity: " + result_new.toString());
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                }

                strREsult = result_new.getValue().toString();

            } else {
                strREsult = "Error";
            }
            return strREsult;


        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();


            System.out.println("GetSoapAction: onPostExc: " + result);
            if (result.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {

                String Split[] = result.split(":");
                // if (Split[0].equals("1")) {
                if (result.equals("1")) {
                    Toast.makeText(getApplicationContext(), "Data  Insert Succesfully !!!", Toast.LENGTH_SHORT).show();
                    Intent newIntent = new Intent(LeadActivity.this, ETrackerHomeNewActivity.class);
                    newIntent.putExtra("addFrom", "new prospect");
                    startActivity(newIntent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    // Constants.callIntent(LeadActivity.this, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);

                } else {
                    Toast.makeText(getApplicationContext(), "Data Not Insert Succesfully !!!", Toast.LENGTH_SHORT).show();

                }
            }

        }

    }

    public class StatusAction_Service extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(LeadActivity.this,R.style.AlertDialogCustom);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            String status_response = "";
            SoapObject request = new SoapObject(NAMESPACE, METHOD_STATUS);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            try {
                httpTransport.call(SOAP_ACTION_STATUS, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
                status_response = e.toString();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                status_response = e.toString();
            }

            if (status_response.equals("")) {
                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                    Log.v("","GetStatusResp: "+result.toString());
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                }


                SoapObject soapObject1 = (SoapObject) result.getProperty(1);

                if (soapObject1.toString().equals("anyType{}")) {
                    status_response = "0";
                } else {
                    SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                    String count_pro = soapObject2.getPropertyCount() + "";
                    String Status_ID = "0,", Status_Name = "Select Status,";
                    for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                        SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                        String str_STATUS_ID = soapObject3.getPropertyAsString("STATUS_ID");
                        String str_STATUS_NAME = soapObject3.getPropertyAsString("STATUS_NAME");

                        Status_ID = Status_ID + str_STATUS_ID + ",";
                        Status_Name = Status_Name + str_STATUS_NAME + ",";

                        status_response = Status_ID + ":" + Status_Name;
                    }
                }

            } else {
                status_response = "Error";
            }
            return status_response;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pDialog.isShowing())
                pDialog.dismiss();
            if (s.equals("Error")) {
                Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
            } else {
                if (s.equals("0")) {
                    Toast.makeText(getApplicationContext(), "Response error", Toast.LENGTH_SHORT).show();
                } else {
                    String[] str_split = s.split(":");

                    final String[] str_status_id = str_split[0].split(",");
                    final String[] str_status_name = str_split[1].split(",");

                    Log.i("TAG", "Status Resposnse :" + s);
                    ArrayAdapter<CharSequence> adapter_Staus;


                    adapter_Staus = new ArrayAdapter<CharSequence>(LeadActivity.this, R.layout.spinner_text, str_status_name);

                    spin_status.setAdapter(adapter_Staus);

                    //spinner onitem click lisner

                    spin_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                            Str_Status = str_status_name[position];
                            Str_Status_Id = str_status_id[position];


                            Log.i("TAG", "Str_Status :" + Str_Status);
                            Log.i("TAG", "Status_Id :" + Str_Status_Id);

                            if (Str_Status.equals("Select Status")) {

                            } else {
                                //  new SubStatusAction_Service().execute(Str_Status_Id);
                            }


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }


        }

       /* private class SubStatusAction_Service extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected String doInBackground(String... params) {
                String status_response = "";
                SoapObject request = new SoapObject(NAMESPACE, METHOD_SUB_STATUS);
                request.addProperty("status_id", params[0]);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(request);
                envelope.dotNet = true;
                HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
                httpTransport.debug = true;
                try {
                    httpTransport.call(SOAP_ACTION_SUB_STATUS, envelope); //send request
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }


                SoapObject result = null;
                try {
                    result = (SoapObject) envelope.getResponse();
                } catch (SoapFault soapFault) {
                    soapFault.printStackTrace();
                }

                SoapObject soapObject1 = (SoapObject) result.getProperty(1);
                SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
                String count_pro = soapObject2.getPropertyCount() + "";
                String Sub_Status_ID = "0,", Sub_Status_Name = "Select Sub Status,";
                for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                    SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                    String str_SUB_STATUS_ID = soapObject3.getPropertyAsString("SUBSTATUS_ID");
                    String str_SUB_STATUS_NAME = soapObject3.getPropertyAsString("SUBSTATUS_NAME");

                    Sub_Status_ID = Sub_Status_ID + str_SUB_STATUS_ID + ",";
                    Sub_Status_Name = Sub_Status_Name + str_SUB_STATUS_NAME + ",";

                    status_response = Sub_Status_ID + ":" + Sub_Status_Name;
                }

                return status_response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (pDialog.isShowing())
                    pDialog.dismiss();

                String[] str_split = s.split(":");
                final String[] str_sub_status_id = str_split[0].split(",");
                final String[] str_sub_status_name = str_split[1].split(",");
                ArrayAdapter<CharSequence> adapter_Sub_Staus;
                adapter_Sub_Staus = new ArrayAdapter<CharSequence>(LeadActivity.this, R.layout.spinner_text, str_sub_status_name);
                spin_sub_status.setAdapter(adapter_Sub_Staus);

                //spinner onitem click lisner

                spin_sub_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        Str_Sub_Status = str_sub_status_name[position];
                        Str_Sub_Status_Id = str_sub_status_id[position];


                        Log.i("TAG", "Str_Sub_Status :" + Str_Sub_Status);
                        Log.i("TAG", "Str_Sub_Status_Id :" + Str_Sub_Status_Id);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }*/
    }

}
