package imonitor.oval.hdfcergo.com.imonitor.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import imonitor.oval.hdfcergo.com.imonitor.ConsUrl;
import imonitor.oval.hdfcergo.com.imonitor.Constants;
import imonitor.oval.hdfcergo.com.imonitor.Activity.EtTracker.LeadActivity;
import imonitor.oval.hdfcergo.com.imonitor.Activity.Homescreen.ETrackerHomeNewActivity;
import imonitor.oval.hdfcergo.com.imonitor.Services.Services;
import imonitor.oval.hdfcergo.com.imonitor.Entity.EtrackernewcustomerListData;
import imonitor.oval.hdfcergo.com.imonitor.R;
import imonitor.oval.hdfcergo.com.imonitor.UserControl.ClickListener;

import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.NAMESPACE;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.updateLead.METHOD_UPDATE_LEAD;
import static imonitor.oval.hdfcergo.com.imonitor.Services.Services.updateLead.SOAP_ACTION_UPDATE_LEAD;


/**
 * Created by Meenakshi Aher on 31/05/2017.
 */


public class EtrackerNewCustomerAdapter extends RecyclerView.Adapter {

    public static final String MY_PREFS_NAME = "HdfcErgo";

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    Context context;
    ColorGenerator generator = ColorGenerator.MATERIAL;
    String letter;
    View popuplayout1, popuplayoutEdit;
    Dialog connectiondialogue;
    EditText edt_remarks;
    Spinner spin_status;
    String remark, status_name;
    ConsUrl con_url;
    String rowID, clientName;
    String SoapObjectResponse;
    String str_STATUS_NAME;
    String lattitude, longitude;
    private ArrayList<EtrackernewcustomerListData> items;
    private ArrayList<String> letterArraylist;
    private int itemLayout;
    private ClickListener clicklistener = null;
    private SharedPreferences sharedPreferences;


    public EtrackerNewCustomerAdapter(Context context, ArrayList<EtrackernewcustomerListData> items, RecyclerView recyclerview, int itemLayout, ArrayList letterlistname) {
        this.items = items;
        this.itemLayout = itemLayout;
        this.context = context;
        this.letterArraylist = letterlistname;
        con_url = new ConsUrl();
        this.context = context;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        getSharedpreferenceData(viewType);
        getSharedprefLatLong();

        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
            vh = new ActivityViewHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);
            vh = new ProgressViewHolder(v);

        }

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ActivityViewHolder) {
            try {
                EtrackernewcustomerListData item = items.get(position);

                System.out.println("Allnames:=>" + items.get(position).CUSTOMER_NAME);
                System.out.println("Status4List 55" + items.get(position).STATUS);
                System.out.println("Remarks4List 55" + items.get(position).REMARKS);


                if (items.get(position).CUSTOMER_NAME != null || items.get(position).CUSTOMER_NAME != "") {
                    String names = items.get(position).CUSTOMER_NAME;

                    String names_list[] = names.split(",");
                    String name1 = names_list[0].toUpperCase();
                    ((ActivityViewHolder) holder).name.setText(name1);
                } else {
                    ((ActivityViewHolder) holder).name.setText("No Name Found");
                }

                ((ActivityViewHolder) holder).tv_profile.setText((items.get(position).PRODUCTNAME));
                ((ActivityViewHolder) holder).tv_venue.setText(items.get(position).ADDRESS1);
                ((ActivityViewHolder) holder).tv_channel.setText((items.get(position).CUST_ROLE).toUpperCase());

                letter = String.valueOf(items.get(position).CUSTOMER_NAME.charAt(0));

                TextDrawable drawable = TextDrawable.builder()
                        .buildRound(letter, generator.getRandomColor());

                ((ActivityViewHolder) holder).letter.setImageDrawable(drawable);
                ((ActivityViewHolder) holder).view.setTag(holder);


                ((ActivityViewHolder) holder).view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        setPopUpView(position);
                        //setPopUpEdit(position);

                    }
                });
                //parentLayout
               /* ((ActivityViewHolder) holder).parentLayout.setOnClickListener( new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        setPopUpView(position);
                    }
                } );*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    private void getSharedprefLatLong() {
        String TAG = "Inside getSharedprefLatLong ";
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        lattitude = preferences.getString("Latitude", "");
        longitude = preferences.getString("Longitude", "");

        System.out.println(TAG + " Latitude: =>" + lattitude);
        System.out.println(TAG + " Longitude: =>" + longitude);
    }

    public void setPopUpEdit(int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        popuplayoutEdit = inflater.inflate(R.layout.popup_editcustomer, null);
        connectiondialogue = new Dialog(context);
        popuplayoutEdit.setFocusable(false);

        TextView txt_client_id, txt_client_name;

        Button btn_save, btn_exit;


        // SPIN STATUS
        LeadActivity leadActivity = new LeadActivity();
        //  LeadActivity.StatusAction_Service statusService= new leadActivity.StatusAction_Service().execute();
        // leadActivity.new StatusAction_Service().execute();
        new StatusAction_Service().execute();

        txt_client_id = (TextView) popuplayoutEdit.findViewById(R.id.txt_cid);
        txt_client_name = (TextView) popuplayoutEdit.findViewById(R.id.txt_cname);
        edt_remarks = (EditText) popuplayoutEdit.findViewById(R.id.edt_meeting_remarks);
        spin_status = (Spinner) popuplayoutEdit.findViewById(R.id.spin_status);
        // spin_sub_status = (Spinner) dialog.findViewById(R.id.spin_sub_status);
        btn_save = (Button) popuplayoutEdit.findViewById(R.id.btn_save);
        btn_exit = (Button) popuplayoutEdit.findViewById(R.id.btn_exit);

      /*  txt_client_id.setText("Client ID : " + sharedPreferences.getString("ROW_ID" + position, ""));
        txt_client_name.setText("Client Name : " + sharedPreferences.getString("CUSTOMER_NAME" + position, ""));*/
        rowID = items.get(position).ROW_ID;
        clientName = items.get(position).CUSTOMER_NAME;
        txt_client_id.setText("Client ID : " + items.get(position).ROW_ID);
        txt_client_name.setText("Client Name : " + items.get(position).CUSTOMER_NAME);


      /*  Str_LeadNo=sharedPreferences.getString("POS_CID" + position, "");
        Str_ADT=sharedPreferences.getString("POS_ADT" + position, "");

        CID=sharedPreferences.getString("POS_CID" + position, "");
        CNAME=sharedPreferences.getString("POS_CNAME" + position, "");
        CPN=sharedPreferences.getString("POS_C_PN" + position, "");
        BusinessType =sharedPreferences.getString("POS_BusinessType" + position, "");





        String replacedString = Str_ADT.replace("+", "T");
        String str_split[]=replacedString.split("T");
        Str_ADT=str_split[0]+" "+str_split[1];



        c_id = txt_client_id.getText().toString();
        c_name = txt_client_name.getText().toString();
*/

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remark = edt_remarks.getText().toString();
                status_name = spin_status.getSelectedItem().toString();
                System.out.println("status_name 22: " + status_name);
                System.out.println("remark 22: " + remark);
                // WebService Calling
                new SlowOperation().execute();
            }
        });
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog.cancel();
                connectiondialogue.dismiss();
            }
        });
        connectiondialogue.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        connectiondialogue.setContentView(popuplayoutEdit);
        //connectiondialogue.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        connectiondialogue.setCancelable(true);
        connectiondialogue.show();

    }

    private void getSharedpreferenceData(int position) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        rowID = sharedPref.getString("ROW_ID" + position, "");
        clientName = sharedPref.getString("CUSTOMER_NAME" + position, "");

        System.out.println("rowID:=>" + rowID);
        System.out.println("clientName:=>" + clientName);
    }

    public void setPopUpView(int position) {

        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        popuplayout1 = inflater.inflate(R.layout.popup_newcustomerdetail, null);

        connectiondialogue = new Dialog(context);
        popuplayout1.setFocusable(false);

        TextView tv_contactpersn, tv_role, tv_venue, tv_contactnumber, tv_appointDate, tv_status, tv_remarks,tv_profile;

        Button btn_newagntexit;
        btn_newagntexit = (Button) popuplayout1.findViewById(R.id.btn_newagntexit);
        tv_contactpersn = (TextView) popuplayout1.findViewById(R.id.tv_contactpersn);

        tv_role = (TextView) popuplayout1.findViewById(R.id.tv_nrole); // channel type
        tv_venue = (TextView) popuplayout1.findViewById(R.id.tv_nvenue);
        tv_contactnumber = (TextView) popuplayout1.findViewById(R.id.tv_contactnumber);
        tv_profile = (TextView) popuplayout1.findViewById(R.id.tv_prodname);
        tv_appointDate = (TextView) popuplayout1.findViewById(R.id.tv_appointDate);
        tv_status = (TextView) popuplayout1.findViewById(R.id.tv_status);
        tv_remarks = (TextView) popuplayout1.findViewById(R.id.tv_remarks);

        tv_contactpersn.setText(items.get(position).CUSTOMER_NAME);
        tv_role.setText(items.get(position).CUST_ROLE);
        tv_venue.setText(items.get(position).ADDRESS1);
        tv_contactnumber.setText(items.get(position).MOBILE_NO);
        tv_profile.setText(items.get(position).PRODUCTNAME);
        tv_appointDate.setText(items.get(position).APPOINTMENT_DATE);
        tv_status.setText(items.get(position).STATUS);
        tv_remarks.setText(items.get(position).REMARKS);
     //   tv_channel.setText(items.get(position).CHANNEL);

        System.out.println("Mobile4List 66" + items.get(position).MOBILE_NO);
        System.out.println("Status4List 66" + items.get(position).STATUS);
        System.out.println("Remarks4List 66" + items.get(position).REMARKS);

        try {
            btn_newagntexit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    connectiondialogue.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }
        connectiondialogue.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        connectiondialogue.setContentView(popuplayout1);
        connectiondialogue.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        connectiondialogue.setCancelable(true);
        connectiondialogue.show();
    }

    @Override
    public int getItemViewType(int position) {

        return items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setClickListener(ClickListener clicklistener) {
        this.clicklistener = clicklistener;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    public class ActivityViewHolder extends RecyclerView.ViewHolder {
        public TextView name, tv_channel, tv_venue, view, tv_profile;
        public LinearLayout parentLayout;
        ImageView letter;
        Typeface calibriFont;

        public ActivityViewHolder(View itemview) {
            super((itemview));
            name = (TextView) itemview.findViewById(R.id.ncfrom);
            tv_profile = (TextView) itemview.findViewById(R.id.tv_prodName);
            tv_channel = (TextView) itemview.findViewById(R.id.tv_channel);
            tv_venue = (TextView) itemview.findViewById(R.id.tv_ncvenue);
            view = (TextView) itemview.findViewById(R.id.ncview);
            letter = (ImageView) itemView.findViewById(R.id.gmailitem_letternc);
            parentLayout = (LinearLayout) itemView.findViewById(R.id.parentLayout);

            calibriFont = Typeface.createFromAsset(context.getAssets(), "fonts/calibri.ttf");


            name.setTypeface(calibriFont);
            tv_profile.setTypeface(calibriFont);
            tv_channel.setTypeface(calibriFont);
            tv_venue.setTypeface(calibriFont);
            view.setTypeface(calibriFont);

           /* parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // int position=(Integer)view.getTag();
                    setPopUpView(getAdapterPosition());
                }
            });*/
        }


    }

    private class StatusAction_Service extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
          /*  pDialog2 = new ProgressDialog(Todays_Meeting.this);
            pDialog2.setMessage("Please wait...");
            pDialog2.show();*/

        }

        @Override
        protected String doInBackground(String... params) {

            String status_response = "";
            SoapObject request = new SoapObject(NAMESPACE, Services.getStatusDetails.METHOD_STATUS);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            try {
                httpTransport.call(Services.getStatusDetails.SOAP_ACTION_STATUS, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }


            SoapObject result = null;
            try {
                result = (SoapObject) envelope.getResponse();
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            }


            SoapObject soapObject1 = (SoapObject) result.getProperty(1);
            SoapObject soapObject2 = (SoapObject) soapObject1.getProperty(0);
            String count_pro = soapObject2.getPropertyCount() + "";
            String Status_ID = "0,", Status_Name = "Select Status,";
            for (int i = 0; i < soapObject2.getPropertyCount(); i++) {
                SoapObject soapObject3 = (SoapObject) soapObject2.getProperty(i);
                String str_STATUS_ID = soapObject3.getPropertyAsString("STATUS_ID");
                str_STATUS_NAME = soapObject3.getPropertyAsString("STATUS_NAME");
                System.out.println("str_STATUS_NAME resp: " + str_STATUS_NAME);

                Status_ID = Status_ID + str_STATUS_ID + ",";
                Status_Name = Status_Name + str_STATUS_NAME + ",";

                status_response = Status_ID + ":" + Status_Name;
            }

            return status_response;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            /*if (pDialog2.isShowing())
                pDialog2.dismiss();*/

            String[] str_split = s.split(":");
            final String[] str_status_id = str_split[0].split(",");
            final String[] str_status_name = str_split[1].split(",");


            ArrayAdapter<CharSequence> adapter_Staus;


            adapter_Staus = new ArrayAdapter<CharSequence>(context, R.layout.spinner_text, str_status_name);

            spin_status.setAdapter(adapter_Staus);

            //spinner onitem click lisner


        }
    }

    //Meeting update webservice
    private class SlowOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

          /*  pDialog1 = new ProgressDialog(Todays_Meeting.this);
            pDialog1.setMessage("Please wait...");
            pDialog1.setCancelable(false);
            pDialog1.show();*/


        }

        @Override
        protected String doInBackground(String... params) {

            SoapObject request = new SoapObject(NAMESPACE, METHOD_UPDATE_LEAD);
            // request.addProperty("token", sharedPreferences.getString("TOKEN",""));
            request.addProperty("ROW_ID", rowID);
            request.addProperty("REMARKS", remark);
            request.addProperty("STATUS", status_name);
            request.addProperty("Latitude", lattitude);
            request.addProperty("Longitude", longitude);
            //request.addProperty("GeoLocation", sharedPreferences.getString("Lat", "") + "," + sharedPreferences.getString("Lng", ""));


            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.setOutputSoapObject(request);
            envelope.dotNet = true;
            HttpTransportSE httpTransport = new HttpTransportSE(con_url.TAG_URL);
            httpTransport.debug = true;
            try {
                httpTransport.call(SOAP_ACTION_UPDATE_LEAD, envelope); //send request
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }


            SoapPrimitive result_new = null;
            try {
                result_new = (SoapPrimitive) envelope.getResponse();
                System.out.println("UpdateMeetingResponse " + result_new.toString());
            } catch (SoapFault soapFault) {
                soapFault.printStackTrace();
            }

            SoapObjectResponse = result_new.getValue().toString();


            return SoapObjectResponse;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           /* if (pDialog1.isShowing())
                pDialog1.dismiss();*/
            String[] str_result = s.split(":");
            if (s.equals("1")) {
                Toast.makeText(context, "Meeting Update Successfully", Toast.LENGTH_SHORT).show();
                Constants.callIntent(context, ETrackerHomeNewActivity.class, R.anim.slide_in_left, R.anim.slide_out_left);

            }

            /*if (str_result[0].equals("1")) {
                Toast.makeText(getApplicationContext(), "Meeting Update Successfully", Toast.LENGTH_SHORT).show();
                dialog.cancel();
                Log.i("TAG","Status :"+Str_Status);

                if(Str_Status.equals("Online Closed"))
                {
                    new feedbacksms().execute();

                }

            } else {
                Toast.makeText(getApplicationContext(), "Error While Updating Status", Toast.LENGTH_SHORT).show();
            }*/
        }
    }

}

